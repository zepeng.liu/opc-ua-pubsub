Pour fabriquer les certificats pour OPC-UA/ROSA :

############################################################################

1) Executer UNE SEULE FOIS (car �a g�n�re les certificats racines de la PKI) :

   ./init_certificate.sh

   - Cette commande ne doit �tre ex�cut� qu'une seule fois.

   - S'il vous demande d'effacer le r�pertoire "openssl", dites "y".
     Ca r�initialise tout.

   - Il va g�n�rer "ca.key" et "ca.crt", la cl� et le certificat
     racine en une seule commande.

   - Il vous demande le "PEM pass phrase" pour la "ca.key", donner
     un mot de passe et retenez-le.

   - Il vous demande successivement :
     * Country Name
     * State or Province Name
     * Locality Name
     * Organization Name
     * Organizational Unit Name
     vous r�pondez comme vous voulez.

   - Il vous demande le Common Name (FDQN or YOUR Name). Ce champ est
     obligatoire. Entrez votre pt�nom et nom.

   - Il vous demande votre Email address. R�pondez.

   - Il vous propose de v�rifier le contenu du certificat. R�pondez "y".

   - Il va maintenant g�n�rer "signingCa.key" et "signingCa.crt", la cl� et
     le certificat qui seront utilis�s par g�n�rer toutes les autres cl�s
     et certificats. Ils seront certifi�s par "ca.crt".

   - Il commence par "signingCa.key" et vous demande le mot de passe pour 
     "signingCa.key",donner un mot de passe et retenez-le.

   - Puis il fait "signingCa.csr" (Certificate Signing Request). Il vous
     redemande le mot de passe de "signingCa.key" (donn� ci-dessous).

   - Il redemande les infos comme ci-dessus. Faites les m�mes r�ponses.

   - Puis il demande a "Challenge password". Taper juste "Entr�e".

   - Il demande "An optional company name". Taper juste "Entr�e".

   - Il cr�e ensuite de "signingCa.crt" (le certificat).

   - Il demande le mot de passe de "ca.key".

   - Il vous propose de signer le certificat. R�pondez "y". 

   - Il propose de commiter le certificat. R�pondez "y".

   - Il vous propose de v�rifier le contenu du certificat. R�pondez "y".
     
   - IL construit ensuite "crl.crt" (Certificate Revocation List) et
     vous demande le mot de passe de "signingCa.key".

   - Et c'est fini ! 
   
############################################################################

2) Pour chacune des machines identifi�es par leurs adresses IP, faire :

   - Editer openssl/intermediate/ca-config, dans la section [v3_ca]
     chercher la ligne qui commence par subjectAltName. Y mettre:

	  subjectAltName=URI:URN:<applicationURI>,DNS.1:8.8.8.8

     o� <applicationURI> est l'URI du service, genre opc-tcp://192.168.1.150:10000

   - ex�cuter :

   ./build_certficates.sh <adresse IP>

   - Cela va construire le certficat pour la machine en question.

   - Il commence par g�n�rer "<address IP>.csr".

   - Il demande les informations habituelles. Vous y r�pondez. Quand il vous 
     demande le Common Name (FDQN or YOUR Name), entrer <addressIP>. C'est
     absolumment n�cessaire.

   - Le reste se passe comme pour "signingCa.crt".

   - Il faut faire cela pour chacune des machines du syst�me.


############################################################################

3) Pour fabriquer le r�pertoire "certficates" d'une machine :

   * Si c'est un serveur d'adresse IP <address IP>, ex�cuter :

     ./build_directory.sh <address IP>

     cela produira le fichier openssl/zip/<address IP>.server.zip

   * Si c'est un client d'adresse IP <address IP> qui s'adressera aux
     serveurs d'adresses IP <address IP1>, ... <address IPn>, ex�cuter :

     ./build_directory.sh <address IP> <address IP1>, ... <address IPn>

     cela produira le fichier openssl/zip/<address IP>.client.zip

   Ces fichiers sont � d�zipper normalement dans la racine Eclipse de 
   vos projets (apr�s avoir supprim� le r�pertoire "certficates").

############################################################################

That's all folks !
