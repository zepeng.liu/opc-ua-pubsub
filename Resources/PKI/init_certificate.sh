#!/bin/zsh

echo
echo "PKI INITIALIZER"
echo

##########################################################################

source ./variables.sh

##########################################################################

function program_stop {
    echo
    echo $1
    echo "Cannot continue. Must stop !" ;
    exit 0 ;
}

#######################################################################

echo "CREATING \"$openssl\" DIRECTORY"
echo

if [ -e "$openssl" ] ; then

    if [ -d "$openssl" ] ; then 
	echo -n "A directory \"$openssl\" already exists. "
    else
	echo -n "A file \"$openssl\" exists. "
    fi

    echo -n "Should I remove it ? [Y/n] "
    read answer

    if [[ "$answer" == "y" ]] || [[ "$answer" == "Y" ]] || [[ "$answer" == "" ]] ; then
	
	rm -Rf "$openssl" 

	if [ -e "$openssl" ] ; then

	    program_stop "This program cannot remove \"$openssl\". Try to remove it with your own means and rerun the program. "

	fi

    else

	program_stop "This program cannot continue without removing \"$openssl\". "

    fi

fi

echo "mkdir \"$openssl\". "
mkdir "$openssl"

echo "cd \"$openssl\""
cd "$openssl"

echo "touch index.txt"
touch index.txt

echo "echo \"01\" > serial"
echo "01" > serial

echo "cp ../ca-config-main ca-config"
cp ../ca-config-main ca-config


#######################################################################

echo 
echo "GENERATING THE PRIVATE KEY AND CERTIFICATE"

echo
echo "In $openssl, creating ca.key (private key) and ca.crt (private certificate)"
echo "- don't forget to enter a PEM password"
echo "- don't forget to enter a FDQN name"
echo "- don't forget to enter your name"
echo

cmd="openssl req \
    -new \
    -x509 \
    -days 3650 \
    -extensions v3_ca \
    -newkey rsa:2048 \
    -keyout ca.key \
    -out ca.crt \
    -config ca-config"

echo $cmd
eval $cmd

if [[ $? -ne 0 ]] ; then
    program_stop "Failure."
fi

echo
echo -n "In $openssl, verifying the content of ca.crt ? [Y/n] "
read answer
    
if [[ "$answer" == "y" ]] || [[ "$answer" == "Y" ]] || [[ "$answer" == "" ]] ; then
	
    echo
    cmd="openssl x509 -noout -text -in ca.crt"
    
    echo $cmd
    eval $cmd

    if [[ $? -ne 0 ]] ; then
	program_stop "Failure."
    fi

fi

echo

#######################################################################

echo "CREATING \"$openssl/$intermediate\" DIRECTORY"
echo

if [ -e "$intermediate" ] ; then

    if [ -d "$intermediate" ] ; then 
	echo -n "A directory \"$intermediate\" already exists. "
    else
	echo -n "A file \"$intermediate\" exists. "
    fi

    answer=
    echo -n "Should I remove it ? [Y/n] "
    read answer

    if [[ "$answer" == "y" ]] || [[ "$answer" == "Y" ]] || [[ "$answer" == "" ]] ; then
	
	rm -Rf "$intermediate" 

	if [ -e "$intermediate" ] ; then

	    program_stop "This program cannot remove \"$intermediate\". Try to remove it with your own means and rerun the program. "

	fi

    else

	program_stop "This program cannot continue without removing \"$intermediate\". "

    fi

fi

echo "mkdir \"$intermediate\""
mkdir "$intermediate"

echo "cd \"$intermediate\""
cd "$intermediate"

echo "touch index.txt"
touch index.txt

echo "echo \"01\" > serial"
echo "01" > serial

echo "echo \"01\" > crlnumber"
echo "01" > crlnumber

echo "cp ../ca-config-main ca-config"
cp ../../ca-config-intermediate ca-config

echo "mkdir csr"
mkdir csr

echo "cd .."
cd ..


#######################################################################

echo
echo "GENERATING THE SIGNING CA KEY AND CERTIFICATE"

echo
echo "Generating \"$intermediate/signingCa.key\", the key "

openssl genrsa -aes256 -out intermediate/signingCa.key 2048

if [[ $? -ne 0 ]] ; then
    program_stop "Failure."
fi

echo
echo "Generating \"$intermediate/signingCa.csr\", the Certficate Signing Request "
echo

cmd=\
"openssl req \
 -config \"$intermediate/ca-config\" \
 -new \
 -sha256 \
 -key \"$intermediate/signingCa.key\" \
 -out \"$intermediate/csr/signingCa.csr\""

echo $cmd
eval $cmd

echo
echo "Generating \"$intermediate/signingCa.crt\", the certificate "
echo

cmd=\
"openssl ca \
 -config ca-config \
 -extensions v3_ca \
 -days 3650 \
 -notext \
 -md sha256 \
 -policy policy_anything \
 -in \"$intermediate/csr/signingCa.csr\" \
 -out \"$intermediate/signingCa.crt\""

echo $cmd
eval $cmd


echo
answer="?"
echo -n "In $openssl/$intermediate, verifying the content signingCa.crt ? [Y/n] "
read answer
    
if [[ "$answer" == "y" ]] || [[ "$answer" == "Y" ]] || [[ "$answer" == "" ]] ; then
	
    echo
    cmd="openssl x509 -noout -text -in \"$intermediate/signingCa.crt\""
    echo $cmd
    eval $cmd

    if [[ $? -ne 0 ]] ; then
	program_stop "Failure."
    fi

fi


#######################################################################

echo
echo "BUILDING THE CHAIN OF TRUST"
echo

cmd="cat ca.crt \"$intermediate/signingCa.crt\" > \"$intermediate/chainOfTrust.crt\""

echo $cmd
eval $cmd


#######################################################################

echo
echo "BUILDING THE CERTIFICATE REVOCATION LIST"
echo

cmd="openssl ca -days 3650 -config \"$intermediate/ca-config\" -gencrl -out \"$intermediate/crl.crt\""

echo $cmd
eval $cmd


#######################################################################

echo
echo "MAKING zip DIRECTORY"
echo

echo "mkdir zip"
mkdir zip


#######################################################################

echo
echo "DONE !"
echo


