public class PData
{
    public static String[] pSOPCDataSDT = { // SOPC Type in StandardDataTypes
	"String",null,
 	"NodeId",null,
 	"SByte",null,
 	"Byte",null,
 	"DateTime",null,
 	"Guid",null,
 	"ByteString",null,
 	"ExpandedNodeId",null,
 	"QualifiedName",null,
 	"LocalizedText",null,
 	"ExtensionObject",null,
 	"Variant",null
    } ;

    public static String[] pSOPCDataCPT = { // SOPC types in CommonParametersTypes
 	"DataValue",null,
 	"DiagnosticInfo",null,
	"StatusCode",null
    } ;

	
    public static String[] pOpcUaDataSDT = { // OpcUa types in StandardDataTypes
 	"Argument",null
    } ;

    public static String[] pOpcUaDataDAS = { // OpcUa types in DataAccess
 	"Range",null,
 	"EUInformation",null
    } ;

    public static String[] pOpcUaDataNMT = { // OpcUa types in NodeManagement
 	"AddNodesItem","WITH_NODEMNGT",
 	"AddNodesResult","WITH_NODEMNGT",
 	"AddReferencesItem","WITH_NODEMNGT"
    } ;

    public static String[] pOpcUaDataNAE = { // OpcUa types in NotificationsAndEvents
 	"MonitoredItemCreateRequest","WITH_SUBSCRIPTION",
	"MonitoredItemCreateResult","WITH_SUBSCRIPTION",
 	"MonitoredItemModifyRequest","WITH_SUBSCRIPTION",
	"MonitoredItemModifyResult","WITH_SUBSCRIPTION",
 	"TransferResult","WITH_SUBSCRIPTION",
 	"SubscriptionAcknowledgement","WITH_SUBSCRIPTION",
 	"MonitoredItemNotification","WITH_SUBSCRIPTION",
 	"EventFieldList","WITH_SUBSCRIPTION"
    } ;

    public static String[] pOpcUaDataCPT = { // OpcUa types in CommonParametersTypes
 	"EndpointDescription",null,
 	"SignedSoftwareCertificate",null,
 	"UserTokenPolicy",null,
 	"ReadValueId","WITH_READ",
 	"WriteValue","WITH_WRITE",
 	"CallMethodRequest","WITH_CALL",
 	"CallMethodResult","WITH_CALL",
 	"BrowseDescription","WITH_BROWSE",
 	"BrowseResult","WITH_BROWSE",
 	"ReferenceDescription","WITH_BROWSE",
 	"BrowsePath","WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS",
 	"RelativePathElement","WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS",
 	"BrowsePathResult","WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS",
 	"BrowsePathTarget","WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS",
 	"ContentFilterElement","WITH_QUERY",
 	"QueryDataDescription","WITH_QUERY",
	"ApplicationDescription","WITH_DISCOVERY"
    } ;

    public static String[] pSpecialCPT = { // Types with special types in CommonParametersTypes
	"FilterOperand","SOPC_ExtensionObject","WITH_QUERY",
 	"IntegerId","uint32_t",null,
 	"Counter","uint32_t",null
    } ;

    public static String[] pSpecialNAE = { // Types with special types in NotificationsAndEvents
	"NotificationData","SOPC_ExtensionObject","WITH_SUBSCRIPTION"
    } ;

    public static String[] pSpecialSDT = { // Types with special types in StandardDataTypes
	"LocaleId","SOPC_String",null,
 	"Boolean","uint8_t",null,
 	"Int16","int16_t",null,
 	"UInt16","uint16_t",null,
	"Int32","int32_t",null,
 	"UInt32","uint32_t",null,
	"Int64","int64_t",null,
 	"UInt64","uint64_t",null,
 	"Float","float",null,
 	"Double","double",null
    } ;
}


