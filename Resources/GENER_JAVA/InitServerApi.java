public class InitServerApi
{

    static public void main(String[] args)
    {
	String[] services = ImplantedServices.services ;

	System.out.println("#ifndef _INITSERVERAPI_H") ;
	System.out.println("#define _INITSERVERAPI_H") ;
	System.out.println() ;
	System.out.println("#include \"lib/OpcUa.h\"") ;
	System.out.println() ;
	System.out.println("extern \"C\" {") ;
	System.out.println() ;
	for (int i = 0; i < services.length ; i+= 2 ) {
	    HTable.printif(services[i+1]) ;
	    System.out.println("extern") ;
	    System.out.println("#ifdef _WIN32") ;
	    System.out.println("__declspec( dllimport )") ;
	    System.out.println("#endif") ;
	    System.out.println("SOPC_ServiceType OpcUa_"+services[i]+"_ServiceType ;") ;
	    HTable.printendif(services[i+1]) ;
	}
	System.out.println() ;
	System.out.println("void initServerApi()") ;
	System.out.println("{") ;
	for (int i = 0; i < services.length ; i+= 2 ) {
	    HTable.printif(services[i+1]) ;
	    System.out.println("   OpcUa_"+services[i]+"_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_"+services[i]+" ;") ;
	    HTable.printendif(services[i+1]) ;
	}
	System.out.println("}") ;
	System.out.println() ;
	System.out.println("} // extern \"C\"") ;
	System.out.println("#endif // _INITSERVERAPI_H") ;
    }









}
