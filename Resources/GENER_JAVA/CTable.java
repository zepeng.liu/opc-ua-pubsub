public class CTable
{
    public static void main(String[] args)
    {
	System.out.println("#include \"OpcUa_IPCS_Table.h\"") ;
	System.out.println() ;

	printSDT (PData.pSOPCDataSDT,2) ;
	printSDT (PData.pOpcUaDataSDT,2) ;
	printSDT (PData.pSpecialSDT,3) ;
	printCPT (PData.pSOPCDataCPT,2) ;
	printCPT (PData.pOpcUaDataCPT,2) ;
	printCPT (PData.pSpecialCPT,3) ;
	printNMT (PData.pOpcUaDataNMT,2) ;
	printNAE (PData.pOpcUaDataNAE,2) ;
	printNAE (PData.pSpecialNAE,3) ;
	printDAS (PData.pOpcUaDataDAS,2) ;

	System.out.println() ;
	System.out.println("namespace opcua {") ;
	System.out.println() ;

	printSOPC  (PData.pSOPCDataSDT) ;
	printOpcUa (PData.pOpcUaDataSDT) ;
	printSOPC  (PData.pSOPCDataCPT) ;
	printOpcUa (PData.pOpcUaDataNMT) ;
	printOpcUa (PData.pOpcUaDataNAE) ;
	printOpcUa (PData.pOpcUaDataCPT) ;
	printOpcUa (PData.pOpcUaDataDAS) ;
	printTypes (PData.pSpecialCPT) ;
	printTypes (PData.pSpecialNAE) ;
	printTypes (PData.pSpecialSDT) ;

	System.out.println() ;
	System.out.println("} //namespace opcua") ;
    }

    private static void printIncls(String dir, String[] t, int k)
    {
	for (int i=0 ; i<t.length ; i+=k) {
	    HTable.printif(t[i+k-1]) ;
	    System.out.println("#include \"../"+dir+"/OpcUa_IPCS_"+t[i]+".h\"") ;
	    HTable.printendif(t[i+k-1]) ;
	}
    }

    private static void printDAS(String[] t, int k) { printIncls("DataAccess",t,k) ; }
    private static void printNMT(String[] t, int k) { printIncls("NodeManagement",t,k) ; }
    private static void printNAE(String[] t, int k) { printIncls("NotificationsAndEvents",t,k) ; }
    private static void printCPT(String[] t, int k) { printIncls("CommonParametersTypes",t,k) ; }
    private static void printSDT(String[] t, int k) { printIncls("StandardDataTypes",t,k) ; }

    private static void printSOPC(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=2) {
	    HTable.printif(t[i+1]) ;
	    print(t[i], "SOPC_"+t[i]) ;	
	    HTable.printendif(t[i+1]) ;
	}
    }

    private static void printOpcUa(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=2) {
	    HTable.printif(t[i+1]) ;
	    print(t[i], "OpcUa_"+t[i]) ;	
	    HTable.printendif(t[i+1]) ;
	}
    }

    private static void printTypes(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=3) {
	    HTable.printif(t[i+2]) ;
	    print(t[i], t[i+1]) ;	
	    HTable.printendif(t[i+2]) ;
	}
    }

    private static void print(String c, String t)
    {
	System.out.println() ;
	System.out.println("MYDLL Table"+c+" * Table"+c+"::getTable(int n) const { return new Table"+c+"(n) ; }") ;
	System.out.println() ;
	System.out.println(c+" * Table"+c+"::get(int32_t i) const { return dynamic_cast<"+c+" *>(tab[i]) ; }") ;
	System.out.println() ;
	System.out.println("MYDLL void Table"+c+"::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOf"+c+"s,"+t+" *& p"+c+"s) const") ;
	System.out.println("{") ;
	System.out.println("    pNoOf"+c+"s = len ;") ;
	System.out.println("    if (len > 0) {") ;
	System.out.println("        p"+c+"s = reinterpret_cast<"+t+" *>(calloc(len, sizeof("+t+"))) ;") ;
	System.out.println("        for (int i = 0 ; i < len ; i++)") ;
	System.out.println("            get(i)->fromCpptoC(pStatus, p"+c+"s[i]) ;") ;
	System.out.println("    } else {") ;
	System.out.println("        p"+c+"s = NULL ;") ;
	System.out.println("    }") ;
	System.out.println("}") ;
	System.out.println() ;
	System.out.println("MYDLL Table"+c+" * Table"+c+"::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOf"+c+"s,"+t+" const * p"+c+"s)") ;
	System.out.println("{") ;
	System.out.println("    Table"+c+" * res = new Table"+c+"(pNoOf"+c+"s) ;") ;
	System.out.println("    for (int i = 0 ; i < pNoOf"+c+"s ; i++)") ;
	System.out.println("        res->set(i,"+c+"::fromCtoCpp(pStatus, p"+c+"s[i])) ;") ;
	System.out.println("    if (*pStatus == STATUS_OK)") ;
	System.out.println("        return res ;") ;
	System.out.println("    delete res ;") ;
	System.out.println("    return NULL ;") ;
      	System.out.println("}") ;
	System.out.println() ;
    }
}
