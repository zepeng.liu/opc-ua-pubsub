/*
 *  Copyright (C) 2018 Systerel and others.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************************************************************************

 File Name            : toolkit_header_init.h

 Date                 : 29/03/2018 14:46:19

 C Translator Version : tradc Java V1.0 (14/03/2012)

******************************************************************************/

#ifndef _toolkit_header_init_h
#define _toolkit_header_init_h

/*------------------------
   INITIALISATION Clause
  ------------------------*/
extern void INITIALISATION(void);

#endif
