#! /bin/zsh

count_opcua=`cat opcua/lib/**/*.{h,cpp} | wc -l`
echo "opcua :        $count_opcua loc"

count_opcua_server=`cat opcua-server/**/*.{h,cpp,xml} | wc -l`
echo "opcua-server : $count_opcua_server loc"

count_opcua_client=`cat opcua-client/**/*.{h,cpp} | wc -l`
echo "opcua-client : $count_opcua_client loc"

count_opcua_dll=`cat opcua-dll/**/*.cpp | wc -l`
echo "opcua-dll :    $count_opcua_dll loc"

echo "----------------------------"

echo "TOTAL :          " $(( $count_opcua + $count_opcua_server + count_opcua_client + count_opcua_dll )) loc

