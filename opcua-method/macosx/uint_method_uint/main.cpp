/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib/OpcUa.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/CommonParametersTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

extern "C" {
namespace opcua {

int32_t method(NodeId                 * objectId,
               NodeId                 * methodId,
               TableVariant           * inputArguments,
               StatusCode            ** statusCode,
               TableStatusCode       ** inputArgumentResults,
               TableDiagnosticInfo   ** inputArgumentDiagnosticInfos,
               TableVariant          ** outputArguments,
               DiagnosticInfo        ** diagnosticInfo)
{
	int length = inputArguments->getLength() ;

	if (length != 1) {
		*statusCode      = StatusCode::Bad_TypeMismatch ;
		return 0 ;
	}

	Variant      * arg = inputArguments->get(0) ;
	BaseDataType * val = arg->getValue() ;
	int32_t        len = val->getLength() ;
	uint8_t        bit = arg->getBuiltinTypeId() ;

	*inputArgumentResults         = new TableStatusCode  (1) ;
	*inputArgumentDiagnosticInfos = new TableDiagnosticInfo  (0) ;

	if (len == -1 && bit == Builtin_UInt32) {
		(*inputArgumentResults)->set(0,StatusCode::Good) ;
	} else {
		(*inputArgumentResults)->set(0,StatusCode::Bad_TypeMismatch) ;
	}

	uint32_t argument = dynamic_cast<UInt32 *>(val)->get() ;

	if (argument != UINT32_MAX)
		*statusCode      = StatusCode::Good ;
	else
		*statusCode      = new StatusCode(_Bad_SubscriptionIdInvalid) ;

	*outputArguments = new TableVariant  (1) ;

	(*outputArguments)->set(0,new Variant(Builtin_UInt32,UInt32::zero)) ;

	*diagnosticInfo  = DiagnosticInfo::fakeDiagnosticInfo ;

	return (int32_t)0 ;
}


}
}
