
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINKDESCRIPTION_H_
#define LINKDESCRIPTION_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_StringUtils.h"
#include <string.h>


namespace opcua {

// No testing that target node has the good nodeClass.

class MYDLL LinkDescription
{
private:

	char   * type ;
	char   * nodeClass ;
	NodeId * source ;
	NodeId * target ;

public:

	LinkDescription(
			const char * _type,
			const char * _nodeClass,
			NodeId     * _source,
			NodeId     * _target
			)
	{
		CHECK_INT(_type != NULL) ;
		CHECK_INT(_nodeClass != NULL) ;

		type = new char[strlen(_type) + 1] ;
		strcpy(type,_type) ;

		nodeClass = new char[strlen(_nodeClass) + 1] ;
		strcpy(nodeClass,_nodeClass) ;

		(source = _source)->take() ;
		(target = _target)->take() ;
	}

	LinkDescription(
			const char  * _type,
			const char * _nodeClass,
			const char * _sourceName,
			NodeId     * _target
			)
	{
		CHECK_INT(_type != NULL) ;
		CHECK_INT(_nodeClass != NULL) ;

		type = new char[strlen(_type) + 1] ;
		strcpy(type,_type) ;

		nodeClass = new char[strlen(_nodeClass) + 1] ;
		strcpy(nodeClass,_nodeClass) ;

		if (StringUtils::isNumeric(_sourceName)) {
			errno = 0 ;
			int64_t value = strtoll(_sourceName,(char **)NULL,0) ;
			if (errno == 0 && static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
				source = new NodeId(UInt16::zero,new UInt32(static_cast<uint32_t>(value))) ;
			} else {
				source = new NodeId(UInt16::zero,new String(_sourceName)) ;
			}
		} else {
			source = new NodeId(UInt16::zero,new String(_sourceName)) ;
		}
		source->take() ;

		(target = _target)->take() ;
	}

	virtual ~LinkDescription()
	{
		delete [] type ;
		delete [] nodeClass ;
		source->release() ;
		target->release() ;
	}

public:

	LinkDescription * implements(class NodesTable * nodesTable, bool withError) ;

};

} /* namespace opcua */
#endif /* LINKDESCRIPTION_H_ */
