/*
 *  Copyright (C) 2018 Systerel and others.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBS_SC_SOPC_SERVICES_API_H_
#define STUBS_SC_SOPC_SERVICES_API_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_SyncHashTableInt.h"

extern "C" {

extern bool isServer ;

void SOPC_Services_EnqueueEvent(SOPC_Services_Event scEvent, uint32_t id, void* params, uintptr_t auxParam) ;

void SOPC_Services_Initialize(void);

void SOPC_Services_ToolkitConfigured(void);

void SOPC_Services_PreClear(void);

void SOPC_Services_Clear(void);

}

#endif /* STUBS_SC_SOPC_SERVICES_API_H_ */
