/*
 *  Copyright (C) 2018 Systerel and others.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../s2opc/S2OPC_StubServices.h"

#include "../OpcUa.h"
#include "../s2opc/S2OPC_ClientServices.h"
#include "../s2opc/S2OPC_ServerServices.h"
#include "../Services/All.h"
#include "../Stacks/OpcUa_ChannelsTable.h"




extern "C" {

int endpointOpened = 0 ; // Managed in s2opc_server/S2OPC_StubServices.cpp

bool isServer = false ;

void SOPC_Services_EnqueueEvent(SOPC_Services_Event scEvent, uint32_t id, void* params, uintptr_t auxParam)
{
	switch (scEvent)
	{

	case SC_TO_SE_EP_SC_CONNECTED: // Secure channel open on server side
	{
		// id = endpoint description config index,
		// *(uint32_t *)params = secure channel config index POINTER,
		// (uint32_t)auxParam = secure channel connection index
		// SERVER: A new secure channel is connected on the endpoint:
		// - retrieve secure channel index
		// - retrieve secure channel config
		uint32_t secureChannelIdx = (uint32_t) auxParam;

		odebug_i(MAIN_LIFE_DBG,"ServerServices_EnqueueEvent","SC_TO_SE_EP_SC_CONNECTED with secureChannelIdx=%d",secureChannelIdx) ;

		if (params != NULL) {
			SOPC_SecureChannel_Config * scConfig = SOPC_ToolkitServer_GetSecureChannelConfig(*(uint32_t*) params);
			if (scConfig != NULL) {
				// Keep association scIdx => scConfig to could retrieve configuration later (on message reception)
#if (SECURITY == UANONE)
				opcua::ChannelsTable::self->createNewChannel(secureChannelIdx,scConfig,id) ;
#else
				const SOPC_Certificate* clientCertificate = scConfig->crt_cli ;

				if (clientCertificate != NULL) {
					SOPC_StatusCode status = STATUS_OK ;
					opcua::ApplicationInstanceCertificate * theClientCertificate = opcua::ApplicationInstanceCertificate::fromCtoCpp(&status,clientCertificate->len_der,clientCertificate->crt_der) ;

					if (status == STATUS_OK) {
						opcua::ChannelsTable::self->createNewChannel(secureChannelIdx,theClientCertificate,scConfig,id) ;
						endpointOpened += 1 ;
					} else {
						odebug_i(S2OPC_ERR,"ServerServices_EnqueueEvent","SC_TO_SE_EP_SC_cannot fromCtoCpp SOPC_Certificate (error) with secureChannelIdx=%d",secureChannelIdx) ;
					}
				} else {
					opcua::ChannelsTable::self->createNewChannel(secureChannelIdx,scConfig,id) ;
					endpointOpened += 1 ;
				}
#endif
			} else {
				odebug_i(S2OPC_ERR,"ServerServices_EnqueueEvent","SC_TO_SE_EP_SC_CONNECTED with scConfig=null (error) with secureChannelIdx=%d",secureChannelIdx) ;
			}
		} else {
			odebug_i(S2OPC_ERR,"ServerServices_EnqueueEvent","SC_TO_SE_EP_SC_CONNECTED with params=null (error) with secureChannelIdx=%d",secureChannelIdx) ;
		}
	}   break;

	case SC_TO_SE_EP_CLOSED: // endpoint is closed on server side
	{
		// id = endpoint description configuration index,
		// params = NULL
		// (uint32_t)auxParams = SOPC_ReturnStatus
		// SERVER: endpoint has been closed
		odebug_i(MAIN_LIFE_DBG,"ServerServices_EnqueueEvent","SC_TO_SE_EP_CLOSED with id=%d",id) ;
		opcua::ChannelsTable::self->removeByEpConfigIdx(id) ;
		endpointOpened -= 1 ;
		SOPC_StatusCode status = (uint32_t)auxParam ;
		if (STATUS_OK != status)
			odebug_ii(S2OPC_ERR,"ServerServices_EnqueueEvent","SC_TO_SE_EP_CLOSED with return status = 0x%08x and  endpointDescriptionIndex=%d",status,id) ;
	}	break;

    case SC_TO_SE_SC_CONNECTED: // Secure channel open on client side
    {
        // id = secure channel connection index,
    	// params = NULL
        // (uint32_t)auxParams = secure channel connection configuration index
        // CLIENT: A new secure channel is connected:
        // - retrieve secure channel index
        // - retrieve secure channel configuration
    	uint32_t secureChannelIdx = (uint32_t) auxParam;
		odebug_i(MAIN_LIFE_DBG,"Services_EnqueueEvent","SC_TO_SE_SC_CONNECTED with secureChannelIdx=%d",id) ;
		SOPC_SecureChannel_Config *scConfig = SOPC_ToolkitClient_GetSecureChannelConfig((uint32_t)auxParam);
        if (scConfig != NULL) {
           	// Keep association scIdx => scConfig to could retrieve config later (on message reception)
           	opcua::ChannelsTable::self->createNewChannel(secureChannelIdx,scConfig,id) ;
           	endpointOpened = 1 ;
        } else {
        	odebug_i(S2OPC_ERR,"Services_EnqueueEvent","SC_TO_SE_SC_CONNECTED with scConfig=null (error) with secureChannelIdx=%d",id) ;
        }
    }   break;

    case SC_TO_SE_SC_CONNECTION_TIMEOUT: // Client cannot connect to Server
    {
        // id = secure channel connection CONFIG index
		odebug_i(MAIN_LIFE_DBG,"Services_EnqueueEvent","SC_TO_SE_SC_CONNECTION_TIMEOUT with secureChannelConfigurationIdx=%d",id) ;
		endpointOpened = 0 ;
    }   break;

    case SC_TO_SE_SC_DISCONNECTED:
    {
        // id = secure channel connection index
    	// auxParam == secure channel configuration index
        // CLIENT / SERVER: secure channel disconnection
 		odebug_i(MAIN_LIFE_DBG,"Services_EnqueueEvent","SC_TO_SE_SC_DISCONNECTED with secureChannelIdx=%d",id) ;
		if (! opcua::ChannelsTable::self->remove(id,true))
	       	odebug_i(S2OPC_ERR,"Services_EnqueueEvent","SC_TO_SE_SC_DISCONNECTED cannot remove channel (error) with secureChannelIdx=%d",id) ;
		endpointOpened = 0 ;
    }   break;

	case SC_TO_SE_SC_SERVICE_RCV_MSG:
	{
		// id = secure channel connection index,
		// params = (SOPC_Buffer*) OPC UA message payload buffer,
		// auxParam = request Id context (server side only)
		// CLIENT / SERVER: message received on secure channel
		uint32_t requestId = (uint32_t)auxParam;
		odebug_i(S2OPC_DBG,"ServerServices_EnqueueEvent","SC_TO_SE_SC_SERVICE_RCV_MSG with requestId=%d",requestId) ;
		if (params != NULL) {
			SOPC_Buffer         * buffer  = (SOPC_Buffer*) params;
			SOPC_EncodeableType * encType = NULL;
			SOPC_ReturnStatus     status  = SOPC_MsgBodyType_Read(buffer, &encType);
			if (SOPC_STATUS_OK == status) {
				if (isServer) { // It's a server
					OpcUa_RequestHeader * requestHeader = NULL;
					status = SOPC_DecodeMsg_HeaderOrBody(buffer, &OpcUa_RequestHeader_EncodeableType, (void **)(&requestHeader));
					if (SOPC_STATUS_OK == status) {
						void * msgBody = NULL ;
						status = SOPC_DecodeMsg_HeaderOrBody(buffer, encType, &msgBody);
						if (SOPC_STATUS_OK == status) {
							odebug_ii(S2OPC_DBG,"ServerServices_EnqueueEvent","SC_TO_SE_SC_SERVICE_RCV_MSG with secureChannelIdx=%d, encType=%d",id,encType->TypeId) ;
							switch (encType->TypeId) {
#if (WITH_DISCOVERY == 1)
							case OpcUaId_FindServersRequest:
								opcua::ServerService_FindServers(id,requestId,requestHeader, (OpcUa_FindServersRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_FindServersOnNetworkRequest:
								break ;
#endif
#if (WITH_DISCOVERY == 1)
							case OpcUaId_GetEndpointsRequest:
								opcua::ServerService_GetEndpoints(id,requestId,requestHeader, (OpcUa_GetEndpointsRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_RegisterServerRequest:
								break ;
#endif
#if 0
							case OpcUaId_RegisterServer2Request:
								break ;
#endif
							case OpcUaId_CreateSessionRequest:
								opcua::ServerService_CreateSession(id,requestId,requestHeader,(OpcUa_CreateSessionRequest *)msgBody) ;
								break ;
							case OpcUaId_ActivateSessionRequest:
								opcua::ServerService_ActivateSession(id,requestId,requestHeader,(OpcUa_ActivateSessionRequest *)msgBody) ;
								break ;
							case OpcUaId_CloseSessionRequest:
								opcua::ServerService_CloseSession(id,requestId,requestHeader,(OpcUa_CloseSessionRequest *)msgBody) ;
								break ;
#if 0
							case OpcUaId_CancelRequest:
								break ;
#endif
#if (WITH_NODEMNGT == 1)
							case OpcUaId_AddNodesRequest:
								opcua::ServerService_AddNodes(id,requestId,requestHeader, (OpcUa_AddNodesRequest *)msgBody) ;
								break ;
#endif
#if (WITH_NODEMNGT == 1)
							case OpcUaId_AddReferencesRequest:
								opcua::ServerService_AddReferences(id,requestId,requestHeader, (OpcUa_AddReferencesRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_DeleteNodesRequest:
								break ;
#endif
#if 0
							case OpcUaId_DeleteReferencesRequest:
								break ;
#endif
#if (WITH_BROWSE == 1)
							case OpcUaId_BrowseRequest:
								opcua::ServerService_Browse(id,requestId,requestHeader, (OpcUa_BrowseRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_BrowseNextRequest:
								break ;
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
							case OpcUaId_TranslateBrowsePathsToNodeIdsRequest:
								opcua::ServerService_TranslateBrowsePathsToNodeIds(id,requestId,requestHeader, (OpcUa_TranslateBrowsePathsToNodeIdsRequest *)msgBody) ;
								break ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
							case OpcUaId_RegisterNodesRequest:
								opcua::ServerService_RegisterNodes(id,requestId,requestHeader, (OpcUa_RegisterNodesRequest *)msgBody) ;
								break ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
							case OpcUaId_UnregisterNodesRequest:
								opcua::ServerService_UnregisterNodes(id,requestId,requestHeader, (OpcUa_UnregisterNodesRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_QueryFirstRequest:
								break ;
#endif
#if 0
							case OpcUaId_QueryNextRequest:
								break ;
#endif
#if (WITH_READ == 1)
							case OpcUaId_ReadRequest:
								opcua::ServerService_Read(id,requestId,requestHeader, (OpcUa_ReadRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_HistoryReadRequest:
								break ;
#endif
#if (WITH_WRITE == 1)
							case OpcUaId_WriteRequest:
								opcua::ServerService_Write(id,requestId,requestHeader, (OpcUa_WriteRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_HistoryUpdateRequest:
								break ;
#endif
#if (WITH_CALL == 1)
							case OpcUaId_CallRequest:
								opcua::ServerService_Call(id,requestId,requestHeader, (OpcUa_CallRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_CreateMonitoredItemsRequest:
								opcua::ServerService_CreateMonitoredItems(id,requestId,requestHeader, (OpcUa_CreateMonitoredItemsRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_ModifyMonitoredItemsRequest:
								opcua::ServerService_ModifyMonitoredItems(id,requestId,requestHeader, (OpcUa_ModifyMonitoredItemsRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_SetMonitoringModeRequest:
								opcua::ServerService_SetMonitoringMode(id,requestId,requestHeader, (OpcUa_SetMonitoringModeRequest *)msgBody) ;
								break ;
#endif
#if 0
							case OpcUaId_SetTriggeringRequest:
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_DeleteMonitoredItemsRequest:
								opcua::ServerService_DeleteMonitoredItems(id,requestId,requestHeader, (OpcUa_DeleteMonitoredItemsRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_CreateSubscriptionRequest:
								opcua::ServerService_CreateSubscription(id,requestId,requestHeader, (OpcUa_CreateSubscriptionRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_ModifySubscriptionRequest:
								opcua::ServerService_ModifySubscription(id,requestId,requestHeader, (OpcUa_ModifySubscriptionRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_SetPublishingModeRequest:
								opcua::ServerService_SetPublishingMode(id,requestId,requestHeader, (OpcUa_SetPublishingModeRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_PublishRequest:
								opcua::ServerService_Publish(id,requestId,requestHeader, (OpcUa_PublishRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_RepublishRequest:
								opcua::ServerService_Republish(id,requestId,requestHeader, (OpcUa_RepublishRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_TransferSubscriptionsRequest:
								opcua::ServerService_TransferSubscriptions(id,requestId,requestHeader, (OpcUa_TransferSubscriptionsRequest *)msgBody) ;
								break ;
#endif
#if (WITH_SUBSCRIPTION == 1)
							case OpcUaId_DeleteSubscriptionsRequest:
								opcua::ServerService_DeleteSubscriptions(id,requestId,requestHeader, (OpcUa_DeleteSubscriptionsRequest *)msgBody) ;
								break ;
#endif
							default:
								odebug_i(S2OPC_ERR,"ServerServices_EnqueueEvent","Service not implemented: %d",encType->TypeId) ;
								opcua::ServerService_SendError(id,requestId,requestHeader,_Bad_ServiceUnsupported) ;
								break ;
							}
						} else {
							odebug_is(S2OPC_ERR,"ServerServices_EnqueueEvent","Cannot decode msg body in request %d (%s)",encType->TypeId,encType->TypeName) ;
						}
					} else {
						odebug_is(S2OPC_ERR,"ServerServices_EnqueueEvent","Cannot decode header in request %d (%s)",encType->TypeId,encType->TypeName) ;
					}
				} else { // It's a client
	            	OpcUa_ResponseHeader * responseHeader = NULL ;
	            	status = SOPC_DecodeMsg_HeaderOrBody(buffer, &OpcUa_ResponseHeader_EncodeableType, (void **)(&responseHeader));
	            	if (SOPC_STATUS_OK == status) {
	            		void * msgBody = NULL ;
	            		status = SOPC_DecodeMsg_HeaderOrBody(buffer, encType, &msgBody);
	            		if (SOPC_STATUS_OK == status) {
	            	 		odebug_ii(S2OPC_DBG,"Services_EnqueueEvent","SC_TO_SE_SC_SERVICE_RCV_MSG with secureChannelIdx=%d, encType=%d",id,encType->TypeId) ;
	         				switch (encType->TypeId) {
	//#if (WITH_DISCOVERY == 1)
	//         				case OpcUaId_FindServersResponse:
	//         					opcua::ClientService_FindServers(id,requestId,responseHeader, (OpcUa_FindServersResponse *)msgBody) ;
	//         					break ;
	//#endif
	#if 0
	         				case OpcUaId_FindServersOnNetworkResponse:
	         					break ;
	#endif
	//#if (WITH_DISCOVERY == 1)
	//         				case OpcUaId_GetEndpointsResponse:
	//         					opcua::ClientService_GetEndpoints(id,requestId,responseHeader, (OpcUa_GetEndpointsResponse *)msgBody) ;
	//         					break ;
	//#endif
	#if 0
	         				case OpcUaId_RegisterServerResponse:
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_RegisterServer2Response:
	         					break ;
	#endif
	         				case OpcUaId_CreateSessionResponse:
	         					opcua::ClientService_CreateSession(id,requestId,responseHeader,(OpcUa_CreateSessionResponse *)msgBody) ;
	         					break ;
	         				case OpcUaId_ActivateSessionResponse:
	         					opcua::ClientService_ActivateSession(id,requestId,responseHeader,(OpcUa_ActivateSessionResponse *)msgBody) ;
	         					break ;
	         				case OpcUaId_CloseSessionResponse:
	         					opcua::ClientService_CloseSession(id,requestId,responseHeader,(OpcUa_CloseSessionResponse *)msgBody) ;
	         					break ;
	#if 0
	         				case OpcUaId_CancelResponse:
	         					break ;
	#endif
	#if (WITH_NODEMNGT == 1)
	         				case OpcUaId_AddNodesResponse:
	         					opcua::ClientService_AddNodes(id,requestId,responseHeader, (OpcUa_AddNodesResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_NODEMNGT == 1)
	         				case OpcUaId_AddReferencesResponse:
	         					opcua::ClientService_AddReferences(id,requestId,responseHeader, (OpcUa_AddReferencesResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_DeleteNodesResponse:
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_DeleteReferencesResponse:
	         					break ;
	#endif
	#if (WITH_BROWSE == 1)
	         				case OpcUaId_BrowseResponse:
	         					opcua::ClientService_Browse(id,requestId,responseHeader, (OpcUa_BrowseResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_BrowseNextResponse:
	         					break ;
	#endif
	#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
	         				case OpcUaId_TranslateBrowsePathsToNodeIdsResponse:
	         					opcua::ClientService_TranslateBrowsePathsToNodeIds(id,requestId,responseHeader, (OpcUa_TranslateBrowsePathsToNodeIdsResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_REGISTER_UNREGISTER_NODES == 1)
	         				case OpcUaId_RegisterNodesResponse:
	         					opcua::ClientService_RegisterNodes(id,requestId,responseHeader, (OpcUa_RegisterNodesResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_REGISTER_UNREGISTER_NODES == 1)
	         				case OpcUaId_UnregisterNodesResponse:
	         					opcua::ClientService_UnregisterNodes(id,requestId,responseHeader, (OpcUa_UnregisterNodesResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_QueryFirstResponse:
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_QueryNextResponse:
	         					break ;
	#endif
	#if (WITH_READ == 1)
	         				case OpcUaId_ReadResponse:
	         					opcua::ClientService_Read(id,requestId,responseHeader, (OpcUa_ReadResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_HistoryReadResponse:
	         					break ;
	#endif
	#if (WITH_WRITE == 1)
	         				case OpcUaId_WriteResponse:
	         					opcua::ClientService_Write(id,requestId,responseHeader, (OpcUa_WriteResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_HistoryUpdateResponse:
	         					break ;
	#endif
	#if (WITH_CALL == 1)
	         				case OpcUaId_CallResponse:
	         					opcua::ClientService_Call(id,requestId,responseHeader, (OpcUa_CallResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_CreateMonitoredItemsResponse:
	         					opcua::ClientService_CreateMonitoredItems(id,requestId,responseHeader, (OpcUa_CreateMonitoredItemsResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_ModifyMonitoredItemsResponse:
	         					opcua::ClientService_ModifyMonitoredItems(id,requestId,responseHeader, (OpcUa_ModifyMonitoredItemsResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_SetMonitoringModeResponse:
	         					opcua::ClientService_SetMonitoringMode(id,requestId,responseHeader, (OpcUa_SetMonitoringModeResponse *)msgBody) ;
	         					break ;
	#endif
	#if 0
	         				case OpcUaId_SetTriggeringResponse:
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_DeleteMonitoredItemsResponse:
	         					opcua::ClientService_DeleteMonitoredItems(id,requestId,responseHeader, (OpcUa_DeleteMonitoredItemsResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_CreateSubscriptionResponse:
	         					opcua::ClientService_CreateSubscription(id,requestId,responseHeader, (OpcUa_CreateSubscriptionResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_ModifySubscriptionResponse:
	         					opcua::ClientService_ModifySubscription(id,requestId,responseHeader, (OpcUa_ModifySubscriptionResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_SetPublishingModeResponse:
	         					opcua::ClientService_SetPublishingMode(id,requestId,responseHeader, (OpcUa_SetPublishingModeResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_PublishResponse:
	         					opcua::ClientService_Publish(id,requestId,responseHeader, (OpcUa_PublishResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_RepublishResponse:
	         					opcua::ClientService_Republish(id,requestId,responseHeader, (OpcUa_RepublishResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_TransferSubscriptionsResponse:
	         					opcua::ClientService_TransferSubscriptions(id,requestId,responseHeader, (OpcUa_TransferSubscriptionsResponse *)msgBody) ;
	         					break ;
	#endif
	#if (WITH_SUBSCRIPTION == 1)
	         				case OpcUaId_DeleteSubscriptionsResponse:
	         					opcua::ClientService_DeleteSubscriptions(id,requestId,responseHeader, (OpcUa_DeleteSubscriptionsResponse *)msgBody) ;
	         					break ;
	#endif
	         				default:
	         					odebug_i(S2OPC_ERR,"Services_EnqueueEvent","Not an answer message: %d",encType->TypeId) ;
	          					break ;
	         				}
	            		} else {
	            			odebug_is(S2OPC_ERR,"Services_EnqueueEvent","Cannot decode msg body in response %d (%s)",encType->TypeId,encType->TypeName) ;
	            			if (encType->TypeId == OpcUaId_ServiceFault) {
	            				opcua::ClientService_ServiceFault(id,requestId,responseHeader,(OpcUa_ServiceFault *)NULL) ;
	            			} else {
	            				odebug(S2OPC_ERR,"","Aborting...")
	            			}
	            		}
	            	} else {
	            		odebug_is(S2OPC_ERR,"Services_EnqueueEvent","Cannot decode header in response %d (%s)",encType->TypeId,encType->TypeName) ;
	            	}
				}
			} else {
				odebug_ii(S2OPC_ERR,"ServerServices_EnqueueEvent","Cannot read encType in message on channel %d with requestId %d",id,requestId) ;
			}
			SOPC_Buffer_Delete(buffer);
		}
	}   break;

	case APP_TO_SE_OPEN_ENDPOINT: // open connections listener for the endpoint corresponding to the given endpoint configuration index
	{
		// id = endpoint description config index
		odebug_is(S2OPC_DBG,"ServerServices_EnqueueEvent","APP_TO_SE_OPEN_ENDPOINT id=%d and url=%s",id,SOPC_ToolkitServer_GetEndpointConfig(id)->endpointURL) ;
		SOPC_Endpoint_Config * epConfig = SOPC_ToolkitServer_GetEndpointConfig(id);
		if (epConfig == NULL) {
			odebug_i(S2OPC_ERR,"ServerServices_EnqueueEvent","APP_TO_SE_OPEN_ENDPOINT Cannot get Endpoint configuration for edConfigIdx=%d",id) ;
		} else {
			SOPC_SecureChannels_EnqueueEvent(EP_OPEN,id, NULL, 0);
		}
	} break ;

    case SC_TO_SE_SND_FAILURE:
    {
    	odebug_iii(S2OPC_DBG,"Services_EnqueueEvent","SC_TO_SE_SND_FAILURE scid=%d, requestid=%d with statusCode=0x%08x",id,*(int *)params,(uint32_t)auxParam) ;
    } break ;
	default:
		odebug_ii(S2OPC_DBG,"ServerServices_EnqueueEvent","Receiving unknown message with scEvent=%d and id=%d",scEvent,id) ;
	}

	odebug_ii(S2OPC_DBG,"ServerServices_EnqueueEvent","Returning from event with scEvent=%d and id=%d",scEvent,id) ;
}
} // extern "C"


extern "C" {

void SOPC_Services_Initialize() {}

void SOPC_Services_ToolkitConfigured() {}

void SOPC_Services_PreClear() {}

void SOPC_Services_Clear() {}

} // extern "C"
