
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_S2OPC_S2OPC_SERVERSERVICES_H_
#define LIB_S2OPC_S2OPC_SERVERSERVICES_H_

#include "../OpcUa.h"
#include "../Services/All.h"

MYDLL void ServerService_SendResponse(
    uint32_t               scConnectionId,
	uint32_t			   requestId,
	SOPC_EncodeableType  * encodeable,
    OpcUa_ResponseHeader * responseHeader,
    void                 * response) ;

namespace opcua {

MYDLL void ServerService_SendError(
	uint32_t               scConnectionId,
	uint32_t			   requestId,
	uint32_t               requestHandle,
	SOPC_StatusCode        status) ;

MYDLL void ServerService_SendError(
	uint32_t               scConnectionId,
	uint32_t			   requestId,
	OpcUa_RequestHeader  * requestHeader,
	SOPC_StatusCode        status) ;


#if (WITH_DISCOVERY == 1)
MYDLL void ServerService_FindServers(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_FindServersRequest  * request) ;
#endif

#if (WITH_DISCOVERY == 1)
MYDLL void ServerService_GetEndpoints(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_GetEndpointsRequest * request) ;
#endif

#if (WITH_CALL == 1)
MYDLL void ServerService_Call(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_CallRequest         * request) ;
#endif

#if (WITH_NODEMNGT == 1)
MYDLL void ServerService_AddNodes(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_AddNodesRequest     * request) ;
#endif

#if (WITH_NODEMNGT == 1)
MYDLL void ServerService_AddReferences(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_AddReferencesRequest * request) ;
#endif

#if (WITH_READ == 1)
MYDLL void ServerService_Read(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_ReadRequest         * request) ;
#endif

#if (WITH_WRITE == 1)
MYDLL void ServerService_Write(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_WriteRequest        * request) ;
#endif

MYDLL void ServerService_CreateSession(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_CreateSessionRequest * request) ;

MYDLL void ServerService_ActivateSession(
    uint32_t                       scConnectionId,
	uint32_t			           requestId,
    OpcUa_RequestHeader          * requestHeader,
    OpcUa_ActivateSessionRequest * request) ;

MYDLL void ServerService_CloseSession(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_CloseSessionRequest  * request) ;

#if (WITH_BROWSE == 1)
MYDLL void ServerService_Browse(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_BrowseRequest       * request) ;
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ServerService_RegisterNodes(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_RegisterNodesRequest * request) ;
#endif

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
MYDLL void ServerService_TranslateBrowsePathsToNodeIds(
    uint32_t                                     scConnectionId,
	uint32_t			                         requestId,
    OpcUa_RequestHeader                        * requestHeader,
    OpcUa_TranslateBrowsePathsToNodeIdsRequest * request) ;
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ServerService_UnregisterNodes(
    uint32_t                       scConnectionId,
	uint32_t			           requestId,
    OpcUa_RequestHeader          * requestHeader,
    OpcUa_UnregisterNodesRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_CreateMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_CreateMonitoredItemsRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_DeleteMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_DeleteMonitoredItemsRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_ModifyMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_ModifyMonitoredItemsRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_SetMonitoringMode(
    uint32_t                         scConnectionId,
	uint32_t			             requestId,
    OpcUa_RequestHeader            * requestHeader,
    OpcUa_SetMonitoringModeRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_CreateSubscription(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_CreateSubscriptionRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_DeleteSubscriptions(
    uint32_t                           scConnectionId,
	uint32_t			               requestId,
    OpcUa_RequestHeader              * requestHeader,
    OpcUa_DeleteSubscriptionsRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_ModifySubscription(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_ModifySubscriptionRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_Republish(
    uint32_t                 scConnectionId,
	uint32_t			     requestId,
    OpcUa_RequestHeader    * requestHeader,
    OpcUa_RepublishRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_SetPublishingMode(
    uint32_t                         scConnectionId,
	uint32_t			             requestId,
    OpcUa_RequestHeader            * requestHeader,
    OpcUa_SetPublishingModeRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_TransferSubscriptions(
    uint32_t                             scConnectionId,
	uint32_t			                 requestId,
    OpcUa_RequestHeader                * requestHeader,
    OpcUa_TransferSubscriptionsRequest * request) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_Publish(
    uint32_t                             scConnectionId,
	uint32_t			                 requestId,
    OpcUa_RequestHeader                * requestHeader,
    OpcUa_PublishRequest               * request) ;
#endif


}

#endif /* LIB_S2OPC_S2OPC_SERVERSERVICES_H_ */
