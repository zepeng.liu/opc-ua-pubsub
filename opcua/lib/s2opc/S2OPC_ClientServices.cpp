
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../s2opc/S2OPC_ClientServices.h"

#include "sopc_types.h"

#include <client/OpcUa_BaseClient.h>

extern "C" {

	extern opcua::BaseClient ** the_clients ;
	extern int                  the_nbClients ;

	extern int endpointOpened ; // Managed in s2opc_server/S2OPC_StubServices.cpp

	opcua::BaseClient ** the_clients  = NULL ;
	int                  the_nbClients = 0 ;
}


MYDLL void ClientService_SendRequest(
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
		SOPC_EncodeableType  * encodeable,
	    OpcUa_RequestHeader  * requestHeader,
	    void                 * request)
{
	SOPC_Buffer * buffer = SOPC_Buffer_Create(SOPC_MAX_MESSAGE_LENGTH) ;

	if (buffer == NULL) {
		odebug_i(S2OPC_ERR,"ClientService_SendRequest","Cannot SOPC_Buffer_Create a buffer of length %d",SOPC_MAX_MESSAGE_LENGTH) ;
		return ;
	}

	SOPC_StatusCode status = SOPC_Buffer_SetDataLength(
			buffer,
			SOPC_UA_SECURE_MESSAGE_HEADER_LENGTH +
            SOPC_UA_SYMMETRIC_SECURITY_HEADER_LENGTH +
            SOPC_UA_SECURE_MESSAGE_SEQUENCE_LENGTH);

	if (status != SOPC_STATUS_OK) {
		odebug(S2OPC_ERR,"ClientService_SendRequest","Cannot SOPC_Buffer_SetDataLength on a buffer") ;
		return ;
	}

	status = SOPC_Buffer_SetPosition(
			buffer,
			SOPC_UA_SECURE_MESSAGE_HEADER_LENGTH +
            SOPC_UA_SYMMETRIC_SECURITY_HEADER_LENGTH +
            SOPC_UA_SECURE_MESSAGE_SEQUENCE_LENGTH);

	if (status != SOPC_STATUS_OK) {
		odebug(S2OPC_ERR,"ClientService_SendRequest","Cannot SOPC_Buffer_SetPosition on a buffer") ;
		return ;
	}

    // Encode OpcUa message (header + body)
     status = SOPC_EncodeMsg_Type_Header_Body(buffer,encodeable,&OpcUa_RequestHeader_EncodeableType,(void*)requestHeader,(void*)request);

 	if (status != SOPC_STATUS_OK) {
 		odebug_i(S2OPC_ERR,"ClientService_SendRequest","Cannot SOPC_EncodeMsg_Type_Header_Body in the buffer with typeId=%d",encodeable->TypeId) ;
 		return ;
 	}

    // SEND EVENT TO SC LAYER: ASK TO SEND MSG ON SC
 	odebug_ii(S2OPC_DBG,"ClientService_SendRequest","EnqueueEvent with scId=%d and requestId=%d",scConnectionId,requestId) ;
    SOPC_SecureChannels_EnqueueEvent(SC_SERVICE_SND_MSG,scConnectionId,(void*) buffer,requestId);

 	if (status != SOPC_STATUS_OK) {
 		odebug_i(S2OPC_ERR,"ClientService_SendRequest","Cannot SOPC_SecureChannels_EnqueueEvent the buffer with scConnectionId=%d",scConnectionId) ;
 		return ;
 	}
}


MYDLL void ClientService_SendResponse(
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
		SOPC_EncodeableType  * encodeable,
	    OpcUa_ResponseHeader * responseHeader,
	    void                 * response)
{
	NOT_USED(scConnectionId) ;
	NOT_USED(requestId) ;
	NOT_USED(encodeable) ;
	NOT_USED(responseHeader) ;
	NOT_USED(response) ;
	OCHECK_INT(0) ;
}


namespace opcua {


MYDLL void ClientService_SendError( /* Because of a link probleme. */
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
	    uint32_t               requestHandle,
		SOPC_StatusCode        status)
{
	NOT_USED(scConnectionId) ;
	NOT_USED(requestId) ;
	NOT_USED(requestHandle) ;
	NOT_USED(status) ;
	CHECK_INT(0) ;
}

#include "lib/Services/Session/OpcUa_IPCS_CreateSessionResponse.h"

MYDLL void ClientService_ServiceFault(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_ServiceFault                * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	ServiceFault          * r      = NULL ;

	r = ServiceFault::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_ServiceFault","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_ServiceFault","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_i(IPCS_DBG,"ClientService_ServiceFault","Receiving ServceFault response with client=%u",client) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_ServiceFault_Clear(response) ;
	free(responseHeader) ;
	free(response) ;

	NOT_USED(scConnectionId) ;
}





#include "lib/Services/Session/OpcUa_IPCS_CreateSessionResponse.h"

MYDLL void ClientService_CreateSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_CreateSessionResponse       * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	CreateSessionResponse * r      = NULL ;

	r = CreateSessionResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_CreateSession","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_CreateSession","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_i(IPCS_DBG,"ClientService_CreateSession","Receiving CreateSession response with client=%u",client) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_CreateSessionResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;

	NOT_USED(scConnectionId) ;
}



#include "lib/Services/Session/OpcUa_IPCS_ActivateSessionResponse.h"

MYDLL void ClientService_ActivateSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_ActivateSessionResponse     * response)
{
	SOPC_StatusCode           status = STATUS_OK ;
	ActivateSessionResponse * r      = NULL ;

	r = ActivateSessionResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_ActivateSession","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_ActivateSession","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_ActivateSession","Receiving ActvateSession response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_ActivateSessionResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}



#include "lib/Services/Session/OpcUa_IPCS_CloseSessionResponse.h"

MYDLL void ClientService_CloseSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_CloseSessionResponse        * response)
{
	SOPC_StatusCode           status = STATUS_OK ;
	CloseSessionResponse * r      = NULL ;

	r = CloseSessionResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_CloseSession","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_CloseSession","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_CloseSession","Receiving CloseSession response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_CloseSessionResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}



#if (WITH_READ == 1)
#include "lib/Services/Query/OpcUa_IPCS_ReadResponse.h"

MYDLL void ClientService_Read(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_ReadResponse                * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	ReadResponse * r      = NULL ;

	r = ReadResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Read","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Read","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Read","Receiving Read response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_ReadResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_WRITE == 1)
#include "lib/Services/Query/OpcUa_IPCS_WriteResponse.h"

MYDLL void ClientService_Write(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_WriteResponse               * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	WriteResponse * r      = NULL ;

	r = WriteResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Write","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Write","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Write","Receiving Write response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_WriteResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_CALL == 1)
#include "lib/Services/Method/OpcUa_IPCS_CallResponse.h"

MYDLL void ClientService_Call(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_CallResponse                * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	CallResponse          * r      = NULL ;

	r = CallResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Call","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Call","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Call","Receiving Call response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_CallResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_BROWSE == 1)
#include "lib/Services/View/OpcUa_IPCS_BrowseResponse.h"

MYDLL void ClientService_Browse(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_BrowseResponse              * response)
{
	SOPC_StatusCode         status = STATUS_OK ;
	BrowseResponse        * r      = NULL ;

	r = BrowseResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Browse","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Browse","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Browse","Receiving Browse response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_BrowseResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_REGISTER_UNREGISTER_NODES == 1)
#include "lib/Services/View/OpcUa_IPCS_RegisterNodesResponse.h"

MYDLL void ClientService_RegisterNodes(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_RegisterNodesResponse       * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	RegisterNodesResponse        * r      = NULL ;

	r = RegisterNodesResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_RegisterNodes","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_RegisterNodes","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_RegisterNodes","Receiving RegisterNodes response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_RegisterNodesResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_REGISTER_UNREGISTER_NODES == 1)
#include "lib/Services/View/OpcUa_IPCS_UnregisterNodesResponse.h"

MYDLL void ClientService_UnregisterNodes(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_UnregisterNodesResponse     * response)
{
	SOPC_StatusCode                  status = STATUS_OK ;
	UnregisterNodesResponse        * r      = NULL ;

	r = UnregisterNodesResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_UnregisterNodes","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_UnregisterNodes","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_UnregisterNodes","Receiving UnregisterNodes response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_UnregisterNodesResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
#include "lib/Services/View/OpcUa_IPCS_TranslateBrowsePathsToNodeIdsResponse.h"

MYDLL void ClientService_TranslateBrowsePathsToNodeIds(
	    uint32_t                                      scConnectionId,
		uint32_t			                          requestId,
	    OpcUa_ResponseHeader                        * responseHeader,
	    OpcUa_TranslateBrowsePathsToNodeIdsResponse * response)
{
	SOPC_StatusCode                         status = STATUS_OK ;
	TranslateBrowsePathsToNodeIdsResponse * r      = NULL ;

	r = TranslateBrowsePathsToNodeIdsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_TranslateBrowsePathsToNodeIds","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_TranslateBrowsePathsToNodeIds","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_TranslateBrowsePathsToNodeIds","Receiving TranslateBrowsePathsToNodeIds response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_TranslateBrowsePathsToNodeIdsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/MonitoredItem/OpcUa_IPCS_CreateMonitoredItemsResponse.h"

MYDLL void ClientService_CreateMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_CreateMonitoredItemsResponse * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	CreateMonitoredItemsResponse * r      = NULL ;

	r = CreateMonitoredItemsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_CreateMonitoredItems","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_CreateMonitoredItems","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_CreateMonitoredItems","Receiving CreateMonitoredItems response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_CreateMonitoredItemsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/MonitoredItem/OpcUa_IPCS_ModifyMonitoredItemsResponse.h"

MYDLL void ClientService_ModifyMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_ModifyMonitoredItemsResponse * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	ModifyMonitoredItemsResponse * r      = NULL ;

	r = ModifyMonitoredItemsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_ModifyMonitoredItems","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_ModifyMonitoredItems","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_ModifyMonitoredItems","Receiving ModifyMonitoredItems response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_ModifyMonitoredItemsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/MonitoredItem/OpcUa_IPCS_SetMonitoringModeResponse.h"

MYDLL void ClientService_SetMonitoringMode(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_SetMonitoringModeResponse    * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	SetMonitoringModeResponse    * r      = NULL ;

	r = SetMonitoringModeResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_SetMonitoringMode","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_SetMonitoringMode","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_SetMonitoringMode","Receiving SetMonitoringMode response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_SetMonitoringModeResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/MonitoredItem/OpcUa_IPCS_DeleteMonitoredItemsResponse.h"

MYDLL void ClientService_DeleteMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_DeleteMonitoredItemsResponse * response)
{
	SOPC_StatusCode                   status = STATUS_OK ;
	DeleteMonitoredItemsResponse    * r      = NULL ;

	r = DeleteMonitoredItemsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_DeleteMonitoredItems","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_DeleteMonitoredItems","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_DeleteMonitoredItems","Receiving DeleteMonitoredItems response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_DeleteMonitoredItemsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_CreateSubscriptionResponse.h"

MYDLL void ClientService_CreateSubscription(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_CreateSubscriptionResponse   * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	CreateSubscriptionResponse   * r      = NULL ;

	r = CreateSubscriptionResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_CreateSubscription","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_CreateSubscription","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_CreateSubscription","Receiving CreateSubscription response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_CreateSubscriptionResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_ModifySubscriptionResponse.h"

MYDLL void ClientService_ModifySubscription(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_ModifySubscriptionResponse   * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	ModifySubscriptionResponse   * r      = NULL ;

	r = ModifySubscriptionResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_ModifySubscription","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_ModifySubscription","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_ModifySubscription","Receiving ModifySubscription response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_ModifySubscriptionResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_SetPublishingModeResponse.h"

MYDLL void ClientService_SetPublishingMode(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_SetPublishingModeResponse    * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	SetPublishingModeResponse    * r      = NULL ;

	r = SetPublishingModeResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_SetPublishingMode","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_SetPublishingMode","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_SetPublishingMode","Receiving SetPublishingMode response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_SetPublishingModeResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_PublishResponse.h"

MYDLL void ClientService_Publish(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_PublishResponse              * response)
{
	SOPC_StatusCode                status = STATUS_OK ;
	PublishResponse              * r      = NULL ;

	r = PublishResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Publish","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Publish","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Publish","Receiving Publish response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_PublishResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_RepublishResponse.h"

MYDLL void ClientService_Republish(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responseHeader,
	    OpcUa_RepublishResponse            * response)
{
	SOPC_StatusCode                  status = STATUS_OK ;
	RepublishResponse              * r      = NULL ;

	r = RepublishResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_Republish","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_Republish","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_Republish","Receiving Republish response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_RepublishResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_TransferSubscriptionsResponse.h"

MYDLL void ClientService_TransferSubscriptions(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responseHeader,
	    OpcUa_TransferSubscriptionsResponse * response)
{
	SOPC_StatusCode                              status = STATUS_OK ;
	TransferSubscriptionsResponse              * r      = NULL ;

	r = TransferSubscriptionsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_TransferSubscriptions","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_TransferSubscriptions","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_TransferSubscriptions","Receiving TransferSubscriptions response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_TransferSubscriptionsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_SUBSCRIPTION == 1)
#include "lib/Services/Subscription/OpcUa_IPCS_DeleteSubscriptionsResponse.h"

MYDLL void ClientService_DeleteSubscriptions(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responseHeader,
	    OpcUa_DeleteSubscriptionsResponse   * response)
{
	SOPC_StatusCode                            status = STATUS_OK ;
	DeleteSubscriptionsResponse              * r      = NULL ;

	r = DeleteSubscriptionsResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_DeleteSubscriptions","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_DeleteSubscriptions","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_DeleteSubscriptions","Receiving DeleteSubscriptions response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_DeleteSubscriptionsResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif



#if (WITH_NODEMNGT == 1)
#include "lib/Services/NodeManagement/OpcUa_IPCS_AddNodesResponse.h"

MYDLL void ClientService_AddNodes(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responseHeader,
	    OpcUa_AddNodesResponse              * response)
{
	SOPC_StatusCode                 status = STATUS_OK ;
	AddNodesResponse              * r      = NULL ;

	r = AddNodesResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_AddNodes","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_AddNodes","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_AddNodes","Receiving AddNodes response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_AddNodesResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif




#if (WITH_NODEMNGT == 1)
#include "../Services/NodeManagement/OpcUa_IPCS_AddReferencesResponse.h"

MYDLL void ClientService_AddReferences(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responseHeader,
	    OpcUa_AddReferencesResponse              * response)
{
	SOPC_StatusCode                 status = STATUS_OK ;
	AddReferencesResponse         * r      = NULL ;

	r = AddReferencesResponse::fromCtoCpp(&status, *responseHeader, *response) ;

	if (status != STATUS_OK) {
		debug_i(IPCS_ERR,"ClientService_AddReferences","Cannot convert from C to Cpp with client=%d",requestId) ;
		if (r != NULL)
			delete r ;
		return ;
	}

	uint32_t client = r->getResponseHeader()->getRequestHandle()->get() & 0x000000FF ;

	if (client > (uint32_t)the_nbClients) {
		debug_ii(IPCS_ERR,"ClientService_AddReferences","Not a client number (<%d): client=%d",the_nbClients,client) ;
		delete r ;
		return ;
	}

	debug_ii(IPCS_DBG,"ClientService_AddReferences","Receiving AddReferences response with scId=%d and requestId=%d",scConnectionId,requestId) ;

	the_clients[client]->pushResponse(r) ;

	OpcUa_ResponseHeader_Clear(responseHeader) ;
	OpcUa_AddReferencesResponse_Clear(response) ;
	free(responseHeader) ;
	free(response) ;
}
#endif






} // namespace opcua


