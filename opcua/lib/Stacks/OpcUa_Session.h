
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SESSION_H_
#define OPCUA_SESSION_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../AddressSpace/OpcUa_Base.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "../Stacks/OpcUa_BaseList.h"
#include "../NotificationsAndEvents/OpcUa_PublishingReqQueue.h"

namespace opcua {

class Subscription ;
class SubscriptionsList ;

class MYDLL Session
{
private:

	uint32_t                         authenticationTokenNum ;
	bool                             activated ;
	bool 	                         closed ;
	Nonce                          * lastCreateOrActivateSessionNonce ;
	BaseList                       * registeredNodes ;
	NodeId                         * sessionId ;
#if (SECURITY != UANONE)
	Nonce 			               * localNonce ;
	Nonce			               * distantNonce ;
	ApplicationInstanceCertificate * distantCertificate ;
#endif
#if (WITH_SUBSCRIPTION == 1)
	uint32_t                         subscriptionsCount ;
	SubscriptionsList              * subscriptionsList ;
	PublishingReqQueue             * publishingReqQueue ;
#endif

public:

	Session(uint32_t _authenticationTokenNum, NodeId * _sessionId)
	{
		authenticationTokenNum           = _authenticationTokenNum ;
		activated                        = false ;
		closed                           = false ;
	    lastCreateOrActivateSessionNonce = NULL ;
	    registeredNodes                  = NULL ;
		(sessionId                       = _sessionId)->take() ;
#if (SECURITY != UANONE)
		localNonce                       = NULL ;
		distantNonce                     = NULL ;
		distantCertificate               = NULL ;
#endif
#if (WITH_SUBSCRIPTION == 1)
		subscriptionsCount               = 0 ;
		subscriptionsList                = NULL ;
		publishingReqQueue               = new PublishingReqQueue() ;
#endif
	}

	Session(uint32_t _authenticationTokenNum, String * sessionName)
	{
		authenticationTokenNum           = _authenticationTokenNum ;
		activated                        = false ;
		closed                           = false ;
	    lastCreateOrActivateSessionNonce = NULL ;
	    registeredNodes                  = NULL ;
		(sessionId                       = new NodeId(new UInt16(0),sessionName))->take() ;
#if (SECURITY != UANONE)
		localNonce                       = NULL ;
		distantNonce                     = NULL ;
		distantCertificate               = NULL ;
#endif
#if (WITH_SUBSCRIPTION == 1)
		subscriptionsCount               = 0 ;
		subscriptionsList                = NULL ;
		publishingReqQueue               = new PublishingReqQueue() ;
#endif
	}

	virtual ~Session()
	{
		if (lastCreateOrActivateSessionNonce != NULL)
			lastCreateOrActivateSessionNonce->release();
		deleteAllRegisteredNodes() ;
		sessionId->release() ;
#if (SECURITY != UANONE)
		if (localNonce != NULL)
			localNonce->release() ;
		if (distantNonce != NULL)
			distantNonce->release() ;
		if (distantCertificate != NULL)
			distantCertificate->release() ;
#endif
#if (WITH_SUBSCRIPTION == 1)
		deleteAllSubscriptions() ;
		delete publishingReqQueue ;
#endif
	}

public:

	inline bool isEqual(Session * session)
	{
		return (this == session) ;
	}

	inline bool isNotEqual(Session * session)
	{
		return (this != session) ;
	}

#if (WITH_SUBSCRIPTION == 1)

public:

	void deleteAllSubscriptions() ;

	inline bool onlyOneSubscription()
	{
		return (subscriptionsCount == 1) ;
	}

	void addSubscription(Subscription * subscription) ;

	void deleteSubscription(Subscription * subscription) ;

	Subscription * getCurrentSubscription() ;

public:

	inline PublishingReqQueue * getPublishingReqQueue()
	{
		return publishingReqQueue ;
	}

#endif // WITH_SUBSCRIPTION


public:

	inline void deleteAllRegisteredNodes()
	{
		if (registeredNodes != NULL) {
			BaseList::deleteAll(registeredNodes) ;
			registeredNodes = NULL ;
		}
	}

	inline void addRegisteredNode(NodeId * nodeId, Base * base)
	{
		if (getRegisteredNode(nodeId) == NULL)
			registeredNodes = new BaseList(base,registeredNodes) ;
	}

	inline void deleteRegisteredNode(Base * base)
	{
			registeredNodes = BaseList::deleteBase(registeredNodes,base) ;
	}

	inline Base * getRegisteredNode(NodeId * nodeId)
	{
		if (registeredNodes)
			return registeredNodes->getBase(nodeId) ;
		return NULL ;
	}

public:

	inline uint32_t getAuthenticationTokenNum()
	{
		return authenticationTokenNum ;
	}

	inline void setActivated(bool _activated)
	{
		activated = _activated ;
	}

	inline bool getActivated()
	{
		return activated ;
	}

	inline NodeId * getSessionId()
	{
		return sessionId ;
	}

	inline bool getClosed()
	{
		return closed ;
	}

	inline void setClosed(bool value)
	{
		closed = value ;
	}

	inline Nonce * getLastCreateOrActivateSessionNonce()
	{
		return lastCreateOrActivateSessionNonce ;
	}

	void setLastCreateOrActivateSessionNonce(Nonce * _lastCreateOrActivateSessionNonce)
	{
		Nonce * tmp = lastCreateOrActivateSessionNonce ;
		(lastCreateOrActivateSessionNonce = _lastCreateOrActivateSessionNonce)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

#if (SECURITY != UANONE)

	inline Nonce * getLocalNonce()
	{
		return localNonce;
	}

	void setLocalNonce(Nonce * _localNonce)
	{
		Nonce *tmp = localNonce ;
		(localNonce = _localNonce)->take();
		if (tmp != NULL)
			tmp->release() ;
	}

	inline Nonce * getDistantNonce()
	{
		return distantNonce;
	}

	inline void setDistantNonce(Nonce * _distantNonce)
	{
		Nonce * tmp = distantNonce ;
		(distantNonce = _distantNonce)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline ApplicationInstanceCertificate * getDistantCertificate()
	{
		return distantCertificate ;
	}

	inline void setDistantCertificate(ApplicationInstanceCertificate * _distantCertificate)
	{
		ApplicationInstanceCertificate * tmp = distantCertificate ;
		(distantCertificate = _distantCertificate)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

#endif

};     /* class */
}      /* namespace opcua */
#endif /* SESSION_H_ */
