/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_FINDSERVERSRESPONSE_H_
#define OPCUA_FINDSERVERSRESPONSE_H_

/*
 * Part 4, 5.4.2.2
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../Utils/OpcUa_GENR_Tables.h"

namespace opcua {

class MYDLL FindServersResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_FindServersResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_FindServersResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader              * responseHeader ;
	TableApplicationDescription * servers ;

public:


	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_FindServersResponse& pFindServersResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		servers         ->fromCpptoC (pStatus, pFindServersResponse.NoOfServers,  pFindServersResponse.Servers) ;
	}

public:

	FindServersResponse(
			ResponseHeader              * _responseHeader,
			TableApplicationDescription * _servers
			)
		: Structure()
	{
		(responseHeader   = _responseHeader) ->take() ;
		(servers          = _servers)        ->take() ;
	}

	virtual ~FindServersResponse()
	{
		responseHeader  ->release() ;
		servers        ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableApplicationDescription * getServers()
	{
		return servers;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_FINDSERVERSRESPONSE_H_ */
