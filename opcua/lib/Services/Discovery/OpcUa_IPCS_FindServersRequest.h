/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_FINDSERVERTSREQUEST_H_
#define OPCUA_FINDSERVERTSREQUEST_H_

/*
 * Part 4, 5.4.2.2
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL FindServersRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_FindServersRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_FindServersRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	String        * endpointUrl ;
	TableLocaleId * localeIds ;
	TableString   * serverUris ;

public:

	static FindServersRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_FindServersRequest const& pFindServersRequest)
	{
		RequestHeader * requestHeader = RequestHeader ::fromCtoCpp (pStatus, pRequestHeader) ;
		String        * endpointUrl   = String        ::fromCtoCpp (pStatus, pFindServersRequest.EndpointUrl) ;
		TableLocaleId * localeIds     = TableLocaleId ::fromCtoCpp (pStatus, pFindServersRequest.NoOfLocaleIds,   pFindServersRequest.LocaleIds) ;
		TableString   * serverUris    = TableString   ::fromCtoCpp (pStatus, pFindServersRequest.NoOfServerUris, pFindServersRequest.ServerUris) ;

		if (*pStatus == STATUS_OK)
			return
					new FindServersRequest(
							requestHeader,
							endpointUrl,
							localeIds,
							serverUris
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (endpointUrl != NULL)
			endpointUrl->checkRefCount() ;
		if (localeIds != NULL)
			localeIds->checkRefCount() ;
		if (serverUris != NULL)
			serverUris->checkRefCount() ;

		return NULL ;
	}

public:

	FindServersRequest(
			RequestHeader * _requestHeader,
			String        * _endpointUrl,
			TableLocaleId * _localeIds,
			TableString   * _serverUris

			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(endpointUrl   = _endpointUrl)   ->take() ;
		(localeIds     = _localeIds)     ->take() ;
		(serverUris    = _serverUris)    ->take() ;
	}

	virtual ~FindServersRequest()
	{
		requestHeader ->release() ;
		endpointUrl   ->release() ;
		localeIds     ->release() ;
		serverUris    ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline String * getEndpointUrl()
	{
		return endpointUrl ;
	}

	inline TableLocaleId * getLocaleIds()
	{
		return localeIds ;
	}

	inline TableString * getServerUris()
	{
		return serverUris ;
	}


};
} /* namespace opcua */
#endif
#endif /* OPCUA_FINDSERVERTSREQUEST_H_ */
