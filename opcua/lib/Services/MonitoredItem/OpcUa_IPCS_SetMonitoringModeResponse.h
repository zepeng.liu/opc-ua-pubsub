/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SETMONITORINGMODERESPONSE_H_
#define OPCUA_SETMONITORINGMODERESPONSE_H_

/*
 * Part 4, 5.12.4.2, Table 70, p. 70
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL SetMonitoringModeResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_SetMonitoringModeResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_SetMonitoringModeResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableStatusCode       * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static SetMonitoringModeResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_SetMonitoringModeResponse const& pSetMonitoringModeResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp     (pStatus, pResponseHeader) ;
		TableStatusCode       * results         = TableStatusCode::fromCtoCpp     (pStatus, pSetMonitoringModeResponse.NoOfResults,         pSetMonitoringModeResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pSetMonitoringModeResponse.NoOfDiagnosticInfos, pSetMonitoringModeResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new SetMonitoringModeResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_SetMonitoringModeResponse& pSetMonitoringModeResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pSetMonitoringModeResponse.NoOfResults,         pSetMonitoringModeResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pSetMonitoringModeResponse.NoOfDiagnosticInfos, pSetMonitoringModeResponse.DiagnosticInfos) ;
	}

public:

	SetMonitoringModeResponse(
			ResponseHeader        * _responseHeader,
			TableStatusCode       * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			) : Structure() {
		(responseHeader  = _responseHeader)  ->take();
		(results         = _results)         ->take();
		(diagnosticInfos = _diagnosticInfos) ->take();
	}

	virtual ~SetMonitoringModeResponse()
	{
		responseHeader  ->release();
		results         ->release();
		diagnosticInfos ->release();
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader;
	}

	inline TableStatusCode   * getResults()
	{
		return results;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SETMONITORINGMODERESPONSE_H_ */
