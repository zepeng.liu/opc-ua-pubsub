/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MODIFYMONITOREDITEMSREQUEST_H_
#define OPCUA_MODIFYMONITOREDITEMSREQUEST_H_

/*
 * Part 4, 5.12.3.2, Table 67, p. 69
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemModifyRequest.h"

namespace opcua {

class MYDLL ModifyMonitoredItemsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ModifyMonitoredItemsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ModifyMonitoredItemsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader                     * requestHeader ;
	IntegerId                         * subscriptionId ;
	TimestampsToReturn                * timestampsToReturn ;
	TableMonitoredItemModifyRequest   * itemsToModify ;

public:

	static ModifyMonitoredItemsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_ModifyMonitoredItemsRequest const& pModifyMonitoredItemsRequest)
	{
		RequestHeader                     * requestHeader      = RequestHeader                  ::fromCtoCpp (pStatus, pRequestHeader) ;
		IntegerId                         * subscriptionId     = IntegerId                      ::fromCtoCpp (pStatus, pModifyMonitoredItemsRequest.SubscriptionId) ;
		TimestampsToReturn                * timestampsToReturn = TimestampsToReturn             ::fromCtoCpp (pStatus, pModifyMonitoredItemsRequest.TimestampsToReturn) ;
		TableMonitoredItemModifyRequest   * itemsToModify      = TableMonitoredItemModifyRequest::fromCtoCpp (pStatus, pModifyMonitoredItemsRequest.NoOfItemsToModify,pModifyMonitoredItemsRequest.ItemsToModify) ;

		if (*pStatus == STATUS_OK)
			return
					new ModifyMonitoredItemsRequest(
							requestHeader,
							subscriptionId,
							timestampsToReturn,
							itemsToModify
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (timestampsToReturn != NULL)
			timestampsToReturn->checkRefCount() ;
		if (itemsToModify != NULL)
			itemsToModify->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_ModifyMonitoredItemsRequest& pModifyMonitoredItemsRequest) const
	{
		requestHeader      ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId     ->fromCpptoC (pStatus, pModifyMonitoredItemsRequest.SubscriptionId) ;
		timestampsToReturn ->fromCpptoC (pStatus, pModifyMonitoredItemsRequest.TimestampsToReturn) ;
		itemsToModify      ->fromCpptoC (pStatus, pModifyMonitoredItemsRequest.NoOfItemsToModify, pModifyMonitoredItemsRequest.ItemsToModify) ;
	}

public:

	ModifyMonitoredItemsRequest(
			RequestHeader                     * _requestHeader,
			IntegerId                         * _subscriptionId,
			TimestampsToReturn                * _timestampsToReturn,
			TableMonitoredItemModifyRequest   * _itemsToModify
			) : Structure() {
		(requestHeader      = _requestHeader)      ->take() ;
		(subscriptionId     = _subscriptionId)     ->take() ;
		(timestampsToReturn = _timestampsToReturn) ->take() ;
		(itemsToModify      = _itemsToModify)      ->take() ;
	}

	virtual ~ModifyMonitoredItemsRequest()
	{
		requestHeader      ->release() ;
		subscriptionId     ->release() ;
		timestampsToReturn ->release() ;
		itemsToModify      ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline TimestampsToReturn * getTimestampsToReturn()
	{
		return timestampsToReturn ;
	}

	inline TableMonitoredItemModifyRequest   * getItemsToModify()
	{
		return itemsToModify ;
	}

};
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MODIFYMONITOREDITEMSREQUEST_H_ */
