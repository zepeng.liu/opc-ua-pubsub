/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/All.h"

#include "OpcUa_IPCS_DeleteMonitoredItemsRequest.h"
#include "OpcUa_IPCS_DeleteMonitoredItemsResponse.h"
#include "OpcUa_IPCS_DeleteMonitoredItems.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_DeleteMonitoredItems(
		uint32_t					         secureChannelIdNum,
		OpcUa_RequestHeader                * pRequestHeader,
		OpcUa_DeleteMonitoredItemsRequest  * pRequest,
		OpcUa_ResponseHeader               * pResponseHeader,
		OpcUa_DeleteMonitoredItemsResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	DeleteMonitoredItemsRequest * request  = NULL ;
	request = DeleteMonitoredItemsRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	DeleteMonitoredItemsResponse * response = NULL ;
	DeleteMonitoredItems::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void DeleteMonitoredItems::answer(
		SOPC_StatusCode                     * pStatus,
		uint32_t					          secureChannelIdNum,
	    class DeleteMonitoredItemsRequest   * request,
	    class DeleteMonitoredItemsResponse ** pResponse)
{
	AddressSpace * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"DeleteMonitoredItems","processing request") ;

	RequestHeader               * requestHeader       = request->getRequestHeader() ;
	IntegerId                   * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken  * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		    * channel ;
	Session                     * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"DeleteMonitoredItems","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"DeleteMonitoredItems","Request : continue (1) Request decoding.") ;

	IntegerId                         * subscriptionId     = request->getSubscriptionId() ;

	debug(COM_DBG,"DeleteMonitoredItems","Request : checking subscription.") ;

	SubscriptionsTable::subscriptionsLock() ;

	Subscription * subscription = SubscriptionsTable::subscriptionsTable->getSubscription(subscriptionId) ;

	if (subscription == NULL) {
		debug(COM_ERR,"DeleteMonitoredItems","Request : subscription id invalid.") ;
		SubscriptionsTable::subscriptionsUnlock() ;
		*pStatus = _Bad_SubscriptionIdInvalid ;
	}

	debug(COM_DBG,"DeleteMonitoredItems","Request : continue (2) Request decoding.") ;

	TableIntegerId   * monitoredItemIds    = request->getMonitoredItemIds() ;
	int32_t            monitoredItemIdsLen = monitoredItemIds->getLength() ;

	debug(COM_DBG,"DeleteMonitoredItems","Request : building response structure.") ;

	TableStatusCode       * results         = new TableStatusCode  (monitoredItemIdsLen) ;

	for (int32_t i = 0 ; i < monitoredItemIdsLen ; i++) {
		results->set(i,subscription->DeleteMonitoredItem(monitoredItemIds->get(i))) ;
	}

	SubscriptionsTable::subscriptionsUnlock() ;

	debug(COM_DBG,"DeleteMonitoredItems","Request : returning response structure.") ;

	NOT_USED(addressSpace) ;

	*pResponse =
			new DeleteMonitoredItemsResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			);
}

} /* namespace opcua */

#endif
