/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MONITOREDITEM_ALL_H_
#define _MONITOREDITEM_ALL_H_

#include "OpcUa_IPCS_CreateMonitoredItems.h"
#include "OpcUa_IPCS_CreateMonitoredItemsRequest.h"
#include "OpcUa_IPCS_CreateMonitoredItemsResponse.h"
#include "OpcUa_IPCS_DeleteMonitoredItems.h"

#include "OpcUa_IPCS_DeleteMonitoredItemsRequest.h"
#include "OpcUa_IPCS_DeleteMonitoredItemsResponse.h"

#include "OpcUa_IPCS_ModifyMonitoredItemsRequest.h"
#include "OpcUa_IPCS_ModifyMonitoredItemsResponse.h"
#include "OpcUa_IPCS_ModifyMonitoredItems.h"

#include "OpcUa_IPCS_SetMonitoringModeRequest.h"
#include "OpcUa_IPCS_SetMonitoringModeResponse.h"
#include "OpcUa_IPCS_SetMonitoringMode.h"

#endif /* _MONITOREDITEM_ALL_H_ */
