
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _OPCUA_CLOSESESSIONREQUEST_H_
#define _OPCUA_CLOSESESSIONREQUEST_H_

/*
 * Part3, 5.6.2, p. 26
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"

namespace opcua {

class MYDLL CloseSessionRequest
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CloseSessionRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CloseSessionRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	Boolean       * deleteSubscriptions ;

public:

	static CloseSessionRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_CloseSessionRequest const& pCloseSessionRequest)
	{
		RequestHeader * requestHeader       = RequestHeader ::fromCtoCpp(pStatus, pRequestHeader) ;
		Boolean       * deleteSubscriptions = Boolean       ::fromCtoCpp(pStatus, pCloseSessionRequest.DeleteSubscriptions) ;

		if (*pStatus == STATUS_OK)
			return
					new CloseSessionRequest(
							requestHeader,
							deleteSubscriptions
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (deleteSubscriptions != NULL)
			deleteSubscriptions->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader &pRequestHeader, OpcUa_CloseSessionRequest& pCloseSessionRequest) const
	{
		requestHeader       ->fromCpptoC(pStatus, pRequestHeader) ;
		deleteSubscriptions ->fromCpptoC(pStatus, pCloseSessionRequest.DeleteSubscriptions) ;
	}

public:

	CloseSessionRequest(
			RequestHeader * _requestHeader,
			Boolean       * _deleteSubscriptions
			)
		: Structure()
	{
		(requestHeader       = _requestHeader)       ->take() ;
		(deleteSubscriptions = _deleteSubscriptions) ->take() ;
	}

	virtual ~CloseSessionRequest()
	{
		requestHeader       ->release() ;
		deleteSubscriptions ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline  Boolean * getDeleteSubscriptions()
	{
		return deleteSubscriptions ;
	}

};
} /* namespace opcua */
#endif /* CLOSESESSIONREQUEST_H_ */
