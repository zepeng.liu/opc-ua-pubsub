
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CREATESESSIONREQUEST_H_
#define OPCUA_CREATESESSIONREQUEST_H_

/*
 * Part3, 5.6.4.2, p. 30
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../CommonParametersTypes/All.h"

namespace opcua {

class MYDLL CreateSessionRequest
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CreateSessionRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CreateSessionRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader                  * requestHeader ;
	ApplicationDescription         * clientDescription ;
	String                         * serverUri ;
	String                         * endpointUrl ;
	String                         * sessionName ;

	Nonce                          * clientNonce ;
	ApplicationInstanceCertificate * clientCertificate ;
	Duration                       * requestedSessionTimeout ;
	UInt32                         * maxResponseMessageSize ;

public:

	static CreateSessionRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_CreateSessionRequest const& pCreateSessionRequest)
	{
		RequestHeader                  * requestHeader           = RequestHeader                  ::fromCtoCpp(pStatus, pRequestHeader) ;
		ApplicationDescription         * clientDescription       = ApplicationDescription         ::fromCtoCpp(pStatus, pCreateSessionRequest.ClientDescription) ;
		String                         * serverUri               = String                         ::fromCtoCpp(pStatus, pCreateSessionRequest.ServerUri) ;
		String                         * endpointUrl             = String                         ::fromCtoCpp(pStatus, pCreateSessionRequest.EndpointUrl) ;
		String                         * sessionName             = String                         ::fromCtoCpp(pStatus, pCreateSessionRequest.SessionName) ;

		Nonce                          * clientNonce             = Nonce					      ::fromCtoCpp(pStatus, pCreateSessionRequest.ClientNonce);
		ApplicationInstanceCertificate * clientCertificate       = ApplicationInstanceCertificate ::fromCtoCpp(pStatus, pCreateSessionRequest.ClientCertificate);
		Duration                       * requestedSessionTimeout = Duration					      ::fromCtoCpp(pStatus, pCreateSessionRequest.RequestedSessionTimeout);
		UInt32                         * maxResponseMessageSize  = UInt32						  ::fromCtoCpp(pStatus, pCreateSessionRequest.MaxResponseMessageSize);

		if (*pStatus == STATUS_OK)
			return
					new CreateSessionRequest(
							requestHeader,
							clientDescription,
							serverUri,
							endpointUrl,
							sessionName,

							clientNonce,
							clientCertificate,
							requestedSessionTimeout,
							maxResponseMessageSize
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (clientDescription != NULL)
			clientDescription->checkRefCount() ;
		if (serverUri != NULL)
			serverUri->checkRefCount() ;
		if (endpointUrl != NULL)
			endpointUrl->checkRefCount() ;
		if (sessionName != NULL)
			sessionName->checkRefCount() ;

		if (clientNonce != NULL)
			clientNonce->checkRefCount() ;
		if (clientCertificate != NULL)
			clientCertificate->checkRefCount() ;
		if (requestedSessionTimeout != NULL)
			requestedSessionTimeout->checkRefCount() ;
		if (maxResponseMessageSize != NULL)
			maxResponseMessageSize->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader &pRequestHeader, OpcUa_CreateSessionRequest& pCreateSessionRequest) const
	{
		requestHeader           ->fromCpptoC(pStatus, pRequestHeader) ;
		clientDescription       ->fromCpptoC(pStatus, pCreateSessionRequest.ClientDescription) ;
		serverUri               ->fromCpptoC(pStatus, pCreateSessionRequest.ServerUri) ;
		endpointUrl             ->fromCpptoC(pStatus, pCreateSessionRequest.EndpointUrl) ;
		sessionName             ->fromCpptoC(pStatus, pCreateSessionRequest.SessionName) ;

		clientNonce             ->fromCpptoC(pStatus, pCreateSessionRequest.ClientNonce) ;
		clientCertificate       ->fromCpptoC(pStatus, pCreateSessionRequest.ClientCertificate) ;
		requestedSessionTimeout ->fromCpptoC(pStatus, pCreateSessionRequest.RequestedSessionTimeout) ;
		maxResponseMessageSize  ->fromCpptoC(pStatus, pCreateSessionRequest.MaxResponseMessageSize) ;
	}

public:

	CreateSessionRequest(
			RequestHeader                  * _requestHeader,
			ApplicationDescription         * _clientDescription,
			String                         * _serverUri,
			String                         * _endpointUrl,
			String                         * _sessionName,

			Nonce                          * _clientNonce,
			ApplicationInstanceCertificate * _clientCertificate,
			Duration                       * _requestedSessionTimeout,
			UInt32                         * _maxResponseMessageSize
			)
		: Structure()
	{
		(requestHeader           = _requestHeader)           ->take() ;
		(clientDescription       = _clientDescription)       ->take() ;
		(serverUri               = _serverUri)               ->take() ;
		(endpointUrl             = _endpointUrl)             ->take() ;
		(sessionName             = _sessionName)             ->take() ;

		(clientNonce             = _clientNonce)             ->take() ;
		(clientCertificate       = _clientCertificate)       ->take() ;
		(requestedSessionTimeout = _requestedSessionTimeout) ->take() ;
		(maxResponseMessageSize  = _maxResponseMessageSize)  ->take() ;
	}

	virtual ~CreateSessionRequest()
	{
		requestHeader           ->release() ;
		clientDescription       ->release() ;
		serverUri               ->release() ;
		endpointUrl             ->release() ;
		sessionName             ->release() ;

		clientNonce             ->release() ;
		clientCertificate       ->release() ;
		requestedSessionTimeout ->release() ;
		maxResponseMessageSize  ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline String * getSessionName()
	{
		return sessionName ;
	}

	void setSessionName(String * sessionName)
	{
		this->sessionName->release() ;
		(this->sessionName = sessionName)->take() ;
	}


	inline ApplicationInstanceCertificate * getClientCertificate()
	{
		return clientCertificate;
	}

	void setClientCertificate( ApplicationInstanceCertificate * _clientCertificate)
	{
		ApplicationInstanceCertificate * tmp = clientCertificate ;
		(clientCertificate = _clientCertificate)->take();

		if (tmp != NULL)
			tmp->release() ;
	}

	inline Nonce * getClientNonce()
	{
		return clientNonce ;
	}

	void setClientNonce(Nonce * _clientNonce)
	{
		Nonce * tmp = clientNonce ;
		(clientNonce = _clientNonce)->take() ;

		if (tmp != NULL)
			tmp->release() ;
	}

#if (VERIF_APPURI == VERIF_APPURI_ENABLED)
	String * getClientApplicationUri()
	{
		return clientDescription->getApplicationUri() ;
	}
#endif

};     /* class CreateSession */
}      /* namespace opcua */
#endif /* CREATESESSIONREQUEST_H_ */
