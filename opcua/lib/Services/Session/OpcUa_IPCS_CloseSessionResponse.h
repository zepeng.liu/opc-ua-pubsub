
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_CLOSESESSIONRESPONSE_H_
#define _OPCUA_CLOSESESSIONRESPONSE_H_

/*
 * Part 4, 5.6.4.2, p. 30
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"

namespace opcua {

class MYDLL CloseSessionResponse
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CloseSessionResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CloseSessionResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;

public:

	static CloseSessionResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_CloseSessionResponse const& pCloseSessionResponse)
	{
		ResponseHeader * responseHeader = ResponseHeader ::fromCtoCpp(pStatus, pResponseHeader) ;

		if (*pStatus == STATUS_OK)
			return
					new CloseSessionResponse(
							responseHeader
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;

		NOT_USED(pCloseSessionResponse) ;

		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_CloseSessionResponse& pCloseSessionResponse) const
	{
		responseHeader ->fromCpptoC(pStatus, pResponseHeader) ;

		NOT_USED(pCloseSessionResponse) ;
	}

public:

	CloseSessionResponse(ResponseHeader * _responseHeader)
		: Structure()
	{
		(responseHeader = _responseHeader) ->take() ;
	}

	virtual ~CloseSessionResponse()
	{
		responseHeader->release() ;
	}

public:

	virtual ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

};
} /* namespace opcua */
#endif /* CLOSESESSIONREQUEST_H_ */
