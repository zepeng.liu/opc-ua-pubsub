
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CREATESESSIONRESPONSE_H_
#define OPCUA_CREATESESSIONRESPONSE_H_

/*
 * Part3, 5.6.2, p. 26
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CreateSessionResponse
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CreateSessionResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CreateSessionResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader                   * responseHeader ;
	NodeId                           * sessionId ;
	SessionAuthenticationToken       * authenticationToken ;
	Duration                         * revisedSessionTimeout ;
	Nonce                            * serverNonce ;

	ApplicationInstanceCertificate   * serverCertificate ;
	TableEndpointDescription         * serverEndpoints ;
	TableSignedSoftwareCertificate   * serverSoftwareCertificates ;
	SignatureData                    * serverSignature ;
	UInt32                           * maxRequestMessageSize ;

public:

	static CreateSessionResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_CreateSessionResponse const& pCreateSessionResponse)
	{
		ResponseHeader                   * responseHeader             = ResponseHeader                   ::fromCtoCpp(pStatus, pResponseHeader) ;
		NodeId                           * sessionId                  = NodeId                           ::fromCtoCpp(pStatus, pCreateSessionResponse.SessionId) ;
		SessionAuthenticationToken       * authenticationToken        = SessionAuthenticationToken       ::fromCtoCpp(pStatus, pCreateSessionResponse.AuthenticationToken) ;
		Duration                         * revisedSessionTimeout      = Duration                         ::fromCtoCpp(pStatus, pCreateSessionResponse.RevisedSessionTimeout) ;
		Nonce                            * serverNonce                = Nonce                            ::fromCtoCpp(pStatus, pCreateSessionResponse.ServerNonce) ;

		ApplicationInstanceCertificate   * serverCertificate          = ApplicationInstanceCertificate	 ::fromCtoCpp(pStatus, pCreateSessionResponse.ServerCertificate) ;
		TableEndpointDescription         * serverEndpoints            = TableEndpointDescription::fromCtoCpp(pStatus, pCreateSessionResponse.NoOfServerEndpoints,pCreateSessionResponse.ServerEndpoints) ;
		TableSignedSoftwareCertificate   * serverSoftwareCertificates = TableSignedSoftwareCertificate::fromCtoCpp(pStatus, pCreateSessionResponse.NoOfServerSoftwareCertificates,pCreateSessionResponse.ServerSoftwareCertificates) ;
		SignatureData                    * serverSignature            = SignatureData					 ::fromCtoCpp(pStatus, pCreateSessionResponse.ServerSignature) ;
		UInt32                           * maxRequestMessageSize      = UInt32						     ::fromCtoCpp(pStatus, pCreateSessionResponse.MaxRequestMessageSize);

		if (*pStatus == STATUS_OK)
			return
					new CreateSessionResponse(
							responseHeader,
							sessionId,
							authenticationToken,
							revisedSessionTimeout,
							serverNonce,

							serverCertificate,
							serverEndpoints,
							serverSoftwareCertificates,
							serverSignature,
							maxRequestMessageSize
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (sessionId != NULL)
			sessionId->checkRefCount() ;
		if (authenticationToken != NULL)
			authenticationToken->checkRefCount() ;
		if (revisedSessionTimeout != NULL)
			revisedSessionTimeout->checkRefCount() ;
		if (serverNonce != NULL)
			serverNonce->checkRefCount() ;

		if (serverCertificate != NULL)
			serverCertificate->checkRefCount() ;
		if (serverEndpoints != NULL)
			serverEndpoints->checkRefCount() ;
		if (serverSoftwareCertificates != NULL)
			serverSoftwareCertificates->checkRefCount() ;
		if (serverSignature != NULL)
			serverSignature->checkRefCount() ;
		if (maxRequestMessageSize != NULL)
			maxRequestMessageSize->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_CreateSessionResponse& pCreateSessionResponse) const
	{
		responseHeader             ->fromCpptoC (pStatus, pResponseHeader) ;
		sessionId                  ->fromCpptoC (pStatus, pCreateSessionResponse.SessionId) ;
		authenticationToken        ->fromCpptoC (pStatus, pCreateSessionResponse.AuthenticationToken) ;
		revisedSessionTimeout      ->fromCpptoC (pStatus, pCreateSessionResponse.RevisedSessionTimeout) ;
		serverNonce                ->fromCpptoC (pStatus, pCreateSessionResponse.ServerNonce) ;

		serverCertificate          ->fromCpptoC (pStatus, pCreateSessionResponse.ServerCertificate) ;
		serverEndpoints            ->fromCpptoC (pStatus, pCreateSessionResponse.NoOfServerEndpoints,pCreateSessionResponse.ServerEndpoints) ;
		serverSoftwareCertificates ->fromCpptoC (pStatus, pCreateSessionResponse.NoOfServerSoftwareCertificates,pCreateSessionResponse.ServerSoftwareCertificates) ;
		serverSignature            ->fromCpptoC (pStatus, pCreateSessionResponse.ServerSignature) ;
		maxRequestMessageSize      ->fromCpptoC (pStatus, pCreateSessionResponse.MaxRequestMessageSize) ;
	}

public:

	CreateSessionResponse(
			ResponseHeader                   * _responseHeader,
			NodeId                           * _sessionId,
			SessionAuthenticationToken       * _authenticationToken,
			Duration                         * _revisedSessionTimeout,
			Nonce                            * _serverNonce,
			ApplicationInstanceCertificate   * _serverCertificate,
			TableEndpointDescription         * _serverEndpoints,
			TableSignedSoftwareCertificate   * _serverSoftwareCertificates,
			SignatureData                    * _serverSignature,
			UInt32                           * _maxRequestMessageSize
			)
		: Structure()
	{
		(responseHeader              = _responseHeader)             ->take() ;
		(sessionId                   = _sessionId)                  ->take() ;
		(authenticationToken         = _authenticationToken)        ->take() ;
		(revisedSessionTimeout       = _revisedSessionTimeout)      ->take() ;
		(serverNonce                 = _serverNonce)                ->take() ;
		(serverCertificate           = _serverCertificate)          ->take() ;
		(serverEndpoints             = _serverEndpoints)            ->take() ;
		(serverSoftwareCertificates  = _serverSoftwareCertificates) ->take() ;
		(serverSignature             = _serverSignature)            ->take() ;
		(maxRequestMessageSize       = _maxRequestMessageSize)      ->take() ;
	}

	virtual ~CreateSessionResponse()
	{
		responseHeader              ->release() ;
		sessionId                   ->release() ;
		authenticationToken         ->release() ;
		revisedSessionTimeout       ->release() ;
		serverNonce                 ->release() ;
		serverCertificate           ->release() ;
		serverEndpoints             ->release() ;
		serverSoftwareCertificates  ->release() ;
		serverSignature             ->release() ;
		maxRequestMessageSize       ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline NodeId * getSessionId()
	{
		return sessionId ;
	}

	inline SessionAuthenticationToken * getAuthenticationToken()
	{
		return authenticationToken ;
	}

	inline Nonce * getServerNonce()
	{
		return serverNonce ;
	}


	inline ApplicationInstanceCertificate* getServerCertificate() {
		return serverCertificate;
	}

	void setServerCertificate( ApplicationInstanceCertificate * _serverCertificate) {
		ApplicationInstanceCertificate * tmp = serverCertificate ;
		(serverCertificate = _serverCertificate)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline SignatureData * getServerSignature()
	{
		return serverSignature;
	}
};

} /* namespace opcua */
#endif /* CREATESESSIONREQUEST_H_ */
