
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"

#include "OpcUa_BaseService.h"

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Stacks/OpcUa_Channel.h"
#include "../Stacks/OpcUa_ChannelsTable.h"
#include "../Stacks/OpcUa_Session.h"
#include "../Stacks/OpcUa_SessionsTable.h"
#include "../Server/OpcUa_AddressSpace.h"

namespace opcua {

MYDLL BaseDataType * BaseService::checkChannelAndSession(
		uint32_t                      secureChannelIdNum,
		SessionAuthenticationToken  * authenticationToken,
		IntegerId                   * requestHandle,
		Channel                    ** pchannel,
		Session                    ** psession
		)
{
	Channel * channel = ChannelsTable::self->get(secureChannelIdNum) ;

	if (channel == NULL) {
		debug(COM_DBG,"CloseSession","Request failed : bad channel id.") ;
		return error(requestHandle,StatusCode::Bad_SecureChannelIdInvalid) ;
	}

	Session * session = channel->getSession(authenticationToken->get()) ;

	if (session == NULL) {
		debug_i(COM_DBG,"CloseSession","Request failed : bad session id (bad authenticationToken).",(int)(authenticationToken->get())) ;
		return error(requestHandle,StatusCode::Bad_SessionIdInvalid) ;
	}

	if (! session->getActivated()) {
		debug(COM_DBG,"CloseSession","Request failed : session is not activated.") ;
		session->setClosed(true) ;
		return error(requestHandle,StatusCode::Bad_SessionNotActivated) ;
	}

	if (session->getClosed()) {
		debug(COM_DBG,"CloseSession","Request failed : session is closed.") ;
		return error(requestHandle,StatusCode::Bad_SessionClosed) ;
	}

	*pchannel = channel ;
	*psession = session ;

	return NULL ;
}


MYDLL uint32_t BaseService::checkChannelAndSession(
		uint32_t                      secureChannelIdNum,
		SessionAuthenticationToken  * authenticationToken,
		Channel                    ** pchannel,
		Session                    ** psession
		)
{
	Channel * channel = ChannelsTable::self->get(secureChannelIdNum) ;

	if (channel == NULL) {
		debug(COM_ERR,"BaseService","checkChannelAndSession: bad channel id.") ;
		return _Bad_SecureChannelIdInvalid ;
	} else {
		debug_p(COM_DBG,"BaseService","checkChannelAndSession: good channel %p",channel) ;

	}

	Session * session = channel->getSession(authenticationToken->get()) ;

	if (session == NULL) {
		debug_i(COM_ERR,"BaseService","checkChannelAndSession: bad session id = %u",(int)(authenticationToken->get())) ;
		return _Bad_SessionIdInvalid ;
	} else {
		debug_i(COM_DBG,"BaseService","checkChannelAndSession: good session id = %u",(int)(authenticationToken->get())) ;
	}

	if (! session->getActivated()) {
		debug(COM_ERR,"BaseService","checkChannelAndSession: session is not activated.") ;
		session->setClosed(true) ;
		return _Bad_SessionNotActivated ;
	}

	if (session->getClosed()) {
		debug(COM_ERR,"BaseService","checkChannelAndSession: session is closed.") ;
		return _Bad_SessionClosed ;
	}

	*pchannel = channel ;
	*psession = session ;

	return _Good ;
}

}



