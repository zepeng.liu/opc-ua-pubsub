
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_WRITERESPONSE_H_
#define OPCUA_WRITERESPONSE_H_

/*
 * Part 4, 5.10.4.2, p. 56
 */

#include "../../OpcUa.h"

#if (WITH_WRITE == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL WriteResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_WriteResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_WriteResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableStatusCode       * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static WriteResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_WriteResponse const& pWriteResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp(pStatus, pResponseHeader) ;
		TableStatusCode       * results         = TableStatusCode::fromCtoCpp     (pStatus, pWriteResponse.NoOfResults,         pWriteResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pWriteResponse.NoOfDiagnosticInfos, pWriteResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new WriteResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_WriteResponse& pWriteResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pWriteResponse.NoOfResults,         pWriteResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pWriteResponse.NoOfDiagnosticInfos, pWriteResponse.DiagnosticInfos) ;
	}

public:

	WriteResponse(
			ResponseHeader        * _responseHeader,
			TableStatusCode       * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~WriteResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableStatusCode   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfo()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_WRITERESPONSE_H_ */
