
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_WRITE == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_WriteRequest.h"
#include "OpcUa_IPCS_WriteResponse.h"
#include "OpcUa_IPCS_Write.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Write(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_WriteRequest         * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_WriteResponse        * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	WriteRequest * request  = NULL ;
	request = WriteRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	WriteResponse * response = NULL ;
	Write::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void Write::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class WriteRequest          * request,
	    class WriteResponse        ** pResponse)
{
	AddressSpace * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"Write","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Write","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"Write","Request : getting parameters.") ;

	TableWriteValue   * nodesToWrite = request->getNodesToWrite() ;

	debug(COM_DBG,"Write","Request : checking parameters.") ;

	// Nothing

	debug(COM_DBG,"Write","Request : preparing results.") ;

	int32_t length = nodesToWrite->getLength() ;

	TableStatusCode       * results         = new TableStatusCode  (length) ;

	debug_i(COM_DBG,"Write","Request : getting %d results.",length) ;

	for (int i = 0 ; i < length ; i++) {

		WriteValue * nodeToWrite = nodesToWrite->get(i) ;
		NodeId     * nodeId      = nodeToWrite->getNodeId() ;

		Base       * base        = addressSpace->get(nodeId) ; ;

		StatusCode * result ;

		if (DEBUG_LEVEL & COM_DBG) {
			String * nodeIdString = nodeId->toString() ;
			nodeIdString->take() ;
			debug_sii(COM_DBG,"Write","NodeId \"%s\"->%d (%d)",nodeIdString->get(),nodeToWrite->getAttributeId()->get(),i) ;
			nodeIdString->release() ;
		}

		if (base == NULL) {

			result = new StatusCode(_Bad_NodeIdUnknown) ;

			String * nodeIdString = nodeId->toString() ;

			debug_si(COM_ERR,"Write","Bad NodeId unknown \"%s\" (%d)",nodeIdString->get(),i) ;

			nodeIdString->checkRefCount() ;

		} else {

			result
				= base->putAttribute(
						nodeToWrite->getAttributeId(),
						nodeToWrite->getIndexRange(),
						nodeToWrite->getValue()
					) ;
		}

		results->set(i, result) ;
	}

	debug(COM_DBG,"Write","Request succeeds : returning Response.") ;

	*pResponse =  // Part 4, 5.10.2, p. 52
			new WriteResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							(length != 0) ? StatusCode::Good : StatusCode::Bad_NothingToDo,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}

} /* namespace opcua */

#endif

