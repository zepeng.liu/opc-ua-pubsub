
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_READREQUEST_H_
#define OPCUA_READREQUEST_H_

/*
 * Part 4, 5.10.2, p. 51
 */

#include "../../OpcUa.h"

#if (WITH_READ == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ReadRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ReadRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ReadRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader      * requestHeader ;
	Duration           * maxAge ;
	TimestampsToReturn * timestampsToReturn ;
	TableReadValueId   * nodesToRead ;

public:

	static ReadRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_ReadRequest const& pReadRequest)
	{
		RequestHeader      * requestHeader      = RequestHeader      ::fromCtoCpp(pStatus, pRequestHeader) ;
		Duration           * maxAge             = Duration           ::fromCtoCpp(pStatus, pReadRequest.MaxAge) ;
		TimestampsToReturn * timestampsToReturn = TimestampsToReturn ::fromCtoCpp(pStatus, pReadRequest.TimestampsToReturn) ;
		TableReadValueId   * nodesToRead        = TableReadValueId::fromCtoCpp(pStatus, pReadRequest.NoOfNodesToRead,pReadRequest.NodesToRead) ;

		if (*pStatus == STATUS_OK)
			return
					new ReadRequest(
							requestHeader,
							maxAge,
							timestampsToReturn,
							nodesToRead
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (maxAge != NULL)
			maxAge->checkRefCount() ;
		if (timestampsToReturn != NULL)
			timestampsToReturn->checkRefCount() ;
		if (nodesToRead != NULL)
			nodesToRead->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader &pRequestHeader, OpcUa_ReadRequest& pReadRequest) const
	{
		requestHeader      ->fromCpptoC (pStatus, pRequestHeader) ;
		maxAge             ->fromCpptoC (pStatus, pReadRequest.MaxAge) ;
		timestampsToReturn ->fromCpptoC (pStatus, pReadRequest.TimestampsToReturn) ;
		nodesToRead        ->fromCpptoC (pStatus, pReadRequest.NoOfNodesToRead,pReadRequest.NodesToRead) ;
	}

public:

	ReadRequest(
			RequestHeader      * _requestHeader,
			Duration           * _maxAge,
			TimestampsToReturn * _timestampsToReturn,
			TableReadValueId   * _nodesToRead
			)
		: Structure()
	{
		(requestHeader      = _requestHeader)      ->take() ;
		(maxAge             = _maxAge)             ->take() ;
		(timestampsToReturn = _timestampsToReturn) ->take() ;
		(nodesToRead        = _nodesToRead)        ->take() ;
	}

	virtual ~ReadRequest()
	{
		requestHeader      ->release() ;
		maxAge             ->release() ;
		timestampsToReturn ->release() ;
		nodesToRead        ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline Duration * getMaxAge()
	{
		return maxAge ;
	}

	inline TimestampsToReturn * getTimestampsToReturn()
	{
		return timestampsToReturn ;
	}

	inline TableReadValueId   * getNodesToRead()
	{
		return nodesToRead ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_READREQUEST_H_ */
