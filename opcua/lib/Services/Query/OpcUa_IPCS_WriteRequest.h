
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_WRITEREQUEST_H_
#define OPCUA_WRITEREQUEST_H_

/*
 * Part 4, 5.10.4, p. 56
 */

#include "../../OpcUa.h"

#if (WITH_WRITE == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL WriteRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_WriteRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_WriteRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader     * requestHeader ;
	TableWriteValue   * nodesToWrite ;

public:

	static WriteRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_WriteRequest const& pWriteRequest)
	{
		RequestHeader     * requestHeader = RequestHeader ::fromCtoCpp(pStatus, pRequestHeader) ;
		TableWriteValue   * nodesToWrite  = TableWriteValue::fromCtoCpp(pStatus, pWriteRequest.NoOfNodesToWrite,pWriteRequest.NodesToWrite) ;

		if (*pStatus == STATUS_OK)
			return
					new WriteRequest(
							requestHeader,
							nodesToWrite
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (nodesToWrite != NULL)
			nodesToWrite->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_WriteRequest& pWriteRequest) const
	{
		requestHeader ->fromCpptoC (pStatus, pRequestHeader) ;
		nodesToWrite  ->fromCpptoC(pStatus, pWriteRequest.NoOfNodesToWrite,pWriteRequest.NodesToWrite) ;
	}

public:

	WriteRequest(
			RequestHeader     * _requestHeader,
			TableWriteValue   * _nodesToWrite
			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(nodesToWrite  = _nodesToWrite)  ->take() ;
	}

	virtual ~WriteRequest()
	{
		requestHeader ->release() ;
		nodesToWrite  ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableWriteValue   * getNodesToWrite()
	{
		return nodesToWrite ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_WRITEREQUEST_H_ */
