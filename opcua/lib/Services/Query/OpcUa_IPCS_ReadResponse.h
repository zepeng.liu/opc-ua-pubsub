
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_READRESPONSE_H_
#define OPCUA_READRESPONSE_H_

/*
 * Part 4, 5.10.2, p. 52
 */

#include "../../OpcUa.h"

#if (WITH_READ == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ReadResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ReadResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ReadResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableDataValue        * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static ReadResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_ReadResponse const& pReadResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader      ::fromCtoCpp (pStatus, pResponseHeader) ;
		TableDataValue        * results         = TableDataValue      ::fromCtoCpp (pStatus, pReadResponse.NoOfResults,         pReadResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo ::fromCtoCpp (pStatus, pReadResponse.NoOfDiagnosticInfos, pReadResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new ReadResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_ReadResponse& pReadResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pReadResponse.NoOfResults,         pReadResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pReadResponse.NoOfDiagnosticInfos, pReadResponse.DiagnosticInfos) ;
	}

public:

	ReadResponse(
			ResponseHeader        * _responseHeader,
			TableDataValue        * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~ReadResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableDataValue   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfo()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_READRESPONSE_H_ */
