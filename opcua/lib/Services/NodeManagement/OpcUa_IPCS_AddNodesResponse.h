
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDNODESRESPONSE_H_
#define OPCUA_ADDNODESRESPONSE_H_

/*
 * Part 4, 5.7.2.2, p. 32, table 18
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NodeManagement/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL AddNodesResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddNodesResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_AddNodesResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableAddNodesResult   * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static AddNodesResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_AddNodesResponse const& pAddNodesResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp     (pStatus, pResponseHeader) ;
		TableAddNodesResult   * results         = TableAddNodesResult::fromCtoCpp (pStatus, pAddNodesResponse.NoOfResults,         pAddNodesResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pAddNodesResponse.NoOfDiagnosticInfos, pAddNodesResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new AddNodesResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_AddNodesResponse& pAddNodesResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pAddNodesResponse.NoOfResults,         pAddNodesResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pAddNodesResponse.NoOfDiagnosticInfos, pAddNodesResponse.DiagnosticInfos) ;
	}

public:

	AddNodesResponse(
			ResponseHeader        * _responseHeader,
			TableAddNodesResult   * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~AddNodesResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableAddNodesResult   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfo()
	{
		return diagnosticInfos ;
	}

};
}      /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDNODESRESPONSE_H_ */
