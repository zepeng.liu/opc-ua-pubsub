/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_AddReferencesRequest.h"
#include "OpcUa_IPCS_AddReferencesResponse.h"
#include "OpcUa_IPCS_AddReferences.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_AddReferences(
		uint32_t					  secureChannelIdNum,
		OpcUa_RequestHeader         * pRequestHeader,
		OpcUa_AddReferencesRequest  * pRequest,
		OpcUa_ResponseHeader        * pResponseHeader,
		OpcUa_AddReferencesResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	AddReferencesRequest * request  = NULL ;
	request = AddReferencesRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	AddReferencesResponse * response = NULL ;
	AddReferences::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void AddReferences::answer(
		SOPC_StatusCode              * pStatus,
		uint32_t					   secureChannelIdNum,
	    class AddReferencesRequest   * request,
	    class AddReferencesResponse ** pResponse)
{
	AddressSpace  * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"AddReferences","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;


	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"AddReferences","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"AddReferences","Request : getting parameters.") ;

	TableAddReferencesItem   * referencesToAdd = request->getReferencesToAdd() ;

	debug(COM_DBG,"AddReferences","Request : preparing results.") ;

	int32_t length = referencesToAdd->getLength() ;

	TableStatusCode       * results         = new TableStatusCode  (length) ;

	debug_i(COM_DBG,"AddReferences","Request : getting %d results.",length) ;

	for (int i = 0 ; i < length ; i++) {

		debug_i(COM_DBG,"AddReferences","Results begins (%d)",i) ;

		AddReferencesItem * addReferencesItem = referencesToAdd->get(i) ;

		debug(COM_DBG,"AddReferences","Get sourceNodeId") ;

		NodeId * sourecNodeId = addReferencesItem->getSourceNodeId() ;

		Base * sourceBase = addressSpace->get(sourecNodeId) ;

		if (sourceBase == NULL) {
			debug(COM_ERR,"AddReferences","parentNodeId does not exist") ;
			results         ->set(i, StatusCode::Bad_SourceNodeIdInvalid) ;
			continue ;
		}

		/* sourceBase ok ! */

		NodeId * referenceTypeId = addReferencesItem->getReferenceTypeId() ;

		if (referenceTypeId->getNamespaceIndex()->isNotZero()) {
			debug(COM_ERR,"AddReferences","referenceTypeId not local (namespaceIndex != 0)") ;
			results         ->set(i, StatusCode::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		if (referenceTypeId->getIdentifierType()->get() != IdType_STRING_1) {
			debug(COM_ERR,"AddReferences","referenceTypeId not a string (identifierType != STRING_1)") ;
			results         ->set(i, StatusCode::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		String * referenceString = referenceTypeId->getString() ;

		const char * const reference_str = referenceString->get() ;

		uint32_t reference_code = ReferenceLink::getCode(reference_str) ;

		if (reference_code == 0) {
			debug(COM_ERR,"AddReferences","referenceTypeId not recognized (see ReferenceLink)") ;
			results         ->set(i, StatusCode::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		/* NOT CHECKED : Is the reference authorized by the data model ? */

		/* reference_code ok ! */

		Boolean * isForward = addReferencesItem->getIsForward() ;

		bool _isForward = isForward->get() ;

		/* _isForward ok ! */

		String * targetServerUri = addReferencesItem->getTargetServerUri() ;

		if (targetServerUri->isNotNullOrEmpty()) {
			debug(COM_ERR,"AddReferences","not implemented : targetServerUri not NULL") ;
			results         ->set(i, StatusCode::Bad_NotImplemented) ;
			continue ;
		}


		debug(COM_DBG,"AddReferences","Get parentNodeId") ;

		ExpandedNodeId * targetNodeId = addReferencesItem->getTargetNodeId() ;

		if (targetNodeId->getNamespaceUri()->isNotNullOrEmpty()) {
			debug(COM_ERR,"AddReferences","Not implemented targetNodeId with namespaceUri set") ;
			results         ->set(i, StatusCode::Bad_NotImplemented) ;
			continue ;
		}

		if (targetNodeId->getServerIndex()->isNotZero()) {
			debug(COM_ERR,"AddReferences","Not implemented targetNodeId with serverIndex not zero") ;
			results         ->set(i, StatusCode::Bad_NotImplemented) ;
			continue ;
		}

		NodeId * trueTargetNodeId = targetNodeId->getNodeId() ;

		Base * targetBase = addressSpace->get(trueTargetNodeId) ;

		if (targetBase == NULL) {
			debug(COM_ERR,"AddReferences","targetNodeId does not exist") ;
			results         ->set(i, StatusCode::Bad_TargetNodeIdInvalid) ;
			continue ;
		}

		/* targetBase ok ! */

		NodeClass * targetNodeClass = addReferencesItem->getTargetNodeClass() ;

		if (! targetBase->hasNodeClass(targetNodeClass)) {
			debug(COM_ERR,"AddReferences","targetNode does not have the good targetNodeClass") ;
			results         ->set(i, StatusCode::Bad_NodeClassInvalid) ;
			continue ;
		}

		debug(COM_DBG,"AddReferences","Creating reference") ;

		if (_isForward) {
			new ReferenceLink(reference_code,sourceBase,targetBase) ;
		} else {
			new ReferenceLink(reference_code,targetBase,sourceBase) ;
		}

	}

	* pResponse =
			new AddReferencesResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}

} /* namespace opcua */

#endif
