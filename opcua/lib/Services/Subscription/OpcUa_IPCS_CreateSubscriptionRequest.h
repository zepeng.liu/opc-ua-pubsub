/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CREATESUBSCRIPTIONREQUEST_H_
#define OPCUA_CREATESUBSCRIPTIONREQUEST_H_

/*
 * Part 4, 5.13.2, p. 81
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CreateSubscriptionRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CreateSubscriptionRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CreateSubscriptionRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	Duration      * requestedPublishingInterval ;
	Counter       * requestedLifetimeCount ;
	Counter       * requestedMaxKeepAliveCount ;
	Counter       * maxNotificationsPerPublish ;

	Boolean	      * publishingEnabled ;
	Byte          * priority ;

public:

	static CreateSubscriptionRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_CreateSubscriptionRequest const& pCreateSubscriptionRequest)
	{
		RequestHeader * requestHeader               = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		Duration      * requestedPublishingInterval = Duration      ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.RequestedPublishingInterval) ;
		Counter       * requestedLifetimeCount      = Counter       ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.RequestedLifetimeCount) ;
		Counter       * requestedMaxKeepAliveCount  = Counter       ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.RequestedMaxKeepAliveCount) ;
		Counter       * maxNotificationsPerPublish  = Counter       ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.MaxNotificationsPerPublish) ;

		Boolean       * publishingEnabled           = Boolean       ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.PublishingEnabled) ;
		Byte          * priority                    = Byte          ::fromCtoCpp( pStatus, pCreateSubscriptionRequest.Priority) ;

		if (*pStatus == STATUS_OK)
			return
					new CreateSubscriptionRequest(
							requestHeader,
							requestedPublishingInterval,
							requestedLifetimeCount,
							requestedMaxKeepAliveCount,
							maxNotificationsPerPublish,

							publishingEnabled,
							priority
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (requestedPublishingInterval != NULL)
			requestedPublishingInterval->checkRefCount() ;
		if (requestedLifetimeCount != NULL)
			requestedLifetimeCount->checkRefCount() ;
		if (requestedMaxKeepAliveCount != NULL)
			requestedMaxKeepAliveCount->checkRefCount() ;
		if (maxNotificationsPerPublish != NULL)
			maxNotificationsPerPublish->checkRefCount() ;

		if (publishingEnabled != NULL)
			publishingEnabled->checkRefCount() ;
		if (priority != NULL)
			priority->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_CreateSubscriptionRequest& pCreateSubscriptionRequest) const
	{
		requestHeader               ->fromCpptoC (pStatus, pRequestHeader) ;
		requestedPublishingInterval ->fromCpptoC (pStatus, pCreateSubscriptionRequest.RequestedPublishingInterval) ;
		requestedLifetimeCount      ->fromCpptoC (pStatus, pCreateSubscriptionRequest.RequestedLifetimeCount) ;
		requestedMaxKeepAliveCount  ->fromCpptoC (pStatus, pCreateSubscriptionRequest.RequestedMaxKeepAliveCount) ;
		maxNotificationsPerPublish  ->fromCpptoC (pStatus, pCreateSubscriptionRequest.MaxNotificationsPerPublish) ;

		publishingEnabled           ->fromCpptoC (pStatus, pCreateSubscriptionRequest.PublishingEnabled) ;
		priority                    ->fromCpptoC (pStatus, pCreateSubscriptionRequest.Priority) ;
}

public:

	CreateSubscriptionRequest(
			RequestHeader * _requestHeader,
			Duration      * _requestedPublishingInterval,
			Counter       * _requestedLifetimeCount,
			Counter       * _requestedMaxKeepAliveCount,
			Counter       * _maxNotificationsPerPublish,
			Boolean	      * _publishingEnabled,
			Byte          * _priority
			)
		: Structure()
	{
		(requestHeader               = _requestHeader)               ->take() ;
		(requestedPublishingInterval = _requestedPublishingInterval) ->take() ;
		(requestedLifetimeCount      = _requestedLifetimeCount)      ->take() ;
		(requestedMaxKeepAliveCount  = _requestedMaxKeepAliveCount)  ->take() ;
		(maxNotificationsPerPublish  = _maxNotificationsPerPublish)  ->take() ;
		(publishingEnabled           = _publishingEnabled)           ->take() ;
		(priority                    = _priority)                    ->take() ;
	}

	virtual ~CreateSubscriptionRequest()
	{
		requestHeader               ->release() ;
		requestedPublishingInterval ->release() ;
		requestedLifetimeCount      ->release() ;
		requestedMaxKeepAliveCount  ->release() ;
		maxNotificationsPerPublish  ->release() ;
		publishingEnabled           ->release() ;
		priority                    ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline Duration * getRequestedPublishingInterval()
	{
		return requestedPublishingInterval ;
	}

	inline Counter * getRequestedLifetimeCount()
	{
		return requestedLifetimeCount ;
	}

	inline Counter * getRequestedMaxKeepAliveCount()
	{
		return requestedMaxKeepAliveCount ;
	}

	inline Counter * getMaxNotificationsPerPublish()
	{
		return maxNotificationsPerPublish ;
	}

	inline Boolean * getPublishingEnabled()
	{
		return publishingEnabled ;
	}

	inline Byte * getPriority()
	{
		return priority ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_CREATESUBSCRIPTIONREQUEST_H_ */
