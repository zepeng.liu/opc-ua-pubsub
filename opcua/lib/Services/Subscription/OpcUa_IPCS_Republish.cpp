/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/All.h"

#include "OpcUa_IPCS_RepublishRequest.h"
#include "OpcUa_IPCS_RepublishResponse.h"
#include "OpcUa_IPCS_Republish.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Republish(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_RepublishRequest     * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_RepublishResponse    * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	RepublishRequest * request  = NULL ;
	request = RepublishRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	RepublishResponse * response = NULL ;
	Republish::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void Republish::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class RepublishRequest      * request,
	    class RepublishResponse    ** pResponse)
{
	debug(MAIN_LIFE_DBG,"Republish","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Republish","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"Republish","Request : continue Request decoding.") ;

	int32_t subscriptionId            = request->getSubscriptionId()->get() ;
	uint32_t retransmitSequenceNumber = request->getRetransmitSequenceNumber()->get() ;

	NotificationMessage * notificationMessage = NULL ;

	SubscriptionsTable::subscriptionsLock() ;

	Subscription * subscription = SubscriptionsTable::subscriptionsTable->getSubscription(subscriptionId) ;

	if (subscription != NULL)
		notificationMessage = subscription->onReceiveRepublishRequest(retransmitSequenceNumber) ;

	SubscriptionsTable::subscriptionsUnlock() ;

	if (subscription == NULL) {
		*pStatus = _Bad_SubscriptionIdInvalid ;
		return ;
	}

	if (notificationMessage == NULL) {
		*pStatus = _Bad_MessageNotAvailable ;
		return ;
	}

	* pResponse =
			new RepublishResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					notificationMessage
			);
}

} /* namespace opcua */

#endif
