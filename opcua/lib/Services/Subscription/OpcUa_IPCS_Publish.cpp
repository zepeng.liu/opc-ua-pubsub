/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/All.h"

#include "OpcUa_IPCS_PublishRequest.h"
#include "OpcUa_IPCS_PublishResponse.h"
#include "OpcUa_IPCS_Publish.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Publish(
		uint32_t					 secureChannelIdNum,
		uint32_t				     requestId,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_PublishRequest       * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_PublishResponse      * pResponse,
		bool                       * pAnswered)
{
	SOPC_StatusCode status = STATUS_OK ;

	PublishRequest * request  = NULL ;
	request = PublishRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	PublishResponse * response = NULL ;
	Publish::answer(&status,secureChannelIdNum,requestId,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	if (response != NULL) {
		response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
		response->checkRefCount() ;
		*pAnswered = true ;
	} else {
		*pAnswered = false ;
	}

	return status ;
}

MYDLL void Publish::answer(
		SOPC_StatusCode              * pStatus,
		uint32_t					   secureChannelIdNum,
		uint32_t				       requestId,
	    class PublishRequest         * request,
	    class PublishResponse       ** pResponse)
{
	debug(MAIN_LIFE_DBG,"Publish","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Publish","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"Publish","Request : continue Request decoding.") ;

	SubscriptionsTable::subscriptionsLock() ;

	Subscription * subscription = session->getCurrentSubscription() ;

	BaseDataType * result = NULL ;

	if (subscription != NULL)
		result = subscription->onReceivePublishRequest(pStatus, secureChannelIdNum, requestId, request) ;

	SubscriptionsTable::subscriptionsUnlock() ;

	if (subscription == NULL)
		result = Subscription::EnqueuePublishingReq(pStatus, secureChannelIdNum, requestId, request,StatusCode::tableZero, session->getPublishingReqQueue()) ;

	if (result != NULL)
		*pResponse = dynamic_cast<PublishResponse *>(result) ;
}

} /* namespace opcua */

#endif
