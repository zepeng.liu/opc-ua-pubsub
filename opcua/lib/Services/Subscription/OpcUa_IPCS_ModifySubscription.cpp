/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/All.h"

#include "OpcUa_IPCS_ModifySubscriptionRequest.h"
#include "OpcUa_IPCS_ModifySubscriptionResponse.h"
#include "OpcUa_IPCS_ModifySubscription.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_ModifySubscription(
		uint32_t					       secureChannelIdNum,
		OpcUa_RequestHeader              * pRequestHeader,
		OpcUa_ModifySubscriptionRequest  * pRequest,
		OpcUa_ResponseHeader             * pResponseHeader,
		OpcUa_ModifySubscriptionResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	ModifySubscriptionRequest * request  = NULL ;
	request = ModifySubscriptionRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	ModifySubscriptionResponse * response = NULL ;
	ModifySubscription::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void ModifySubscription::answer(
		SOPC_StatusCode                   * pStatus,
		uint32_t					        secureChannelIdNum,
	    class ModifySubscriptionRequest   * request,
	    class ModifySubscriptionResponse ** pResponse)
{
	debug(MAIN_LIFE_DBG,"ModifySubscription","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"ModifySubscription","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"ModifySubscription","Request : continue Request decoding.") ;

	int32_t  subscriptionId              = request->getSubscriptionId()->get() ;
	double   requestedPublishingInterval = request->getRequestedPublishingInterval()->get() ;
	uint32_t requestedLifetimeCount      = request->getRequestedLifetimeCount()->get() ;
	uint32_t requestedMaxKeepAliveCount  = request->getRequestedMaxKeepAliveCount()->get() ;
	uint32_t maxNotificationsPerPublish  = request->getMaxNotificationsPerPublish()->get() ;
	uint8_t  priority                    = request->getPriority()->get() ;

	SubscriptionsTable::subscriptionsLock() ;

	Subscription * subscription = SubscriptionsTable::subscriptionsTable->getSubscription(subscriptionId) ;

	if (subscription != NULL)
		subscription->onReceiveModifySubscriptionRequest(
									&requestedPublishingInterval,
									&requestedLifetimeCount,
									&requestedMaxKeepAliveCount,
									maxNotificationsPerPublish,
									priority
					) ;

	SubscriptionsTable::subscriptionsUnlock() ;

	if (subscription == NULL) {
		*pStatus = _Bad_SubscriptionIdInvalid ;
		return ;
	}

	*pResponse =
			new ModifySubscriptionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					new Duration(requestedPublishingInterval),
					new Counter(requestedLifetimeCount),
					new Counter(maxNotificationsPerPublish)
			);

}

} /* namespace opcua */

#endif
