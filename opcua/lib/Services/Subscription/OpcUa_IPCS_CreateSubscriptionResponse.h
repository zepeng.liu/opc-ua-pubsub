/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CREATESUBSCRIPTIONRESPONSE_H_
#define OPCUA_CREATESUBSCRIPTIONRESPONSE_H_

/*
 * Part 4, 5.13.2.2, p. 81
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CreateSubscriptionResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CreateSubscriptionResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CreateSubscriptionResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;
	IntegerId      * subscriptionId ;
	Duration       * revisedPublishingInterval ;
	Counter        * revisedLifetimeCount ;
	Counter        * revisedMaxKeepAliveCount ;

public:

	static CreateSubscriptionResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_CreateSubscriptionResponse const& pCreateSubscriptionResponse)
	{
		ResponseHeader * responseHeader            = ResponseHeader ::fromCtoCpp (pStatus, pResponseHeader) ;
		IntegerId      * subscriptionId            = IntegerId      ::fromCtoCpp (pStatus, pCreateSubscriptionResponse.SubscriptionId) ;
		Duration       * revisedPublishingInterval = Duration       ::fromCtoCpp (pStatus, pCreateSubscriptionResponse.RevisedPublishingInterval) ;
		Counter        * revisedLifetimeCount      = Counter        ::fromCtoCpp (pStatus, pCreateSubscriptionResponse.RevisedLifetimeCount) ;
		Counter        * revisedMaxKeepAliveCount  = Counter        ::fromCtoCpp (pStatus, pCreateSubscriptionResponse.RevisedMaxKeepAliveCount) ;

		if (*pStatus == STATUS_OK)
			return
					new CreateSubscriptionResponse(
							responseHeader,
							subscriptionId,
							revisedPublishingInterval,
							revisedLifetimeCount,
							revisedMaxKeepAliveCount
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (revisedPublishingInterval != NULL)
			revisedPublishingInterval->checkRefCount() ;
		if (revisedLifetimeCount != NULL)
			revisedLifetimeCount->checkRefCount() ;
		if (revisedMaxKeepAliveCount != NULL)
			revisedMaxKeepAliveCount->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_CreateSubscriptionResponse& pCreateSubscriptionResponse) const
	{
		responseHeader            ->fromCpptoC (pStatus, pResponseHeader) ;
		subscriptionId            ->fromCpptoC (pStatus, pCreateSubscriptionResponse.SubscriptionId) ;
		revisedPublishingInterval ->fromCpptoC (pStatus, pCreateSubscriptionResponse.RevisedPublishingInterval) ;
		revisedLifetimeCount      ->fromCpptoC (pStatus, pCreateSubscriptionResponse.RevisedLifetimeCount) ;
		revisedMaxKeepAliveCount  ->fromCpptoC (pStatus, pCreateSubscriptionResponse.RevisedMaxKeepAliveCount) ;
	}

public:

	CreateSubscriptionResponse(
			ResponseHeader * _responseHeader,
			IntegerId      * _subscriptionId,
			Duration       * _revisedPublishingInterval,
			Counter        * _revisedLifetimeCount,
			Counter        * _revisedMaxKeepAliveCount
			)
		: Structure()
	{
		(responseHeader            = _responseHeader)            ->take() ;
		(subscriptionId            = _subscriptionId)            ->take() ;
		(revisedPublishingInterval = _revisedPublishingInterval) ->take() ;
		(revisedLifetimeCount      = _revisedLifetimeCount)      ->take() ;
		(revisedMaxKeepAliveCount  = _revisedMaxKeepAliveCount)  ->take() ;
	}

	virtual ~CreateSubscriptionResponse()
	{
		responseHeader            ->release() ;
		subscriptionId            ->release() ;
		revisedPublishingInterval ->release() ;
		revisedLifetimeCount      ->release() ;
		revisedMaxKeepAliveCount  ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline Duration * getRevisedPublishingInterval()
	{
		return revisedPublishingInterval ;
	}

	inline Counter * getRevisedLifetimeCount()
	{
		return revisedLifetimeCount ;
	}

	inline Counter * getRevisedMaxKeepAliveCount()
	{
		return revisedMaxKeepAliveCount ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_CREATESUBSCRIPTIONRESPONSE_H_ */
