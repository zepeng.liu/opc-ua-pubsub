/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_TRANSFERSUBSCRIPTIONSRESPONSE_H_
#define OPCUA_TRANSFERSUBSCRIPTIONSRESPONSE_H_

/*
 * Part 4, 5.13.7.2, p. 88
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_TransferResult.h"

namespace opcua {

class MYDLL TransferSubscriptionsResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_TransferSubscriptionsResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_TransferSubscriptionsResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableTransferResult   * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static TransferSubscriptionsResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_TransferSubscriptionsResponse const& pTransferSubscriptionsResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader     ::fromCtoCpp (pStatus, pResponseHeader) ;
		TableTransferResult   * results         = TableTransferResult::fromCtoCpp (pStatus, pTransferSubscriptionsResponse.NoOfResults,         pTransferSubscriptionsResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pTransferSubscriptionsResponse.NoOfDiagnosticInfos, pTransferSubscriptionsResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new TransferSubscriptionsResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_TransferSubscriptionsResponse& pTransferSubscriptionsResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pTransferSubscriptionsResponse.NoOfResults,         pTransferSubscriptionsResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pTransferSubscriptionsResponse.NoOfDiagnosticInfos, pTransferSubscriptionsResponse.DiagnosticInfos) ;
	}

public:

	TransferSubscriptionsResponse(
			ResponseHeader        * _responseHeader,
			TableTransferResult   * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~TransferSubscriptionsResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableTransferResult   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_TRANSFERSUBSCRIPTIONSRESPONSE_H_ */
