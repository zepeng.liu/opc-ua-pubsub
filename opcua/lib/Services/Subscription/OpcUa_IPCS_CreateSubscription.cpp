/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_Subscription.h"
#include "../../NotificationsAndEvents/OpcUa_SubscriptionsTable.h"

#include "OpcUa_IPCS_CreateSubscriptionRequest.h"
#include "OpcUa_IPCS_CreateSubscriptionResponse.h"
#include "OpcUa_IPCS_CreateSubscription.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_CreateSubscription(
		uint32_t					       secureChannelIdNum,
		OpcUa_RequestHeader              * pRequestHeader,
		OpcUa_CreateSubscriptionRequest  * pRequest,
		OpcUa_ResponseHeader             * pResponseHeader,
		OpcUa_CreateSubscriptionResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	CreateSubscriptionRequest * request  = NULL ;
	request = CreateSubscriptionRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	CreateSubscriptionResponse * response = NULL ;
	CreateSubscription::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void CreateSubscription::answer(
		SOPC_StatusCode                   * pStatus,
		uint32_t					        secureChannelIdNum,
	    class CreateSubscriptionRequest   * request,
	    class CreateSubscriptionResponse ** pResponse)
{
	debug(MAIN_LIFE_DBG,"CreateSubscription","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"CreateSubscription","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"CreateSubscription","Request : building Subscription.") ;

	Subscription * subscription = new Subscription(request) ;

	int32_t rc = subscription->isOk() ;

	if (rc != 0) {
		delete subscription ;
		*pStatus = rc ;
		return ;
	}

	SubscriptionsTable::subscriptionsLock() ;

	BaseDataType * result = subscription->onReceiveCreateSubscriptionRequest(request,session) ;

	SubscriptionsTable::subscriptionsTable->putSubscription(subscription) ;

	if (subscription->isClosed()) {
		subscription->UnsetSession() ;
		SubscriptionsTable::subscriptionsTable->removeSubscription(subscription) ;
		delete subscription ;
	}

	SubscriptionsTable::subscriptionsUnlock() ;

	*pResponse = dynamic_cast<CreateSubscriptionResponse *>(result) ;
}

} /* namespace opcua */
#endif
