/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MODIFYSUBSCRIPTIONREQUEST_H_
#define OPCUA_MODIFYSUBSCRIPTIONREQUEST_H_

/*
 * Part 4, 5.13.3.2, p. 83
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ModifySubscriptionRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ModifySubscriptionRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ModifySubscriptionRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	IntegerId     * subscriptionId ;
	Duration      * requestedPublishingInterval ;
	Counter       * requestedLifetimeCount ;
	Counter       * requestedMaxKeepAliveCount ;

	Counter       * maxNotificationsPerPublish ;
	Byte          * priority ;

public:

	static ModifySubscriptionRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_ModifySubscriptionRequest const& pModifySubscriptionRequest)
	{
		RequestHeader * requestHeader               = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		IntegerId     * subscriptionId              = IntegerId     ::fromCtoCpp( pStatus, pModifySubscriptionRequest.SubscriptionId) ;
		Duration      * requestedPublishingInterval = Duration      ::fromCtoCpp( pStatus, pModifySubscriptionRequest.RequestedPublishingInterval) ;
		Counter       * requestedLifetimeCount      = Counter       ::fromCtoCpp( pStatus, pModifySubscriptionRequest.RequestedLifetimeCount) ;
		Counter       * requestedMaxKeepAliveCount  = Counter       ::fromCtoCpp( pStatus, pModifySubscriptionRequest.RequestedMaxKeepAliveCount) ;

		Counter       * maxNotificationsPerPublish  = Counter       ::fromCtoCpp( pStatus, pModifySubscriptionRequest.MaxNotificationsPerPublish) ;
		Byte          * priority                    = Byte          ::fromCtoCpp( pStatus, pModifySubscriptionRequest.Priority) ;

		if (*pStatus == STATUS_OK)
			return
					new ModifySubscriptionRequest(
							requestHeader,
							subscriptionId,
							requestedPublishingInterval,
							requestedLifetimeCount,
							requestedMaxKeepAliveCount,

							maxNotificationsPerPublish,
							priority
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (requestedPublishingInterval != NULL)
			requestedPublishingInterval->checkRefCount() ;
		if (requestedLifetimeCount != NULL)
			requestedLifetimeCount->checkRefCount() ;
		if (requestedMaxKeepAliveCount != NULL)
			requestedMaxKeepAliveCount->checkRefCount() ;

		if (maxNotificationsPerPublish != NULL)
			maxNotificationsPerPublish->checkRefCount() ;
		if (priority != NULL)
			priority->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_ModifySubscriptionRequest& pModifySubscriptionRequest) const
	{
		requestHeader               ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId              ->fromCpptoC (pStatus, pModifySubscriptionRequest.SubscriptionId) ;
		requestedPublishingInterval ->fromCpptoC (pStatus, pModifySubscriptionRequest.RequestedPublishingInterval) ;
		requestedLifetimeCount      ->fromCpptoC (pStatus, pModifySubscriptionRequest.RequestedLifetimeCount) ;
		requestedMaxKeepAliveCount  ->fromCpptoC (pStatus, pModifySubscriptionRequest.RequestedMaxKeepAliveCount) ;

		maxNotificationsPerPublish  ->fromCpptoC (pStatus, pModifySubscriptionRequest.MaxNotificationsPerPublish) ;
		priority                    ->fromCpptoC (pStatus, pModifySubscriptionRequest.Priority) ;
}

public:

	ModifySubscriptionRequest(
			RequestHeader * _requestHeader,
			IntegerId     * _subscriptionId,
			Duration      * _requestedPublishingInterval,
			Counter       * _requestedLifetimeCount,
			Counter       * _requestedMaxKeepAliveCount,
			Counter       * _maxNotificationsPerPublish,
			Byte          * _priority
			)
		: Structure()
	{
		(requestHeader               = _requestHeader)               ->take() ;
		(subscriptionId              = _subscriptionId)              ->take() ;
		(requestedPublishingInterval = _requestedPublishingInterval) ->take() ;
		(requestedLifetimeCount      = _requestedLifetimeCount)      ->take() ;
		(requestedMaxKeepAliveCount  = _requestedMaxKeepAliveCount)  ->take() ;
		(maxNotificationsPerPublish  = _maxNotificationsPerPublish)  ->take() ;
		(priority                    = _priority)                    ->take() ;
	}

	virtual ~ModifySubscriptionRequest()
	{
		requestHeader               ->release() ;
		subscriptionId              ->release() ;
		requestedPublishingInterval ->release() ;
		requestedLifetimeCount      ->release() ;
		requestedMaxKeepAliveCount  ->release() ;
		maxNotificationsPerPublish  ->release() ;
		priority                    ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline Duration * getRequestedPublishingInterval()
	{
		return requestedPublishingInterval ;
	}

	inline Counter * getRequestedLifetimeCount()
	{
		return requestedLifetimeCount ;
	}

	inline Counter * getRequestedMaxKeepAliveCount()
	{
		return requestedMaxKeepAliveCount ;
	}

	inline Counter * getMaxNotificationsPerPublish()
	{
		return maxNotificationsPerPublish ;
	}

	inline Byte * getPriority()
	{
		return priority ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MODIFYSUBSCRIPTIONREQUEST_H_ */
