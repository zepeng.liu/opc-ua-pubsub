/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MODIFYSUBSCRIPTIONRESPONSE_H_
#define OPCUA_MODIFYSUBSCRIPTIONRESPONSE_H_

/*
 * Part 4, 5.13.3.2, p. 83
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ModifySubscriptionResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ModifySubscriptionResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ModifySubscriptionResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;
	Duration       * revisedPublishingInterval ;
	Counter        * revisedLifetimeCount ;
	Counter        * revisedMaxKeepAliveCount ;

public:

	static ModifySubscriptionResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_ModifySubscriptionResponse const& pModifySubscriptionResponse)
	{
		ResponseHeader * responseHeader            = ResponseHeader ::fromCtoCpp (pStatus, pResponseHeader) ;
		Duration       * revisedPublishingInterval = Duration       ::fromCtoCpp (pStatus, pModifySubscriptionResponse.RevisedPublishingInterval) ;
		Counter        * revisedLifetimeCount      = Counter        ::fromCtoCpp (pStatus, pModifySubscriptionResponse.RevisedLifetimeCount) ;
		Counter        * revisedMaxKeepAliveCount  = Counter        ::fromCtoCpp (pStatus, pModifySubscriptionResponse.RevisedMaxKeepAliveCount) ;

		if (*pStatus == STATUS_OK)
			return
					new ModifySubscriptionResponse(
							responseHeader,
							revisedPublishingInterval,
							revisedLifetimeCount,
							revisedMaxKeepAliveCount
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (revisedPublishingInterval != NULL)
			revisedPublishingInterval->checkRefCount() ;
		if (revisedLifetimeCount != NULL)
			revisedLifetimeCount->checkRefCount() ;
		if (revisedMaxKeepAliveCount != NULL)
			revisedMaxKeepAliveCount->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_ModifySubscriptionResponse& pModifySubscriptionResponse) const
	{
		responseHeader            ->fromCpptoC (pStatus, pResponseHeader) ;
		revisedPublishingInterval ->fromCpptoC (pStatus, pModifySubscriptionResponse.RevisedPublishingInterval) ;
		revisedLifetimeCount      ->fromCpptoC (pStatus, pModifySubscriptionResponse.RevisedLifetimeCount) ;
		revisedMaxKeepAliveCount  ->fromCpptoC (pStatus, pModifySubscriptionResponse.RevisedMaxKeepAliveCount) ;
	}

public:

	ModifySubscriptionResponse(
			ResponseHeader * _responseHeader,
			Duration       * _revisedPublishingInterval,
			Counter        * _revisedLifetimeCount,
			Counter        * _revisedMaxKeepAliveCount
			)
		: Structure()
	{
		(responseHeader            = _responseHeader)               ->take() ;
		(revisedPublishingInterval = _revisedPublishingInterval) ->take() ;
		(revisedLifetimeCount      = _revisedLifetimeCount)      ->take() ;
		(revisedMaxKeepAliveCount  = _revisedMaxKeepAliveCount)  ->take() ;
	}

	virtual ~ModifySubscriptionResponse()
	{
		responseHeader            ->release() ;
		revisedPublishingInterval ->release() ;
		revisedLifetimeCount      ->release() ;
		revisedMaxKeepAliveCount  ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline Duration * getRevisedPublishingInterval()
	{
		return revisedPublishingInterval ;
	}

	inline Counter * getRevisedLifetimeCount()
	{
		return revisedLifetimeCount ;
	}

	inline Counter * getRevisedMaxKeepAliveCount()
	{
		return revisedMaxKeepAliveCount ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MODIFYSUBSCRIPTIONRESPONSE_H_ */
