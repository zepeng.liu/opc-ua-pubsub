/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SETPUBLISHINGMODEREQUEST_H_
#define OPCUA_SETPUBLISHINGMODEREQUEST_H_

/*
 * Part 4, 5.13.4.2, p. 84
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL SetPublishingModeRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_SetPublishingModeRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_SetPublishingModeRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader    * requestHeader ;
	Boolean          * publishingEnabled ;
	TableIntegerId   * subscriptionIds ;

public:

	static SetPublishingModeRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_SetPublishingModeRequest const& pSetPublishingModeRequest)
	{
		RequestHeader    * requestHeader     = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		Boolean          * publishingEnabled = Boolean       ::fromCtoCpp( pStatus, pSetPublishingModeRequest.PublishingEnabled) ;
		TableIntegerId   * subscriptionIds   = TableIntegerId::fromCtoCpp  ( pStatus, pSetPublishingModeRequest.NoOfSubscriptionIds, pSetPublishingModeRequest.SubscriptionIds) ;

		if (*pStatus == STATUS_OK)
			return
					new SetPublishingModeRequest(
							requestHeader,
							publishingEnabled,
							subscriptionIds
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (publishingEnabled != NULL)
			publishingEnabled->checkRefCount() ;
		if (subscriptionIds != NULL)
			subscriptionIds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_SetPublishingModeRequest& pSetPublishingModeRequest) const
	{
		requestHeader     ->fromCpptoC (pStatus, pRequestHeader) ;
		publishingEnabled ->fromCpptoC (pStatus, pSetPublishingModeRequest.PublishingEnabled) ;
		subscriptionIds   ->fromCpptoC (pStatus, pSetPublishingModeRequest.NoOfSubscriptionIds, pSetPublishingModeRequest.SubscriptionIds) ;
	}

public:

	SetPublishingModeRequest(
			RequestHeader    * _requestHeader,
			Boolean          * _publishingEnabled,
			TableIntegerId   * _subscriptionIds
			)
		: Structure()
	{
		(requestHeader      = _requestHeader)     ->take() ;
		(publishingEnabled  = _publishingEnabled) ->take() ;
		(subscriptionIds    = _subscriptionIds)   ->take() ;
	}

	virtual ~SetPublishingModeRequest()
	{
		requestHeader     ->release() ;
		publishingEnabled ->release() ;
		subscriptionIds   ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline Boolean * getPublishingEnabled()
	{
		return publishingEnabled ;
	}

	inline TableIntegerId   * getSubscriptionIds()
	{
		return subscriptionIds ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SETPUBLISHINGMODEREQUEST_H_ */
