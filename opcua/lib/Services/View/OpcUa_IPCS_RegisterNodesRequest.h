
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REGISTERNODESREQUEST_H_
#define OPCUA_REGISTERNODESREQUEST_H_

/*
 * Part3, 5.8.5, p. 44
 */

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL RegisterNodesRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RegisterNodesRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_RegisterNodesRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	TableNodeId   * nodesToRegister ;

public:

	static RegisterNodesRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_RegisterNodesRequest const& pRegisterNodesRequest)
	{
		RequestHeader * requestHeader   = RequestHeader ::fromCtoCpp(pStatus, pRequestHeader) ;
		TableNodeId   * nodesToRegister = TableNodeId::fromCtoCpp(pStatus, pRegisterNodesRequest.NoOfNodesToRegister,pRegisterNodesRequest.NodesToRegister) ;

		if (*pStatus == STATUS_OK)
			return
					new RegisterNodesRequest(
							requestHeader,
							nodesToRegister
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (nodesToRegister != NULL)
			nodesToRegister->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_RegisterNodesRequest& pRegisterNodesRequest) const
	{
		requestHeader   ->fromCpptoC (pStatus, pRequestHeader) ;
		nodesToRegister ->fromCpptoC (pStatus, pRegisterNodesRequest.NoOfNodesToRegister,pRegisterNodesRequest.NodesToRegister) ;
	}

public:

	RegisterNodesRequest(
			RequestHeader * _requestHeader,
			TableNodeId   * _nodesToRegister
			)
		: Structure()
	{
		(requestHeader   = _requestHeader)   ->take() ;
		(nodesToRegister = _nodesToRegister) ->take() ;
	}

	virtual ~RegisterNodesRequest()
	{
		requestHeader   ->release() ;
		nodesToRegister ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableNodeId   * getNodesToRegister()
	{
		return nodesToRegister;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_REGISTERNODESREQUEST_H_ */
