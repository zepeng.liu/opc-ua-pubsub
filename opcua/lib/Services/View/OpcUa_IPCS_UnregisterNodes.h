
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UNREGISTERNODES_H_
#define OPCUA_UNREGISTERNODES_H_

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../OpcUa_BaseService.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_UnregisterNodes(
		uint32_t					    secureChannelIdNum,
		OpcUa_RequestHeader           * pRequestHeader,
		OpcUa_UnregisterNodesRequest  * pRequest,
		OpcUa_ResponseHeader          * pResponseHeader,
		OpcUa_UnregisterNodesResponse * pResponse) ;

class MYDLL UnregisterNodes
	: public BaseService
{
public:

	static void answer(
			SOPC_StatusCode                * pStatus,
			uint32_t					     secureChannelIdNum,
		    class UnregisterNodesRequest   * request,
		    class UnregisterNodesResponse ** pResponse) ;

};

} /* namespace opcua */
#endif
#endif /* UNREGISTERNODES_H_ */
