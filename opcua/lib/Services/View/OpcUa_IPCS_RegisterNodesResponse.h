
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REGISTERNODESRESPONSE_H_
#define OPCUA_REGISTERNODESRESPONSE_H_

/*
 * Part3, 5.8.5, p. 44
 */

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL RegisterNodesResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RegisterNodesResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_RegisterNodesResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;
	TableNodeId    * registeredNodeIds ;

public:

	static RegisterNodesResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_RegisterNodesResponse const& pRegisterNodesResponse)
	{
		ResponseHeader * responseHeader    = ResponseHeader ::fromCtoCpp(pStatus, pResponseHeader) ;
		TableNodeId    * registeredNodeIds = TableNodeId::fromCtoCpp(pStatus, pRegisterNodesResponse.NoOfRegisteredNodeIds,pRegisterNodesResponse.RegisteredNodeIds) ;

		if (*pStatus == STATUS_OK)
			return
					new RegisterNodesResponse(
							responseHeader,
							registeredNodeIds
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (registeredNodeIds != NULL)
			registeredNodeIds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_RegisterNodesResponse& pRegisterNodesResponse) const
	{
		responseHeader    ->fromCpptoC (pStatus, pResponseHeader) ;
		registeredNodeIds ->fromCpptoC (pStatus, pRegisterNodesResponse.NoOfRegisteredNodeIds,pRegisterNodesResponse.RegisteredNodeIds) ;
	}

public:

	RegisterNodesResponse(
			ResponseHeader * _responseHeader,
			TableNodeId    * _registeredNodeIds
			)
		: Structure()
	{
		(responseHeader    = _responseHeader)    ->take() ;
		(registeredNodeIds = _registeredNodeIds) ->take() ;
	}

	virtual ~RegisterNodesResponse()
	{
		responseHeader    ->release() ;
		registeredNodeIds ->release() ;
	}

public:

	virtual ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

};
}      /* namespace opcua */
#endif /* #if (WITH_REGISTER_UNREGISTER_NODES == 1) */
#endif /* OPCUA_REGISTERNODESRESPONSE_H_ */
