
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSEREQUEST_H_
#define OPCUA_BROWSEREQUEST_H_

/*
 * Part 4, 5.8.2.2, p. 38-39
 */

#include "../../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL BrowseRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowseRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_BrowseRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader            * requestHeader ;
	ViewDescription          * view ;
	Counter                  * requestedMaxReferencesPerNode ;
	TableBrowseDescription   * nodesToBrowse ;

public:

	static BrowseRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_BrowseRequest const& pBrowseRequest)
	{
		RequestHeader            * requestHeader                 = RequestHeader       ::fromCtoCpp( pStatus, pRequestHeader) ;
		ViewDescription          * view                          = ViewDescription     ::fromCtoCpp( pStatus, pBrowseRequest.View) ;
		Counter                  * requestedMaxReferencesPerNode = Counter             ::fromCtoCpp( pStatus, pBrowseRequest.RequestedMaxReferencesPerNode) ;
		TableBrowseDescription   * nodesToBrowse                 = TableBrowseDescription::fromCtoCpp( pStatus, pBrowseRequest.NoOfNodesToBrowse, pBrowseRequest.NodesToBrowse) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowseRequest(
							requestHeader,
							view,
							requestedMaxReferencesPerNode,
							nodesToBrowse
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (view != NULL)
			view->checkRefCount() ;
		if (requestedMaxReferencesPerNode != NULL)
			requestedMaxReferencesPerNode->checkRefCount() ;
		if (nodesToBrowse != NULL)
			nodesToBrowse->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_BrowseRequest& pBrowseRequest) const
	{
		requestHeader                 ->fromCpptoC (pStatus, pRequestHeader) ;
		view                          ->fromCpptoC (pStatus, pBrowseRequest.View) ;
		requestedMaxReferencesPerNode ->fromCpptoC (pStatus, pBrowseRequest.RequestedMaxReferencesPerNode) ;
		nodesToBrowse                 ->fromCpptoC (pStatus, pBrowseRequest.NoOfNodesToBrowse, pBrowseRequest.NodesToBrowse) ;
	}

public:

	BrowseRequest(
			RequestHeader            * _requestHeader,
			ViewDescription          * _view,
			Counter                  * _requestedMaxReferencesPerNode,
			TableBrowseDescription   * _nodesToBrowse
			)
		: Structure()
	{
		(requestHeader                 = _requestHeader)                 ->take() ;
		(view                          = _view)                          ->take() ;
		(requestedMaxReferencesPerNode = _requestedMaxReferencesPerNode) ->take() ;
		(nodesToBrowse                 = _nodesToBrowse)                 ->take() ;
	}

	virtual ~BrowseRequest()
	{
		requestHeader                 ->release() ;
		view                          ->release() ;
		requestedMaxReferencesPerNode ->release() ;
		nodesToBrowse                 ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline ViewDescription * getView()
	{
		return view ;
	}

	inline Counter * getRequestedMaxReferencesPerNode()
	{
		return requestedMaxReferencesPerNode ;
	}

	inline TableBrowseDescription   * getNodesToBrowse()
	{
		return nodesToBrowse ;
	}

};
} /* namespace opcua */
#endif /* BROWSE */
#endif /* OPCUA_BROWSEREQUEST_H_ */
