/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_CALL == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_CallRequest.h"
#include "OpcUa_IPCS_CallResponse.h"
#include "OpcUa_IPCS_Call.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Call(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_CallRequest          * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_CallResponse         * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	CallRequest * request  = NULL ;
	request = CallRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	CallResponse * response = NULL ;
	Call::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void Call::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class CallRequest           * request,
	    class CallResponse         ** pResponse)
{
	AddressSpace * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"Call","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Call","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(MTH_DBG,"Call","Request : getting parameters.") ;

	TableCallMethodRequest   * methodsToCall = request->getMethodsToCall() ;
	int32_t                    length        = methodsToCall->getLength() ;

	debug(MTH_DBG,"Call","Request : preparing results.") ;

	TableCallMethodResult   * results = new TableCallMethodResult  (length) ;

	debug_i(MTH_DBG,"Call","Request : calling %d methods.",length) ;

	int32_t                 sc ;
	CallMethodRequest     * methodToCall ;
	NodeId                * objectId ;
	Base                  * object ;
	NodeId                * methodId ;
	Base                  * linked ;
	Method                * method ;
	TableVariant          * inputArguments ;
	StatusCode            * statusCode ;
	TableStatusCode       * inputArgumentResults ;
	TableVariant          * outputArguments ;
	method_t              * methodf ;
	int32_t                 rc ;

	for (int i = 0 ; i < length ; i++) {

		sc = _Good ;

		debug_i(MTH_DBG,"Call","Request : calling method n. %d.",i) ;

		methodToCall   = methodsToCall->get(i) ;
		inputArguments = methodToCall->getInputArguments() ;

		statusCode                   = NULL ;
		inputArgumentResults         = NULL ;
		outputArguments              = NULL ;

		objectId = methodToCall->getObjectId() ;
		object   = addressSpace->get(objectId) ;

		debug_p(MTH_DBG,"Call","object of method call is %p.",object) ;

		if (object == NULL) {
			debug_i(COM_ERR,"Call","Error : object of method call %d is NULL.",i) ;
			sc = _Bad_NodeIdUnknown ;
			goto error ;
		}

		switch (object->getNodeClass()->get()) {
		case NodeClass_OBJECT_1:
			debug_i(MTH_DBG,"Call","object of method call %d is an Object.",i) ;
			break ;
		case NodeClass_OBJECT_TYPE_8:
			debug_i(MTH_DBG,"Call","object of method call %d is an ObjectType.",i) ;
			break ;
		default:
			debug_i(COM_ERR,"Call","Error : object of method call %d is not an Object or an ObjectType.",i) ;
			sc = _Bad_NodeIdInvalid ;
			goto error ;
		}

		methodId = methodToCall->getMethodId() ;

		linked = addressSpace->get(methodId) ;

		debug_p(MTH_DBG,"Call","method of method call is %p.",linked) ;

		if (linked == NULL) {
			debug_i(COM_ERR,"Call","Error : method of method call %d is NULL.",i) ;
			sc = _Bad_NodeIdUnknown ;
			goto error ;
		}

		switch (linked->getNodeClass()->get()) {
		case NodeClass_METHOD_4:
			debug_i(MTH_DBG,"Call","method of method call %d is a Method.",i) ;
			break ;
		default:
			debug_i(COM_ERR,"Call","Error : method of method call %d is not a Method.",i) ;
			sc = _Bad_NodeIdInvalid ;
			goto error ;
		}

		method = dynamic_cast<Method *>(linked) ;

		if (! object->hasComponent(method)) {
			debug_i(COM_ERR,"Call","Error : method of method call %d is not a HasComponent of object.",i) ;
			sc = _Bad_NodeIdInvalid ;
			goto error ;
		}

		methodf = method->getMethod() ;

		if (methodf == NULL) {
			debug_i(COM_ERR,"Call","Error : C++ function method of method call %d is NULL.",i) ;
			sc = _Bad_NodeIdInvalid ;
			goto error ;
		}

		{
			TableDiagnosticInfo   * inputArgumentDiagnosticInfos = NULL ;
			DiagnosticInfo        * diagnosticInfo               = NULL ;

			rc = (*methodf)(
					objectId,
					methodId,
					inputArguments,
					&statusCode,
					&inputArgumentResults,
					&inputArgumentDiagnosticInfos,
					&outputArguments,
					&diagnosticInfo
					) ;

			if (diagnosticInfo != NULL)
				diagnosticInfo->checkRefCount() ;

			if (inputArgumentDiagnosticInfos != NULL)
				inputArgumentDiagnosticInfos->checkRefCount() ;
		}

		if (rc != (int32_t)0) {
			debug_ii(COM_ERR,"Call","Error : C++ function method of method call %d returns %d.",i,rc) ;
			sc = _Bad_NodeIdInvalid ;
			goto error ;
		}

	error:

		if (sc != _Good)
			debug_i(COM_ERR,"Call","Error : method call %d, false initialization of statusCode.",i) ;

		if (statusCode == NULL) {
			debug_i(MTH_DBG,"Call","method call %d, false initialization of statusCode.",i) ;
			statusCode = new StatusCode(sc) ;
		}

		if (inputArgumentResults == NULL) {
			debug_i(MTH_DBG,"Call","method call %d, false initialization of inputArgumentResults.",i) ;
			int32_t n = inputArguments->getLength() ;
			inputArgumentResults = new TableStatusCode (n) ;
			for (int i = 0 ; i < n ; i++)
				inputArgumentResults->set(i,StatusCode::Bad_NodeIdInvalid) ;
		}

		if (outputArguments == NULL) {
			debug_i(MTH_DBG,"Call","method call %d, false initialization of outputArguments.",i) ;
			outputArguments = new TableVariant  (0) ;
		}

		debug_i(MTH_DBG,"Call","method call %d,setting two arrays.",i) ;

		results->set(i,new CallMethodResult(statusCode,inputArgumentResults,DiagnosticInfo::tableZero,outputArguments)) ;

		debug_i(MTH_DBG,"Call","method call %d, end of call.",i) ;
	}

	debug(MTH_DBG,"Call","Request succeeds : returning Response.") ;

	*pResponse = // Part 4, 5.11.2.2, p. 60
			new CallResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}

} /* namespace opcua */

#endif
