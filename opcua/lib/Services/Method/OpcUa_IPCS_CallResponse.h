/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CALLRESPONSE_H_
#define OPCUA_CALLRESPONSE_H_

/*
 * Part 4, 5.11.2.2, p. 60
 */

#include "../../OpcUa.h"

#if (WITH_CALL == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CallResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CallResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CallResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader          * responseHeader ;
	TableCallMethodResult   * results ;
	TableDiagnosticInfo     * diagnosticInfos ;

public:

	static CallResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_CallResponse const& pCallResponse)
	{
		ResponseHeader          * responseHeader  = ResponseHeader ::fromCtoCpp       (pStatus, pResponseHeader) ;
		TableCallMethodResult   * results         = TableCallMethodResult::fromCtoCpp (pStatus, pCallResponse.NoOfResults,         pCallResponse.Results) ;
		TableDiagnosticInfo     * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp   (pStatus, pCallResponse.NoOfDiagnosticInfos, pCallResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new CallResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_CallResponse& pCallResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pCallResponse.NoOfResults,         pCallResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pCallResponse.NoOfDiagnosticInfos, pCallResponse.DiagnosticInfos) ;
	}

public:

	CallResponse(
			ResponseHeader          * _responseHeader,
			TableCallMethodResult   * _results,
			TableDiagnosticInfo     * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader   = _responseHeader)  ->take() ;
		(results          = _results)         ->take() ;
		(diagnosticInfos  = _diagnosticInfos) ->take() ;
	}

	virtual ~CallResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableCallMethodResult   * getResults()
	{
		return results;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_CALLRESPONSE_H_ */
