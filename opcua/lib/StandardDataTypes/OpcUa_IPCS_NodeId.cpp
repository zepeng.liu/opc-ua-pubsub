
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part3, 8.2, p. 61
 */

#include "OpcUa_IPCS_NodeId.h"

#include "../Server/OpcUa_NodesTable.h"

#include <sopc_builtintypes.h>

namespace opcua {

	NodeId * NodeId::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_NodeId const& pNodeId)
	{
		UInt16 * namespaceIndex = UInt16::fromCtoCpp(pStatus, pNodeId.Namespace) ;
		NodeId * nodeId = NULL ;
		switch (pNodeId.IdentifierType) {
		case 0x00: //IdentifierType_Numeric:
			nodeId = new NodeId(namespaceIndex,UInt32::fromCtoCpp(pStatus, pNodeId.Data.Numeric)) ;
			break ;
		case 0x01: //IdentifierType_String:
			nodeId = new NodeId(namespaceIndex,String::fromCtoCpp(pStatus, pNodeId.Data.String)) ;
			break ;
		case 0x02: //IdentifierType_Guid:
			nodeId = new NodeId(namespaceIndex,Guid::fromCtoCpp(pStatus, *pNodeId.Data.Guid)) ;
			break ;
		case 0x03: //IdentifierType_ByteString:
			nodeId = new NodeId(namespaceIndex,ByteString::fromCtoCpp(pStatus, pNodeId.Data.Bstring)) ;
			break ;
		default:
			*pStatus = _Bad_DecodingError ;
			break ;
		}

		if (*pStatus == STATUS_OK)
			return nodeId ;

		if (namespaceIndex != NULL)
			namespaceIndex->checkRefCount() ;
		if (nodeId != NULL)
			nodeId->checkRefCount() ;

		return NULL ;
	}

	void NodeId::fromCpptoC(SOPC_StatusCode * pStatus, SOPC_NodeId& pNodeId) const
	{
		identifierType->fromCpptoC(pStatus, pNodeId.IdentifierType) ;

		namespaceIndex->fromCpptoC(pStatus, pNodeId.Namespace) ;

		if (*pStatus != STATUS_OK)
			return ;

		switch (pNodeId.IdentifierType) {
		case 0x00: //IdentifierType_Numeric:
			((UInt32 *)identifier)->fromCpptoC(pStatus, pNodeId.Data.Numeric) ;
			break ;
		case 0x01: //IdentifierType_String:
			((String *)identifier)->fromCpptoC(pStatus, pNodeId.Data.String) ;
			break ;
		case 0x02: //IdentifierType_Guid:
			if (pNodeId.Data.Guid == NULL)
				pNodeId.Data.Guid = (SOPC_Guid *)malloc(sizeof(SOPC_Guid)) ;
			((Guid *)identifier)->fromCpptoC(pStatus, *(pNodeId.Data.Guid)) ;
			break ;
		case 0x03: //IdentifierType_ByteString:
			((ByteString *)identifier)->fromCpptoC(pStatus, pNodeId.Data.Bstring) ;
			break ;
		default:
			*pStatus = _Bad_EncodingError ;
			break ;
		}
	}

	NodeId * NodeId::get_NodeId_element(const char * const file_str, node_t * node)
	{
		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		UInt16        * namespaceIndex  = NULL ;
		_IdType         _identifierType = IdType_STRING_1 ;
		char          * _content        = NULL ;

		int nba = roxml_get_attr_nb(node) ;

		for (int j = 0 ; j < nba ; j++) {

			node_t * attr = roxml_get_attr(node,NULL,j) ;
			char   * anam = roxml_get_name(attr,NULL,0) ;

			if (anam != NULL) {

				if (StringUtils::strcmpi(anam,"NamespaceIndex") == 0) {
					if (namespaceIndex != NULL) {
						char * nodeNameStr = roxml_get_name(node,NULL,0) ;
						debug_ss(COM_ERR,"NodeId","Configuration file \"%s\", Error : two NamespaceIndex for \"%s\"",file_str,nodeNameStr) ;
						NodesTable::print(file_str,node,1000) ;
						roxml_release(nodeNameStr) ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL) {
							namespaceIndex = UInt16::get_UInt16(file_str,content) ;
						}
						roxml_release(content) ;
					}
				}
				else
				if (StringUtils::strcmpi(anam,"IdentifierType") == 0) {
					char * content = roxml_get_content(attr,NULL,0,NULL) ;
					if (content != NULL) {
						if (StringUtils::strcmpi(content,"Numeric") == 0) {
							_identifierType = IdType_NUMERIC_0 ;
						} else
						if (StringUtils::strcmpi(content,"String") == 0) {
							_identifierType = IdType_STRING_1 ;
						} else
						if (StringUtils::strcmpi(content,"Guid") == 0) {
							_identifierType = IdType_GUID_2 ;
						} else
						if (StringUtils::strcmpi(content,"Opaque") == 0) {
							_identifierType = IdType_OPAQUE_3 ;
						} else {
							char * nodeNameStr = roxml_get_name(node,NULL,0) ;
							debug_sss(COM_ERR,"NodeId","Configuration file \"%s\", Error : bad IdentifierType for \"%s\" = \"%s\"",file_str,nodeNameStr,content) ;
							NodesTable::print(file_str,node,1000) ;
							roxml_release(nodeNameStr) ;
						}
						roxml_release(content) ;
					}
				}
				else
				if (StringUtils::strcmpi(anam,"Value") == 0) {
					if (_content != NULL) {
						char * nodeNameStr = roxml_get_name(node,NULL,0) ;
						debug_ss(COM_ERR,"NodeId","Configuration file \"%s\", Error : two Value for \"%s\"",file_str,nodeNameStr) ;
						NodesTable::print(file_str,node,1000) ;
						roxml_release(nodeNameStr) ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL) {
							_content = new char[strlen(content) + 1] ;
							strcpy(_content,content) ;
						}
						roxml_release(content) ;
					}
				}
				else {
					char * nodeNameStr = roxml_get_name(node,NULL,0) ;
					debug_sss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : unknown attribute \"%s\" for \"%s\"",file_str,anam,nodeNameStr) ;
					NodesTable::print(file_str,node,1000) ;
					roxml_release(nodeNameStr) ;
				}

				roxml_release(anam) ;

			} else {
				char * nodeNameStr = roxml_get_name(node,NULL,0) ;
				debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : unknown attribute for \"%s\"",file_str,nodeNameStr) ;
				NodesTable::print(file_str,node,1000) ;
				roxml_release(nodeNameStr) ;
			}
		}

		if (_content == NULL) {
			char * nodeNameStr = roxml_get_name(node,NULL,0) ;
			debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : no Value attribute for \"%s\"",file_str,nodeNameStr) ;
			NodesTable::print(file_str,node,1000) ;
			roxml_release(nodeNameStr) ;
			return NULL ;
		}

		if (namespaceIndex == NULL)
			namespaceIndex = UInt16::zero ;

		NodeId * result ;

		switch (_identifierType)
		{
		case IdType_NUMERIC_0: {
			UInt32 * identifier = UInt32::get_UInt32(file_str,_content) ;
			if (identifier == NULL) {
				result = NULL ;
			} else {
				result = new NodeId(namespaceIndex,identifier) ;
			}
		} break ;
		case IdType_STRING_1:
			result = new NodeId(namespaceIndex,new String(_content)) ;
			break ;
		case IdType_GUID_2:
			result = NULL ; // for compiler
			CHECK_INT(0) ;
			break ;
		case IdType_OPAQUE_3:
			result = NULL ; // for compiler
			CHECK_INT(0) ;
			break ;
		default:
			result = NULL ; // for compiler
			CHECK_INT(0) ;
			break ;
		}

		delete [] _content ;

		return result ;

	}

}



