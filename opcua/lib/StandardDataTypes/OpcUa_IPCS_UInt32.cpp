
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA 3, 8.36, p. 67
 */

#include "OpcUa_IPCS_UInt32.h"
#include "OpcUa_IPCS_Boolean.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {


TableUInt32   * UInt32::get_UInt32Array_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * arrayLengthStr = BaseDataType::get_Attribute("ArrayLength",node) ;

	if (arrayLengthStr == NULL) {
		char * nodeStr = roxml_get_name(node,NULL,0) ;
		debug_ss(COM_ERR,"UInt32","File \"%s\", No ArrayLength \"%s\"",file_str,nodeStr) ;
		NodesTable::print(file_str,node,1000) ;
		roxml_release(nodeStr) ;
		return NULL ;
	}

	UInt32 * arrayLengthObj = UInt32::get_UInt32(file_str, arrayLengthStr) ;

	if (arrayLengthObj == NULL) {
		char * nodeStr = roxml_get_name(node,NULL,0) ;
		debug_sss(COM_ERR,"UInt32","File \"%s\", ArrayLength reading error \"%s\".\"%s\"",file_str,nodeStr,arrayLengthStr) ;
		NodesTable::print(file_str,node,1000) ;
		roxml_release(arrayLengthStr) ;
		roxml_release(nodeStr) ;
		return NULL ;
	}

	roxml_release(arrayLengthStr) ;

	int arrayLength = arrayLengthObj->get() ;
	delete arrayLengthObj ;

	int nb = roxml_get_chld_nb(node) ;

	if (arrayLength > 0 && nb == 0) {
		char * nodeStr = roxml_get_name(node,NULL,0) ;
		debug_ss(COM_ERR,"UInt32","File \"%s\", No initialiser for array \"%s\"",file_str,nodeStr) ;
		NodesTable::print(file_str,node,1000) ;
		roxml_release(nodeStr) ;
		return NULL ;
	}

	int i = 0 ;
	int j = 0 ;

	TableUInt32   * table = new TableUInt32  (arrayLength) ;

	while (i < arrayLength && j < nb) {
		table->set(i,UInt32::get_UInt32_element(file_str,roxml_get_chld(node,NULL,j))) ;
		i++ ; j++ ;
	}

	while (i< arrayLength) {
		table->set(i,table->get(i-1)) ;
		i++ ;
	}

	return table ;
}

UInt32 * UInt32::get_UInt32_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		return UInt32::zero ;
	}

	UInt32 * result = get_UInt32(file_str,value) ;
	roxml_release(value) ;
	return result ;
}

UInt32 * UInt32::get_UInt32(const char * const file_str, const char * const content)
{
	if (content == NULL) {
		debug_s(COM_ERR,"NodesTable","File \"%s\", UInt32 reading error (NULL value)",file_str) ;
		return NULL ;
	}

	errno = 0 ;

	uint64_t value = strtoull(content,(char **)NULL,0) ;

	if (errno != 0) {
		debug_ss(COM_ERR,"NodesTable","File \"%s\", UInt32 reading error (not an int) \"%s\"",file_str,content) ;
		errno = 0 ;
		return NULL ;
	}

	if (value <= static_cast<uint64_t>(UINT32_MAX)) {
		debug_ss(XML_DBG,"NodesTable","get UInt32 in file \"%s\", value is \"%s\"",file_str,content) ;
		return new UInt32(static_cast<uint32_t>(value)) ;
	}

	debug_ss(COM_ERR,"NodesTable","File \"%s\", UInt32 reading error (not a UInt32) \"%s\"",file_str,content) ;
	return NULL ;
}


UInt32 * UInt32::get_WM_element(const char * const write_mask, String * browseName, const char * const file_str, node_t * node)
{
	debug_sss(XML_DBG,"NodesTable","Configuration file \"%s\", DataType = \"%s\", %s",file_str,browseName->get(),write_mask) ;

	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	Boolean  * accessLevelBit             = NULL ; /*  0 */
	Boolean  * arrayDimensionsBit         = NULL ; /*  1 */
	Boolean  * browseNameBit              = NULL ; /*  2 */
	Boolean  * containsNoLoopBit          = NULL ; /*  3 */
	Boolean  * dataTypeBit                = NULL ; /*  4 */
	Boolean  * descriptionBit             = NULL ; /*  5 */
	Boolean  * displayNameBit             = NULL ; /*  6 */
	Boolean  * eventNotifierBit           = NULL ; /*  7 */
	Boolean  * executableBit              = NULL ; /*  8 */
	Boolean  * historizingBit             = NULL ; /*  9 */
	Boolean  * inverseNameBit             = NULL ; /* 10 */
	Boolean  * isAbstractBit              = NULL ; /* 11 */
	Boolean  * minimumSamplingIntervalBit = NULL ; /* 12 */
	Boolean  * nodeClassBit               = NULL ; /* 13 */
	Boolean  * nodeIdBit                  = NULL ; /* 14 */
	Boolean  * symmetricBit               = NULL ; /* 15 */
	Boolean  * userAccessLevelBit         = NULL ; /* 16 */
	Boolean  * userExecutableBit          = NULL ; /* 17 */
	Boolean  * userWriteMaskBit           = NULL ; /* 18 */
	Boolean  * valueRankBit               = NULL ; /* 19 */
	Boolean  * writeMaskBit               = NULL ; /* 20 */
	Boolean  * valueForVariableTypeBit    = NULL ; /* 21 */


	int nb = roxml_get_chld_nb(node) ;

	for (int i = 0 ; i < nb ; i++) {

		node_t * chld = roxml_get_chld(node,NULL,i) ;
		char *   name = roxml_get_name(chld,NULL,0) ;

		debug_sss(XML_DBG,"NodesTable","Configuration file \"%s\", %s element = \"%s\"",file_str,write_mask,name) ;

		if (StringUtils::strcmpi(name,"AccessLevel") == 0 && accessLevelBit == NULL) { /* 0 */
			(accessLevelBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"ArrayDimensions") == 0 && arrayDimensionsBit == NULL) { /* 1 */
			(arrayDimensionsBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"BrowseName") == 0 && browseNameBit == NULL) { /* 2 */
			(browseNameBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"ContainsNoLoop") == 0 && containsNoLoopBit == NULL) { /* 3 */
			(containsNoLoopBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"DataType") == 0 && dataTypeBit == NULL) { /* 4 */
			(dataTypeBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"Description") == 0 && descriptionBit == NULL) { /* 5 */
			(descriptionBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"DisplayName") == 0 && displayNameBit == NULL) { /* 6 */
			(displayNameBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"EventNotifier") == 0 && eventNotifierBit == NULL) { /* 7 */
			(eventNotifierBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"Executable") == 0 && executableBit == NULL) { /* 8 */
			(executableBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"Historizing") == 0 && historizingBit == NULL) { /* 9 */
			(historizingBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"InverseName") == 0 && inverseNameBit == NULL) { /* 10 */
			(inverseNameBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"IsAbstract") == 0 && isAbstractBit == NULL) { /* 11 */
			(isAbstractBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"MinimumSamplingInterval") == 0 && minimumSamplingIntervalBit == NULL) { /* 12 */
			(minimumSamplingIntervalBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"NodeClass") == 0 && nodeClassBit == NULL) { /* 13  */
			(nodeClassBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"NodeId") == 0 && nodeIdBit == NULL) {  /* 14 */
			(nodeIdBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"SymmetricBit") == 0 && symmetricBit == NULL) { /* 15 */
			(symmetricBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"UserAccessLevel") == 0 && userAccessLevelBit == NULL) { /* 16 */
			(userAccessLevelBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"UserExecutable") == 0 && userExecutableBit == NULL) { /* 17 */
			(userExecutableBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMaskBit == NULL) { /* 18 */
			(userWriteMaskBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"ValueRank") == 0 && valueRankBit == NULL) { /* 19 */
			(valueRankBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMaskBit == NULL) { /* 20 */
			(writeMaskBit = Boolean::booleanTrue)->take() ;
		} else
		if (StringUtils::strcmpi(name,"ValueForVariableType") == 0 && valueForVariableTypeBit == NULL) { /* 21 */
			(valueForVariableTypeBit = Boolean::booleanTrue)->take() ;
		} else {
			debug_ssss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : unknown or duplicate element \"%s.%s\" in \"%s\"",file_str,write_mask,name,browseName->get()) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(name) ;
	}

	UInt32 * result
		= new UInt32(
				accessLevelBit, /* 0 */
				arrayDimensionsBit, /* 1 */
				browseNameBit, /* 2 */
				containsNoLoopBit, /* 3 */
				dataTypeBit, /* 4 */
				descriptionBit, /* 5 */
				displayNameBit, /* 6 */
				eventNotifierBit, /* 7 */
				executableBit, /* 8 */
				historizingBit, /* 9 */
				inverseNameBit, /* 10 */
				isAbstractBit, /* 11 */
				minimumSamplingIntervalBit, /* 12 */
				nodeClassBit, /* 13 */
				nodeIdBit, /* 14 */
				symmetricBit, /* 15 */
				userAccessLevelBit, /* 16 */
				userExecutableBit, /* 17 */
				userWriteMaskBit, /* 18 */
				valueRankBit, /* 19 */
				writeMaskBit, /* 20 */
				valueForVariableTypeBit, /* 21 */
				NULL, NULL, NULL, NULL,  /* 25 */
				NULL, NULL, NULL, NULL, NULL, /* 30 */
				NULL /* 31 */
			) ;

	if (accessLevelBit)
		accessLevelBit->release() ;
	if (arrayDimensionsBit)
		arrayDimensionsBit->release() ;
	if (browseNameBit)
		browseNameBit->release() ;
	if (containsNoLoopBit)
		containsNoLoopBit->release() ;
	if (dataTypeBit)
		dataTypeBit->release() ;

	if (descriptionBit)
		descriptionBit->release() ;
	if (displayNameBit)
		displayNameBit->release() ;
	if (eventNotifierBit)
		eventNotifierBit->release() ;
	if (executableBit)
		executableBit->release() ;
	if (historizingBit)
		historizingBit->release() ;

	if (inverseNameBit)
		inverseNameBit->release() ;
	if (isAbstractBit)
		isAbstractBit->release() ;
	if (minimumSamplingIntervalBit)
		minimumSamplingIntervalBit->release() ;
	if (nodeClassBit)
		nodeClassBit->release() ;
	if (nodeIdBit)
		nodeIdBit->release() ;

	if (symmetricBit)
		symmetricBit->release() ;
	if (userAccessLevelBit)
		userAccessLevelBit->release() ;
	if (userExecutableBit)
		userExecutableBit->release() ;
	if (userWriteMaskBit)
		userWriteMaskBit->release() ;
	if (valueRankBit)
		valueRankBit->release() ;

	if (writeMaskBit)
		writeMaskBit->release() ;
	if (valueForVariableTypeBit)
		valueForVariableTypeBit->release() ;

	return result ;
}

UInt32 * UInt32::get_UserWriteMask_element(String * browseName, const char * const file_str, node_t * node)
{
	return get_WM_element("UserWriteMask",browseName,file_str,node) ;
}

UInt32 * UInt32::get_WriteMask_element(String * browseName, const char * const file_str, node_t * node)
{
	return get_WM_element("WriteMask",browseName,file_str,node) ;
}

}


