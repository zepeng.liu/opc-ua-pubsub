
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_UInt32.h"

#ifndef LIB_STANDARDDATATYPES_OPCUANONCE_H_
#define LIB_STANDARDDATATYPES_OPCUANONCE_H_

namespace opcua {


class MYDLL Nonce
	: public ByteString
{
public:

	static Nonce * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ByteString ; }

public:

	Nonce(int32_t const _length, const uint8_t * const _array)
		: ByteString(_length,_array)
	{}

public:

	static inline Nonce * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ByteString const& pByteString)
	{
		NOT_USED(pStatus) ;
		return new Nonce(pByteString.Length,pByteString.Data) ;
	}

};
} // Namespace
#endif /* LIB_STANDARDDATATYPES_OPCUANONCE_H_ */
