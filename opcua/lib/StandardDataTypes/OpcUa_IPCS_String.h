
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public LicensePRI for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_STRING_H_
#define OPCUA_STRING_H_

/*
 * OPC-UA Part3, 8.32, p.67
 */

#define __STDC_FORMAT_MACROS

#ifndef _WIN32
#include <inttypes.h>
#endif

#include "../OpcUa.h"
#include "../Utils/OpcUa_Alea.h"
#include "../Utils/OpcUa_HashCode.h"
#include "OpcUa_IPCS_Variant.h"
#include "OpcUa_BaseDataType.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include <string.h>
extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL String
		: public BaseDataType
{
public:

	static String * empty ;

	static TableString * tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_String ; }

private:

	char * str ;

public:

	static String * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_String const& pSopcString)
	{
		NOT_USED(pStatus) ;

		if (pSopcString.Length <= 0)
			return empty ;

		char * res_str = new char[pSopcString.Length + 1] ;
		memcpy(res_str,(char *)pSopcString.Data,static_cast<size_t>(pSopcString.Length)) ;
		res_str[pSopcString.Length] = '\0' ;

		return new String(res_str,1) ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_String& pSopcString) const
	{
		NOT_USED(pStatus) ;
		if (str == NULL || *str == '\0') {
			SOPC_String_Initialize(&pSopcString) ;
		} else {
			pSopcString.Length     = strlen(str) ;
			pSopcString.Data       = (SOPC_Byte *)malloc(static_cast<size_t>(pSopcString.Length)) ;
			pSopcString.DoNotClear = 0 ;
			memcpy((char *)pSopcString.Data,str,static_cast<size_t>(pSopcString.Length)) ;
		}
	}

public:

	String(char * const _str, int takeItForYou)
		: BaseDataType()
	{
		str = _str ;
		NOT_USED(takeItForYou) ;
	}

	String(const char * const _str)
		: BaseDataType()
	{
		if (_str == NULL) {
			str = NULL ;
		} else {
			str = new char[strlen(_str)+1] ;
			strcpy(str,_str) ;
		}
	}

	String(int len, const char * const _str)
		: BaseDataType()
	{
		if (len == 0) {
			str = NULL ;
		} else {
			str = new char[len+1] ;
			memcpy(str,_str,len) ;
			str[len] = '\0' ;
		}
	}

	virtual ~String()
	{
		if (str != NULL)
			delete[] str ;
	}

public:

	static String * getRandomString()
	{
		char r[128] ;
#ifdef _WIN32
		sprintf(r,"r%llu",Alea::alea->random_uint64()) ;
#else
#   ifdef __arm__
		sprintf(r,"r%llu",Alea::alea->random_uint64()) ;
#   else
		sprintf(r,"r%llu",(long long unsigned int)Alea::alea->random_uint64()) ;
#   endif
#endif
		return new String(r) ;
	}

public:

	virtual Variant * getVariant() ;

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		if (str == NULL)
			return hash ;
		return HashCode::hashCode(hash,(uint8_t *)str,(int32_t)strlen(str)) ;
	}

	virtual uint64_t hashCode64(uint64_t hash) const
	{
		if (str == NULL)
			return hash ;
		return HashCode::hashCode64(hash,(uint8_t *)str,(int32_t)strlen(str)) ;
	}

public:

	bool equals(const char * other)
	{
		if (str == NULL)
			return (other == NULL) ;

		if (other == NULL)
			return false ;

		return  (strcmp(str,other) == 0) ;
	}

	bool equals(String * other)
	{
		return other->equals(str) ;
	}

public:

	bool beginsWith(const char * beginning)
	{
		if (str == NULL)
			return false ;
		if (beginning == NULL)
			return false ;
		return (strncmp(str,beginning,strlen(beginning)) == 0) ;
	}

public:

	inline int isNullOrEmpty()
	{
		return (str == NULL) || (strlen(str) == 0) ;
	}

	inline int isNotNullOrEmpty()
	{
		return (str != NULL) && (strlen(str) != 0) ;
	}

public:

	inline const char * get() const
	{
		return str ;
	}

public:

	static NodeId * get_NodeId_element(const char * const file_str, node_t * node) ;

	static String * get_String_element(const char * const file_str, node_t * node) ;

	static String * get_File_element(const char * const file_str, node_t * node) ;

};

} /* namespace opcua */
#endif /* OPCUA_STRING_H_ */
