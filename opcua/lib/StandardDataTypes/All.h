
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ALL_STANDARDDATATYPES_H_
#define _ALL_STANDARDDATATYPES_H_

/*
 * All the standard data types.
 * OPC-UA Part 3, 8, p. 61
 */

#include "../OpcUa_BuiltinDefine.h"

#include "OpcUa_IPCS_Argument.h"
#include "OpcUa_BaseDataType.h"
#include "OpcUa_IPCS_Boolean.h"
#include "OpcUa_IPCS_Byte.h"
#include "OpcUa_IPCS_ByteString.h"
#include "OpcUa_IPCS_DateTime.h"
#include "OpcUa_IPCS_Double.h"
#include "OpcUa_IPCS_Duration.h"
#include "OpcUa_Enumeration.h"
#include "OpcUa_IPCS_ExpandedNodeId.h"
#include "OpcUa_IPCS_ExtensionObject.h"
#include "OpcUa_IPCS_Float.h"
#include "OpcUa_IPCS_Guid.h"
#include "OpcUa_IPCS_IdType.h"
#include "OpcUa_Image.h"
#include "OpcUa_ImageBMP.h"
#include "OpcUa_ImageGIF.h"
#include "OpcUa_ImageJPG.h"
#include "OpcUa_ImagePNG.h"
#include "OpcUa_IPCS_Int16.h"
#include "OpcUa_IPCS_Int32.h"
#include "OpcUa_IPCS_Int64.h"
#include "OpcUa_Integer.h"
#include "OpcUa_IPCS_LocaleId.h"
#include "OpcUa_IPCS_LocalizedText.h"
#include "OpcUa_NamingRuleType.h"
#include "OpcUa_IPCS_NodeClass.h"
#include "OpcUa_IPCS_NodeId.h"
#include "OpcUa_IPCS_Nonce.h"
#include "OpcUa_Number.h"
#include "OpcUa_IPCS_QualifiedName.h"
#include "OpcUa_IPCS_SByte.h"
#include "OpcUa_IPCS_String.h"
#include "OpcUa_Structure.h"
#include "OpcUa_TimeZoneDataType.h"
#include "OpcUa_IPCS_UInt16.h"
#include "OpcUa_IPCS_UInt32.h"
#include "OpcUa_IPCS_UInt64.h"
#include "OpcUa_UInteger.h"
#include "OpcUa_IPCS_UtcTime.h"
#include "OpcUa_IPCS_Variant.h"
#include "OpcUa_XmlElement.h"

#endif /* _ALL_STANDARDDATATYPES_H_ */
