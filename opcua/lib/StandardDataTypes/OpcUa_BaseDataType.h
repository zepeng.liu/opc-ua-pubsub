
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_BASEDATATYPE_H_
#define OPCUA_BASEDATATYPE_H_

/*
 * OPC-UA Part 3, 8.7, p. 64
 * Enhanced with reference counting.
 */

#include "../OpcUa.h"
#include "../Utils/OpcUa_RefCount.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class UInt32 ;
class TableUInt32 ;

class MYDLL BaseDataType
		: public RefCount
{
public:

	virtual uint32_t getTypeId() const = 0 ;

	virtual uint32_t getBinaryEncodingId() const
	{
		return (uint32_t) 0 ;
	}

	virtual uint32_t getXmlEncodingId() const
	{
		return (uint32_t) 0 ;
	}

	inline bool hasTypeId(uint32_t typeId)
	{
		return (getTypeId() == typeId) ;
	}

public:

	BaseDataType()
		: RefCount()
	{}

	virtual ~BaseDataType()
	{}

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		return hash ;
	}

public: // Must be redefined in all data that have a RequestHeader

	virtual class RequestHeader  * getRequestHeader()
	{
		return NULL ;
	}

public: // Must be redefined in all data that have a ResponseHeader

	virtual class ResponseHeader * getResponseHeader()
	{
		return NULL ;
	}

public: // Must be redefined in all data that can be variant

	virtual class Variant * getVariant()
	{
		debug(COM_ERR,"BaseDataType","getVariant() is called !") ;
		CHECK_INT(0) ;
		return NULL ;
	}

public: // Can be redefined in all base data. Currently redefined in:
		// Boolean, Byte, Int32, Int64, Float, Double

	virtual class NodeId * getDataType()
	{
		debug(COM_ERR,"BaseDataType","getDataType() is called !") ;
		CHECK_INT(0) ;
		return NULL ;
	}

public:

	inline virtual int32_t getLength() const // redefined in Table*
	{
		return -1 ;
	}

	virtual bool isArray() const // redefined in Table
	{
		return false ;
	}

	inline virtual BaseDataType * getArrayElt(int32_t idx) // redefined in Table
	{
		NOT_USED(idx) ;
		return NULL ;
	}

public:

# ifdef _WIN32
	virtual BaseDataType * takeArray(
			class NumericRange  * indexRange,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions) ;
# else
	virtual BaseDataType * takeArray(
			class NumericRange * indexRange,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions) ;
# endif

# ifdef _WIN32
	virtual void setArray(
			class NumericRange  * indexRange,
			class BaseDataType  * newValue,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions) ;
# else
	virtual void setArray(
			class NumericRange * indexRange,
			class BaseDataType * newValue,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions) ;
# endif

public:

	static char * get_Attribute(const char * attribute, node_t * node) ;

	static BaseDataType * get_BaseDataType_element(class NodeId ** nodeId, const char * const file_str, node_t * node) ;

} ;    /* class */
}      /* namespace opcua */
#endif /* OPCUA_BASEDATATYPE_H_ */
