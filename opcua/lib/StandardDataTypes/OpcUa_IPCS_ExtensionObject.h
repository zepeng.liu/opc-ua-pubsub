
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_EXTENSIONOBJECT_H_
#define OPCUA_EXTENSIONOBJECT_H_

/*
 * OPC-UA Part 6, 5.1.4, p. 8
 *                5.2.2.15, p. 15
 */

#include "../OpcUa.h"
#include "OpcUa_BaseDataType.h"

namespace opcua {

class MYDLL ExtensionObject
		: public BaseDataType
{
public:

	static ExtensionObject * null ;

public:

	virtual uint32_t getTypeId() const { return 0 ; }

private:

	ExpandedNodeId * typeId ;
	Byte           * encoding ; // 0x00 : no body, 0x01 : body as ByteString, 0x02 : body as XmlElement
	ByteString     * body ;

public:

	static inline ExtensionObject * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pExtensionObject)
	{
		ExpandedNodeId * typeId = ExpandedNodeId::fromCtoCpp(pStatus, pExtensionObject.TypeId) ;

		if (*pStatus == STATUS_OK) {
			switch (pExtensionObject.Encoding) {
			case SOPC_ExtObjBodyEncoding_None:
				return new ExtensionObject(typeId,Byte::zero,NULL) ;
			case SOPC_ExtObjBodyEncoding_ByteString:
			{
				ByteString * byteString = ByteString::fromCtoCpp(pStatus, pExtensionObject.Body.Bstring) ;
				if (*pStatus == STATUS_OK)
					return new ExtensionObject(typeId,Byte::zero,byteString) ;
				if (byteString != NULL)
					byteString->checkRefCount() ;
			}
			case SOPC_ExtObjBodyEncoding_XMLElement:
				debug(COM_ERR,"ExtensionObject","fromCtoCpp with encoding=SOPC_ExtObjBodyEncoding_XMLElement") ;
				*pStatus = _Bad_DecodingError ;
				break ;
			case SOPC_ExtObjBodyEncoding_Object:
				debug(COM_ERR,"ExtensionObject","fromCtoCpp with encoding=SOPC_ExtObjBodyEncoding_Object") ;
				*pStatus = _Bad_DecodingError ;
				break ;
			default:
				debug_i(COM_ERR,"ExtensionObject","fromCtoCpp with unknown encoding=%d",pExtensionObject.Encoding) ;
				*pStatus = _Bad_DecodingError ;
				break ;
			}
		}

		if (typeId != NULL)
			typeId->checkRefCount() ;

		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pExtensionObject) const
	{
		typeId   ->fromCpptoC(pStatus, pExtensionObject.TypeId) ;
		encoding ->fromCpptoC(pStatus, pExtensionObject.Encoding) ;

		if (*pStatus == STATUS_OK) {
			switch (pExtensionObject.Encoding) {
			case SOPC_ExtObjBodyEncoding_None:
				pExtensionObject.Length = 0 ;
				return ;
			case SOPC_ExtObjBodyEncoding_ByteString:
				body->fromCpptoC(pStatus, pExtensionObject.Body.Bstring) ;
				pExtensionObject.Length = body->getLen() ;
				return ;
			case SOPC_ExtObjBodyEncoding_XMLElement:
				debug(COM_ERR,"ExtensionObject","fromCtoCpp with encoding=SOPC_ExtObjBodyEncoding_XMLElement") ;
				*pStatus = _Bad_EncodingError ;
				break ;
			case SOPC_ExtObjBodyEncoding_Object:
				debug(COM_ERR,"ExtensionObject","fromCpptoC with encoding=SOPC_ExtObjBodyEncoding_Object") ;
				*pStatus = _Bad_EncodingError ;
				break ;
			default:
				debug_i(COM_ERR,"ExtensionObject","fromCpptoC with unknown encoding=%d",pExtensionObject.Encoding) ;
				*pStatus = _Bad_EncodingError ;
				break ;
			}
		}
	}

public:

	ExtensionObject(
			ExpandedNodeId * _typeId,
			Byte           * _encoding,
			ByteString     * _body
			)
		: BaseDataType()
	{
		(typeId   = _typeId)    ->take() ;
		(encoding = _encoding)  ->take() ;
		if ((body = _body))
			body  ->take() ;
	}

	virtual ~ExtensionObject()
	{
		typeId   ->release() ;
		encoding ->release() ;
		if (body != NULL)
			body ->release() ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_ExtensionObject,this) ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_EXTENSIONOBJECT_H_ */
