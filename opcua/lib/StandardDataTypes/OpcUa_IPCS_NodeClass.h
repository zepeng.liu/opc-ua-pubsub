
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_NODECLASS_H_
#define _OPCUA_NODECLASS_H_

/*
 * OPC Part3, 8.30, p. 66
 * IdType = 257
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"
#include "../CommonParametersTypes/OpcUa_ReadValueIdDefine.h"

namespace opcua {

enum _NodeClass {
	NodeClass_OBJECT_1          =   1,
	NodeClass_VARIABLE_2        =   2,
	NodeClass_METHOD_4          =   4,
	NodeClass_OBJECT_TYPE_8     =   8,
	NodeClass_VARIABLE_TYPE_16  =  16,
	NodeClass_REFERENCE_TYPE_32 =  32,
	NodeClass_DATA_TYPE_64      =  64,
	NodeClass_VIEW_128          = 128
} ;

class MYDLL NodeClass
		: public Enumeration
{
public:

	static NodeClass * object_1 ;
	static NodeClass * variable_2 ;
	static NodeClass * method_4 ;
	static NodeClass * object_type_8 ;
	static NodeClass * variable_type_16 ;
	static NodeClass * reference_type_32 ;
	static NodeClass * data_type_64 ;
	static NodeClass * view_128 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_NodeClass ; }

public:

	static NodeClass * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_NodeClass const& value)
	{
		switch (value) {
		case OpcUa_NodeClass_Unspecified:
		case OpcUa_NodeClass_Object:
		case OpcUa_NodeClass_Variable:
		case OpcUa_NodeClass_Method:
		case OpcUa_NodeClass_ObjectType:
		case OpcUa_NodeClass_VariableType:
		case OpcUa_NodeClass_ReferenceType:
		case OpcUa_NodeClass_DataType:
		case OpcUa_NodeClass_View:
			return new NodeClass((_NodeClass)value) ;
		default:
			break ;
		}
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_NodeClass& pValue) const
	{
		int32_t value = get() ;
		switch (value) {
		case NodeClass_OBJECT_1:
		case NodeClass_VARIABLE_2:
		case NodeClass_METHOD_4:
		case NodeClass_OBJECT_TYPE_8:
		case NodeClass_VARIABLE_TYPE_16:
		case NodeClass_REFERENCE_TYPE_32:
		case NodeClass_DATA_TYPE_64:
		case NodeClass_VIEW_128:
			pValue = (OpcUa_NodeClass)value ;
			break ;
		default:
			*pStatus = _Bad_EncodingError ;
			break ;
		}
	}

private:

	NodeClass(_NodeClass _value)
		: Enumeration(_value)
	{}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Int32,this) ;
	}

};
} /* namespace opcua */
#endif /* _OPCUA_NODECLASS_H_ */
