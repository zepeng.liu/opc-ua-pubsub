
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_FLOAT_H_
#define OPCUA_FLOAT_H_

/*
 * OPC-UA Part 3, 8.15, p. 65
 */

#include "../OpcUa.h"
#include "OpcUa_Number.h"
#include "OpcUa_IPCS_Variant.h"

namespace opcua {

class MYDLL Float
		: public Number
{
public:

	static Float * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Float ; }

private:

	float value ;

public:

	static inline Float * fromCtoCpp(SOPC_StatusCode * pStatus, float const& value)
	{
		NOT_USED(pStatus) ;
		return new Float(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, float& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	Float(float _value)
		: Number(),
		  value(_value)
	{}

	virtual ~Float()
	{}

public:

	inline float get()
	{
		return value ;
	}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Float,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	static Float * get_Float_element(const char * const file_str, node_t * node) ;

	static Float * get_Float(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_FLOAT_H_ */
