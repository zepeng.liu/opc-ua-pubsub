
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPC-UA Part 3, 8.8, p. 64
 */

#include "OpcUa_IPCS_Boolean.h"

#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

	Boolean * Boolean::get_Boolean_element(const char * const file_str, node_t * node)
	{
		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		char * value = BaseDataType::get_Attribute("Value",node) ;

		if (value == NULL) {
			return Boolean::booleanFalse ;
		}

		Boolean * result = get_Boolean(file_str,value) ;
		roxml_release(value) ;
		return result ;
	}


	Boolean * Boolean::get_Boolean(const char * const file_str, const char * const content)
	{
		if (content == NULL) {
			debug_s(COM_ERR,"Boolean","File \"%s\", Boolean reading error (NULL value)",file_str) ;
			return NULL ;
		}

		if (StringUtils::strcmpi(content,"True") == 0)
			return Boolean::booleanTrue ;

		if (StringUtils::strcmpi(content,"False") == 0)
			return Boolean::booleanFalse ;

		debug_ss(COM_ERR,"Boolean","File \"%s\", Boolean reading error (not a Boolean) \"%s\"",file_str,content) ;
		return NULL ;
	}

}
