
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UINT32_H_
#define OPCUA_UINT32_H_

/*
 * OPC-UA 3, 8.36, p. 67
 */

#include "../OpcUa.h"
#include "../Utils/OpcUa_Bits.h"
#include "../Utils/OpcUa_HashCode.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_UInteger.h"
#include "OpcUa_IPCS_Variant.h"
#include "OpcUa_IPCS_Boolean.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL UInt32
		: public UInteger
{
public:

	static UInt32 * zero ;
	static UInt32 * maximum ;

	static TableUInt32   * tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_UInt32 ; }

private:

	uint32_t value ;

public:

	static inline UInt32 * fromCtoCpp(SOPC_StatusCode * pStatus, uint32_t const& value)
	{
		//debug_i(IPCS_DBG,"UInt32","Aller fromCtoCpp: value = %d",value) ;
		NOT_USED(pStatus) ;
		return new UInt32(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, uint32_t& pValue) const
	{
		//debug_i(IPCS_DBG,"UInt32","Retour fromCpptoC: value = %d",value) ;
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	UInt32(uint32_t _value)
		: UInteger(),
		  value(_value)
	{}

	virtual ~UInt32()
	{}

private:

	void setBit(Boolean * bit, int n)
	{
		if (bit != NULL && bit->isSet())
			selfOr(((uint32_t)1) <<  n) ;
	}

public:

	UInt32(
			Boolean * bit00, Boolean * bit01, Boolean * bit02, Boolean * bit03, Boolean * bit04,
			Boolean * bit05, Boolean * bit06, Boolean * bit07, Boolean * bit08, Boolean * bit09,

			Boolean * bit10, Boolean * bit11, Boolean * bit12, Boolean * bit13, Boolean * bit14,
			Boolean * bit15, Boolean * bit16, Boolean * bit17, Boolean * bit18, Boolean * bit19,

			Boolean * bit20, Boolean * bit21, Boolean * bit22, Boolean * bit23, Boolean * bit24,
			Boolean * bit25, Boolean * bit26, Boolean * bit27, Boolean * bit28, Boolean * bit29,

			Boolean * bit30, Boolean * bit31
		)
	{
		value = (uint8_t) 0 ;

		setBit(bit00,  0) ; setBit(bit01,  1) ; setBit(bit02,  2) ; setBit(bit03,  3) ; setBit(bit04,  4) ;
		setBit(bit05,  5) ; setBit(bit06,  6) ; setBit(bit07,  7) ; setBit(bit08,  8) ; setBit(bit09,  9) ;

		setBit(bit10, 10) ; setBit(bit11, 11) ; setBit(bit12, 12) ; setBit(bit13, 13) ; setBit(bit14, 14) ;
		setBit(bit15, 15) ; setBit(bit16, 16) ; setBit(bit17, 17) ; setBit(bit18, 18) ; setBit(bit19, 19) ;

		setBit(bit20, 20) ; setBit(bit21, 21) ; setBit(bit22, 22) ; setBit(bit23, 23) ; setBit(bit24, 24) ;
		setBit(bit25, 25) ; setBit(bit26, 26) ; setBit(bit27, 27) ; setBit(bit28, 28) ; setBit(bit29, 29) ;

		setBit(bit30, 30) ; setBit(bit31, 31) ;
	}


public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		uint32_t regValue = Bits::to_little_endian_uint32(value) ;
		return HashCode::hashCode(hash,(uint8_t *)&regValue,(int32_t)sizeof(uint32_t)) ;
	}

public:

	inline uint32_t get() const
	{
		return value ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_UInt32,this) ;
	}

public:

	inline bool isZero()
	{
		return (value == (uint32_t)0) ;
	}

	inline bool isNotZero()
	{
		return (value != (uint32_t)0) ;
	}

public:

	inline void selfAnd(UInt32 * other)
	{
		selfAnd(other->get()) ;
	}

	inline void selfAnd(uint32_t other)
	{
		value &= other ;
	}

	inline void selfOr(UInt32 * other)
	{
		selfOr(other->get()) ;
	}

	inline void selfOr(uint32_t other)
	{
		value |= other ;
	}

public:

	inline bool less(uint32_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(uint32_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(uint32_t other)
	{
		return (value == other) ;
 	}

	inline bool notEquals(uint32_t other)
	{
		return (value != other) ;
 	}

	inline bool greaterOrEquals(uint32_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(uint32_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(UInt32 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(UInt32 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(UInt32 * other)
	{
		return other->equals(value) ;
 	}

	inline bool notEquals(UInt32 * other)
	{
		return other->notEquals(value) ;
 	}

	inline bool greaterOrEquals(UInt32 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(UInt32 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	inline bool isSet(uint32_t mask)
	{
		return ((value & mask) != 0) ;
	}

	inline bool isUnset(uint32_t mask)
	{
		return ((value & mask) == 0) ;
	}

private:

	static UInt32 * get_WM_element(const char * const write_mask, class String * browseName, const char * const file_str, node_t * node) ;

public:

	static TableUInt32   * get_UInt32Array_element(const char * const file_str, node_t * node) ;

	static UInt32 * get_UInt32_element(const char * const file_str, node_t * node) ;

	static UInt32 * get_UInt32(const char * const file_str, const char * const content) ;

	static UInt32 * get_UserWriteMask_element(class String * browseName, const char * const file_str, node_t * node) ;

	static UInt32 * get_WriteMask_element(class String * browseName, const char * const file_str, node_t * node) ;

};

} /* namespace opcua */
#endif /* OPCUA_UINT32_H_ */
