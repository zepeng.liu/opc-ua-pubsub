
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.9, p. 64
 */

#include "OpcUa_IPCS_Byte.h"
#include "OpcUa_IPCS_Boolean.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

	Byte * Byte::get_Byte_element(const char * const file_str, node_t * node)
	{
		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		char * value = BaseDataType::get_Attribute("Value",node) ;

		if (value == NULL) {
			return Byte::zero ;
		}

		Byte * result = get_Byte(file_str,value) ;
		roxml_release(value) ;
		return result ;
	}

	Byte * Byte::get_Byte(const char * const file_str, const char * const content)
	{
		if (content == NULL) {
			debug_s(COM_ERR,"Byte","File \"%s\", Byte reading error (NULL value)",file_str) ;
			return NULL ;
		}

		errno = 0 ;

		uint64_t value = strtoull(content,(char **)NULL,0) ;

		if (errno != 0) {
			debug_ss(COM_ERR,"Byte","File \"%s\", Byte reading error (not an int) \"%s\"",file_str,content) ;
			errno = 0 ;
			return NULL ;
		}

		if (value <= static_cast<uint64_t>(UINT8_MAX)) {
			debug_ss(XML_DBG,"Byte","get Byte in file \"%s\", value is \"%s\"",file_str,content) ;
			return new Byte(static_cast<uint8_t>(value)) ;
		}

		debug_ss(COM_ERR,"Byte","File \"%s\", Byte reading error (not a Byte) \"%s\"",file_str,content) ;
		return NULL ;
	}


	Byte * Byte::get_UserAccessLevel_element(String * browseName, const char * const file_str, node_t * node)
	{
		debug_ss(XML_DBG,"Byte","Configuration file \"%s\", DataType = \"%s\", AccessLevel",file_str,browseName->get()) ;

		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		Boolean  * currentReadBit  = NULL ;
		Boolean  * currentWriteBit = NULL ;
		Boolean  * historyReadBit  = NULL ;
		Boolean  * historyWriteBit = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Byte","Configuration file \"%s\", AccessLevel element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"CurrentRead") == 0 && currentReadBit == NULL) {
				(currentReadBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"CurrentWrite") == 0 && currentWriteBit == NULL) {
				(currentWriteBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryRead") == 0 && historyReadBit == NULL) {
				(historyReadBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryWrite") == 0 && historyWriteBit == NULL) {
				(historyWriteBit = Boolean::booleanTrue)->take() ;
			} else {
				debug_sss(COM_ERR,"Byte","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s.UserAccessLevel\"",file_str,name,browseName->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		Byte * result
			= new Byte(
				currentReadBit,
				currentWriteBit,
				historyReadBit,
				historyWriteBit,
				NULL,
				NULL,
				NULL,
				NULL
				) ;

		if (currentReadBit)
			currentReadBit->release() ;
		if (currentWriteBit)
			currentWriteBit->release() ;
		if (historyReadBit)
			historyReadBit->release() ;
		if (historyWriteBit)
			historyWriteBit->release() ;

		return result ;
	}

	Byte * Byte::get_AccessLevel_element(String * browseName, const char * const file_str, node_t * node)
	{
		debug_ss(XML_DBG,"Byte","Configuration file \"%s\", DataType = \"%s\", AccessLevel",file_str,browseName->get()) ;

		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		Boolean  * currentReadBit    = NULL ;
		Boolean  * currentWriteBit   = NULL ;
		Boolean  * historyReadBit    = NULL ;
		Boolean  * historyWriteBit   = NULL ;
		Boolean  * semanticChangeBit = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Byte","Configuration file \"%s\", AccessLevel element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"CurrentRead") == 0 && currentReadBit == NULL) {
				(currentReadBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"CurrentWrite") == 0 && currentWriteBit == NULL) {
				(currentWriteBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryRead") == 0 && historyReadBit == NULL) {
				(historyReadBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryWrite") == 0 && historyWriteBit == NULL) {
				(historyWriteBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"SemanticChange") == 0 && semanticChangeBit == NULL) {
				(semanticChangeBit = Boolean::booleanTrue)->take() ;
			} else {
				debug_sss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s.AccessLevel\"",file_str,name,browseName->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		Byte * result
			= new Byte(
				currentReadBit,
				currentWriteBit,
				historyReadBit,
				historyWriteBit,
				semanticChangeBit,
				NULL,
				NULL,
				NULL
				) ;

		if (currentReadBit)
			currentReadBit->release() ;
		if (currentWriteBit)
			currentWriteBit->release() ;
		if (historyReadBit)
			historyReadBit->release() ;
		if (historyWriteBit)
			historyWriteBit->release() ;
		if (semanticChangeBit)
			semanticChangeBit->release() ;

		return result ;
	}

	Byte * Byte::get_EventNotifier_element(const char * const file_str, node_t * node)
	{
		debug_s(XML_DBG,"Byte","Configuration file \"%s\", EventNotifier",file_str) ;

		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		Boolean  * subscribeToEventsBits = NULL ;
		Boolean  * historyReadBit        = NULL ;
		Boolean  * historyWriteBit       = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Byte","Configuration file \"%s\", EventNotifier element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"SubscribeToEvents") == 0 && subscribeToEventsBits == NULL) {
				(subscribeToEventsBits = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryRead") == 0 && historyReadBit == NULL) {
				(historyReadBit = Boolean::booleanTrue)->take() ;
			} else
			if (StringUtils::strcmpi(name,"HistoryWrite") == 0 && historyWriteBit == NULL) {
				(historyWriteBit = Boolean::booleanTrue)->take() ;
			} else {
				debug_ss(COM_ERR,"Byte","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"EventNotifier\"",file_str,name) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		Byte * result
			= new Byte(
				subscribeToEventsBits,
				NULL,
				historyReadBit,
				historyWriteBit,
				NULL,
				NULL,
				NULL,
				NULL
				) ;

		if (subscribeToEventsBits)
			subscribeToEventsBits->release() ;
		if (historyReadBit)
			historyReadBit->release() ;
		if (historyWriteBit)
			historyWriteBit->release() ;

		return result ;
	}

}


