
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_INT16_H_
#define OPCUA_INT16_H_

/*
 * OPC-UA 3, 8.25, p. 66
 */

#include "../OpcUa.h"
#include "OpcUa_Integer.h"
#include "OpcUa_IPCS_Variant.h"

extern "C" {
#include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL Int16
		: public Integer
{
public:

	static Int16 * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Int16 ; }

private:

	int16_t value ;

public:

	static inline Int16 * fromCtoCpp(SOPC_StatusCode * pStatus, int16_t const& value)
	{
		NOT_USED(pStatus) ;
		return new Int16(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, int16_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}
public:

	Int16(int16_t _value)
		: Integer(),
		  value(_value)
	{}

	virtual ~Int16()
	{}

public:

	inline int16_t get()
	{
		return value ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Int16,this) ;
	}

public:

	inline bool less(int16_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(int16_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(int16_t other)
	{
		return (value == other) ;
 	}

	inline bool greaterOrEquals(int16_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(int16_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(Int16 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(Int16 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(Int16 * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(Int16 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(Int16 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static Int16 * get_Int16_element(const char * const file_str, node_t * node) ;

	static Int16 * get_Int16(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_INT16_H_ */
