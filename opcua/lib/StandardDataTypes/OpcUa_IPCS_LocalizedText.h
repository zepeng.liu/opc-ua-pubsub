
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_LOCALIZEDTEXT_H_
#define OPCUA_LOCALIZEDTEXT_H_

/*
 * OPC-UA Part 3, 8.5, p. 63
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_String.h"
#include "OpcUa_IPCS_LocaleId.h"
#include "OpcUa_BaseDataType.h"

namespace opcua {

class MYDLL LocalizedText
		: public BaseDataType
{
public:

	static LocalizedText * null ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_LocalizedText ; }

private:

	String   * text ;
	LocaleId * locale ;

public:

	static LocalizedText * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_LocalizedText const& pLocalizedText)
	{
		String   * text   = String   ::fromCtoCpp(pStatus, pLocalizedText.Text) ;
		LocaleId * locale = LocaleId ::fromCtoCpp(pStatus, pLocalizedText.Locale);

		if (*pStatus == STATUS_OK)
			return
					new LocalizedText(
							text,
							locale
					) ;

		if (text != NULL)
			text->checkRefCount() ;
		if (locale != NULL)
			locale->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_LocalizedText& pLocalizedText) const
	{
		text   ->fromCpptoC(pStatus, pLocalizedText.Text) ;
		locale ->fromCpptoC(pStatus, pLocalizedText.Locale) ;
	}

public:

	LocalizedText(String   * _text,
			      LocaleId * _locale)
		: BaseDataType()
	{
		(text   = _text)   ->take() ;
		(locale = _locale) ->take() ;
	}

	virtual ~LocalizedText()
	{
		text   ->release() ;
		locale ->release() ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_LocalizedText,this) ;
	}

public:

	static LocalizedText * get_LocalizedText_element(const char * const file_str, node_t * node) ;

};

} /* namespace opcua */
#endif /* OPCUA_LOCALIZEDTEXT_H_ */
