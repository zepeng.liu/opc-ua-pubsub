
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_INTEGER_H_
#define OPCUA_INTEGER_H_

/*
 * OPC-UA Part3, 8.24, p. 65
 * IdType = 27
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Number.h"

namespace opcua {

class MYDLL Integer
	: public Number
{
public:

	Integer()
		: Number()
	{}

	virtual ~Integer()
	{}

};

} /* namespace opcua */
#endif /* OPCUA_INTEGER_H_ */
