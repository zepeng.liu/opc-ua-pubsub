
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_UINTEGER_H_
#define _OPCUA_UINTEGER_H_

/*
 * OPC UA Part3, 8.34, p. 67
 * IdType = 28
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Number.h"

namespace opcua {

class MYDLL UInteger
		: public Number
{};

} /* namespace opcua */
#endif /* _OPCUA_UINTEGER_H_ */
