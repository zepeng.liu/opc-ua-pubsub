
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BYTESTRING_H_
#define OPCUA_BYTESTRING_H_

/*
 * OPC UA Part 3, 8.10, p. 64
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_Byte.h"
#include "OpcUa_BaseDataType.h"
#include "../Utils/OpcUa_HashCode.h"

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <typeinfo>
#include <errno.h>

namespace opcua {

class MYDLL ByteString
	: public BaseDataType
{
public:

	static ByteString * empty ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ByteString ; }

private:

	int32_t   length ;
	uint8_t  * array ;

public:

	static inline ByteString * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ByteString const& pByteString)
	{
		NOT_USED(pStatus) ;
		return new ByteString(pByteString.Length,pByteString.Data) ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ByteString& pByteString) const
	{
		if (length <= 0) {
			SOPC_ByteString_Initialize(&pByteString) ;
		} else {
			pByteString.Length = length ;
			pByteString.Data   = (uint8_t *)malloc(length * sizeof(uint8_t)) ;
			memcpy(pByteString.Data,array,static_cast<size_t>(length)) ;
		}

		NOT_USED(pStatus) ;
	}

public:

	ByteString(int32_t const _length, const uint8_t * const _array)
		: BaseDataType()
	{
		if (_length < 0 || _array == NULL) {
			length = 0 ;
			array = NULL ;
			return ;
		}
		length = _length ;
		array  = new uint8_t[_length] ;
		memcpy(array,_array,static_cast<size_t>(_length)) ;
	}

	ByteString(int32_t const _length, uint8_t * const _array, int takeItForYou)
		: BaseDataType()
	{
		if (_length < 0 || _array == NULL) {
			length = 0 ;
			array = NULL ;
			return ;
		}
		length = _length ;
		array  = _array ;
		NOT_USED(takeItForYou) ;
	}

	ByteString(uint32_t value)
		: BaseDataType(),
		  length(8)
	{
		array = new uint8_t[9] ;
		sprintf((char *)array,"%08x",value) ;
	}

	virtual ~ByteString()
	{
		if (array)
			delete[] array ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_ByteString,this) ;
	}

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		if (length == (int32_t)0)
			return hash ;
		return HashCode::hashCode(hash,array,length) ;
	}

public:

	bool equals(ByteString * other)
	{
		int32_t otherLength = other->getLen() ;
		if (length != otherLength)
			return false ;
		return (memcmp(array,other->getArray(),static_cast<size_t>(otherLength)) == 0) ;
	}

public:

	inline int isNullOrEmpty()
	{
		return (length == 0) ;
	}

public:

	inline int32_t getLen() const // attention au faux-ami getLength()
	{
		return length ;
	}

	inline const uint8_t * getArray() const
	{
		return array ;
	}

//public:
//
//	void print_for_debug(const char * str) const
//	{
//		int length = getLen() ;
//		const uint8_t * array = getArray() ;
//		fprintf(stderr,"X DEBUT -----------------------------------------------------------------------------------------\n") ;
//		fprintf(stderr,"Name: %s, length=%d\n",str,length) ;
//		int pos = 0 ;
//		for (int i = 0 ; i < length ; i++) {
//			fprintf(stderr,"0x%02x ",array[i]) ;
//			if (pos++ == 16) {
//				fprintf(stderr,"\n") ;
//				pos = 0 ;
//			}
//		}
//		fprintf(stderr,"\n") ;
//		fprintf(stderr,"X FIN   -----------------------------------------------------------------------------------------\n") ;
//	}

public :

	int get_uint32_x(uint32_t * val)
	{
		if (length < 0)
			return 1 ;

		char * str = new char[length + 1] ;
		memcpy(str,array,static_cast<size_t>(length)) ;
		str[length] = '\0' ;

		errno = 0 ;
		int64_t value = strtoll(str,(char **)NULL,16) ;

		if (errno != 0) {
			delete[] str ;
			return 1 ;
		}

		delete[] str ;

		if (static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
			* val = static_cast<uint32_t>(value) ;
			return 0 ;
		}

		return 2 ;
	}

public:

	static ByteString * get_ByteString_element(const char * const file_str, node_t * node) ;

};
} /* namespace opcua */
#endif /* OPCUA_BYTESTRING_H_ */
