
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_EXPANDEDNODEID_H_
#define OPCUA_EXPANDEDNODEID_H_

/*
 * OPC-UA Part 6, 5.2.2.10, p. 12 (NodeId, Part6, 5.2.2.9, p. 11)
 */

#include "../OpcUa.h"
#include "OpcUa_BaseDataType.h"

namespace opcua {

class MYDLL ExpandedNodeId
		: public BaseDataType
{
public:

	static ExpandedNodeId * const nullExpandedNodeId ;

	bool isNullExpandedNodeId()
	{
		return
				namespaceUri->isNullOrEmpty() &&
				serverIndex->isZero() &&
				nodeId->isNullNodeId() ;
	}

	inline bool isNotNullExpandedNodeId()
	{
		return ! isNullExpandedNodeId() ;
	}

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ExpandedNodeId ; }

private:

	NodeId     * nodeId ;
	String     * namespaceUri ;
	UInt32     * serverIndex ;

public:

	static ExpandedNodeId * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExpandedNodeId const& pExpandedNodeId)
	{
		NodeId     * nodeId       = NodeId::fromCtoCpp(pStatus, pExpandedNodeId.NodeId) ;
		String     * namespaceUri = String::fromCtoCpp(pStatus, pExpandedNodeId.NamespaceUri) ;
		UInt32     * serverIndex  = UInt32::fromCtoCpp(pStatus, pExpandedNodeId.ServerIndex);

		if (*pStatus == STATUS_OK)
			return
					new ExpandedNodeId(
							nodeId,
							namespaceUri,
							serverIndex
							) ;

		if (nodeId != NULL)
			nodeId->checkRefCount() ;
		if (namespaceUri != NULL)
			namespaceUri->checkRefCount() ;
		if (serverIndex != NULL)
			serverIndex->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExpandedNodeId& pExpandedNodeId) const
	{
		nodeId       ->fromCpptoC(pStatus, pExpandedNodeId.NodeId) ;
		namespaceUri ->fromCpptoC(pStatus, pExpandedNodeId.NamespaceUri) ;
		serverIndex  ->fromCpptoC(pStatus, pExpandedNodeId.ServerIndex) ;
	}

private:

	ExpandedNodeId(
			NodeId     * _nodeId,
			String     * _namespaceUri,
			UInt32     * _serverIndex
			)
		: BaseDataType()
	{
		(nodeId       = _nodeId)       ->take() ;
		(namespaceUri = _namespaceUri) ->take() ;
		(serverIndex  = _serverIndex)  ->take() ;
	}

public:

	ExpandedNodeId(
			NodeId     * _nodeId
			)
		: BaseDataType()
	{
		(nodeId       = _nodeId)       ->take() ;
		(namespaceUri = String::empty) ->take() ;
		(serverIndex  = UInt32::zero)  ->take() ;
	}

	virtual ~ExpandedNodeId()
	{
		nodeId       ->release() ;
		namespaceUri ->release() ;
		serverIndex  ->release() ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_ExpandedNodeId,this) ;
	}

public:

	inline NodeId * getNodeId()
	{
		return nodeId ;
	}

	inline String * getNamespaceUri()
	{
		return namespaceUri ;
	}

	inline UInt32 * getServerIndex()
	{
		return serverIndex ;
	}

public:

	String * toString()
	{
		String *  strNodeId = nodeId->toString() ;
		strNodeId->take() ;

		const char * _strNodeId    = strNodeId->get() ;
		if (_strNodeId == NULL)
			_strNodeId = "" ;

		const char * _namespaceUri = namespaceUri->get() ;
		if (_namespaceUri == NULL)
			_namespaceUri = "" ;

		uint32_t     _serverIndex  = serverIndex->get() ;

		int    length = strlen(_strNodeId) + strlen(_namespaceUri) + 128 ;
		char * str    = new char[length] ;

		sprintf(str,"ExpandedNodeId(%s,\"%s\",%d)",_strNodeId,_namespaceUri,_serverIndex) ;

		String * result = new String(str) ;

		strNodeId->release() ;
		delete [] str ;

		return result ;
	}

};     /* class ExpandedNodeId */
}      /* namespace opcua */
#endif /* OPCUA_EXPANDEDNODEID_H_ */
