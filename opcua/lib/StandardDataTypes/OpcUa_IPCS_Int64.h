
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_INT64_H_
#define OPCUA_INT64_H_

/*
 * OPC-UA 3, 8.27, p. 66
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Integer.h"
#include "OpcUa_IPCS_Variant.h"

namespace opcua {

class MYDLL Int64
		: public Integer
{
public:

	static Int64 * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Int64 ; }

private:

	int64_t value ;

public:

	static inline Int64 * fromCtoCpp(SOPC_StatusCode * pStatus, int64_t const& value)
	{
		NOT_USED(pStatus) ;
		return new Int64(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, int64_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	Int64(int64_t _value)
		: Integer(),
		  value(_value)
	{}

	virtual ~Int64()
	{}

public:

	inline int64_t get()
	{
		return value ;
	}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Int64,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	inline bool less(int64_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(int64_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(int64_t other)
	{
		return (value == other) ;
 	}

	inline bool greaterOrEquals(int64_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(int64_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(Int64 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(Int64 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(Int64 * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(Int64 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(Int64 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static Int64 * get_Int64_element(const char * const file_str, node_t * node) ;

	static Int64 * get_Int64(const char * const file_str, const char * const content) ;

};
} /* namespace opcua */
#endif /* OPCUA_INT64_H_ */
