
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_QUALIFIEDNAME_H_
#define OPCUA_QUALIFIEDNAME_H_

/*
 * OPC-UA Part 3, 8.3, p. 62
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_Variant.h"
#include "OpcUa_IPCS_UInt16.h"
#include "OpcUa_IPCS_String.h"
#include "OpcUa_BaseDataType.h"

namespace opcua {

class MYDLL QualifiedName
		: public BaseDataType
{
public:

	static QualifiedName * null ;

	static QualifiedName * defaultBinary ;
	static QualifiedName * defaultXML ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_QualifiedName ; }

private:

	UInt16 * namespaceIndex ;
	String * name ;

public:

	static QualifiedName * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_QualifiedName const& pQualifiedName)
	{
		debug(IPCS_DBG,"QualifiedName","Aller fromCtoCpp") ;

		UInt16 * namespaceIndex = UInt16 ::fromCtoCpp(pStatus, pQualifiedName.NamespaceIndex) ;
		String * name           = String ::fromCtoCpp(pStatus, pQualifiedName.Name) ;

		if (*pStatus == STATUS_OK)
			return
					new QualifiedName(
							namespaceIndex,
							name
							) ;

		if (namespaceIndex != NULL)
			namespaceIndex->checkRefCount() ;
		if (name != NULL)
			name->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_QualifiedName& pQualifiedName) const
	{
		debug(IPCS_DBG,"QualifiedName","Retour fromCpptoC") ;

		SOPC_QualifiedName_Initialize(&pQualifiedName) ;

		namespaceIndex ->fromCpptoC(pStatus, pQualifiedName.NamespaceIndex) ;
		name           ->fromCpptoC(pStatus, pQualifiedName.Name) ;
	}

public:

	QualifiedName(UInt16 * _namespaceIndex,
				  String * _name)
		: BaseDataType()
	{
		(namespaceIndex = _namespaceIndex) ->take() ;
		(name           = _name)           ->take() ;
	}


	virtual ~QualifiedName()
	{
		namespaceIndex ->release() ;
		name           ->release() ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_QualifiedName,this) ;
	}

public:

	inline UInt16 * getNamespaceIndex()
	{
		return namespaceIndex ;
	}

	inline String * getName()
	{
		return name ;
	}

public:

	bool isNull()
	{
		return namespaceIndex->isZero() && name->isNullOrEmpty() ;
	}

public:

	char * to_str()
	{
		const char * str = name->get() ;
		int          len = (str == NULL)?0:((int)strlen(str)) ;

		char * buf = new char[len + 32] ;
		sprintf(buf,"%d:",namespaceIndex->get()) ;
		if (str != NULL)
			strcat(buf,str) ;

		return buf ;
	}

	String * toString()
	{
		char   * str = to_str() ;
		String * res = new String(str) ;
		delete[] str ;
		return res ;
	}

public:

	bool equals(QualifiedName * other)
	{
		return
				namespaceIndex->equals(other->getNamespaceIndex()) &&
				name->equals(other->getName()) ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_QUALIFIEDNAME_H_ */
