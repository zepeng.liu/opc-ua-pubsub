
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ENUMERATION_H_
#define OPCUA_ENUMERATION_H_

/*
 * OPC-UA Part3, 8.14, p. 65
 * IdType = 29
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int32.h"

namespace opcua {

class MYDLL Enumeration
	: public Int32
{
public:

	Enumeration(int32_t _value)
		: Int32(_value)
	{}

	virtual ~Enumeration()
	{}

};

} /* namespace opcua */
#endif /* OPCUA_ENUMERATION_H_ */
