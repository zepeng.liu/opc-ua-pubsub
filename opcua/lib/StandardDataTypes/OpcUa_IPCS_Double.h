
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DOUBLE_H_
#define OPCUA_DOUBLE_H_

/*
 * OPC-UA Part 3, 8.12, p. 64
 */

#include "../OpcUa.h"
#include "../OpcUa_BuiltinDefine.h"
#include "OpcUa_Number.h"
#include "OpcUa_IPCS_Variant.h"

#include <math.h>
#include <float.h>

namespace opcua {

#define EPSILON_DOUBLE 0.0001

class MYDLL Double
	: public Number
{
public:

	static Double * zero ;
	static Double * maximum ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Double ; }

private:

	double value ;

public:

	Double(double _value)
		: Number(),
		  value(_value)
	{}

	virtual ~Double()
	{}

public:

	static inline Double * fromCtoCpp(SOPC_StatusCode * pStatus, double const& value)
	{
		NOT_USED(pStatus) ;
		return new Double(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, double& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Double,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	inline bool lessOrEquals(double number)
	{
		return (value < number + EPSILON_DOUBLE) ;
	}

	inline bool less(double number)
	{
		return (value < number - EPSILON_DOUBLE) ;
	}

	inline bool equals(double number)
	{
		return (fabs(value - number) < EPSILON_DOUBLE) ;
	}

	inline bool greater(double number)
	{
		return (value > number + EPSILON_DOUBLE) ;
	}

	inline bool greaterOrEquals(double number)
	{
		return (value > number - EPSILON_DOUBLE) ;
	}

public:

	inline double get()
	{
		return value ;
	}

public:

	static Double * get_Double_element(const char * const file_str, node_t * node) ;

	static Double * get_Double(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_DOUBLE_H_ */
