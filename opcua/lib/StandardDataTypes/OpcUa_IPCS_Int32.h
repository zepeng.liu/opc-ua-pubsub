
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_INT32_H_
#define OPCUA_INT32_H_

/*
 * OPC-UA 3, 8.26, p. 66
 * OPC-UA 6, 5.2.2.2, p. 5
 */

#include "../OpcUa.h"
#include "OpcUa_Integer.h"
#include "OpcUa_IPCS_Variant.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL Int32
		: public Integer
{
public:

	static Int32 * minusTwo ;
	static Int32 * minusOne ;
	static Int32 * zero ;
	static Int32 * maximum ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Int32 ; }

private:

	int32_t value ;

public:

	static inline Int32 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& value)
	{
		NOT_USED(pStatus) ;
		return new Int32(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	Int32(int32_t _value)
		: Integer(),
		  value(_value)
	{}

	virtual ~Int32()
	{}

public:

	inline int32_t get() const
	{
		return value ;
	}

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		return HashCode::hashCode(hash,reinterpret_cast<const uint8_t *>(&value),sizeof(int32_t)) ;
	}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Int32,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	inline bool less(int32_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(int32_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(int32_t other)
	{
		return (value == other) ;
 	}

	inline bool notEquals(int32_t other)
	{
		return (value != other) ;
 	}

	inline bool greaterOrEquals(int32_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(int32_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(Int32 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(Int32 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(Int32 * other)
	{
		return other->equals(value) ;
 	}

	inline bool notEquals(Int32 * other)
	{
		return other->notEquals(value) ;
 	}

	inline bool greaterOrEquals(Int32 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(Int32 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static Int32 * get_Int32_element(const char * const file_str, node_t * node) ;

	static Int32 * get_Int32(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_INT32_H_ */
