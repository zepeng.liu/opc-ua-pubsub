
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.12, p. 64
 */

#include "OpcUa_IPCS_Double.h"
#include "OpcUa_IPCS_Boolean.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

	Double * Double::get_Double_element(const char * const file_str, node_t * node)
	{
		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		char * value = BaseDataType::get_Attribute("Value",node) ;

		if (value == NULL) {
			return Double::zero ;
		}

		Double * result = get_Double(file_str,value) ;
		roxml_release(value) ;
		return result ;
	}

	Double * Double::get_Double(const char * const file_str, const char * const content)
	{
		if (content == NULL) {
			debug_s(COM_ERR,"NodesTable","File \"%s\", Double reading error (NULL value)",file_str) ;
			return NULL ;
		}

		errno = 0 ;

		double value = strtod(content,(char **)NULL) ;

		if (errno != 0) {
			debug_ss(COM_ERR,"NodesTable","File \"%s\", Double reading error (not an double) \"%s\"",file_str,content) ;
			errno = 0 ;
			return NULL ;
		}

		return new Double(static_cast<double>(value)) ;
	}

}
