
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.5, p. 63
 */

#include "OpcUa_IPCS_LocalizedText.h"

#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

LocalizedText * LocalizedText::get_LocalizedText_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	String   * text     = NULL ;
	LocaleId * localeId = NULL ;

	int nb = roxml_get_chld_nb(node) ;

	for (int i = 0 ; i < nb ; i++) {

		node_t * chld = roxml_get_chld(node,NULL,i) ;
		char *   name = roxml_get_name(chld,NULL,0) ;

		debug_ss(XML_DBG,"LocalizedText","Configuration file \"%s\", LocalizedText element = \"%s\"",file_str,name) ;

		if (StringUtils::strcmpi(name,"Text") == 0 && text == NULL) {
			if ((text = String::get_String_element(file_str,chld)))
				text->take() ;
		} else
		if (StringUtils::strcmpi(name,"LocaleId") == 0 && localeId == NULL) {
			if ((localeId = LocaleId::get_LocaleId_element(file_str,chld)))
				localeId->take() ;
		} else {
			debug_ss(COM_ERR,"LocalizedText","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"LocalizedText\"",file_str,name) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(name) ;
	}

	LocalizedText * result ;

	if (text == NULL) {
		debug_s(COM_ERR,"LocalizedText","Configuration file \"%s\", LocalizedText has no Text element",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}

	if (localeId == NULL) {
		(localeId = LocaleId::en)->take() ;
	}

	result = new LocalizedText(
			text,
			localeId
			) ;

error:

	if (text != NULL)
		text->release() ;
	if (localeId != NULL)
		localeId->release() ;

	return result ;
}

}
