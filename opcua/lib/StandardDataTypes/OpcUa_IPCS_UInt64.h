
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_UINT64_H_
#define OPCUA_UINT64_H_

/*
 * OPC-UA 3, 8.37, p. 67
 */

#include "../OpcUa.h"
#include "OpcUa_UInteger.h"
#include "OpcUa_IPCS_Variant.h"

namespace opcua {

class MYDLL UInt64
		: public UInteger
{
public:

	static UInt64 * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_UInt64 ; }

private:

	uint64_t value ;

public:

	static inline UInt64 * fromCtoCpp(SOPC_StatusCode * pStatus, uint64_t const& value)
	{
		NOT_USED(pStatus) ;
		return new UInt64(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, uint64_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	UInt64(uint64_t _value)
		: UInteger(),
		  value(_value)
	{}

	virtual ~UInt64()
	{}

public:

	inline uint64_t get()
	{
		return value ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_UInt64,this) ;
	}

public:

	inline bool less(uint64_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(uint64_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(uint64_t other)
	{
		return (value == other) ;
 	}

	inline bool greaterOrEquals(uint64_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(uint64_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(UInt64 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(UInt64 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(UInt64 * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(UInt64 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(UInt64 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static UInt64 * get_UInt64_element(const char * const file_str, node_t * node) ;

	static UInt64 * get_UInt64(const char * const file_str, const char * const content) ;

} ;

} /* namespace opcua */
#endif /* OPCUA_UINT64_H_ */
