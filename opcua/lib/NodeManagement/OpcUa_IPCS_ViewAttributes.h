
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VIEW_ATTRIBUTES_H_
#define OPCUA_VIEW_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.9, p. 131
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL ViewAttributes
	: public NodeAttributes
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ViewAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Boolean       * containsNoLoops ;
	Byte          * eventNotifier ;

public:

	static ViewAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_ViewAttributes const& pViewAttributes)
	{
		uint32_t specifiedAttributes = pViewAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pViewAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pViewAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pViewAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pViewAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Boolean * containsNoLoops ;
		if (specifiedAttributes & NodeAttributeId_ContainsNoLoops)
			containsNoLoops = Boolean ::fromCtoCpp(pStatus,pViewAttributes.ContainsNoLoops) ;
		else
			containsNoLoops = NULL ;

		Byte * eventNotifier ;
		if (specifiedAttributes & NodeAttributeId_EventNotifier)
			eventNotifier = Byte ::fromCtoCpp(pStatus,pViewAttributes.EventNotifier) ;
		else
			eventNotifier = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new ViewAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							containsNoLoops,
							eventNotifier
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (containsNoLoops != NULL)
			containsNoLoops->checkRefCount() ;
		if (eventNotifier != NULL)
			eventNotifier->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pViewAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pViewAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pViewAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pViewAttributes.Body.Object.ObjType = &OpcUa_ViewAttributes_EncodeableType ;
			pViewAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_ViewAttributes)) ;
			OpcUa_ViewAttributes_Initialize(pViewAttributes.Body.Object.Value) ;
			ViewAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_ViewAttributes *>(pViewAttributes.Body.Object.Value)) ;
			pViewAttributes.Length = sizeof(_OpcUa_ViewAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_ViewAttributes& pViewAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pViewAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pViewAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pViewAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pViewAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pViewAttributes.UserWriteMask) ;

		if (containsNoLoops != NULL)
			containsNoLoops    ->fromCpptoC(pStatus,pViewAttributes.ContainsNoLoops) ;
		if (eventNotifier != NULL)
			eventNotifier    ->fromCpptoC(pStatus,pViewAttributes.EventNotifier) ;
}

public:

	ViewAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Boolean       * _containsNoLoops,
			Byte          * _eventNotifier
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;



		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;


		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((containsNoLoops = _containsNoLoops)) {
			_specifiedAttributes |= NodeAttributeId_ContainsNoLoops ;
		} else {
			containsNoLoops = Boolean::booleanFalse ;
		}
		containsNoLoops->take() ;

		if ((eventNotifier = _eventNotifier)) {
			_specifiedAttributes |= NodeAttributeId_EventNotifier ;
		} else {
			eventNotifier = Byte::zero ;
		}
		eventNotifier->take() ;


		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~ViewAttributes()
	{
		specifiedAttributes ->release() ;

		displayName         ->release() ;
		description         ->release() ;

		writeMask           ->release() ;
		userWriteMask       ->release() ;

		containsNoLoops     ->release() ;
		eventNotifier       ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}


	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}

	inline Boolean * getContainsNoLoops()
	{
		return containsNoLoops ;
	}

	inline Byte * getEventNotifier()
	{
		return eventNotifier ;
	}

	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_VIEW_ATTRIBUTES_H_ */
