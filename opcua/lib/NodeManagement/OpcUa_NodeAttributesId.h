
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NODEATTRIBUTESIF_H_
#define NODEATTRIBUTESIF_H_

#include "../OpcUa.h"

// Part 4, 7.18.1, Table 138, page 129

#if (WITH_NODEMNGT == 1)

#define NodeAttributeId_AccessLevel                 (((uint32_t)1) <<  0)
#define NodeAttributeId_ArrayDimensions             (((uint32_t)1) <<  1)
#define NodeAttributeId_Reserved_00                 (((uint32_t)1) <<  2)
#define NodeAttributeId_ContainsNoLoops             (((uint32_t)1) <<  3)
#define NodeAttributeId_DataType                    (((uint32_t)1) <<  4)
#define NodeAttributeId_Description                 (((uint32_t)1) <<  5)
#define NodeAttributeId_DisplayName                 (((uint32_t)1) <<  6)
#define NodeAttributeId_EventNotifier               (((uint32_t)1) <<  7)
#define NodeAttributeId_Executable                  (((uint32_t)1) <<  8)
#define NodeAttributeId_Historizing                 (((uint32_t)1) <<  9)
#define NodeAttributeId_InverseName                 (((uint32_t)1) << 10)
#define NodeAttributeId_IsAbstract                  (((uint32_t)1) << 11)
#define NodeAttributeId_MinimumSamplingInterval     (((uint32_t)1) << 12)
#define NodeAttributeId_Reserved_01                 (((uint32_t)1) << 13)
#define NodeAttributeId_Reserved_02                 (((uint32_t)1) << 14)
#define NodeAttributeId_Symmetric                   (((uint32_t)1) << 15)
#define NodeAttributeId_UserAccessLevel             (((uint32_t)1) << 16)
#define NodeAttributeId_UserExecutable              (((uint32_t)1) << 17)
#define NodeAttributeId_UserWriteMask               (((uint32_t)1) << 18)
#define NodeAttributeId_ValueRank                   (((uint32_t)1) << 19)
#define NodeAttributeId_WriteMask                   (((uint32_t)1) << 20)
#define NodeAttributeId_Value                       (((uint32_t)1) << 21)
#define NodeAttributeId_Reserved_03                 (((uint32_t)1) << 22)
#define NodeAttributeId_Reserved_04                 (((uint32_t)1) << 23)
#define NodeAttributeId_Reserved_05                 (((uint32_t)1) << 24)
#define NodeAttributeId_Reserved_06                 (((uint32_t)1) << 25)
#define NodeAttributeId_Reserved_07                 (((uint32_t)1) << 26)
#define NodeAttributeId_Reserved_08                 (((uint32_t)1) << 27)
#define NodeAttributeId_Reserved_09                 (((uint32_t)1) << 28)
#define NodeAttributeId_Reserved_10                 (((uint32_t)1) << 29)
#define NodeAttributeId_Reserved_11                 (((uint32_t)1) << 30)
#define NodeAttributeId_Reserved_12                 (((uint32_t)1) << 31)

/** The following defines mandatory attributes for checking that attributes
 *  are instanciated on ../Services/NodeManagement/OpcUa_AddNodes.cpp
 */

#define Mandatory_Object_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_EventNotifier \
	)

#define Mandatory_Variable_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_Value \
			| \
			NodeAttributeId_DataType \
			| \
			NodeAttributeId_ValueRank \
			| \
			NodeAttributeId_AccessLevel \
			| \
			NodeAttributeId_UserAccessLevel \
			| \
			NodeAttributeId_Historizing \
	)

#define Mandatory_Method_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_Executable \
			| \
			NodeAttributeId_UserExecutable \
	)

#define Mandatory_ObjectType_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_IsAbstract \
	)

#define Mandatory_VariableType_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_DataType \
			| \
			NodeAttributeId_ValueRank \
			| \
			NodeAttributeId_IsAbstract \
	)

#define Mandatory_ReferenceType_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_IsAbstract \
			| \
			NodeAttributeId_Symmetric \
	)

#define Mandatory_DataType_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_IsAbstract \
	)

#define Mandatory_View_Attributes \
	( \
			NodeAttributeId_DisplayName \
			| \
			NodeAttributeId_ContainsNoLoops \
			| \
			NodeAttributeId_EventNotifier \
	)

#endif // NODEMNGT == 1
#endif /* NODEATTRIBUTESIF_H_ */
