
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATATYPE_ATTRIBUTES_H_
#define OPCUA_DATATYPE_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.8, p. 131 différent of XML in Part 6
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL DataTypeAttributes
	: public NodeAttributes
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_DataTypeAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Boolean       * isAbstract ;

public:

	static DataTypeAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_DataTypeAttributes const& pDataTypeAttributes)
	{
		uint32_t specifiedAttributes = pDataTypeAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pDataTypeAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pDataTypeAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pDataTypeAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pDataTypeAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Boolean * isAbstract ;
		if (specifiedAttributes & NodeAttributeId_IsAbstract)
			isAbstract = Boolean ::fromCtoCpp(pStatus,pDataTypeAttributes.IsAbstract) ;
		else
			isAbstract = NULL ;

		if (*pStatus == STATUS_OK)
			return
					new DataTypeAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							isAbstract
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (isAbstract != NULL)
			isAbstract->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pDataTypeAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pDataTypeAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pDataTypeAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pDataTypeAttributes.Body.Object.ObjType = &OpcUa_DataTypeAttributes_EncodeableType ;
			pDataTypeAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_DataTypeAttributes)) ;
			OpcUa_DataTypeAttributes_Initialize(pDataTypeAttributes.Body.Object.Value) ;
			DataTypeAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_DataTypeAttributes *>(pDataTypeAttributes.Body.Object.Value)) ;
			pDataTypeAttributes.Length = sizeof(_OpcUa_DataTypeAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_DataTypeAttributes& pDataTypeAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pDataTypeAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pDataTypeAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pDataTypeAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pDataTypeAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pDataTypeAttributes.UserWriteMask) ;

		if (isAbstract != NULL)
			isAbstract    ->fromCpptoC(pStatus,pDataTypeAttributes.IsAbstract) ;
	}

public:

	DataTypeAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Boolean       * _isAbstract
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;


		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((isAbstract = _isAbstract)) {
			_specifiedAttributes |= NodeAttributeId_IsAbstract ;
		} else {
			isAbstract = Boolean::booleanFalse ;
		}
		isAbstract->take() ;


		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~DataTypeAttributes()
	{
		specifiedAttributes ->release() ;

		displayName         ->release() ;
		description         ->release() ;

		writeMask           ->release() ;
		userWriteMask       ->release() ;

		isAbstract          ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}


	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}

	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}

	inline Boolean * getIsAbstract()
	{
		return isAbstract ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_DATATYPE_ATTRIBUTES_H_ */
