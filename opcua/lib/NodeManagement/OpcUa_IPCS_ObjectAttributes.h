
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_OBJECT_ATTRIBUTES_H_
#define OPCUA_OBJECT_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.2, p. 129
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL ObjectAttributes
	: public NodeAttributes
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ObjectAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Byte          * eventNotifier ;

public:

	static ObjectAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_ObjectAttributes const& pObjectAttributes)
	{
		uint32_t specifiedAttributes = pObjectAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pObjectAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pObjectAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pObjectAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pObjectAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Byte * eventNotifier ;
		if (specifiedAttributes & NodeAttributeId_EventNotifier)
			eventNotifier = Byte ::fromCtoCpp(pStatus,pObjectAttributes.EventNotifier) ;
		else
			eventNotifier = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new ObjectAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							eventNotifier
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (eventNotifier != NULL)
			eventNotifier->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pObjectAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pObjectAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pObjectAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pObjectAttributes.Body.Object.ObjType = &OpcUa_ObjectAttributes_EncodeableType ;
			pObjectAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_ObjectAttributes)) ;
			OpcUa_ObjectAttributes_Initialize(pObjectAttributes.Body.Object.Value) ;
			ObjectAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_ObjectAttributes *>(pObjectAttributes.Body.Object.Value)) ;
			pObjectAttributes.Length = sizeof(_OpcUa_ObjectAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_ObjectAttributes& pObjectAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pObjectAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pObjectAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pObjectAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pObjectAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pObjectAttributes.UserWriteMask) ;

		if (eventNotifier != NULL)
			eventNotifier ->fromCpptoC(pStatus,pObjectAttributes.EventNotifier) ;
}

public:

	ObjectAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,
			UInt32        * _writeMask,
			UInt32        * _userWriteMask,
			Byte          * _eventNotifier
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;


		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((eventNotifier = _eventNotifier)) {
			_specifiedAttributes |= NodeAttributeId_EventNotifier ;
		} else {
			eventNotifier = Byte::zero ;
		}
		eventNotifier->take() ;


		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~ObjectAttributes()
	{
		specifiedAttributes ->release() ;

		displayName         ->release() ;
		description         ->release() ;

		writeMask           ->release() ;
		userWriteMask       ->release() ;

		eventNotifier       ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}


	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}


	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}


	inline Byte * getEventNotifier()
	{
		return eventNotifier ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_OBJECT_ATTRIBUTES_H_ */
