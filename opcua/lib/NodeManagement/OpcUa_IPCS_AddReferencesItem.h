
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDREFERENCESITEM_H_
#define OPCUA_ADDREFERENCESITEM_H_

/*
 * OPC-UA, Part 4, 5.7.3.2, p. 34, inside Table 21
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL AddReferencesItem
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddReferencesItem ; }

private:

	NodeId         * sourceNodeId ;
	NodeId         * referenceTypeId ;
	Boolean		   * isForward ;
	String         * targetServerUri ;
	ExpandedNodeId * targetNodeId ;

	NodeClass      * targetNodeClass ;

public:

	static AddReferencesItem * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_AddReferencesItem const& pAddReferencesItem)
	{
		NodeId         * sourceNodeId    = NodeId         ::fromCtoCpp ( pStatus, pAddReferencesItem.SourceNodeId) ;
		NodeId         * referenceTypeId = NodeId         ::fromCtoCpp ( pStatus, pAddReferencesItem.ReferenceTypeId) ;
		Boolean        * isForward       = Boolean        ::fromCtoCpp ( pStatus, pAddReferencesItem.IsForward) ;
		String         * targetServerUri = String         ::fromCtoCpp ( pStatus, pAddReferencesItem.TargetServerUri) ;
		ExpandedNodeId * targetNodeId    = ExpandedNodeId ::fromCtoCpp ( pStatus, pAddReferencesItem.TargetNodeId) ;

		NodeClass      * targetNodeClass = NodeClass      ::fromCtoCpp ( pStatus, pAddReferencesItem.TargetNodeClass) ;

		if (*pStatus == STATUS_OK)
			return
					new AddReferencesItem(
							sourceNodeId,
							referenceTypeId,
							isForward,
							targetServerUri,
							targetNodeId,

							targetNodeClass
							) ;

		if (sourceNodeId != NULL)
			sourceNodeId->checkRefCount() ;
		if (referenceTypeId != NULL)
			referenceTypeId->checkRefCount() ;
		if (isForward != NULL)
			isForward->checkRefCount() ;
		if (targetServerUri != NULL)
			targetServerUri->checkRefCount() ;
		if (targetNodeId != NULL)
			targetNodeId->checkRefCount() ;

		if (targetNodeClass != NULL)
			targetNodeClass->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_AddReferencesItem& pAddReferencesItem) const
	{
		sourceNodeId    ->fromCpptoC (pStatus, pAddReferencesItem.SourceNodeId) ;
		referenceTypeId ->fromCpptoC (pStatus, pAddReferencesItem.ReferenceTypeId) ;
		isForward       ->fromCpptoC (pStatus, pAddReferencesItem.IsForward) ;
		targetServerUri ->fromCpptoC (pStatus, pAddReferencesItem.TargetServerUri) ;
		targetNodeId    ->fromCpptoC (pStatus, pAddReferencesItem.TargetNodeId) ;

		targetNodeClass ->fromCpptoC (pStatus, pAddReferencesItem.TargetNodeClass) ;
	}

public:

	AddReferencesItem(
			NodeId         * _sourceNodeId,
			NodeId         * _referenceTypeId,
			Boolean		   * _isForward,
			String         * _targetServerUri,
			ExpandedNodeId * _targetNodeId,
			NodeClass      * _targetNodeClass
			)
		: Structure()
	{
		(sourceNodeId    = _sourceNodeId)    ->take() ;
		(referenceTypeId = _referenceTypeId) ->take() ;
		(isForward       = _isForward)       ->take() ;
		(targetServerUri = _targetServerUri) ->take() ;
		(targetNodeId    = _targetNodeId)    ->take() ;
		(targetNodeClass = _targetNodeClass) ->take() ;
	}

	virtual ~AddReferencesItem()
	{
		sourceNodeId    ->release() ;
		referenceTypeId ->release() ;
		isForward       ->release() ;
		targetServerUri ->release() ;
		targetNodeId    ->release() ;
		targetNodeClass ->release() ;
	}

public:

	inline NodeId * getSourceNodeId()
	{
		return sourceNodeId ;
	}

	inline NodeId * getReferenceTypeId()
	{
		return referenceTypeId ;
	}

	inline Boolean * getIsForward()
	{
		return isForward ;
	}

	inline String * getTargetServerUri()
	{
		return targetServerUri ;
	}

	inline ExpandedNodeId * getTargetNodeId()
	{
		return targetNodeId ;
	}

	inline NodeClass * getTargetNodeClass()
	{
		return targetNodeClass ;
	}

};
} /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDREFERENCESITEM_H_ */
