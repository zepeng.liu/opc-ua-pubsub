
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIABLE_ATTRIBUTES_H_
#define OPCUA_VARIABLE_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.3, p. 129
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL VariableAttributes
	: public NodeAttributes
{
public:
	// This function is just to keep compatibility with OPC-UA/ROSA: there was a field
	// named String * aux which contains the DLL of an ExternalVariable (DataItem).

	inline String * getAux() { return NULL ; }

public:

	virtual uint32_t getTypeId() const { return OpcUaId_VariableAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Variant       * value ;
	NodeId		  * dataType ;
	Int32         * valueRank ;

	TableUInt32   * arrayDimensions ;

	Byte		  * accessLevel ;
	Byte		  * userAccessLevel ;

	Duration      * minimumSamplingInterval ;
	Boolean       * historizing ;

public:

	static VariableAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_VariableAttributes const& pVariableAttributes)
	{
		uint32_t specifiedAttributes = pVariableAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pVariableAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pVariableAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pVariableAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pVariableAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Variant * value ;
		if (specifiedAttributes & NodeAttributeId_Value)
			value = Variant ::fromCtoCpp(pStatus,pVariableAttributes.Value) ;
		else
			value = NULL ;

		NodeId * dataType ;
		if (specifiedAttributes & NodeAttributeId_DataType)
			dataType = NodeId ::fromCtoCpp(pStatus,pVariableAttributes.DataType) ;
		else
			dataType = NULL ;

		Int32 * valueRank ;
		if (specifiedAttributes & NodeAttributeId_ValueRank)
			valueRank = Int32 ::fromCtoCpp(pStatus,pVariableAttributes.ValueRank) ;
		else
			valueRank = NULL ;


		TableUInt32   * arrayDimensions ;
		if (specifiedAttributes & NodeAttributeId_ArrayDimensions)
			arrayDimensions = TableUInt32::fromCtoCpp(pStatus,pVariableAttributes.NoOfArrayDimensions,pVariableAttributes.ArrayDimensions) ;
		else
			arrayDimensions = NULL ;


		Byte * accessLevel ;
		if (specifiedAttributes & NodeAttributeId_AccessLevel)
			accessLevel = Byte ::fromCtoCpp(pStatus,pVariableAttributes.AccessLevel) ;
		else
			accessLevel = NULL ;

		Byte * userAccessLevel ;
		if (specifiedAttributes & NodeAttributeId_UserAccessLevel)
			userAccessLevel = Byte ::fromCtoCpp(pStatus,pVariableAttributes.UserAccessLevel) ;
		else
			userAccessLevel = NULL ;


		Duration * minimumSamplingInterval ;
		if (specifiedAttributes & NodeAttributeId_MinimumSamplingInterval)
			minimumSamplingInterval = Duration ::fromCtoCpp(pStatus,pVariableAttributes.MinimumSamplingInterval) ;
		else
			minimumSamplingInterval = NULL ;

		Boolean * historizing ;
		if (specifiedAttributes & NodeAttributeId_Historizing)
			historizing = Boolean ::fromCtoCpp(pStatus,pVariableAttributes.Historizing) ;
		else
			historizing = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new VariableAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							value,
							dataType,
							valueRank,

							arrayDimensions,

							accessLevel,
							userAccessLevel,

							minimumSamplingInterval,
							historizing,

							NULL // aux
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (value != NULL)
			value->checkRefCount() ;
		if (dataType != NULL)
			dataType->checkRefCount() ;
		if (valueRank != NULL)
			valueRank->checkRefCount() ;

		if (arrayDimensions != NULL)
			arrayDimensions->checkRefCount() ;

		if (accessLevel != NULL)
			accessLevel->checkRefCount() ;
		if (userAccessLevel != NULL)
			userAccessLevel->checkRefCount() ;

		if (minimumSamplingInterval != NULL)
			minimumSamplingInterval->checkRefCount() ;
		if (historizing != NULL)
			historizing->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pVariableAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pVariableAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pVariableAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pVariableAttributes.Body.Object.ObjType = &OpcUa_VariableAttributes_EncodeableType ;
			pVariableAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_VariableAttributes)) ;
			OpcUa_VariableAttributes_Initialize(pVariableAttributes.Body.Object.Value) ;
			VariableAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_VariableAttributes *>(pVariableAttributes.Body.Object.Value)) ;
			pVariableAttributes.Length = sizeof(_OpcUa_VariableAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_VariableAttributes& pVariableAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pVariableAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pVariableAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pVariableAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pVariableAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pVariableAttributes.UserWriteMask) ;

		if (value != NULL)
			value     ->fromCpptoC(pStatus,pVariableAttributes.Value) ;
		if (dataType != NULL)
			dataType  ->fromCpptoC(pStatus,pVariableAttributes.DataType) ;
		if (valueRank != NULL)
			valueRank ->fromCpptoC(pStatus,pVariableAttributes.ValueRank) ;

		if (arrayDimensions != NULL)
			arrayDimensions ->fromCpptoC (pStatus,pVariableAttributes.NoOfArrayDimensions,pVariableAttributes.ArrayDimensions) ;

		if (accessLevel != NULL)
			accessLevel  ->fromCpptoC(pStatus,pVariableAttributes.AccessLevel) ;
		if (userAccessLevel != NULL)
			userAccessLevel ->fromCpptoC(pStatus,pVariableAttributes.UserAccessLevel) ;

		if (minimumSamplingInterval != NULL)
			minimumSamplingInterval  ->fromCpptoC(pStatus,pVariableAttributes.MinimumSamplingInterval) ;
		if (historizing != NULL)
			historizing ->fromCpptoC(pStatus,pVariableAttributes.Historizing) ;
	}

public:

	VariableAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Variant       * _value,
			NodeId		  * _dataType,
			Int32         * _valueRank,

			TableUInt32   * _arrayDimensions,

			Byte		  * _accessLevel,
			Byte		  * _userAccessLevel,
			Duration      * _minimumSamplingInterval,
			Boolean       * _historizing,

			String		  * _aux
			)
		: NodeAttributes()
	{
		if (_aux != NULL)
			_aux->release() ;

		uint32_t _specifiedAttributes = 0 ;


		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;



		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;



		if ((value = _value)) {
			_specifiedAttributes |= NodeAttributeId_Value ;
		} else {
			value = new Variant(Builtin_Byte,Byte::zero) ;
		}
		value->take() ;

		if ((dataType = _dataType)) {
			_specifiedAttributes |= NodeAttributeId_DataType ;
		} else {
			dataType = NodeId::nullNodeId ;
		}
		dataType->take() ;

		if ((valueRank = _valueRank)) {
			_specifiedAttributes |= NodeAttributeId_ValueRank ;
		} else {
			valueRank = Int32::zero ;
		}
		valueRank->take() ;



		if ((arrayDimensions = _arrayDimensions)) {
			_specifiedAttributes |= NodeAttributeId_ArrayDimensions ;
		} else {
			arrayDimensions = UInt32::tableZero ;
		}
		arrayDimensions->take() ;



		if ((accessLevel = _accessLevel)) {
			_specifiedAttributes |= NodeAttributeId_AccessLevel ;
		} else {
			accessLevel = Byte::zero ;
		}
		accessLevel->take() ;

		if ((userAccessLevel = _userAccessLevel)) {
			_specifiedAttributes |= NodeAttributeId_UserAccessLevel ;
		} else {
			userAccessLevel = Byte::zero ;
		}
		userAccessLevel->take() ;

		if ((minimumSamplingInterval = _minimumSamplingInterval)) {
			_specifiedAttributes |= NodeAttributeId_MinimumSamplingInterval ;
		} else {
			minimumSamplingInterval = Duration::zero ;
		}
		minimumSamplingInterval->take() ;

		if ((historizing = _historizing)) {
			_specifiedAttributes |= NodeAttributeId_Historizing ;
		} else {
			historizing = Boolean::booleanFalse ;
		}
		historizing->take() ;



		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~VariableAttributes()
	{
		specifiedAttributes     ->release() ;

		displayName             ->release() ;
		description             ->release() ;

		writeMask               ->release() ;
		userWriteMask           ->release() ;

		value                   ->release() ;
		dataType                ->release() ;
		valueRank               ->release() ;

		arrayDimensions         ->release() ;

		accessLevel             ->release() ;
		userAccessLevel         ->release() ;
		minimumSamplingInterval ->release() ;
		historizing             ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}



	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}


	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}



	inline Variant * getValue()
	{
		return value ;
	}

	inline NodeId * getDataType()
	{
		return dataType ;
	}

	inline Int32 * getValueRank()
	{
		return valueRank ;
	}


	inline TableUInt32   * getArrayDimensions()
	{
		return arrayDimensions ;
	}


	inline Byte * getAccessLevel()
	{
		return accessLevel ;
	}

	inline Byte * getUserAccessLevel()
	{
		return userAccessLevel ;
	}

	inline Duration *getMinimumSamplingInterval()
	{
		return minimumSamplingInterval ;
	}

	inline Boolean * getHistorizing()
	{
		return historizing ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_VARIABLE_ATTRIBUTES_H_ */
