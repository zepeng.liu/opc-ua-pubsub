
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCETYPE_ATTRIBUTES_H_
#define OPCUA_REFERENCETYPE_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.7, p. 131
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL ReferenceTypeAttributes
	: public NodeAttributes
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ReferenceTypeAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Boolean       * isAbstract ;
	Boolean       * symmetric ;
	LocalizedText * inverseName ;

public:

	static ReferenceTypeAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_ReferenceTypeAttributes const& pReferenceTypeAttributes)
	{
		uint32_t specifiedAttributes = pReferenceTypeAttributes.SpecifiedAttributes ;


		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pReferenceTypeAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pReferenceTypeAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pReferenceTypeAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pReferenceTypeAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Boolean * isAbstract ;
		if (specifiedAttributes & NodeAttributeId_IsAbstract)
			isAbstract = Boolean ::fromCtoCpp(pStatus,pReferenceTypeAttributes.IsAbstract) ;
		else
			isAbstract = NULL ;

		Boolean * symmetric ;
		if (specifiedAttributes & NodeAttributeId_Symmetric)
			symmetric = Boolean ::fromCtoCpp(pStatus,pReferenceTypeAttributes.Symmetric) ;
		else
			symmetric = NULL ;

		LocalizedText * inverseName ;
		if (specifiedAttributes & NodeAttributeId_InverseName)
			inverseName = LocalizedText ::fromCtoCpp(pStatus,pReferenceTypeAttributes.InverseName) ;
		else
			inverseName = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new ReferenceTypeAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							isAbstract,
							symmetric,
							inverseName
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (isAbstract != NULL)
			isAbstract->checkRefCount() ;
		if (symmetric != NULL)
			symmetric->checkRefCount() ;
		if (inverseName != NULL)
			inverseName->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pReferenceTypeAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pReferenceTypeAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pReferenceTypeAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pReferenceTypeAttributes.Body.Object.ObjType = &OpcUa_ReferenceTypeAttributes_EncodeableType ;
			pReferenceTypeAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_ReferenceTypeAttributes)) ;
			OpcUa_ReferenceTypeAttributes_Initialize(pReferenceTypeAttributes.Body.Object.Value) ;
			ReferenceTypeAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_ReferenceTypeAttributes *>(pReferenceTypeAttributes.Body.Object.Value)) ;
			pReferenceTypeAttributes.Length = sizeof(_OpcUa_ReferenceTypeAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_ReferenceTypeAttributes& pReferenceTypeAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pReferenceTypeAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pReferenceTypeAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pReferenceTypeAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pReferenceTypeAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pReferenceTypeAttributes.UserWriteMask) ;

		if (isAbstract != NULL)
			isAbstract   ->fromCpptoC(pStatus,pReferenceTypeAttributes.IsAbstract) ;
		if (symmetric != NULL)
			symmetric    ->fromCpptoC(pStatus,pReferenceTypeAttributes.Symmetric) ;
		if (inverseName != NULL)
			inverseName  ->fromCpptoC(pStatus,pReferenceTypeAttributes.InverseName) ;
	}

public:

	ReferenceTypeAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Boolean       * _isAbstract,
			Boolean       * _symmetric,
			LocalizedText * _inverseName
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;



		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((isAbstract = _isAbstract)) {
			_specifiedAttributes |= NodeAttributeId_IsAbstract ;
		} else {
			isAbstract = Boolean::booleanFalse ;
		}
		isAbstract->take() ;

		if ((symmetric = _symmetric)) {
			_specifiedAttributes |= NodeAttributeId_Symmetric ;
		} else {
			symmetric = Boolean::booleanFalse ;
		}
		symmetric->take() ;

		if ((inverseName = _inverseName)) {
			_specifiedAttributes |= NodeAttributeId_InverseName ;
		} else {
			inverseName = LocalizedText::null ;
		}
		inverseName->take() ;


		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~ReferenceTypeAttributes()
	{
		specifiedAttributes ->release() ;

		displayName         ->release() ;
		description         ->release() ;

		writeMask           ->release() ;
		userWriteMask       ->release() ;

		isAbstract          ->release() ;
		symmetric           ->release() ;
		inverseName         ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}



	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}



	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}



	inline Boolean * getIsAbstract()
	{
		return isAbstract ;
	}

	inline Boolean * getSymmetric()
	{
		return symmetric ;
	}

	inline LocalizedText * getInverseName()
	{
		return inverseName ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_REFERENCETYPE_ATTRIBUTES_H_ */
