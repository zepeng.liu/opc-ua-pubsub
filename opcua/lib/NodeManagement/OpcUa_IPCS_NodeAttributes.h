
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_NODEATTRIBUTES_H_
#define OPCUA_NODEATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18, p. 128
 * An extensible parameters.
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL NodeAttributes
	: public Structure
{
public:

	virtual ExpandedNodeId * getNodeId() const = 0 ;

public:

	NodeAttributes()
		: Structure()
	{}

	virtual ~NodeAttributes()
	{}

public:

	static NodeAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pNodeAttributes) ;

	virtual void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pNodeAttributes) const = 0 ;

};
}      /* namespace opcua */
#endif // NODEMNGT == 1
#endif /* OPCUA_NODEATTRIBUTES_H_ */
