/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.


 * OpcUa_SortedList.h
 *
 *  Created on: Mar 30, 2015
 *      Author: chautn
 */

#ifndef LIB_UTILS_OPCUA_SORTEDLIST_H_
#define LIB_UTILS_OPCUA_SORTEDLIST_H_

#include "OpcUa_LinkedList.h"

namespace opcua {

template <typename OBJ> class MYDLL SortedList
{
private:

	LinkedList<OBJ> * head;
	LinkedList<OBJ> * tail;

public:

	SortedList() {
		head = NULL;
		tail = NULL;
	}

	virtual ~SortedList() {
		if (head != NULL)
			head->deleteAll();
	}

public:

	/*
	 * This method has to be implemented. It defines the condition to sort the list
	 * return value
	 *  *  0 : object1 = object2
	 *  * +1 : object1 > object2
	 *  * -1 : object1 < object2
	 * */
	virtual int compare(OBJ * object1, OBJ * object2) = 0;

	/*
	 * This method verifies if these 2 objects are equal
	 * */
	virtual bool equals(OBJ * object1, OBJ * object2) = 0;

public:

	void push(OBJ * object) {
		if (head == NULL) {
			head = new LinkedList<OBJ>(object, NULL);
			tail = head;
		} else {
			if (compare(head->getCar(), object) >= 0) {
				pushHead(object);
			} else if (compare(object, tail->getCar()) >= 0) {
				pushTail(object);
			} else pushMid(object);
		}
	}

	/*
	 * Remove head from but but no delete object
	 * */
	OBJ * removeHead() {
		if (head == NULL)
			return NULL;

		OBJ * obj = head->getCar();

		if (tail == head) {
			delete head ;
			head = tail = NULL ;
		} else {
			LinkedList<OBJ> * tmp = head;
			head = head->getCdr() ;
			delete tmp ;
		}

		return obj ;
	}

	/*
	 * Remove from list but no delete object
	 * */
	OBJ * remove(OBJ * object) {
		if (head == NULL)
			return NULL;

		if (equals(object, head->getCar()))
			return removeHead();

		LinkedList<OBJ> * prev = head;
		LinkedList<OBJ> * curr = head->getCdr() ;

		while (curr != NULL && ! equals(object,curr->getCar()))
			curr = (prev = curr)->getCdr() ;

		if (curr == NULL)
			return NULL ;

		prev->setCdr(curr->getCdr()) ;
		OBJ * obj = curr->getCar() ;
		delete curr ;
		return obj ;
	}

public:

	LinkedList<OBJ> * getOBJNode(OBJ * obj)
	{
		if (head == NULL)
			return NULL;

		LinkedList<OBJ> * tmp = head;
		while (!equals(obj, tmp->getCar()))
			tmp = tmp->getCdr() ;
		return tmp;
	}

	LinkedList<OBJ> * getHeadNode()
	{
		return head;
	}

	OBJ * getHeadOBJ() {
		return (head == NULL) ? NULL : head->getCar();
	}

	bool isEmpty() {
		return head == NULL;
	}

private:

	void pushHead(OBJ * object) {
		LinkedList<OBJ> * tmp = new LinkedList<OBJ>(object, NULL);
		tmp->setCdr(head);
		head = tmp;
	}

	void pushTail(OBJ * object) {
		LinkedList<OBJ> * tmp = new LinkedList<OBJ>(object, NULL);
		tail->setCdr(tmp);
		tail = tmp;
	}

	void pushMid(OBJ * object) {
		LinkedList<OBJ> * tmp = head;
		while ( compare(object, tmp->getCdr()->getCar()) == 1 ) {
			tmp = tmp->getCdr();
		}

		LinkedList<OBJ> * tmpObj = new LinkedList<OBJ>(object, tmp->getCdr());
		tmp->setCdr(tmpObj);
	}

}; // class

} // namespace

#endif /* LIB_UTILS_OPCUA_SORTEDLIST_H_ */
