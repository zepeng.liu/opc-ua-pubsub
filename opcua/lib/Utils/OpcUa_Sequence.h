/*
 * OpcUa_Sequence.h
 *
 *  Created on: Feb 13, 2015
 *      Author: chautn
 */

#ifndef LIB_STACKS_SECURITY_OPCUA_SEQUENCE_H_
#define LIB_STACKS_SECURITY_OPCUA_SEQUENCE_H_

#include "../OpcUa.h"
#include "../Threads/OpcUa_Mutex.h"
#include "../Utils/OpcUa_RefCount.h"
namespace opcua {

class Sequence: public RefCount {

private:

	/* page 37, part 6*/
	const static uint32_t MAX_SEQUENCE = 0x00FFFFFF - 1024 ;
	const static uint32_t START_SEQUENCE = 1;


	uint32_t current;

public:

	Sequence()
		: RefCount(),
		  current(START_SEQUENCE)
	{}

public:

	inline uint32_t getCurrent() {
		return current;
	}

public:

	/* This method is used when sending messages.
	 * The messages are divided into several chunks.
	 * Use this method to allocate a block of sequences of chunk for each message */
	uint32_t allocate(int number) {
		uint32_t tmp = current;
		current += number;
		if (current > MAX_SEQUENCE)
			current = START_SEQUENCE;
		return tmp;
	}

	uint32_t next()
	{
		uint32_t tmp = current;
		current += 1;
		return tmp;
	}

	/* This method is used when current > MAX_SEQUENCE.
	 * This method is called only when
	 * 	- receiving the last chunk (with 'F' in TcpHeader)
	 * 	or
	 * 	- the new security channel is created
	 * */
	void tryReset() {
		if (current > MAX_SEQUENCE) {
			current = START_SEQUENCE;
		}
	}

}; // class

} // namespace

#endif /* LIB_STACKS_SECURITY_OPCUA_SEQUENCE_H_ */
