
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_TABLE8_H_
#define OPCUA_TABLE8_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL Table8Byte
	: public TableByte
{
public:

	Table8Byte()
		: TableByte(8)
	{}

public:

	static Table8Byte * fromCtoCpp_8(SOPC_StatusCode * pStatus, SOPC_Byte const * pTable)
	{
		Table8Byte   * res = new Table8Byte() ;
		for (int i = 0 ; i < 8 ; i++)
			res->set(i,Byte::fromCtoCpp(pStatus, pTable[i])) ;
		return res ;
	}

	void fromCpptoC8(SOPC_StatusCode * pStatus, SOPC_Byte * pTable) const
	{
		for (int i = 0 ; i < 8 ; i++)
			dynamic_cast<Byte *>(this->get(i))->fromCpptoC(pStatus, pTable[i]) ;
	}

	bool equals(Table8Byte * other)
	{
		int32_t otherLength = other->getLength() ;
		if (len <= 0 && otherLength <= 0)
			return true ;
		if (len != otherLength)
			return false ;
		for (int32_t i = 0 ; i < len ; i++)
			if (! get(i)->equals(other->get(i)))
				return false ;
		return true ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_TAB8_H_ */
