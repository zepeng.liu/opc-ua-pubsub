/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_HashTableHash_H_
#define OPCUA_HashTableHash_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua
{

template <typename KEY,typename OBJ>
class MYDLL HashTableHashEntry
{
private:

	KEY                         * key ;
	OBJ                         * value ;
	HashTableHashEntry<KEY,OBJ> * next ;

public:

	HashTableHashEntry(
			KEY                         * _key,
			OBJ                         * _value,
			HashTableHashEntry<KEY,OBJ> * _next
			)
		: key(_key),
		  value(_value),
		  next(_next)
	{}

	virtual ~HashTableHashEntry()
	{}

public:

	inline KEY * getKey()
	{
		return key ;
	}

	inline OBJ * getValue()
	{
		return value ;
	}

	inline HashTableHashEntry<KEY,OBJ> * getNext()
	{
		return next ;
	}

public:

	inline void setKey(KEY * _key)
	{
		key = _key ;
	}

	inline void setValue(OBJ * _value)
	{
		value = _value ;
	}

	inline void setNext(HashTableHashEntry<KEY,OBJ> * _next)
	{
		next = _next ;
	}

};


template <typename KEY,typename OBJ> class HashTableHash
{
private:

	uint32_t                       length ;
	HashTableHashEntry<KEY,OBJ> ** table ;

public:

	HashTableHash(uint32_t _length)
	{
		length = _length ;
		table  = new  HashTableHashEntry<KEY,OBJ> *[length] ;
		for (uint32_t i = 0 ; i < length ; i++)
			table[i] = NULL ;
	}

	virtual ~HashTableHash()
	{
		delete[] table ;
	}

public:

	void put(KEY * key, OBJ * value)
	{
		uint32_t idx = (key->hashCode(0) % length) ;
		table[idx] = new HashTableHashEntry<KEY,OBJ>(key,value,table[idx]) ;
		key->take() ;
		value->take() ;
	}

	OBJ * replace(KEY * key, OBJ * value)
	{
		uint32_t idx = (key->hashCode(0) % length) ;
		HashTableHashEntry<KEY,OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (key->equals(entry->getKey())) {
				OBJ * obj = entry->getValue() ;
				entry->setValue(value) ;
				value->take() ;
				obj->decrement() ;
				return obj ;
			}
			entry = entry->getNext() ;
		}
		table[idx] = new HashTableHashEntry<KEY,OBJ>(key,value,table[idx]) ;
		key->take() ;
		value->take() ;
		return NULL ;
	}

	OBJ * get(KEY * key) const
	{
		uint32_t idx = (key->hashCode(0) % length) ;
		HashTableHashEntry<KEY,OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (key->equals(entry->getKey())) {
				return entry->getValue() ;
			}
			entry = entry->getNext() ;
		}
		return NULL ;
	}

	bool exists(KEY * key) const
	{
		uint32_t idx = (key->hashCode(0) % length) ;
		HashTableHashEntry<KEY,OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (key->equals(entry->getKey()))
				return true ;
			entry->getNext() ;
		}
		return false ;
	}

	bool remove(KEY *  key)
	{
		uint32_t idx = (key->hashCode(0) % length) ;
		HashTableHashEntry<KEY,OBJ> * prev = NULL ;
		HashTableHashEntry<KEY,OBJ> * curr = table[idx] ;
		while (curr != NULL) {
			if (key->equals(curr->getKey())) {
				if (prev == NULL)
					table[idx] = curr->getNext() ;
				else
					prev->setNext(curr->getNext()) ;
				curr->getKey()->release() ;
				curr->getValue()->release() ;
				delete curr ;
				return true ;
			}
			prev = curr ;
			curr = curr->getNext() ;
		}
		return false ;
	}

public:

	bool removeOne()
	{
		HashTableHashEntry<KEY,OBJ> * entry ;
		uint32_t i = 0 ;
		while (i < length) {
			if ((entry = table[i]) != NULL) {
				remove(entry->getKey()) ;
				return true ;
			}
			i++ ;
		}
		return false ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_HASHTABLEHASH_H_ */
