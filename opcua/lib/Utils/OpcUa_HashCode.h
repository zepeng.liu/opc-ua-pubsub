
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASHCODE_H_
#define HASHCODE_H_

#include "../OpcUa.h"

#include "mbedtls/sha1.h"


namespace opcua {

class MYDLL HashCode
{
public:

	static uint32_t hashCode(uint32_t result, const uint8_t * buffer, int32_t length)
	{
		return (result ^ (sha1_32(buffer,length))) ;
	}

	static uint64_t hashCode64(uint64_t result, const uint8_t * buffer, int32_t length)
	{
		return (result ^ (sha1_64(buffer,length))) ;
	}

public:

	static uint32_t sha1_32(const uint8_t * buffer, int32_t length)
	{
		unsigned char md[20] ;
		mbedtls_sha1((const unsigned char *) buffer, (size_t) length, md) ;

		uint32_t res = (uint32_t)0 ;

		for (int i = 0 ; i < 20 ; i++)
			res ^= (((uint32_t)md[i]) << ((i % 4) * 8)) ;

		return res ;
	}

	static uint64_t sha1_64(const uint8_t * buffer, int32_t length)
	{
		unsigned char md[20] ;
		mbedtls_sha1((const unsigned char *) buffer, (size_t) length, md) ;

		uint64_t res = (uint64_t)0 ;

		for (int i = 0 ; i < 20 ; i++)
			res ^= (((uint64_t)md[i]) << ((i % 8) * 8)) ;

		return res ;
	}
};

} /* namespace opcua */
#endif /* HASHCODE_H_ */
