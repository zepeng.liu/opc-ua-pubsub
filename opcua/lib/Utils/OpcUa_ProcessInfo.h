
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_PROCESSINFO_H_
#define OPCUA_PROCESSINFO_H_

#include "../OpcUa.h"

#ifdef _WIN32
#else
# include <sys/resource.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <string.h>

namespace opcua {

#ifndef RUSAGE_SELF
# define   RUSAGE_SELF          0
#endif

#ifndef RUSAGE_CHILDREN
# define   RUSAGE_CHILDREN     -1
#endif

class ProcessInfo
{
public:

	ProcessInfo()
	{}

	virtual ~ProcessInfo()
	{}

public:

	static void printProcessInfo(const char * label)
	{
#if (DEBUG_LEVEL & PROC_INFO)
#  ifdef _WIN32
#  else

		struct rusage r_usage ;

		if (getrusage(RUSAGE_SELF,&r_usage) == 0) {
			debug_si(PROC_INFO,"ProcessInfo","label=%s, max resident size=%d",label,(int)(r_usage.ru_maxrss)) ;
		} else {
			debug_si(COM_ERR,"ProcessInfo","Cannot get rusage errmsg=%s errno=%d",strerror(errno),errno) ;
		}

#  endif
#else
		NOT_USED(label) ;
#endif
	}
};

} /* namespace opcua */
#endif /* PROCESSINFO_H_ */
