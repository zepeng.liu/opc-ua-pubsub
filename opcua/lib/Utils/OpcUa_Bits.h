
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BITS_H_
#define OPCUA_BITS_H_

#include "../OpcUa.h"

#ifdef _WIN32
# include "intrin.h"
#endif

namespace opcua {

class MYDLL Bits
{

public:

	inline static uint16_t to_little_endian_uint16(uint16_t data)
	{
#if (IS_BIG_ENDIAN)
		return ((uint16_t)(data >> 8) | (uint16_t)(data << 8)) ;
#else
		return data ;
#endif
	}

	inline static uint16_t from_little_endian_uint16(uint16_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_uint16(data) ;
#else
		return data ;
#endif
	}

public:

	inline static int16_t to_little_endian_int16(int16_t data)
	{
#if (IS_BIG_ENDIAN)
		uint16_t result = to_little_endian_uint16(*(uint16_t *)&data) ;
		return *reinterpret_cast<int16_t *>(&result) ;
#else
		return data ;
#endif
	}

	inline static int16_t from_little_endian_int16(int16_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_int16(data) ;
#else
		return data ;
#endif
	}

public:

	inline static uint32_t to_little_endian_uint32(uint32_t data)
	{
#if (IS_BIG_ENDIAN)
#   ifdef _WIN32
		unsigned long result = _byteswap_ulong(*(unsigned long *)&data) ;
#   else
		uint32_t      result = __builtin_bswap32(data) ;
#   endif
		return *reinterpret_cast<uint32_t *>(&result) ;
#else
		return data ;
#endif
	}

	inline static uint32_t from_little_endian_uint32(uint32_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_uint32(data) ;
#else
		return data ;
#endif
	}

public:

	inline static int32_t to_little_endian_int32(int32_t data)
	{
#if (IS_BIG_ENDIAN)
		uint32_t result = to_little_endian_uint32(*(uint32_t *)&data) ;
		return *reinterpret_cast<int32_t *>(&result) ;
#else
		return data ;
#endif
	}

	inline static int32_t from_little_endian_int32(int32_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_int32(data) ;
#else
		return data ;
#endif
	}

public:


	inline static float to_little_endian_float(float data)
	{
#if (IS_BIG_ENDIAN)
		uint32_t result = to_little_endian_uint32(*(uint32_t *)&data) ;
		return *reinterpret_cast<float *>(&result) ;
#else
		return data ;
#endif
	}

	inline static float from_little_endian_float(float data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_float(data) ;
#else
		return data ;
#endif
	}

public:


	inline static uint64_t to_little_endian_uint64(uint64_t data)
	{
#if (IS_BIG_ENDIAN)
#   ifdef _WIN32
		unsigned __int64  result = _byteswap_ulong(*reinterpret_cast<unsigned __int64 *>(&data)) ;
#   else
		long unsigned int result = __builtin_bswap64(*reinterpret_cast<long unsigned int *>(&data)) ;
#   endif
		return *reinterpret_cast<uint64_t *>(&result) ;
#else
		return data ;
#endif
	}

	inline static uint64_t from_little_endian_uint64(uint64_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_uint64(data) ;
#else
		return data ;
#endif
	}

	inline static int64_t to_little_endian_int64(int64_t data)
	{
#if (IS_BIG_ENDIAN)
		uint64_t result = to_little_endian_uint64(*(uint64_t *)&data) ;
		return *reinterpret_cast<int64_t *>(&result) ;
#else
		return data ;
#endif
	}

	inline static int64_t from_little_endian_int64(int64_t data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_int64(data) ;
#else
		return data ;
#endif
	}

public:


	inline static double to_little_endian_double(double data)
	{
#if (IS_BIG_ENDIAN)
		uint64_t result = to_little_endian_uint64(*(uint64_t *)&data) ;
		return *reinterpret_cast<double *>(&result) ;
#else
		return data ;
#endif
	}

	inline static double from_little_endian_double(double data)
	{
#if (IS_BIG_ENDIAN)
		return to_little_endian_double(data) ;
#else
		return data ;
#endif
	}

};

} /* namespace opcua */
#endif /* BITS_H_ */
