
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_REFCOUNT_H_
#define OPCUA_REFCOUNT_H_

#include "../OpcUa.h"
#include "../Threads/OpcUa_Mutex.h"

namespace opcua {

class MYDLL RefCount
{
private:

	static Mutex * mutex ;

#if (MEM_DBG & DEBUG_LEVEL)
	static int32_t refTotal ;
	static int32_t refAlive ;
#endif

public:

	static inline void printMemoryState()
	{
#if (MEM_DBG & DEBUG_LEVEL)
		debug_ii(MEM_DBG,"RefCount","Creation : %d objects alive (%d objects total)",refAlive,refTotal) ;
#endif
	}

	static inline void printMemoryState(const char *help)
	{
#if (MEM_DBG & DEBUG_LEVEL)
		debug_sii(MEM_DBG,"RefCount","%s : Creation : %d objects alive (%d objects total)",help,refAlive,refTotal) ;
#else
		NOT_USED(help);
#endif
	}

public:

	mutable int refCount ;

public:

	void checkRefCount()
	{
		mutex->lock() ;
		if (refCount <= 0) {
			mutex->unlock() ;
			delete this ;
		} else {
			mutex->unlock() ;
		}
	}

	void take() const
	{
		mutex->lock() ;
		refCount++ ;
		mutex->unlock() ;
	}

	void release() const
	{
		mutex->lock() ;
#if (MEM_DBG & DEBUG_LEVEL)
		if (refCount <= 0) {
			debug(COM_ERR,"RefCount","Bug here : refCount <= 0") ;
		}
#endif
		if (--refCount <= 0) {
			mutex->unlock() ;
			delete this ;
		} else {
			mutex->unlock() ;
		}
	}

	void decrement() const
	{
		mutex->lock() ;
		--refCount ;
		mutex->unlock() ;
	}

	const RefCount * takeAndReturn() const
	{
		mutex->lock() ;
		refCount++ ;
		mutex->unlock() ;
		return this ;
	}

private:

#if (MEM_DBG & DEBUG_LEVEL)
	bool isDeleted ;
#endif

public:

	inline RefCount()
		: refCount(0)
	{
#if (MEM_DBG & DEBUG_LEVEL)
		mutex->lock() ;
		refTotal++ ;
		refAlive++ ;
		isDeleted = false ;
		mutex->unlock() ;
#endif
	}

	virtual ~RefCount()
	{
#if (MEM_DBG & DEBUG_LEVEL)
		mutex->lock() ;
		refAlive-- ;
		if (refCount != 0) {
			debug_i(COM_ERR,"RefCount","Here is the first other bug : %d !",refCount) ;
			debug_ii(COM_ERR,"RefCount","Creation : %d objects alive (%d objects total)",refAlive,refTotal) ;
			CHECK_INT(0) ;
		}
		if (isDeleted) {
			debug_i(COM_ERR,"RefCount","Here is the second other other bug : %d !",refCount) ;
			debug_ii(COM_ERR,"RefCount","Creation : %d objects alive (%d objects total)",refAlive,refTotal) ;
			CHECK_INT(0) ;
		}
		isDeleted = true ;
		mutex->unlock() ;
#endif
	}

};

} /* namespace opcua */
#endif /* REFCOUNT_H_ */
