/*
 * OpcUa_Memory.h
 *
 *  Created on: Apr 14, 2015
 *      Author: chautn
 */

#ifndef OPCUA_MEMORY_H_
#define OPCUA_MEMORY_H_

#include <OpcUa.h>

#include "sys/types.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#ifdef _WIN32
#else
# ifdef __APPLE__
# else
#  include "sys/sysinfo.h"
# endif
#endif

namespace opcua {

class MYDLL Memory {

private:

	static int parseLine(char* line)
	{
        int i = strlen(line);
        while (*line < '0' || *line > '9') line++;
        line[i-3] = '\0';
        i = atoi(line);
        return i;
    }

public:

	/*
	 * VmSize : VIRT
	 * VmRSS  : RES
	 * */
    static int getValue(const char * mName){
#ifdef __WIN32__
    	NOT_USED(mName) ;
    	return 0 ;
#else
# ifdef __APPLE__
    	NOT_USED(mName) ;
    	return 0 ;
# else
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];


        while (fgets(line, 128, file) != NULL){
            if (strncmp(line, mName, strlen(mName)) == 0){
                result = parseLine(line);
                break;
            }
        }
        fclose(file);
        return result;
# endif
#endif
    }

}; // class

} // namespace

#endif /* OPCUA_MEMORY_H_ */
