
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CLOCK_H_
#define OPCUA_CLOCK_H_

#include "../OpcUa.h"

#ifdef _WIN32
# include <Windows.h>
#include <ctime>
#else
# include <sys/time.h>
# include <unistd.h>
# include <time.h>
#endif

namespace opcua
{

class MYDLL Clock
{
public:

	static void getLocalDate(char *date, int length)
	{
		   time_t now = time (NULL);
		   struct tm tm_now = *localtime (&now);
		   strftime (date,(unsigned long)length,"%d/%m/%Y %H:%M:%S", &tm_now);
	}

public:

	static int64_t get_timestamp()
	{
		return (int64_t)(get_microsecondssince1601() / 100) ;
	}

public:

#ifdef _WIN32
	static void sleep(int milliseconds)
	{
			Sleep(milliseconds) ;
	}
#else
	static void sleep(int milliseconds)
	{
			usleep(milliseconds * 1000) ;
	}
#endif

public:

	static uint64_t get_secondssince1970()
	{
		return get_secondssince1601() - 11644473600ULL ;
	}

	static uint64_t get_millisecondssince1970()
	{
		return get_millisecondssince1601() - 11644473600000ULL ;
	}

	static uint64_t get_microsecondssince1970()
	{
		return get_microsecondssince1601() - 11644473600000000ULL ;
	}

public:

	static uint32_t get_secondssince1601()
	{
		return (uint32_t)(get_microsecondssince1601() / 1000000) ;
	}

	static uint64_t get_millisecondssince1601()
	{
		return get_microsecondssince1601() / 1000 ;
	}

	static uint64_t get_microsecondssince1601()
	{
#ifdef _WIN32
		FILETIME ft;
		unsigned long long tt ;
		GetSystemTimeAsFileTime(&ft);
		tt = ft.dwHighDateTime;
		tt <<= 32;
		tt |= ft.dwLowDateTime;
		tt /= 10 ;
		return (uint64_t)(tt) ;
#else
#  define EPOCH_DIFF 11644473600LL
		struct timeval tv;
		long long result = EPOCH_DIFF;
		gettimeofday(&tv,NULL);
		result += tv.tv_sec;
		result *= 1000000;
		result += tv.tv_usec ;
		return (uint64_t)result;
#endif
	}

};     /* class */
}      /* namespace opcua */
#endif /* CLOCK_H_ */
