
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_LINKEDLIST_H_
#define OPCUA_LINKEDLIST_H_

#include "../OpcUa.h"

namespace opcua {

template <typename OBJ> class MYDLL LinkedList
{
private:

	OBJ             * car ;
	LinkedList<OBJ> * cdr ;

public:

	LinkedList(OBJ * _car, LinkedList<OBJ> * _cdr)
		: car(_car),
		  cdr(_cdr)
	{}

	virtual ~LinkedList()
	{}

public:

	void deleteAll()
	{
		LinkedList<OBJ> * cdr = getCdr() ;
		if (cdr != NULL)
			cdr->deleteAll() ;
		delete this ;
	}

	void deleteAllWithCar()
	{
		LinkedList<OBJ> * cdr = getCdr() ;
		if (cdr != NULL)
			cdr->deleteAllWithCar() ;
		delete this->getCar() ;
		delete this ;
	}

public:

	inline OBJ * getCar()
	{
		return car ;
	}

	inline LinkedList<OBJ> * getCdr()
    {
		return cdr ;
    }

	inline void setCdr(LinkedList<OBJ> * _cdr)
	{
		cdr = _cdr ;
	}

	inline void setCar(OBJ* _car)
	{
		car = _car;
	}

public:

	static int32_t getLength(LinkedList<OBJ> * list)
	{
		int32_t len = 0 ;
		while (list != NULL) {
			len += 1 ;
			list = list->getCdr() ;
		}
		return len ;
	}

public:

	static LinkedList<OBJ> * deleteElement(LinkedList<OBJ> * list, OBJ * element)
	{
		if (list == NULL)
			return list ;

		if (list->getCar() == element)  {
			LinkedList<OBJ> * res = list->getCdr() ;
			// Do not delete car
			list->setCdr(NULL) ;
			delete list ;
			return res ;
		}

		LinkedList<OBJ> * prev = list ;
		LinkedList<OBJ> * next ;

		while (prev != NULL) {
			next = prev->getCdr() ;
			if (next == NULL)
				break ;
			if (next->getCar() == element) {
				prev->setCdr(next->getCdr()) ;
				// Do not delete car
				next->setCdr(NULL) ;
				delete next ;
				break ;
			}
			prev = next ;
		}

		return list ;
	}

public:

	static LinkedList<OBJ> * addElement(LinkedList<OBJ> * list, OBJ * element, OBJ * eltAprec)
	{
		if (eltAprec == NULL){
			LinkedList<OBJ> * prev = list;
			while (prev->getCdr() != NULL)
				prev = prev->getCdr();
			LinkedList<OBJ> * linkList
				= new LinkedList<OBJ>(element, NULL);
			prev->setCdr(linkList);
			return list;

		}

		if (list->getCar() == eltAprec){
			LinkedList<OBJ> * linkList
				= new LinkedList<OBJ>(element, list);
			list = linkList;
			return list;
		}

		LinkedList<OBJ> * prev = list ;
		LinkedList<OBJ> * next = prev->getCdr();


		while  (next != NULL &&
				next->getCar() != eltAprec) {
			next = prev->getCdr() ;
			if (next != NULL)
				prev = next ;
		}
		LinkedList<OBJ> * linkList
			= new LinkedList<OBJ>(element, next);
		prev->setCdr(linkList) ;

		return list ;
	}

public:

	inline void append(OBJ * element){
		LinkedList<OBJ> * tmp = this;
		while (tmp->getCdr() != NULL)
			tmp = getCdr();
		LinkedList<OBJ> * linkList
			= new LinkedList<OBJ>(element, NULL);
		tmp->setCdr(linkList);
	}

	inline LinkedList<OBJ> * prepend(OBJ * element){

		LinkedList<OBJ> * linkList
					= new LinkedList<OBJ>(element, this);
		return linkList;
	}


	static OBJ *getElement(LinkedList<OBJ> * list, OBJ * element){
		LinkedList<OBJ> * tmp = list;
		if ( tmp     == NULL ||
			 element == NULL	)
			return NULL;
		while (tmp != NULL &&
			   tmp->getCar() != element )
			tmp = tmp->getCdr();
		if (tmp != NULL)
			return tmp->getCar();
		else
			return NULL;
	}

};

} /* namespace opcua */
#endif /* LIST_H_ */

