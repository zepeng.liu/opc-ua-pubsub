
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_TIMER_H_
#define OPCUA_TIMER_H_

#include "../OpcUa.h"
#include "../Threads/All.h"
#include "OpcUa_Clock.h"

namespace opcua
{

class MYDLL TimerInterface
{
public:
	virtual void onTimerExpires() = 0 ;
};

class MYDLL Timer
	: public Thread
{
private:

	Semaphore      * semaphore ;
	int              milliseconds ;
	bool             ended ;
	TimerInterface * timerInterface ;

public:

	Timer(TimerInterface * _timerInterface)
		: Thread()
	{
		timerInterface = _timerInterface ;
		milliseconds   = 1 ;
		ended          = false ;
		semaphore      = NULL ;

		semaphore = new Semaphore(0) ;
		Thread::start() ;
	}

	virtual ~Timer()
	{
		stop() ;
	}

public:

	void start(int _milliseconds)
	{
		milliseconds = _milliseconds ;
		semaphore->post() ;
	}

	void stop()
	{
		ended = true ;
		semaphore->post() ;
		Thread::join() ;
	}

public:

	void run()
	{
		while (true) {

			if (ended)
				break ;

			semaphore->wait() ;

			if (ended)
				break ;

			Clock::sleep(milliseconds) ;

			if (ended)
				break ;

			timerInterface->onTimerExpires() ;
		}

		if ((semaphore)) {
			delete semaphore ;
			semaphore = NULL ;
		}
	}

};

} /* namespace opcua */
#endif /* OPCUA_TIMER_H_ */
