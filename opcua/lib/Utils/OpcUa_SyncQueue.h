
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SYNCHRONIZEDQUEUE_H_
#define OPCUA_SYNCHRONIZEDQUEUE_H_

#include "../OpcUa.h"
#include "../Threads/All.h"

namespace opcua {

template <typename OBJ> class MYDLL SyncQueue
{
private:

	int          length ;
	OBJ		  ** table ;

	Semaphore  * free_places ;
	Semaphore  * ready_to_be_delivered ;

	int          next_deliver ;
	int          next_enter ;

	int          count ;

	Mutex      * mutex ;

public:

	SyncQueue(int _length)
	{
		length = _length ;

		table = new OBJ *[_length] ;

		free_places           = new Semaphore(length) ;
		ready_to_be_delivered = new Semaphore(0) ;

		next_deliver = 0 ;
		next_enter   = 0 ;

		count = 0 ;

		mutex = new Mutex() ;
	}

	virtual ~SyncQueue()
	{
		setEmptyDelete() ;

		delete[] table ;

		delete free_places ;
		delete ready_to_be_delivered ;

		delete mutex ;
	}

public:

	int getCount()
	{
		mutex->lock();
		int count_buf = count;
		mutex->unlock();
		return count_buf;
	}

	bool isEmpty()
	{
		mutex->lock() ;
		bool result = (count == 0) ;
		mutex->unlock() ;
		return result ;
	}

	bool isNotEmpty()
	{
		mutex->lock() ;
		bool result = (count != 0) ;
		mutex->unlock() ;
		return result ;
	}

public:

	bool isFull()
	{
		mutex->lock() ;
		bool result = (count == length) ;
		mutex->unlock() ;
		return result ;
	}

	bool isNotFull()
	{
		mutex->lock() ;
		bool result = (count != length) ;
		mutex->unlock() ;
		return result ;
	}


public:

	void push(OBJ * obj)
	{

		free_places->wait() ;

	    mutex->lock() ;

	    table[next_enter] = obj ;

	    if (++next_enter == length)
	    	next_enter = 0 ;

	    count++ ;

	    mutex->unlock() ;

	    ready_to_be_delivered->post() ;
	}

	OBJ * pop()
	{
		ready_to_be_delivered->wait() ;

	    mutex->lock() ;

	    OBJ * result = table[next_deliver] ;

	    if (++next_deliver == length)
	    	next_deliver = 0 ;

	    count-- ;

	    mutex->unlock() ;

	    free_places->post() ;

	    return result ;
	  }

public:

	bool tryPush(OBJ * obj)
	{
	    mutex->lock() ;

	    if (count == length) {
	    	mutex->unlock() ;
	    	return false ;
	    }

		free_places->wait() ;

	    table[next_enter] = obj ;

	    if (++next_enter == length)
	    	next_enter = 0 ;

	    count++ ;

	    debug_pp(REF_DBG,"SyncQueue<OBJ>","After push 0x%p (queue %p)",obj,this) ;

	    mutex->unlock() ;

	    ready_to_be_delivered->post() ;

	    return true ;
	  }

	OBJ * tryPop()
	{
	    mutex->lock() ;

	    if (count == 0) {
	    	mutex->unlock() ;
	    	return NULL ;
	    }

		ready_to_be_delivered->wait() ;

	    debug_pp(REF_DBG,"SyncQueue<OBJ>","Before pop 0x%p (queue 0x%p)",table[next_deliver],this) ;

	    OBJ * result = table[next_deliver] ;

	    if (++next_deliver == length)
	    	next_deliver = 0 ;

	    count-- ;

	    mutex->unlock() ;

	    free_places->post() ;

	    return result ;
	  }

public:

	bool timedPush(OBJ * obj, uint32_t timeoutMillis)
	{

		if (!free_places->wait(timeoutMillis))
			return false ;

	    mutex->lock() ;

	    table[next_enter] = obj ;

	    if (++next_enter == length)
	    	next_enter = 0 ;

	    count++ ;

	    debug_pp(REF_DBG,"SyncQueue<OBJ>","After push 0x%p (queue %p)",obj,this) ;

	    mutex->unlock() ;

	    ready_to_be_delivered->post() ;

	    return true ;
	  }


	OBJ * timedPop(uint32_t timeoutMillis)
	{
		if (! ready_to_be_delivered->wait(timeoutMillis))
			return NULL ;

	    mutex->lock() ;

	    OBJ * result = table[next_deliver] ;

	    if (++next_deliver == length)
	    	next_deliver = 0 ;

	    count-- ;

	    mutex->unlock() ;

	    free_places->post() ;

	    return result ;
	  }

private:

	void setEmptyDelete()
	{
		while (next_enter != next_deliver) {
			OBJ * obj = pop() ;
			if (obj != NULL)
				delete obj ;
		}
	}

};

} /* namespace opcua */
#endif /* SYNCHRONIZEDQUEUE_H_ */
