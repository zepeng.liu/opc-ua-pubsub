
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CHECK_H_
#define OPCUA_CHECK_H_

#include "../OpcUa.h"
#include "../Error/OpcUa_Error.h"

namespace opcua {

/*
 * Checks an internal error.
 */

#define CHECK_INT(expr)   if (! (expr)) Error::int_error(__FILE__,__LINE__)
#define OCHECK_INT(expr)  if (! (expr)) opcua::Error::int_error(__FILE__,__LINE__)

/*
 * Checks a library function error.
 */

#define CHECK_LIB(expr)  if (! (expr)) Error::lib_error(__FILE__,__LINE__)

/*
 * Checks a communication error.
 */

#define CHECK_COM(expr)  if (! (expr)) Error::com_error(__FILE__,__LINE__)

}
#endif /* OPCAU_CHECK_H_ */

