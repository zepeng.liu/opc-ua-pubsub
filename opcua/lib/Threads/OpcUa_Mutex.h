
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MUTEX_H_
#define OPCUA_MUTEX_H_

#include "../OpcUa.h"
#include "../Error/OpcUa_Check.h"

#ifdef _WIN32
# include <Windows.h>
#else
# include <unistd.h>
# include <pthread.h>
#endif

namespace opcua
{

class MYDLL Mutex
{
private:

#ifdef _WIN32
	HANDLE          mutex ;
#else
	pthread_mutex_t mutex ;
#endif

public:

	Mutex()
	{
#ifdef _WIN32
		mutex = CreateMutex(NULL,FALSE,NULL) ;
		CHECK_LIB(mutex != NULL) ;
#else
		CHECK_LIB(pthread_mutex_init(&mutex, NULL) == 0) ;
#endif
	}

	virtual ~Mutex()
	{
#ifdef _WIN32
		CHECK_LIB(CloseHandle(mutex) != 0) ;
#else
		CHECK_LIB(pthread_mutex_destroy(&mutex) == 0) ;
#endif
	}

public:

	void lock()
	{
#ifdef _WIN32
		CHECK_LIB(WaitForSingleObject(mutex,INFINITE) == WAIT_OBJECT_0) ;
#else
		CHECK_LIB(pthread_mutex_lock(&mutex) == 0) ;
#endif
	}

public:

	void unlock()
	{
#ifdef _WIN32
		CHECK_LIB(ReleaseMutex(mutex) != 0) ;
#else
	 	CHECK_LIB(pthread_mutex_unlock(&mutex) == 0) ;
#endif
	}

};

} /* namespace opcua */
#endif /* OPCUA_MUTEX_H_ */
