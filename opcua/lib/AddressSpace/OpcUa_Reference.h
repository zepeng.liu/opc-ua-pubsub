
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCE_H_
#define OPCUA_REFERENCE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_Base.h"

/** Just for XML Reference */

namespace opcua {

class MYDLL Reference
{
public:

	static void explore_element(
			QualifiedName           * browseName,
			const char              * file_str,
			node_t                  * node,
			class LinkDescriptions ** pLinkDescriptions
			) ;

private:

	static bool getSource(
			const char    * file_str,
			node_t        * node,
			class NodeId ** pNodeId
			) ;

	static bool getTarget(
			const char    * file_str,
			node_t        * node,
			class NodeId ** pNodeId,
			char         ** pNodeClass
			) ;

};

} /* namespace opcua */
#endif /* OPCUA_REFERENCE_H_ */
