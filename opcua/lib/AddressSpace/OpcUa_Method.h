
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_METHOD_H_
#define OPCUA_METHOD_H_

/*
 * OPC UA Part 3, 5.7, p. 31
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "OpcUa_Base.h"

#include <float.h>
#include <stdio.h>

#ifdef _WIN32
# include <windows.h>
#else
# include <dlfcn.h>
#endif

#ifdef __GNUC__
__extension__
#endif

namespace opcua {

typedef int32_t method_t(
			   NodeId                 * objectId,
               NodeId                 * methodId,
               TableVariant           * inputArguments,
               StatusCode            ** statusCode,
               TableStatusCode       ** inputArgumentResults,
               TableDiagnosticInfo   ** inputArgumentDiagnosticInfos,
               TableVariant          ** outputArguments,
               DiagnosticInfo        ** diagnosticInfo) ;


class MYDLL Method
	: public Base
{
private:

	Boolean         * executable ;     // M
	Boolean         * userExecutable ; // M

	NodeId          * methodDeclarationId ;

	String          * dllName ;

#ifdef _WIN32
	HINSTANCE    dll_handle ;
#else
	void       * dll_handle ;
	const char * dlerr ;
#endif

	method_t * method ;

public:

	Method(
			NodeId		   * _nodeId,           // M
			QualifiedName  * _browseName,       // M
			String         * _symbolicName,
			LocalizedText  * _displayName,      // M
			LocalizedText  * _description,      // O
			UInt32    	   * _writeMask,        // O
			UInt32	       * _userWriteMask,    // O

			Boolean        * _executable,       // M
			Boolean        * _userExecutable,   // M

			NodeId         * _methodDeclarationId,


			String          * _dllName          // M
			)
		: Base(
				_nodeId,             // M
				NodeClass::method_4, // M
				_browseName,         // M
				_symbolicName,
				_displayName,        // M
				_description,        // O
				_writeMask,          // O
				_userWriteMask       // O
				)
	{
		(executable = _executable)->take() ;
		(userExecutable = _userExecutable)->take() ;
		if ((methodDeclarationId = _methodDeclarationId))
			methodDeclarationId->take() ;
		if ((dllName = _dllName))
			dllName->take() ;

		dll_handle = NULL ;
#ifdef _WIN32
#else
		dlerr      = NULL ;
#endif
		method     = NULL ;

		if (dllName != NULL) {
			if (! load_dll()) {
				debug(COM_ERR,"Method","load_dll() function failed") ;
				unload_dll() ;
			}
#ifdef _WIN32
#else
			dlerr = NULL ;
#endif
		}


	}

	virtual ~Method()
	{
		if (dll_handle != NULL) {
			if (! unload_dll()) {
				debug(COM_ERR,"ExternalVariable","unload_dll() function failed") ;
			}
		}

		executable->release() ;
		userExecutable->release() ;
		if (methodDeclarationId != NULL)
			methodDeclarationId->release() ;
		if (dllName != NULL)
			dllName->release() ;
	}

public:

	inline method_t * getMethod()
	{
		return method ;
	}

private:

	bool load_dll()
	{
		const char * str_base = dllName->get() ;

		if (str_base == NULL) {
			debug(COM_ERR,"Method","dll name is NULL") ;
			dll_handle = NULL ;
			return 1 ;
		}

		int str_len  = (int)strlen(str_base) ;

		char * dll_name = new char[str_len+16] ;
		strcpy(dll_name,str_base) ;

#if _WIN32
		strcpy(dll_name+str_len,".dll") ;
#else
# ifdef __APPLE__
		strcpy(dll_name+str_len,".dylib") ;
# else
		strcpy(dll_name+str_len,".so") ;
# endif
#endif

#ifdef _WIN32
		dll_handle = LoadLibrary(TEXT(dll_name)) ;
#else
		dll_handle = dlopen(dll_name,RTLD_LAZY | RTLD_LOCAL) ;
#endif

		delete[] dll_name ;

#ifdef _WIN32
		if (dll_handle == NULL) {
			if (COM_ERR) {
				Error::error_message("Method : dll not loadable",__FILE__,__LINE__,GetLastError()) ;
			}
			return false ;
		}
#else
		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"Method","dll not loadable (dll error = %s) in \"%s\"",dlerr,str_base) ;
			return false ;
		}
		errno = 0 ; // TBD
#endif

#ifdef _WIN32
		method = reinterpret_cast<method_t *>(GetProcAddress(dll_handle,"method")) ;

		if (method == NULL) {
			if (COM_ERR & DEBUG_LEVEL) {
				Error::error_message("Method : cannot find method()",__FILE__,__LINE__,GetLastError()) ;
			}
		}
#else
		method = reinterpret_cast<method_t *>(dlsym(dll_handle,"method")) ;

		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"Method","cannot find method() (dll error = %s) in \"%s\"",dlerr,str_base) ;
		}
#endif

		return true ;
	}


	bool unload_dll()
	{
		method = NULL ;

#ifdef _WIN32
		if (dll_handle == 0) {
			if (FreeLibrary(dll_handle) == 0) {
				if (COM_ERR)
					Error::error_message("Method : cannot unload dll",__FILE__,__LINE__,GetLastError()) ;
			}
		}
#else
		if (dlerr == NULL && dlclose(dll_handle)) {
			debug_ss(COM_ERR,"Method","cannot unload dll (dll error = %s) in \"%s\"",dlerror(),dllName->get()) ;
		}
#endif

		dll_handle = NULL ;

		return true ;
	}



public:

	virtual StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			uint32_t             builtinTypeId,
			BaseDataType       * value
			)
	{
		if (! indexRange->isNullOrEmpty()) {
			// true for all attribute id
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		BaseDataType * tmp ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::putAttribute(attributeId,indexRange,builtinTypeId,value) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
			return StatusCode::Bad_AttributeIdInvalid ;

		case AttributeId_Executable:
			if (builtinTypeId != Builtin_Boolean)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_Executable)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_Executable)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp        = executable ;
			executable = reinterpret_cast<Boolean *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserExecutable:
			if (builtinTypeId != Builtin_Boolean)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_UserExecutable)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_UserExecutable)) {
				return StatusCode::Bad_NotWritable ;
			}
			tmp            = userExecutable ;
			userExecutable = reinterpret_cast<Boolean *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		if (tmp != NULL)
			tmp->release() ;

		return StatusCode::Good ;
	}

public:

	virtual DataValue * getAttribute(
							IntegerId          * attributeId,
							NumericRange       * indexRange,
							Duration           * maxAge,
							TimestampsToReturn * timestampsToReturn
						)
	{
		if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
			return DataValue::Bad_IndexRangeInvalid ;
		}

		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
			return DataValue::Bad_AttributeIdInvalid ;

		case AttributeId_Executable:
			getMutex()->lock() ;
			if ((data = executable))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserExecutable:
			getMutex()->lock() ;
			if ((data = userExecutable))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		default:
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_BOTH_2:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		DataValue * dataValue
				= new DataValue(
						(data==NULL)?(reinterpret_cast<Variant *>(NULL)):(data->getVariant()),
						StatusCode::Good,
						NULL, serverTimestamp
						) ;

		if (data != NULL)
			data->release() ;

		return dataValue ;
	}

public:

	static Method * explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			class LinkDescriptions ** pLinkDescriptions
			) ;

};

} /* namespace opcua */
#endif /* OPCUA_METHOD_H_ */
