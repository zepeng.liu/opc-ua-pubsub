
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BASE_H_
#define OPCUA_BASE_H_

/*
 * OPC UA PART 3, 5.2, P. 13
 * OPC UA Part 5, 5.1, p. 5
 */

#include "../OpcUa.h"

#include "../Utils/OpcUa_RefCount.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "../CommonParametersTypes/OpcUa_IPCS_DataValue.h"
#include "../CommonParametersTypes/OpcUa_IPCS_IntegerId.h"
#include "../CommonParametersTypes/OpcUa_ReadValueIdDefine.h"
#include "../CommonParametersTypes/OpcUa_IPCS_ReadValueId.h"
#include "../CommonParametersTypes/OpcUa_IPCS_TimestampsToReturn.h"
#include "../CommonParametersTypes/OpcUa_IPCS_NumericRange.h"
#include "../Threads/OpcUa_Mutex.h"

#include "All/OpcUa_ReferenceLink.h"
#include "All/OpcUa_ReferenceLinkList.h"

namespace opcua {

class MYDLL Base
	: public RefCount
{
private:

	NodeId		  * nodeId ;        // M
	NodeClass	  * nodeClass ;     // M
	QualifiedName * browseName ;    // M
	String		  * symbolicName ;  //
	LocalizedText * displayName ;   // M
	LocalizedText * description ;   // O

protected:

	UInt32        * writeMask ;     // O
	UInt32	      * userWriteMask ; // O

private:

	ReferenceLinkList * forward ;
	ReferenceLinkList * backward ;

private:

	static Mutex * mutex ;

public:

	Base(
			NodeId		  * _nodeId,        // M
			NodeClass	  * _nodeClass,     // M
			QualifiedName * _browseName,    // M
			String        * _symbolicName,  //
			LocalizedText * _displayName,   // M
			LocalizedText * _description,   // O
			UInt32    	  * _writeMask,     // O
			UInt32	      * _userWriteMask  // O
			)
		: RefCount(),
		  forward(NULL),
		  backward(NULL)
	{
		(nodeId = _nodeId)->take() ;
		(nodeClass = _nodeClass)->take() ;
		(browseName = _browseName)->take() ;
		if ((symbolicName = _symbolicName))
			symbolicName->take() ;
		(displayName = _displayName)->take() ;
		if ((description = _description))
			description->take() ;
		if ((writeMask = _writeMask))
			writeMask->take() ;
		if ((userWriteMask = _userWriteMask))
			userWriteMask->take() ;
	}

	virtual ~Base()
	{
		nodeId->release() ;
		nodeClass->release() ;
		browseName->release() ;
		if (symbolicName != NULL)
			symbolicName->release() ;
		displayName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;
		if (forward != NULL)
			delete forward ;
		if (backward != NULL)
			delete backward ;
	}

public:

	inline NodeId * getNodeId()
	{
		return nodeId ;
	}

	inline NodeClass * getNodeClass()
	{
		return nodeClass ;
	}

	inline QualifiedName * getBrowseName()
	{
		return browseName ;
	}

	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

public:

	bool equals(QualifiedName * browseName)
	{
		return this->browseName->equals(browseName) ;
	}

public:

	inline bool hasNodeClass(NodeClass * _nodeClass)
	{
		return nodeClass->equals(_nodeClass) ;
	}

public:

	inline Mutex * getMutex()
	{
		return mutex ;
	}

public:

	inline ReferenceLinkList * getForward()
	{
		return forward ;
	}

	inline ReferenceLinkList * getBackward()
	{
		return backward ;
	}

public:

	inline bool hasComponent(Base * target)
	{
		return hasForward(OpcUaRef_HasComponent,target) ;
	}

private:

	bool hasForward(uint8_t linkId, Base * target)
	{
		ReferenceLinkList * current = forward ;
		while (current != NULL) {
			ReferenceLink * car = current->getCar() ;
			if (car->getLinkId() == linkId && car->getTarget() == target)
				return true ;
			current = current->getCdr() ;
		}
		return false ;
	}

public:

	inline ExpandedNodeId * hasTypeDefinition()
	{
		return hasForward(OpcUaRef_HasTypeDefinition) ;
	}

private:

	ExpandedNodeId * hasForward(uint32_t linkId)
	{
		ReferenceLinkList * current = forward ;
		while (current != NULL) {
			ReferenceLink * car = current->getCar() ;
			if (car->getLinkId() == linkId) {
				Base * target = car->getTarget() ;
				if (target != NULL)
					return new ExpandedNodeId(target->getNodeId()) ;
			}
			current = current->getCdr() ;
		}
		return ExpandedNodeId::nullExpandedNodeId ;
	}

public:

	void setForward(ReferenceLink * link)
	{
		forward = new ReferenceLinkList(link,forward) ;
	}

	void unsetForward(ReferenceLink * link)
	{
		forward = ReferenceLinkList::deleteElement(forward,link) ;
	}

	void setBackward(ReferenceLink * link)
	{
		backward = new ReferenceLinkList(link,backward) ;
	}

	void unsetBackward(ReferenceLink * link)
	{
		backward = ReferenceLinkList::deleteElement(backward,link) ;
	}

public:

	StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			DataValue          * dataValue
			)
	{
//		if (dataValue->getSourceTimeStamp() != NULL) {
//			return StatusCode::Bad_NotImplemented ;
//		}
//
//		if (dataValue->getServerTimeStamp() != NULL) {
//			return StatusCode::Bad_NotImplemented ;
//		}

		Variant * variant = dataValue->getVariantAttr() ;

		return
			putAttribute(
					attributeId,
					indexRange,
					variant->getBuiltinTypeId(),
					variant->getValue()
					) ;
	}

public:

	virtual StatusCode * putAttribute(
				IntegerId    * attributeId,
				NumericRange * indexRange,
				uint32_t       builtinTypeId,
				BaseDataType * value
			)
	{
		if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
			// true for all attribute id
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		BaseDataType * tmp = NULL ;

		switch (attributeId->get())
		{

		case AttributeId_NodeId:
			if (builtinTypeId != Builtin_NodeId)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_NodeId)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_NodeId)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp    = nodeId ;
			nodeId = dynamic_cast<NodeId *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_NodeClass:
			debug(COM_ERR,"Base","Attempt to write the NodeClass attribute") ;
			break ;

		case AttributeId_BrowseName:
			if (builtinTypeId != Builtin_QualifiedName)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_BrowseName)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_BrowseName)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp        = browseName ;
			browseName = dynamic_cast<QualifiedName *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_DisplayName:
			if (builtinTypeId != Builtin_LocalizedText)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_DisplayName)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_DisplayName)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp         = displayName ;
			displayName = dynamic_cast<LocalizedText *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Description:
			if (builtinTypeId != Builtin_LocalizedText)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_Description)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_Description)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp         = description ;
			description = dynamic_cast<LocalizedText *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_WriteMask:
			if (builtinTypeId != Builtin_UInt32)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_WriteMask)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_WriteMask)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp       = writeMask ;
			writeMask = dynamic_cast<UInt32 *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserWriteMask:
			if (builtinTypeId != Builtin_UInt32)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_UserWriteMask)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_UserWriteMask)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp           = userWriteMask ;
			userWriteMask = dynamic_cast<UInt32 *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		if (tmp != NULL)
			tmp->release() ;

		return StatusCode::Good ;
	}

public:

	virtual DataValue * getAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			Duration           * maxAge,
			TimestampsToReturn * timestampsToReturn
			)
	{
		if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
			// true for all attribute id
			return DataValue::Bad_IndexRangeInvalid ;
		}

		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
			getMutex()->lock() ;
			if ((data = nodeId))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_NodeClass:
			getMutex()->lock() ;
			if ((data = nodeClass))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_BrowseName:
			getMutex()->lock() ;
			if ((data = browseName))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_DisplayName:
			getMutex()->lock() ;
			if ((data = displayName))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Description:
			getMutex()->lock() ;
			if ((data = description))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_WriteMask:
			getMutex()->lock() ;
			if ((data = writeMask))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserWriteMask:
			getMutex()->lock() ;
			if ((data = userWriteMask))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_BOTH_2:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			NOT_USED(indexRange) ; // for compiler
			NOT_USED(maxAge)     ; // for compiler
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		DataValue * dataValue
				= new DataValue(
						(data==NULL)?(NULL):(data->getVariant()),
						StatusCode::Good,
						NULL, serverTimestamp
						) ;

		if (data != NULL)
			data->release() ;

		return dataValue ;
	}

protected: // Used in Variable and ExternalVariable

	bool timesMatch(uint32_t builtinTypeId, NodeId * dataType)
	{
	  if (dataType->getNamespaceIndex()->isZero()) {

	    if (dataType->getIdentifierType()->equals(IdType_NUMERIC_0)) {

	      uint32_t identifier = dataType->getNumeric()->get() ; // DataType of the Value

//	      fprintf(stderr,"timesMatch(%d,%d)\n",builtinTypeId,identifier) ;

	      if (identifier == builtinTypeId)
	    	  return true ;

	      switch (builtinTypeId) { // New value
	      case  Builtin_SByte:
	    	  return
					  identifier==Builtin_Int16 ||
					  identifier==Builtin_Int32 ||
					  identifier==Builtin_Int64 ;
	      case  Builtin_Int16:
	    	  return
					  identifier==Builtin_Int32 ||
					  identifier==Builtin_Int64 ;
	      case  Builtin_Int32:
	    	  return
					  identifier==Builtin_Int64 ;
	      case  Builtin_Byte:
	    	  return
	    			  identifier==Builtin_Byte   ||
					  identifier==Builtin_UInt16 ||
					  identifier==Builtin_UInt32 ||
					  identifier==Builtin_UInt64 ;
	      case  Builtin_UInt16:
	    	  return
					  identifier==Builtin_UInt32 ||
					  identifier==Builtin_UInt64 ;
	      case  Builtin_UInt32:
	    	  return
					  identifier==Builtin_UInt64 ;
	      }
	    } else {
//		    if (dataType->getIdentifierType()->equals(IdType_STRING_1)) {
//		    	fprintf(stderr,"timesMatch(%d,\"%s\")\n",builtinTypeId,dataType->getString()->get()) ;
//		    } else {
//		    	fprintf(stderr,"timesMatch : dataType not numeric and not string\n") ;
//		    }
	    }
	  } else {
//		  fprintf(stderr,"timesMatch : dataType with non zero namespaceindex\n") ;
	  }
//	  CHECK_INT(0) ;
	  return false ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_BASE_H_ */
