
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCELINK_H_
#define OPCUA_REFERENCELINK_H_

#include "../../OpcUa.h"

namespace opcua {

#define OpcUaRef_References					((uint32_t )     31)
#define OpcUaRef_NonHierarchicalReferences	((uint32_t )     32)
#define OpcUaRef_HierarchicalReferences	    ((uint32_t )     33)
#define OpcUaRef_HasChild					((uint32_t )     34)
#define OpcUaRef_Organizes              	((uint32_t )     35)
#define OpcUaRef_HasEventSource            	((uint32_t )     36)
#define OpcUaRef_HasModellingRule         	((uint32_t )     37)
#define OpcUaRef_HasEncoding	         	((uint32_t )     38)
#define OpcUaRef_HasDescription	         	((uint32_t )     39)
#define OpcUaRef_HasTypeDefinition	        ((uint32_t )     40)
#define OpcUaRef_GeneratesEvent		        ((uint32_t )     41)
#define OpcUaRef_Aggregates			        ((uint32_t )     44)
#define OpcUaRef_HasSubtype			        ((uint32_t )     45)
#define OpcUaRef_HasProperty			    ((uint32_t )     46)
#define OpcUaRef_HasComponent         	  	((uint32_t )     47)
#define OpcUaRef_HasNotifier         	  	((uint32_t )     48)
#define OpcUaRef_HasOrderedComponent    	((uint32_t )     49)
#define OpcUaRef_FromState   	    		((uint32_t )     51)
#define OpcUaRef_ToState	   	    		((uint32_t )     52)
#define OpcUaRef_HasCause	   	    		((uint32_t )     53)
#define OpcUaRef_HasEffect	   	    		((uint32_t )     54)
#define OpcUaRef_HasHistoricalConfiguration	((uint32_t )     56)
#define OpcUaRef_HasSubStateMachine	    	((uint32_t )    117)
#define OpcUaRef_AlwaysGeneratesEvent  		((uint32_t )   3065)
#define OpcUaRef_HasTrueSubState   			((uint32_t )   9004)
#define OpcUaRef_HasFalseSubState   		((uint32_t )   9005)
#define OpcUaRef_HasCondition		   		((uint32_t )   9006)
#define OpcUaRef_HasPubSubConnection		((uint32_t )  14476)
#define OpcUaRef_DataSetToWriter			((uint32_t )  14936)
#define OpcUaRef_HasDataSetWriter			((uint32_t )  15296)
#define OpcUaRef_HasDataSetReader			((uint32_t )  15297)
#define OpcUaRef_HasAlarmSuppressionGroup	((uint32_t )  16361)
#define OpcUaRef_AlarmGroupMember			((uint32_t )  16362)


class MYDLL ReferenceLink
{
public:

	static class ReferenceType * Organizes ;
	static class ReferenceType * HasComponent ;
	static class ReferenceType * HasOrderedComponent ;
	static class ReferenceType * HasProperty ;
	static class ReferenceType * HasSubtype ;
	static class ReferenceType * HasModellingRule ;
	static class ReferenceType * HasTypeDefinition ;
	static class ReferenceType * HasEncoding ;
	static class ReferenceType * HasDescription ;
	static class ReferenceType * HasEventSource ;
	static class ReferenceType * HasNotifier ;
	static class ReferenceType * GeneratesEvent ;
	static class ReferenceType * AlwaysGeneratesEvent ;

	static class ReferenceType * fromCode(uint32_t  code) ;

private:

	static class ReferenceType * getReferenceType(class ReferenceType ** pReferenceType, uint32_t code) ;

public:

	static uint32_t  getCode(const char * type)
	{
		if (type == NULL)
			return (uint32_t ) 0 ;

		uint32_t  type_flag = (uint32_t )0 ;

		if (strcmp(type,"Organizes") == 0) {
			type_flag = OpcUaRef_Organizes ;
		} else if (strcmp(type,"HasComponent") == 0) {
			type_flag = OpcUaRef_HasComponent ;
		} else if (strcmp(type,"HasOrderedComponent") == 0) {
			type_flag = OpcUaRef_HasOrderedComponent ;
		} else if (strcmp(type,"HasProperty") == 0) {
			type_flag = OpcUaRef_HasProperty ;
		} else if (strcmp(type,"HasSubtype") == 0) {
			type_flag = OpcUaRef_HasSubtype ;
		} else if (strcmp(type,"HasModellingRule") == 0) {
	    	type_flag = OpcUaRef_HasModellingRule ;
	    } else if (strcmp(type,"HasTypeDefinition") == 0) {
	    	type_flag = OpcUaRef_HasTypeDefinition ;
	    } else if (strcmp(type,"HasEncoding") == 0) {
	    	type_flag = OpcUaRef_HasEncoding ;
	    } else if (strcmp(type,"HasDescription") == 0) {
			type_flag = OpcUaRef_HasDescription ;
		} else if (strcmp(type,"HasEventSource") == 0) {
			type_flag = OpcUaRef_HasEventSource ;
		} else if (strcmp(type,"HasNotifier") == 0) {
			type_flag = OpcUaRef_HasNotifier ;
		} else if (strcmp(type,"GeneratesEvent") == 0) {
	    	type_flag = OpcUaRef_GeneratesEvent ;
	    } else if (strcmp(type,"AlwaysGeneratesEvent") == 0) {
	    	type_flag = OpcUaRef_AlwaysGeneratesEvent ;
	    } else if (strcmp(type,"HasTrueSubState") == 0) {
	    	type_flag = OpcUaRef_HasTrueSubState ;
	    } else if (strcmp(type,"FromState") == 0) {
	    	type_flag = OpcUaRef_FromState ;
	    } else if (strcmp(type,"ToState") == 0) {
	    	type_flag = OpcUaRef_ToState ;
	    } else if (strcmp(type,"HasEffect") == 0) {
	    	type_flag = OpcUaRef_HasEffect ;
	    } else if (strcmp(type,"HasCause") == 0) {
	    	type_flag = OpcUaRef_HasCause ;
	    } else if (strcmp(type,"HasPubSubConnection") == 0) {
	    	type_flag = OpcUaRef_HasPubSubConnection ;
	    } else if (strcmp(type,"DataSetToWriter") == 0) {
	    	type_flag = OpcUaRef_DataSetToWriter ;
	    } else if (strcmp(type,"HasDataSetWriter") == 0) {
	    	type_flag = OpcUaRef_HasDataSetWriter ;
	    } else if (strcmp(type,"HasDataSetReader") == 0) {
	    	type_flag = OpcUaRef_HasDataSetReader ;
	    } else {
	    	debug_s(COM_ERR,"ReferenceLink","uint32_t  getCode(\"%s\") not a reference name",type) ;
	    	CHECK_INT(0) ;
	    }

		return type_flag ;
	}

private:

	uint32_t     linkId ;
	class Base * source ;
	class Base * target ;

public:

	ReferenceLink(
			uint32_t     _linkId,
			class Base * _source,
			class Base * _target
			) ;

	virtual ~ReferenceLink() ;

public:

	inline uint32_t  getLinkId()
	{
		return linkId ;
	}

	inline Base * getSource()
	{
		return source ;
	}

	inline Base * getTarget()
	{
		return target ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_REFERENCELINK_H_ */
