
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCELINKLIST_H_
#define OPCUA_REFERENCELINKLIST_H_

#include "OpcUa_ReferenceLink.h"
#include "../../Utils/OpcUa_LinkedList.h"

namespace opcua {

class MYDLL ReferenceLinkList
	: public LinkedList<ReferenceLink>
{
public:

	ReferenceLinkList(ReferenceLink * _car, ReferenceLinkList * _cdr)
		: LinkedList<ReferenceLink>(_car,_cdr)
    {}

	virtual ~ReferenceLinkList()
	{
		ReferenceLinkList * cdr = getCdr() ;
		if (cdr != NULL)
			delete cdr ;
	}

public:

	inline ReferenceLinkList * getCdr() { return (ReferenceLinkList *)LinkedList<ReferenceLink>::getCdr() ; }

public:

	static ReferenceLinkList * deleteElement(ReferenceLinkList * list, ReferenceLink * element)
	{
		return (ReferenceLinkList *)LinkedList<ReferenceLink>::deleteElement(list,element) ;
	}
};


} /* namespace opcua */
#endif /* REFERENCELINKLIST_H_ */
