
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIABLEDEFINE_H_
#define OPCUA_VARIABLEDEFINE_H_

namespace opcua {

#define AccessLevel_CurrentRead         ((uint8_t) 1)
#define AccessLevel_CurrentWrite        ((uint8_t) 1*2)
#define AccessLevel_HistoryRead         ((uint8_t) 1*2*2)
#define AccessLevel_HistoryWrite        ((uint8_t) 1*2*2*2)
#define AccessLevel_SemanticChange      ((uint8_t) 1*2*2*2*2)

#define UserAccessLevel_CurrentRead     ((uint8_t) 1)
#define UserAccessLevel_CurrentWrite    ((uint8_t) 1*2)
#define UserAccessLevel_HistoryRead     ((uint8_t) 1*2*2)
#define UserAccessLevel_HistoryWrite    ((uint8_t) 1*2*2*2)

}

#endif /* VARIABLEDEFINE_H_ */
