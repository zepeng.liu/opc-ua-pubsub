
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC UA Part 3, 5.6.5, p. 29
 */

#include "OpcUa_VariableType.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "../Utils/OpcUa_StringUtils.h"

namespace opcua {

MYDLL VariableType * VariableType::explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			LinkDescriptions       ** pLinkDescriptions
			)
	{
		String * _browseNameStr = browseName->toString() ;
		_browseNameStr->take() ;

		debug_ss(XML_DBG,"VariableType","Configuration file \"%s\", VariableType = \"%s\"",file_str,_browseNameStr->get()) ;

		if (NodesTable::bad_ns(node,file_str)) {
			_browseNameStr->release() ;
			return NULL ;
		}

		String        * symbolicName  = NULL ;
		LocalizedText * displayName	  = NULL ;
		LocalizedText * description   = NULL ;
		UInt32        * writeMask     = NULL ;
		UInt32        * userWriteMask = NULL ;

		BaseDataType     * value           = NULL ;
		NodeId           * dataType        = NULL ;
		Int32            * valueRank       = NULL ;
		TableUInt32      * arrayDimensions = NULL ;
		Boolean          * isAbstract      = NULL ;

		VariableType * result = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"VariableType","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMask == NULL) {
				if ((writeMask = UInt32::get_WriteMask_element(_browseNameStr ,file_str,chld))) {
					writeMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMask == NULL) {
				if ((userWriteMask = UInt32::get_UserWriteMask_element(_browseNameStr ,file_str,chld))) {
					userWriteMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"DataType") == 0 && dataType == NULL) {
				if ((dataType = NodeId::get_NodeId_element(file_str, chld))) {
					dataType->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"DataValue") == 0 && value == NULL && dataType == NULL) {
				if ((value = BaseDataType::get_BaseDataType_element(&dataType,file_str,chld))) {
					value->take() ;
					dataType->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"ValueRank") == 0 && valueRank == NULL) {
				if ((valueRank = Int32::get_Int32_element(file_str,chld))) {
					valueRank->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"ArrayDimensions") == 0 && arrayDimensions == NULL) {
				if ((arrayDimensions = UInt32::get_UInt32Array_element(file_str,chld))) {
					arrayDimensions->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"IsAbstract") == 0 && isAbstract == NULL) {
				if ((isAbstract = Boolean::get_Boolean_element(file_str,chld))) {
					isAbstract->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasProperty") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasComponent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasSubtype") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"GeneratesEvent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else {
				debug_sss(COM_ERR,"VariableType","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s\"",file_str,name,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		if (displayName == NULL) {
			(displayName = new LocalizedText(_browseNameStr,LocaleId::en))->take() ;
		}

		if (dataType == NULL) {
			debug_ss(COM_ERR,"VariableType","Configuration file \"%s\", Error : no DataType in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (valueRank == NULL) {
			debug_ss(COM_ERR,"VariableType","Configuration file \"%s\", Error : no ValueRank in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (isAbstract == NULL) {
			debug_ss(COM_ERR,"VariableType","Configuration file \"%s\", Error : no IsAbstract in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		if (valueRank->get() <= 0) {
			if (arrayDimensions != NULL) {
				debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank <= 0 but arrayDimensions not null in \"%s\"",file_str,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
				goto error ;
			}
		} else {
			if (arrayDimensions != NULL) {
				if (valueRank->notEquals(arrayDimensions->getLength())) {
					debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank>0 but arrayDimensions!=valueRank in \"%s\"",file_str,_browseNameStr->get()) ;
					NodesTable::print(file_str,node,1000) ;
					goto error ;
				}
			} else {
				debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank > 0 but arrayDimensions is null in \"%s\"",file_str,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
				goto error ;
			}
		}

		debug_ss(XML_DBG,"VariableType","Configuration file \"%s\", registering element \"%s\"",file_str,_browseNameStr->get()) ;

		result =
				new VariableType(
						nodeId,
						browseName,
						symbolicName,
						displayName,
						description,
						writeMask,
						userWriteMask,

						value,
						dataType,
						valueRank,
						arrayDimensions,
						isAbstract
				) ;

	error:

		if (symbolicName != NULL)
			symbolicName->release() ;
		if (displayName != NULL)
			displayName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;

		if (value != NULL)
			value->release() ;
		if (dataType != NULL)
			dataType->release() ;
		if (valueRank != NULL)
			valueRank->release() ;
		if (arrayDimensions != NULL)
			arrayDimensions->release() ;
		if (isAbstract != NULL)
			isAbstract->release() ;

		if (_browseNameStr != NULL)
			_browseNameStr->release() ;

		return result ;
	}


}


