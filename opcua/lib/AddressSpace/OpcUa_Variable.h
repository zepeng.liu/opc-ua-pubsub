
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIABLE_H_
#define OPCUA_VARIABLE_H_

/*
 * OPC UA Part 3, 5.6, p. 23
 */

#include "../OpcUa.h"

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_Clock.h"

#include "OpcUa_Base.h"
#include "OpcUa_VariableDefine.h"

#include <float.h>
#include <string.h>
#include <stdlib.h>

namespace opcua {

class MYDLL Variable
	: public Base
{
private:

	NodeId           * parentNodeId ;            // Part6, F6
	BaseDataType     * value ;                   // M
	NodeId           * dataType ;                // M
	Int32            * valueRank ;               // M
	TableUInt32      * arrayDimensions ;         // O

protected:

	Byte             * accessLevel ;             // M
	Byte             * userAccessLevel ;         // M

private:

	Duration         * minimumSamplingInterval ; // O
	Boolean          * historizing ;             // M

public:

	Variable(
			NodeId		   * _nodeId,        // M
			QualifiedName  * _browseName,    // M
			String         * _symbolicName,  //
			LocalizedText  * _displayName,   // M
			LocalizedText  * _description,   // O
			UInt32    	   * _writeMask,     // O
			UInt32	       * _userWriteMask, // O

			NodeId           * _parentNodeId,
			BaseDataType     * _value,                   // M
			NodeId           * _dataType,                // M
			Int32            * _valueRank,               // M
			TableUInt32      * _arrayDimensions,         // O
			Byte             * _accessLevel,             // M
			Byte             * _userAccessLevel,         // M
			Duration         * _minimumSamplingInterval, // O
			Boolean          * _historizing              // M
			)
		: Base(
				_nodeId,               // M
				NodeClass::variable_2, // M
				_browseName,           // M
				_symbolicName,
				_displayName,          // M
				_description,          // O
				_writeMask,            // O
				_userWriteMask         // O
				)
	{
		if ((parentNodeId = _parentNodeId))
			parentNodeId->take() ;
		(value = _value)->take() ;
		(dataType = _dataType)->take() ;
		(valueRank = _valueRank)->take() ;
		if ((arrayDimensions = _arrayDimensions))
			arrayDimensions->take() ;
		(accessLevel = _accessLevel)->take() ;
		(userAccessLevel = _userAccessLevel)->take() ;
		if ((minimumSamplingInterval = _minimumSamplingInterval))
			minimumSamplingInterval->take() ;
		(historizing = _historizing)->take() ;
	}

	virtual ~Variable()
	{
		if ((parentNodeId))
			parentNodeId->release() ;
		value->release() ;
		dataType->release() ;
		valueRank->release() ;
		if ((arrayDimensions))
			arrayDimensions->release() ;
		accessLevel->release() ;
		userAccessLevel->release() ;
		if ((minimumSamplingInterval))
			minimumSamplingInterval->release() ;
		historizing->release() ;
	}

protected:

	inline BaseDataType * getValue()
	{
		return value ;
	}

	inline void setValue(BaseDataType * _value)
	{
		BaseDataType * tmpValue = value ;
		if ((value = _value))
			_value->take() ;
		if (tmpValue != NULL)
			tmpValue->release() ;
	}

	inline Byte * getAccessLevel()
	{
		return accessLevel ;
	}

	inline Byte * getUserAccessLevel()
	{
		return userAccessLevel ;
	}

	inline NodeId * getDataType()
	{
		return dataType ;
	}

public:

	virtual StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			uint32_t              builtinTypeId,
			BaseDataType       * newValue
			)
	{
		BaseDataType * tmp = NULL ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::putAttribute(attributeId,indexRange,builtinTypeId,newValue) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			return StatusCode::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			getMutex()->lock() ;
			if (userAccessLevel->isSet(UserAccessLevel_CurrentWrite)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (accessLevel != NULL && accessLevel->isUnset(AccessLevel_CurrentWrite)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			if (timesMatch(builtinTypeId,dataType)) {
				if (indexRange->isNullOrEmpty()) {
					tmp   = value ;
					value = reinterpret_cast<BaseDataType *>(newValue) ;
					if (newValue != NULL)
						newValue->take() ;
					getMutex()->unlock() ;
				} else {
					StatusCode * rc = checkArray(indexRange,newValue) ;
					if (rc != NULL) {
						getMutex()->unlock() ;
						return rc ;
					}
					setArray(indexRange,newValue) ;
					getMutex()->unlock() ;
				}
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_TypeMismatch ;
			}
			break ;

		case AttributeId_DataType:
			if (builtinTypeId != Builtin_NodeId)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_DataType)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_DataType)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp      = dataType ;
			dataType = reinterpret_cast<NodeId *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ValueRank:
			if (builtinTypeId != Builtin_Int32)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_ValueRank)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_ValueRank)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp       = valueRank ;
			valueRank = reinterpret_cast<Int32 *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ArrayDimensions:
			if (builtinTypeId != Builtin_UInt32)
				return StatusCode::Bad_TypeMismatch ;
			if (! newValue->isArray())
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_AccessLevel)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_AccessLevel)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp             = arrayDimensions ;
			arrayDimensions = reinterpret_cast<TableUInt32   *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_AccessLevel:
			if (builtinTypeId != Builtin_Byte)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_AccessLevel)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_AccessLevel)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp         = accessLevel ;
			accessLevel = reinterpret_cast<Byte *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserAccessLevel:
			if (builtinTypeId != Builtin_Byte)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_UserAccessLevel)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_UserAccessLevel)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp             = userAccessLevel ;
			userAccessLevel = reinterpret_cast<Byte *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_MinimumSamplingInterval:
			if (builtinTypeId != Builtin_Duration)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_MinimumSamplingInterval)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_MinimumSamplingInterval)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp                     = minimumSamplingInterval ;
			minimumSamplingInterval = reinterpret_cast<Duration *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Historizing:
			if (builtinTypeId != Builtin_Boolean)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_Historizing)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_Historizing)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp         = historizing ;
			historizing = reinterpret_cast<Boolean *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		if (tmp != NULL)
			tmp->release() ;

		return StatusCode::Good ;
	}

protected:

	StatusCode * checkArray(NumericRange * indexRange, BaseDataType * newValue)
	{
		debug_p(ARRAY_DBG,"Variable","checkArray: step 0 : valueRank = %p",valueRank) ;

		if (valueRank == NULL)
			return StatusCode::Bad_NotImplemented ;

		/* valueRank is not null */

		int32_t valueRankInt32 = valueRank->get() ;

		debug_i(ARRAY_DBG,"Variable","checkArray: step 1 : valueRankInt32 = %d",valueRankInt32) ;

		if (valueRankInt32 < 1)
			return StatusCode::Bad_NotImplemented ;

		/* valueRankInt32 >= 1 */

		debug_p(ARRAY_DBG,"Variable","checkArray: step 2 : arrayDimensions = %p",arrayDimensions) ;

		if (arrayDimensions == NULL)
			return StatusCode::Bad_NotImplemented ;

		/* arrayDimensions is not NULL */

		debug_i(ARRAY_DBG,"Variable","checkArray: step 3 : arrayDimensions length = %d",arrayDimensions->getLength()) ;

		if (arrayDimensions->getLength() != valueRankInt32)
			return StatusCode::Bad_NotImplemented ;

		/* arrayDimensions has length valueRankInt32 */

		debug(ARRAY_DBG,"Variable","checkArray: step 4 : computing somDim") ;

		int32_t somDim = 1 ;
		for (int i = 0 ; i < valueRankInt32 ; i ++) {
			int32_t dim = arrayDimensions->get(i)->get() ;
			if (dim <= 0) {
				debug_ii(ARRAY_DBG,"Variable","checkArray: step 4.a : arrayDimensions[i] = %d < 0",i,dim) ;
				return StatusCode::Bad_NotImplemented ;
			}
			somDim *= dim ;
		}

		debug_i(ARRAY_DBG,"Variable","checkArray: step 5 : somDim = %d",somDim) ;

		/* all dimension in arrayDimension are > 0 */

		if (somDim != value->getLength()) {
			debug_ii(ARRAY_DBG,"Variable","checkArray: step 5.a : %d != %d",somDim,value->getLength()) ;
			return StatusCode::Bad_NotImplemented ;
		}

		/* value is a one-dimension array with the good length */

		debug_p(ARRAY_DBG,"Variable","checkArray: step 6 : indexRange = %p",indexRange) ;

		if (indexRange == NULL)
			return StatusCode::Bad_IndexRangeInvalid ;

		/* indexRange is not NULL */

		const char * indexRangeStr = indexRange->get() ;


		if (indexRangeStr == NULL) {
			debug(ARRAY_DBG,"Variable","checkArray: step 8 : indexRangeStr = NULL") ;
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		debug_s(ARRAY_DBG,"Variable","checkArray: step 8 : indexRangeStr = \"%s\"",indexRangeStr) ;

		/* indexRangeStr is not NULL */

		const char * indexRangeEnd = indexRangeStr + strlen(indexRangeStr) ;

		/* indexRangeEnd is the end of the string indexRangeStr */

		int32_t      nbDim  = 0 ;
		int32_t      nbElt  = 1 ;
		const char * tmpStr = indexRangeStr ;
		char       * idxEnd ;


		while (tmpStr < indexRangeEnd) {

			if (nbDim > valueRankInt32)
				return StatusCode::Bad_IndexRangeNoData ;

			long dim = static_cast<long>(arrayDimensions->get(nbDim)->get()) ;

			const char * comma = strchr(tmpStr,',') ;
			if (comma == NULL)
				comma = indexRangeEnd ;

			const char * colon = strchr(tmpStr,':') ;
			if (colon == NULL)
				colon = comma ;

			if (colon >= comma) {
				if (tmpStr >= comma)
					return StatusCode::Bad_IndexRangeInvalid ;
				long idx = strtol(tmpStr,&idxEnd,0) ;
				if (idx < 0 || idxEnd != comma)
					return StatusCode::Bad_IndexRangeInvalid ;
				if (dim <= idx)
					return StatusCode::Bad_IndexRangeNoData ;
			} else {
				if (tmpStr >= colon)
					return StatusCode::Bad_IndexRangeInvalid ;
				long idx1 = strtol(tmpStr,&idxEnd,0) ;
				if (idx1 < 0 || idxEnd != colon)
					return StatusCode::Bad_IndexRangeInvalid ;
				if (dim <= idx1)
					return StatusCode::Bad_IndexRangeNoData ;
				colon++ ;
				if (colon >= comma)
					return StatusCode::Bad_IndexRangeInvalid ;
				long idx2 = strtol(colon,&idxEnd,0) ;
				if (idx2 < 0 || idxEnd != comma || idx2 < idx1)
					return StatusCode::Bad_IndexRangeInvalid ;
				if (dim <= idx2)
					return StatusCode::Bad_IndexRangeNoData ;
				nbElt *= (idx2 + 1 - idx1) ;
			}

			nbDim++ ;
			tmpStr = comma+1 ;
		}

		debug_i(ARRAY_DBG,"Variable","checkArray: step 9 : nbDim = %d",nbDim) ;

		if (nbDim != valueRankInt32) {
			debug_ii(ARRAY_DBG,"Variable","checkArray: step 9.a : nbDim = %d != %d = valueRankInt32",nbDim,valueRankInt32) ;
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		debug_i(ARRAY_DBG,"Variable","check newData: step 10 : nbElt = %d",nbElt) ;

		if (nbElt != newValue->getLength()) {
			debug_ii(ARRAY_DBG,"Variable","check newData: step 10.a : nbElt = %d != %d = nb elts newValue",nbElt,newValue->getLength()) ;
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		return NULL ;
	}

protected:

	inline void setArray(NumericRange * indexRange, BaseDataType * newValue)
	{
		value->setArray(indexRange,newValue,valueRank,arrayDimensions) ;
	}

public:

	virtual DataValue * getAttribute(
							IntegerId          * attributeId,
							NumericRange       * indexRange,
							Duration           * maxAge,
							TimestampsToReturn * timestampsToReturn
						)
	{
		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			debug(COM_DBG,"Variable","getAttribute::Base") ;
			return Base::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			debug(COM_DBG,"Variable","getAttribute BadAttributeValueId [1]") ;
			return DataValue::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			debug(COM_DBG,"Variable","getAttribute Value") ;
			getMutex()->lock() ;
			debug(COM_DBG,"Variable","getAttribute mutex lock") ;
			if (userAccessLevel->isSet(UserAccessLevel_CurrentRead)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return DataValue::Bad_UserAccessDenied ;
			}
			if (accessLevel != NULL && accessLevel->isUnset(AccessLevel_CurrentRead)) {
				debug(COM_DBG,"Variable","getAttribute accessLevel denied") ;
				getMutex()->unlock() ;
				return DataValue::Bad_NotReadable ;
			}
			if (indexRange->isNullOrEmpty()) {
				debug(COM_DBG,"Variable","getAttribute indexRange is NULL") ;
				if ((data = value))
					data->take() ;
				getMutex()->unlock() ;
			} else {
				debug(COM_DBG + ARRAY_DBG,"Variable","getAttribute indexRange is not NULL") ;
				DataValue * rc = checkArray(indexRange) ;
				debug_p(ARRAY_DBG,"Variable","getAttribute: checkArray returns %p",rc) ;
				if (rc != NULL) {
					getMutex()->unlock() ;
					return rc ;
				}
				debug(ARRAY_DBG,"Variable","getAttribute: entering takeArray") ;
				if ((data = takeArray(indexRange)))
					data->take() ;
				debug_p(ARRAY_DBG,"Variable","getAttribute: takeArray returns %p",data) ;
				getMutex()->unlock() ;
			}
			break ;

		case AttributeId_DataType:
			debug(COM_DBG,"Variable","getAttribute DataType") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = dataType))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ValueRank:
			debug(COM_DBG,"Variable","getAttribute ValueRank") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = valueRank))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ArrayDimensions:
			debug(COM_DBG,"Variable","getAttribute ArrayDimensions") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = arrayDimensions))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_AccessLevel:
			debug(COM_DBG,"Variable","getAttribute AccessLevel") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = accessLevel))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_UserAccessLevel:
			debug(COM_DBG,"Variable","getAttribute UserAccessLevel") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = userAccessLevel))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_MinimumSamplingInterval:
			debug(COM_DBG,"Variable","getAttribute MinimumSamplingInterval") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = minimumSamplingInterval))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Historizing:
			debug(COM_DBG,"Variable","getAttribute Historizing") ;
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = historizing))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			debug(COM_DBG,"Variable","getAttribute BadAttributeValueId [2]") ;
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * sourceTimestamp = NULL ;
		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_BOTH_2:
			serverTimestamp = UtcTime::now() ;
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = serverTimestamp ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		debug(COM_DBG,"Variable","getAttribute: building the result") ;

		DataValue * dataValue
			= new DataValue((data==NULL)?(reinterpret_cast<Variant *>(NULL)):(data->getVariant()),
							StatusCode::Good,
							sourceTimestamp,
							serverTimestamp
							) ;

		debug(COM_DBG,"Variable","getAttribute: result is built") ;

		if (data != NULL)
			data->release() ;

		debug(COM_DBG,"Variable","getAttribute: returning the result") ;
		return dataValue ;
	}

protected:

	DataValue * checkArray(NumericRange * indexRange)
	{
		debug_p(ARRAY_DBG,"Variable","checkArray: step 0 : valueRank = %p",valueRank) ;

		if (valueRank == NULL)
			return DataValue::Bad_NotImplemented ;

		/* valueRank is not null */

		int32_t valueRankInt32 = valueRank->get() ;

		debug_i(ARRAY_DBG,"Variable","checkArray: step 1 : valueRankInt32 = %d",valueRankInt32) ;

		if (valueRankInt32 < 1)
			return DataValue::Bad_NotImplemented ;

		/* valueRankInt32 >= 1 */

		debug_p(ARRAY_DBG,"Variable","checkArray: step 2 : arrayDimensions = %p",arrayDimensions) ;

		if (arrayDimensions == NULL)
			return DataValue::Bad_NotImplemented ;

		/* arrayDimensions is not NULL */

		debug_i(ARRAY_DBG,"Variable","checkArray: step 3 : arrayDimensions length = %d",arrayDimensions->getLength()) ;

 		if (arrayDimensions->getLength() != valueRankInt32)
			return DataValue::Bad_NotImplemented ;

		/* arrayDimensions has length valueRankInt32 */

		debug(ARRAY_DBG,"Variable","checkArray: step 4 : computing somDim") ;

		int32_t somDim = 1 ;
		for (int i = 0 ; i < valueRankInt32 ; i ++) {
			int32_t dim = arrayDimensions->get(i)->get() ;
			if (dim <= 0) {
				debug_ii(ARRAY_DBG,"Variable","checkArray: step 4.a : arrayDimensions[i] = %d < 0",i,dim) ;
				return DataValue::Bad_NotImplemented ;
			}
			somDim *= dim ;
		}

		debug_i(ARRAY_DBG,"Variable","checkArray: step 5 : somDim = %d",somDim) ;

		/* all dimension in arrayDimension are > 0 */

		if (somDim != value->getLength()) {
			debug_ii(ARRAY_DBG,"Variable","checkArray: step 5.a : %d != %d",somDim,value->getLength()) ;
			return DataValue::Bad_NotImplemented ;
		}

		/* value is a one-dimension array with the good length */

		debug_p(ARRAY_DBG,"Variable","checkArray: step 6 : indexRange = %p",indexRange) ;

		if (indexRange == NULL)
			return DataValue::Bad_NotImplemented ;

		/* indexRange is not NULL */

		const char * indexRangeStr = indexRange->get() ;

		if (indexRangeStr == NULL) {
			debug(ARRAY_DBG,"Variable","checkArray: step 8 : indexRangeStr = NULL") ;
			return DataValue::Bad_NotImplemented ;
		}

		debug_s(ARRAY_DBG,"Variable","checkArray: step 8 : indexRangeStr = \"%s\"",indexRangeStr) ;

		/* indexRangeStr is not NULL */

		const char * indexRangeEnd = indexRangeStr + strlen(indexRangeStr) ;

		/* indexRangeEnd is the end of the string indexRangeStr */

		int32_t      nbDim  = 0 ;
		const char * tmpStr = indexRangeStr ;
		char       * idxEnd ;


		while (tmpStr < indexRangeEnd) {

			if (nbDim > valueRankInt32)
				return DataValue::Bad_NotImplemented ;

			long dim = static_cast<long>(arrayDimensions->get(nbDim)->get()) ;

			const char * comma = strchr(tmpStr,',') ;
			if (comma == NULL)
				comma = indexRangeEnd ;

			const char * colon = strchr(tmpStr,':') ;
			if (colon == NULL)
				colon = comma ;

			if (colon >= comma) {
				if (tmpStr >= comma)
					return DataValue::Bad_IndexRangeInvalid ;
				long idx = strtol(tmpStr,&idxEnd,0) ;
				if (idxEnd != comma)
					return DataValue::Bad_IndexRangeInvalid ;
				if (idx < 0 || dim <= idx)
					return DataValue::Bad_NotImplemented ;
			} else {
				if (tmpStr >= colon)
					return DataValue::Bad_IndexRangeInvalid ;
				long idx1 = strtol(tmpStr,&idxEnd,0) ;
				if (idxEnd != colon)
					return DataValue::Bad_IndexRangeInvalid ;
				if (idx1 < 0 || dim <= idx1)
					return DataValue::Bad_NotImplemented ;
				colon++ ;
				if (colon >= comma)
					return DataValue::Bad_IndexRangeInvalid ;
				long idx2 = strtol(colon,&idxEnd,0) ;
				if (idxEnd != comma)
					return DataValue::Bad_IndexRangeInvalid ;
				if (idx2 <= idx1 || dim <= idx2)
					return DataValue::Bad_NotImplemented ;
			}

			nbDim++ ;
			tmpStr = comma+1 ;
		}

		debug_i(ARRAY_DBG,"Variable","checkArray: step 9 : nbDim = %d",nbDim) ;

		if (nbDim != valueRankInt32) {
			debug_ii(ARRAY_DBG,"Variable","checkArray: step 9.a : nbDim = %d != %d = valueRankInt32",nbDim,valueRankInt32) ;
			return DataValue::Bad_NotImplemented ;
		}

		return NULL ;
	}

protected:

	inline BaseDataType * takeArray(NumericRange * indexRange)
	{
		return value->takeArray(indexRange,valueRank,arrayDimensions) ;
	}

public:

	static Variable * explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			class LinkDescriptions ** pLinkDescriptions
			) ;

};

} /* namespace opcua */
#endif /* OPCUA_VARIABLE_H_ */
