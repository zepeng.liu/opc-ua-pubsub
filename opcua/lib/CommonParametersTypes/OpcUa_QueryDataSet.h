
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_QUERYDATASET_H_
#define OPCUA_QUERYDATASET_H_

/*
 * OPC-UA Part 4, 7.22, p. 134
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL QueryDataSet
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_QueryDataSet ; }

private:

	ExpandedNodeId * nodeId ;
	ExpandedNodeId * typeDefinitionNode ;
	TableVariant   * values ;

public:

	QueryDataSet(
			ExpandedNodeId * _nodeId,
			ExpandedNodeId * _typeDefinitionNode,
			TableVariant   * _values
			)
		: Structure()
	{
		(nodeId             = _nodeId)             ->take() ;
		(typeDefinitionNode = _typeDefinitionNode) ->take() ;
		(values             = _values)             ->take() ;
	}

	virtual ~QueryDataSet()
	{
		nodeId             ->release() ;
		typeDefinitionNode ->release() ;
		values             ->release() ;
	}

public:

	inline ExpandedNodeId * getNodeId()
	{
		return nodeId ;
	}

	inline ExpandedNodeId * getTypeDefinitionNode()
	{
		return typeDefinitionNode ;
	}

	inline TableVariant   * getValues()
	{
		return values ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_QUERYDATASET_H_ */
