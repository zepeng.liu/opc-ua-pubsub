
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SIGNEDSOFTWARECERTIFICATE_H_
#define OPCUA_SIGNEDSOFTWARECERTIFICATE_H_

/*
 * OPC-UA Part 4, 7.31, p. 141
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL SignedSoftwareCertificate
		: public Structure
{
public:

	static SignedSoftwareCertificate * const fakeSignedSoftwareCertificate ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_SignedSoftwareCertificate ; }

private:

	ByteString * certificateData ;
	ByteString * signature ;

public:

	static SignedSoftwareCertificate * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_SignedSoftwareCertificate const& pSignedSoftwareCertificate)
	{
		ByteString * certificateData = ByteString ::fromCtoCpp(pStatus, pSignedSoftwareCertificate.CertificateData) ;
		ByteString * signature       = ByteString ::fromCtoCpp(pStatus, pSignedSoftwareCertificate.Signature) ;

		if (*pStatus == STATUS_OK)
			return
					new SignedSoftwareCertificate(
							certificateData,
							signature
							) ;

		if (certificateData != NULL)
			certificateData->checkRefCount() ;
		if (signature != NULL)
			signature->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_SignedSoftwareCertificate& pSignedSoftwareCertificate) const
	{
		certificateData ->fromCpptoC(pStatus, pSignedSoftwareCertificate.CertificateData) ;
		signature       ->fromCpptoC(pStatus, pSignedSoftwareCertificate.Signature) ;
	}

public:

	SignedSoftwareCertificate(
			ByteString            * _certificateData,
			ByteString            * _signature
			)
		: Structure()
	{
		(certificateData = _certificateData) ->take() ;
		(signature       = _signature)       ->take() ;
	}

	virtual ~SignedSoftwareCertificate()
	{
		certificateData ->release() ;
		signature       ->release() ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_APPLICATIONINSTANCECERTIFICATE_H_ */
