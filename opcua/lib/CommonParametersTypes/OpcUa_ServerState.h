
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SERVERSTATE_H_
#define OPCUA_SERVERSTATE_H_

#include "../OpcUa.h"

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 5.8.8.2, p. 39, inside Table 30
 */

enum _ServerState {
	ServerState_RUNNING_0            = 0,
	ServerState_FAILED_1             = 1,
	ServerState_NOCONFIGURATION_2    = 2,
	ServerState_SUSPENDED_3          = 3,
	ServerState_SHUTDOWN_4           = 4,
	ServerState_TEST_5               = 5,
	ServerState_COMMUNICATIONFAULT_6 = 6,
	ServerState_UNKNOWN_7            = 7
} ;

class MYDLL ServerState
		: public Enumeration
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ServerState ; }

public:

	static ServerState * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ServerState const& value)
	{
		if (OpcUa_ServerState_Running <= value && value <= OpcUa_ServerState_Unknown)
			return new ServerState((_ServerState)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ServerState& pValue) const
	{
		int32_t value = get() ;
		if (ServerState_RUNNING_0 <= value && value <= ServerState_UNKNOWN_7)
			pValue = (OpcUa_ServerState)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

public:

	ServerState(_ServerState _value)
		: Enumeration(_value)
	{}

};     /* Class */
}      /* namespace opcua */
#endif /* OPCUA_SERVERSTATE_H_ */
