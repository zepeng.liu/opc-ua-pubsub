
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSEPATHRESULT_H_
#define OPCUA_BROWSEPATHRESULT_H_

/*
 * OPC-UA, Part 4, 5.8.4.2, p. 43
 */


#include "../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_Index.h"
#include "OpcUa_IPCS_BrowsePathTarget.h"

namespace opcua {

class MYDLL BrowsePathResult
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowsePathResult ; }

private:

	StatusCode              * statusCode ;
	TableBrowsePathTarget   * targets ;

public:

	static BrowsePathResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowsePathResult const& pBrowsePathResult)
	{
		StatusCode              * statusCode = StatusCode         ::fromCtoCpp (pStatus, pBrowsePathResult.StatusCode) ;
		TableBrowsePathTarget   * targets    = TableBrowsePathTarget::fromCtoCpp (pStatus, pBrowsePathResult.NoOfTargets, pBrowsePathResult.Targets) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowsePathResult(
							statusCode,
							targets
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (targets != NULL)
			targets->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowsePathResult& pBrowsePathResult) const
	{
		statusCode ->fromCpptoC (pStatus, pBrowsePathResult.StatusCode) ;
		targets    ->fromCpptoC (pStatus, pBrowsePathResult.NoOfTargets, pBrowsePathResult.Targets) ;
	}

public:

	BrowsePathResult(
			StatusCode              * _statusCode,
			TableBrowsePathTarget   * _targets
			)
		: Structure()
	{
		(statusCode         = _statusCode)         ->take() ;
		(targets            = _targets)            ->take() ;
	}

	virtual ~BrowsePathResult()
	{
		statusCode         ->release() ;
		targets            ->release() ;
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline TableBrowsePathTarget   * getTargets()
	{
		return targets ;
	}

};
} /* namespace opcua */
#endif /* WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS */
#endif /* OPCUA_BROWSEPATHRESULT_H_ */
