
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SECURITYTOKENREQUESTTYPE_H_
#define OPCUA_SECURITYTOKENREQUESTTYPE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 5.5.2.2, p. 22
 */

enum _SecurityTokenRequestType {
	SecurityTokenRequestType_ISSUE_0 = 0,
	SecurityTokenRequestType_RENEW_1 = 1
} ;

class MYDLL SecurityTokenRequestType
		: public Enumeration
{
public:

	static SecurityTokenRequestType * issue_0 ;
	static SecurityTokenRequestType * renew_1 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_SecurityTokenRequestType ; }

public:

	static SecurityTokenRequestType * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_SecurityTokenRequestType const& value)
	{
		if (OpcUa_SecurityTokenRequestType_Issue <= value && value <= OpcUa_SecurityTokenRequestType_Renew)
			return new SecurityTokenRequestType((_SecurityTokenRequestType)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_SecurityTokenRequestType& pValue) const
	{
		int32_t value = get() ;
		if (SecurityTokenRequestType_ISSUE_0 <= value && value <= SecurityTokenRequestType_RENEW_1)
			pValue = (OpcUa_SecurityTokenRequestType)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	SecurityTokenRequestType(_SecurityTokenRequestType _value)
		: Enumeration(_value)
	{}

};

} /* namespace opcua */
#endif /* OPCUA_SECURITYTOKENREQUESTTYPE_H_ */
