
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ISSUEDIDENTITYTOKEN_H_
#define OPCUA_ISSUEDIDENTITYTOKEN_H_

/*
 * OPC-UA Part 4, 7.35.5, p. 149
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_UserIdentityToken.h"

namespace opcua {

class MYDLL IssuedIdentityToken
	: public UserIdentityToken
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_IssuedIdentityToken ; }

private:

	String     * policyId ;
	ByteString * tokenData ;
	String     * encryptionAlgorithm ;

public:

	static IssuedIdentityToken * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_IssuedIdentityToken const& pIssuedIdentityToken)
	{
		String     * policyId            = String     ::fromCtoCpp(pStatus,pIssuedIdentityToken.PolicyId) ;
		ByteString * tokenData           = ByteString ::fromCtoCpp(pStatus,pIssuedIdentityToken.TokenData) ;
		String     * encryptionAlgorithm = String     ::fromCtoCpp(pStatus,pIssuedIdentityToken.EncryptionAlgorithm) ;

		if (*pStatus == STATUS_OK)
			return
					new IssuedIdentityToken(
							policyId,
							tokenData,
							encryptionAlgorithm
							) ;

		if (policyId != NULL)
			policyId->checkRefCount() ;
		if (tokenData != NULL)
			tokenData->checkRefCount() ;
		if (encryptionAlgorithm != NULL)
			encryptionAlgorithm->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pIssuedIdentityToken) const
	{
		SOPC_ExtensionObject_Initialize(&pIssuedIdentityToken) ;

		expandedNodeId->fromCpptoC(pStatus,pIssuedIdentityToken.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pIssuedIdentityToken.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pIssuedIdentityToken.Body.Object.ObjType = &OpcUa_X509IdentityToken_EncodeableType ;
			pIssuedIdentityToken.Body.Object.Value = (void *)malloc(sizeof(OpcUa_IssuedIdentityToken)) ;
			OpcUa_IssuedIdentityToken_Initialize(pIssuedIdentityToken.Body.Object.Value) ;
			IssuedIdentityToken::fromCpptoC(pStatus,*static_cast<OpcUa_IssuedIdentityToken *>(pIssuedIdentityToken.Body.Object.Value)) ;
			pIssuedIdentityToken.Length = sizeof(OpcUa_IssuedIdentityToken) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_IssuedIdentityToken& pIssuedIdentityToken) const
	{
		policyId            ->fromCpptoC(pStatus,pIssuedIdentityToken.PolicyId) ;
		tokenData           ->fromCpptoC(pStatus,pIssuedIdentityToken.TokenData) ;
		encryptionAlgorithm ->fromCpptoC(pStatus,pIssuedIdentityToken.EncryptionAlgorithm) ;
	}

public:

	IssuedIdentityToken(
			String     * _policyId,
			ByteString * _tokenData,
			String     * _encryptionAlgorithm
			)
		: UserIdentityToken()
	{
		(policyId            = _policyId)            ->take() ;
		(tokenData           = _tokenData)           ->take() ;
		(encryptionAlgorithm = _encryptionAlgorithm) ->take() ;
	}

	virtual ~IssuedIdentityToken()
	{
		policyId            ->release() ;
		tokenData           ->release() ;
		encryptionAlgorithm ->release() ;
	}

public:

	inline String * getPolicyId()
	{
		return policyId ;
	}

	inline ByteString * getTokenData()
	{
		return tokenData ;
	}

	inline String * getEncryptionAlgorithm()
	{
		return encryptionAlgorithm ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_X509IDENTITYTOKEN_H_ */
