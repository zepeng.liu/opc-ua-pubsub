
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ELEMENTOPERAND_H_
#define OPCUA_ELEMENTOPERAND_H_

/*
 * OPC-UA Part 4, 7.4.4.2, p. 115
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_FilterOperand.h"

namespace opcua {

#ifdef Index
#   undef Index
#endif

class MYDLL ElementOperand
	: public FilterOperand
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ElementOperand ; }

private:

	UInt32 * index ;

public:

	static ElementOperand * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ElementOperand const& pElementOperand)
	{
		UInt32 * index = UInt32 ::fromCtoCpp(pStatus,pElementOperand.Index) ;

		if (*pStatus == STATUS_OK)
			return
					new ElementOperand(
							index
							) ;

		if (index != NULL)
			index->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pElementOperand) const
	{
		expandedNodeId->fromCpptoC(pStatus,pElementOperand.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pElementOperand.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pElementOperand.Body.Object.ObjType = &OpcUa_ElementOperand_EncodeableType ;
			pElementOperand.Body.Object.Value = (void *)malloc(sizeof(OpcUa_ElementOperand)) ;
			OpcUa_ElementOperand_Initialize(pElementOperand.Body.Object.Value) ;
			ElementOperand::fromCpptoC(pStatus,*static_cast<OpcUa_ElementOperand *>(pElementOperand.Body.Object.Value)) ;
			pElementOperand.Length = sizeof(OpcUa_ElementOperand) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ElementOperand& pElementOperand) const
	{
		index ->fromCpptoC(pStatus,pElementOperand.Index) ;
	}

public:

	ElementOperand(
			UInt32 * _index
			)
		: FilterOperand()
	{
		(index = _index) ->take() ;
	}

	virtual ~ElementOperand()
	{
		index  ->release() ;
	}

public:

	inline UInt32 * getIndex()
	{
		return index ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};

#include "OpcUa_Index.h"

} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_ELEMENTOPERAND_H_ */
