
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_USERIDENTITYTOKENTYPE_H_
#define OPCUA_USERIDENTITYTOKENTYPE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 7.36
 * IdType = 315
 */

enum _UserIdentityTokenType {
	UserIdentityTokenType_ANONYMOUS_0 = 0,
	UserIdentityTokenType_USERNAME_1 = 1,
	UserIdentityTokenType_CERTIFICATE_2 = 2,
	UserIdentityTokenType_ISSUEDTOKEN_3 = 3
	// ??? OpcUa_UserTokenType_Kerberos
} ;

class MYDLL UserIdentityTokenType
		: public Enumeration
{
public:

	static UserIdentityTokenType * anonymous_0 ;
	static UserIdentityTokenType * username_1 ;
	static UserIdentityTokenType * certificate_2 ;
	static UserIdentityTokenType * issuedtoken_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_UserIdentityToken ; } // Is this good ?

public:

	static UserIdentityTokenType * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_UserTokenType const& value)
	{
		if (OpcUa_UserTokenType_Anonymous <= value && value <= OpcUa_UserTokenType_IssuedToken)
			return new UserIdentityTokenType((_UserIdentityTokenType)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_UserTokenType& pValue) const
	{
		int32_t value = get() ;
		if (UserIdentityTokenType_ANONYMOUS_0 <= value && value <= UserIdentityTokenType_ISSUEDTOKEN_3)
			pValue = (OpcUa_UserTokenType)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	UserIdentityTokenType(_UserIdentityTokenType _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* OPCUA_SECURITYTOKENREQUESTTYPE_H_ */
