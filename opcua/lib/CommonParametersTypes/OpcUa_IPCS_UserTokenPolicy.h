
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_USERTOKENPOLICY_H_
#define OPCUA_USERTOKENPOLICY_H_

/*
 * OPC-UA Part 7, 7.26, p. 174
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_UserIdentityTokenType.h"

namespace opcua {

class MYDLL UserTokenPolicy
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_UserTokenPolicy ; }

private:

	String                * policyId ;
	UserIdentityTokenType * tokenType ;
	String                * issuedTokenType ;
	String                * issuerEndPointUrl ;
    String                * securityProfileUrl ;

public:

	static UserTokenPolicy * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_UserTokenPolicy const& pUserTokenPolicy)
	{
		String                * policyId           = String                ::fromCtoCpp(pStatus, pUserTokenPolicy.PolicyId) ;
		UserIdentityTokenType * tokenType          = UserIdentityTokenType ::fromCtoCpp(pStatus, pUserTokenPolicy.TokenType) ;
		String                * issuedTokenType    = String                ::fromCtoCpp(pStatus, pUserTokenPolicy.IssuedTokenType) ;
		String                * issuerEndPointUrl  = String                ::fromCtoCpp(pStatus, pUserTokenPolicy.IssuerEndpointUrl) ;
	    String                * securityProfileUrl = String                ::fromCtoCpp(pStatus, pUserTokenPolicy.SecurityPolicyUri);

	    if (*pStatus == STATUS_OK)
			return
					new UserTokenPolicy(
							policyId,
							tokenType,
							issuedTokenType,
							issuerEndPointUrl,
							securityProfileUrl
							) ;

	    if (policyId != NULL)
	    	policyId->checkRefCount() ;
	    if (tokenType != NULL)
	    	tokenType->checkRefCount() ;
	    if (issuedTokenType != NULL)
	    	issuedTokenType->checkRefCount() ;
	    if (issuerEndPointUrl != NULL)
	    	issuerEndPointUrl->checkRefCount() ;
	    if (securityProfileUrl != NULL)
	    	securityProfileUrl->checkRefCount() ;

	    return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_UserTokenPolicy& pUserTokenPolicy) const
	{
		policyId           ->fromCpptoC(pStatus, pUserTokenPolicy.PolicyId) ;
		tokenType          ->fromCpptoC(pStatus, pUserTokenPolicy.TokenType) ;
		issuedTokenType    ->fromCpptoC(pStatus, pUserTokenPolicy.IssuedTokenType) ;
		issuerEndPointUrl  ->fromCpptoC(pStatus, pUserTokenPolicy.IssuerEndpointUrl) ;
		securityProfileUrl ->fromCpptoC(pStatus, pUserTokenPolicy.SecurityPolicyUri) ;
	}

public:

    UserTokenPolicy(
    		String                * _policyId,
    		UserIdentityTokenType * _tokenType,
    		String                * _issuedTokenType,
    		String                * _issuerEndPointUrl,
    	    String                * _securityProfileUrl
			)
		: Structure()
	{
		(policyId           = _policyId)           ->take() ;
		(tokenType          = _tokenType)          ->take() ;
		(issuedTokenType    = _issuedTokenType)    ->take() ;
		(issuerEndPointUrl  = _issuerEndPointUrl)  ->take() ;
		(securityProfileUrl = _securityProfileUrl) ->take() ;
	}

	virtual ~UserTokenPolicy()
	{
		policyId           ->release()  ;
		tokenType          ->release() ;
		issuedTokenType    ->release() ;
		issuerEndPointUrl  ->release() ;
		securityProfileUrl ->release() ;
	}

public:

	inline String * getPolicyId()
	{
		return policyId;
	}

	inline UserIdentityTokenType * getTokenType()
	{
		return tokenType;
	}

	inline String * getIssuedTokenType()
	{
		return issuedTokenType;
	}

	inline String * getIssuerEndPointUrl()
	{
		return issuerEndPointUrl;
	}

	inline String * getSecurityProfileUrl()
	{
		return securityProfileUrl;
	}

public:

	bool equals(UserTokenPolicy * other)
	{
		return
				policyId           ->equals(other->getPolicyId()) &&
				tokenType          ->equals(other->getTokenType()) &&
				issuedTokenType    ->equals(other->getIssuedTokenType()) &&
				issuerEndPointUrl  ->equals(other->getIssuerEndPointUrl()) &&
				securityProfileUrl ->equals(other->getSecurityProfileUrl()) ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_RESPONSEHEADER_H_ */
