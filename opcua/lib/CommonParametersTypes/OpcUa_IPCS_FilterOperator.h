
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_FILTEROPERATOR_H_
#define OPCUA_FILTEROPERATOR_H_

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/* OPC-UA Part 4, 7.4.3, p. 108 */

enum _FilterOperator {
	FilterOperator_EQUALS_0 = 0,
	FilterOperator_ISNULL_1 = 1,
	FilterOperator_GREATERTHAN_2 = 2,
	FilterOperator_LESSTHAN_3 = 3,
	FilterOperator_GREATERTHANOREQUAL_4 = 4,
	FilterOperator_LESSTHANOREQUAL_5 = 5,
	FilterOperator_LIKE_6 = 6,
	FilterOperator_NOT_7 = 7,
	FilterOperator_BETWEEN_8 = 8,
	FilterOperator_INLIST_9 = 9,
	FilterOperator_AND_10 = 10,
	FilterOperator_OR_11 = 11,
	FilterOperator_CAST_12 = 12,
	FilterOperator_INVIEW_13 = 13,
	FilterOperator_OFTYPE_14 = 14,
	FilterOperator_RELATEDTO_15 = 15,
	FilterOperator_BITWISEAND_16 = 16,
	FilterOperator_BITWISEOR_17 = 17
} ;

class MYDLL FilterOperator
		: public Enumeration
{
public:

	static FilterOperator * Equals_0 ;
	static FilterOperator * IsNull_1 ;
	static FilterOperator * GreaterThan_2 ;
	static FilterOperator * LessThan_3 ;
	static FilterOperator * GreaterThanOrEqual_4 ;
	static FilterOperator * LessThanOrEqual_5 ;
	static FilterOperator * Like_6 ;
	static FilterOperator * Not_7 ;
	static FilterOperator * Between_8 ;
	static FilterOperator * InList_9 ;
	static FilterOperator * And_10 ;
	static FilterOperator * Or_11 ;
	static FilterOperator * Cast_12 ;
	static FilterOperator * InView_13 ;
	static FilterOperator * OfType_14 ;
	static FilterOperator * RelatedTo_15 ;
	static FilterOperator * BitwiseAnd_16 ;
	static FilterOperator * BitwiseOr_17 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_FilterOperator ; }

public:

	static FilterOperator * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_FilterOperator const& value)
	{
		if (OpcUa_FilterOperator_Equals <= value && value <= OpcUa_FilterOperator_BitwiseOr)
			return new FilterOperator((_FilterOperator)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_FilterOperator& pValue) const
	{
		int32_t value = get() ;
		if (FilterOperator_EQUALS_0 <= value && value <= FilterOperator_BITWISEOR_17)
			pValue = (OpcUa_FilterOperator)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	FilterOperator(_FilterOperator _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_FILTEROPERATOR_H_ */
