
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSRESULT_H_
#define OPCUA_BROWSRESULT_H_

/*
 * OPC-UA, Part 4, 7.3, p. 107
 */

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_IPCS_ContinuationPoint.h"
#include "OpcUa_IPCS_ReferenceDescription.h"

namespace opcua {

class MYDLL BrowseResult
		: public Structure
{
public:

	static BrowseResult * Bad_NodeIdInvalid ;
	static BrowseResult * Bad_ReferenceTypeIdInvalid ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowseResult ; }

private:

	StatusCode                  * statusCode ;
	ContinuationPoint           * continuationPoint ;
	TableReferenceDescription   * references ;

public:

	static BrowseResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowseResult const& pBrowseResult)
	{
		StatusCode                  * statusCode        = StatusCode             ::fromCtoCpp (pStatus, pBrowseResult.StatusCode) ;
		ContinuationPoint           * continuationPoint = ContinuationPoint      ::fromCtoCpp (pStatus, pBrowseResult.ContinuationPoint) ;
		TableReferenceDescription   * references        = TableReferenceDescription::fromCtoCpp (pStatus, pBrowseResult.NoOfReferences, pBrowseResult.References) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowseResult(
							statusCode,
							continuationPoint,
							references
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (continuationPoint != NULL)
			continuationPoint->checkRefCount() ;
		if (references != NULL)
			references->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowseResult& pBrowseResult) const
	{
		statusCode        ->fromCpptoC (pStatus, pBrowseResult.StatusCode) ;
		continuationPoint ->fromCpptoC (pStatus, pBrowseResult.ContinuationPoint) ;
		references        ->fromCpptoC (pStatus, pBrowseResult.NoOfReferences, pBrowseResult.References) ;
	}

public:

	BrowseResult(
			StatusCode * _statusCode
			)
		: Structure()
	{
		(statusCode        = _statusCode)                     ->take() ;
		(continuationPoint = ContinuationPoint::empty)        ->take() ;
		(references        = ReferenceDescription::tableZero) ->take() ;
	}

	BrowseResult(
			TableReferenceDescription   * _references
			)
		: Structure()
	{
		(statusCode        = StatusCode::Good)         ->take() ;
		(continuationPoint = ContinuationPoint::empty) ->take() ;
		(references        = _references)              ->take() ;
	}

	BrowseResult(
			StatusCode                  * _statusCode,
			ContinuationPoint           * _continuationPoint,
			TableReferenceDescription   * _references
			)
		: Structure()
	{
		(statusCode        = _statusCode)        ->take() ;
		(continuationPoint = _continuationPoint) ->take() ;
		(references        = _references)        ->take() ;
	}

	virtual ~BrowseResult()
	{
		statusCode        ->release() ;
		continuationPoint ->release() ;
		references        ->release() ;
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline ContinuationPoint * getContinuationPoint()
	{
		return continuationPoint ;
	}

	inline TableReferenceDescription   * getReferences()
	{
		return references ;
	}

};
} /* namespace opcua */
#endif /* WITH_BROWSE */
#endif /* OPCUA_BROWSRESULT_H_ */
