
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_APPLICATIONINSTANCECERTIFICATE_H_
#define OPCUA_APPLICATIONINSTANCECERTIFICATE_H_

/*
 * OPC-UA Part 4, 7.2, p. 106
 */

#include "../OpcUa.h"

#ifdef _WIN32
#include <Windows.h>
#include <io.h>
#endif

#include <fcntl.h>
#include <time.h>
#include <math.h>


#include "OpcUa_SecurityProfileUri.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include <string.h>


/*Used documentation: https://tls.mbed.org/api/group__x509__module.html#ga003c2483a2691e025641fa9d68d4a96a */
#include "mbedtls/x509_crt.h"
#include "mbedtls/pk.h"
#include "mbedtls/rsa.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h" // the random number generator module
#include "mbedtls/sha256.h"
#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/asn1.h"


#define MAX_DER_FILE (64 * 1024)


namespace opcua {

class MYDLL ApplicationInstanceCertificate
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ApplicationInstanceCertificate ; }

private:

#if (SECURITY != UANONE)
	String        * signatureAlgorithm ;
	ByteString    * signature ;
#endif

	ByteString	  * content;
	String        * fileName ;

#if (SECURITY != UANONE)
	mbedtls_x509_crt    certificate ;
#endif

	bool    valid ;
	int32_t error ;

public:

	static ApplicationInstanceCertificate * getApplicationInstanceCertificate(const char * fileName)
	{
#ifdef _WIN32
		int file = open(fileName,_O_RDONLY | _O_BINARY) ;
#else
		int file = open(fileName,O_RDONLY) ;
#endif

		if (file < 0) {
			debug_com_s(COM_ERR,"ApplicationInstanceCertificate","getApplicationInstanceCertificate : cannot open ",fileName) ;
			return NULL ;
		}

		uint8_t buffer[MAX_DER_FILE] ;

		int offset = 0 ;
		int nbread ;

		do {
			nbread = (int)read(file,buffer+offset,MAX_DER_FILE-offset) ;
			if (nbread > 0)
				offset += nbread ;
		} while (nbread > 0) ;

		if (nbread < 0) {
			debug_com_s(COM_ERR,"ApplicationInstanceCertificate","getApplicationInstanceCertificate : cannot read ",fileName) ;
			close(file) ;
			return NULL ;
		}

		if (close(file) < 0) {
			debug_com_s(COM_ERR,"ApplicationInstanceCertificate","getApplicationInstanceCertificate : cannot close ",fileName) ;
			return NULL ;
		}

		ByteString * content   = new ByteString((int32_t)offset,buffer) ;
		String     * sFileName = new String(fileName) ;

		ApplicationInstanceCertificate * result = new ApplicationInstanceCertificate(sFileName,content) ;

		content   ->checkRefCount() ;
		sFileName ->checkRefCount() ;

		return result ;
	}

	public:

		ApplicationInstanceCertificate(String * _fileName, ByteString * _content)
			: Structure()
		{
			(fileName = _fileName) ->take() ;
			(content  = _content)  ->take() ;


			valid = true ;
			error = _Good ;

#if (SECURITY != UANONE)
			if ((decode_content())) {
				debug_s(COM_ERR,"ApplicationInstanceCertificate","Certificate unreadable (1) \"%s\"",fileName->get()) ;
				valid = false ;
				error = _Bad_CertificateInvalid ;
			}
#endif // (SECURITY != UANONE)
		}

public:

	ApplicationInstanceCertificate(String * _fileName, int32_t arrayLength, const uint8_t * array)
		: Structure()
	{
		(fileName = _fileName)                         ->take() ;
		(content  = new ByteString(arrayLength,array)) ->take() ;

		valid = true ;
		error = _Good ;

#if (SECURITY != UANONE)
		if ((decode_content())) {
			debug_s(COM_ERR,"ApplicationInstanceCertificate","Certificate unreadable (2) \"%s\"",fileName->get()) ;
			valid = false ;
			error = _Bad_CertificateInvalid ;
		}
#endif // (SECURITY != UANONE)
	}

public:

	virtual ~ApplicationInstanceCertificate()
	{
#if (SECURITY != UANONE)
		if (signatureAlgorithm != NULL)
			signatureAlgorithm ->release() ;
		if (signature != NULL)
			signature ->release() ;
#endif
		fileName ->release() ;
		content  ->release() ;

#if (SECURITY != UANONE)
		mbedtls_x509_crt_free (&certificate) ;
#endif
	}

public:

	static String * fake_name_tranfer_from_C_to_Cpp ;

	static inline ApplicationInstanceCertificate * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ByteString const& pByteString)
	{
		NOT_USED(pStatus) ;
		return
				new ApplicationInstanceCertificate(
						fake_name_tranfer_from_C_to_Cpp,
						pByteString.Length,
						(const uint8_t *)pByteString.Data) ;
	}

	static inline ApplicationInstanceCertificate * fromCtoCpp(SOPC_StatusCode * pStatus, uint32_t len_der, uint8_t * crt_der)
	{
		NOT_USED(pStatus) ;
		return
				new ApplicationInstanceCertificate(
						fake_name_tranfer_from_C_to_Cpp,
						len_der,
						(const uint8_t *)crt_der) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ByteString& pByteString) const
	{
		return content->fromCpptoC(pStatus,pByteString) ;
	}

public:

	inline ByteString * getContent()
	{
		return content;
	}


	inline String * getFileName()
	{
		return fileName ;
	}

	inline bool isNotValid()
	{
		return ! valid ;
	}

	inline int32_t getError()
	{
		return error ;
	}

#if (SECURITY != UANONE)
	String * getSignatureAlgorithm() const {
		return signatureAlgorithm;
	}
#endif

#if (SECURITY != UANONE)
	ByteString * getSignature()
	{
		return signature;
	}
#endif



#if (SECURITY != UANONE)
	bool equals(ApplicationInstanceCertificate * other)
	{
		return getContent()->equals(other->getContent()) ;
	}
#endif

#if (SECURITY != UANONE)
	inline mbedtls_x509_crt  getCertificate() const {
		return certificate;
	}
#endif


private:

	int32_t decode_content()
	{
		int32_t rc = (int32_t)0;

		debug(SEC_DBG,"ApplicationInstanceCertificate","x509Certificate and private key decoding begin") ;

#if (SECURITY != UANONE)
		signatureAlgorithm  = NULL ;
		signature 			= NULL ;
		char tmpSignatureAlgorithm [100] ; // 100 is the max of the signature name algorithm that we can imagine

		if  (content == NULL) {
			debug(COM_ERR,"ApplicationInstanceCertificate", "extractCertificate : content == NULL ") ;
			rc = (int32_t) 1 ;
			goto error ;
		}

		mbedtls_x509_crt_init (&certificate) ; // Removed in the destructor

		if ( (rc = mbedtls_x509_crt_parse_der(&certificate, (const unsigned char *) content->getArray(), content->getLen())) != 0) {
			debug_i(COM_ERR,"ApplicationInstanceCertificate", "decode_content : mbedtls_x509_crt_parse_der (%d) ", rc) ;
			goto error ;
		}

		mbedtls_x509_sig_alg_gets ((char*) tmpSignatureAlgorithm, 100, &(certificate.sig_oid), certificate.sig_pk, certificate.sig_md, &(certificate.sig_opts)) ;

		signatureAlgorithm = new String(tmpSignatureAlgorithm) ; // this constructor takes the strlen of tmpSignatureAlgorithm + 1 for the \0
		signatureAlgorithm->take() ;

		signature = new ByteString ((int32_t)(certificate.sig).len, (uint8_t*) (certificate.sig).p) ;
		signature->take() ;

		debug_i(SEC_DBG,"ApplicationInstanceCertificate","x509Certificate and private key decoding ends with rc = %d",rc) ;

#endif // (SECURITY != UANONE)

#if (SECURITY != UANONE)
error:
#endif
	    return (int32_t) rc ;
	}

public:
#if (SECURITY != UANONE)

	int32_t static extractPrivateKey(mbedtls_pk_context * privateKey, const char * privateKeyPath)
	{
		int32_t rc = 0 ;

		mbedtls_pk_init (privateKey) ; // Removed in the destructor

		if ((rc = mbedtls_pk_parse_keyfile(privateKey, privateKeyPath, NULL ))) {   // the last value (NULL) represents the password of the private key
			debug_i(COM_ERR,"ApplicationInstanceCertificate", "decode_content : mbedtls_pk_parse_keyfile (%d)", rc) ;
			goto error ;
		}

		if( (rc = mbedtls_rsa_check_privkey((mbedtls_rsa_context *) privateKey->pk_ctx)) != 0) {
			debug(COM_ERR, "ApplicationInstanceCertificate", "the rsa private key is not correct") ;
			goto error ;
		}

		debug(SEC_DBG, "ApplicationInstanceCertificate", "extractPrivateKey finished with success") ;
error:
		return rc ;
	}
#endif

#if (SECURITY != UANONE)
	int32_t static asymetricSignatureVerification(
													const uint8_t    * signature,
													uint32_t           signatureLength,
													const uint8_t    * plain_text_buffer,
													uint32_t           plain_text_buffer_length,
													mbedtls_x509_crt * cert
												   )

	{
		int32_t rc = 0 ;
		uint8_t md [MessageDigestLength] ;

		if (signatureLength != SignatureLength) {
			debug_ii(COM_ERR, "ApplicationInstanceCertificate",
					"asymmetricVerification operation failed, signatureLength (%d) != SignatureLength (%d)\n", signatureLength, SignatureLength) ;
			rc = (int32_t) 1 ;
			goto error ;
		}

		//RSA_PKCS1_PADDING
		mbedtls_rsa_init((mbedtls_rsa_context *) (cert->pk).pk_ctx, MBEDTLS_RSA_PKCS_V15, 0) ; // the last parameter is the  MBEDTLS_RSA_PKCS_V21 hash identifier


		if((rc = mbedtls_rsa_check_pubkey((mbedtls_rsa_context *) (cert->pk).pk_ctx)) != 0) {
			debug_i(COM_ERR, "ApplicationInstanceCertificate", "asymmetricSignature operation failed, mbedtls_rsa_check_pubkey failed %d\n", rc) ;
			goto error ;
		}

		if ((rc = mbedtls_pk_can_do(&(cert->pk), MBEDTLS_PK_RSA)) == 0) {
			debug_i(COM_ERR, "ApplicationInstanceCertificate", "asymmetricSignature operation failed, public key can not can't used to verify %d\n", rc) ;
			goto error ;
		}

		mbedtls_sha256(plain_text_buffer, (size_t) plain_text_buffer_length, md,
				0) ; // the last parameter can take 0 => use SHA256, or 1 => use SHA224

		if ((rc = mbedtls_rsa_pkcs1_verify((mbedtls_rsa_context *) (cert->pk).pk_ctx, NULL, NULL, MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA256, MessageDigestLength, md, signature))) {
			// the second and 3rd parameters: RNG function (Needed for PKCS#1 v2.1), RNG parameter
				debug (COM_ERR, "ApplicationInstanceCertificate", "asymmetricVerification operation failed, mbedtls_rsa_pkcs1_verify failed\n");
				rc = (int32_t) 1 ;
				goto error ;
		}

		debug(SEC_DBG,"ApplicationInstanceCertificate","asymmetricVerification operation finished with success") ;

		/**/debug(COM_ERR,"ApplicationInstanceCertificate","VerifData ok") ; //todo to remove


error:
		return rc ;

	}
#endif


#if (SECURITY != UANONE)

	int32_t static asymmetricSignature(
			uint8_t                 ** signature,
			uint32_t                 * signatureLength,
			const uint8_t            * plain_text_buffer,
			uint32_t                   plain_text_buffer_length,
			const mbedtls_pk_context & privKey)
	{

		int32_t rc = 0 ;
		uint8_t md [MessageDigestLength] ;

		if( (rc = mbedtls_rsa_check_privkey((mbedtls_rsa_context *) privKey.pk_ctx)) != 0) {
			debug_i(COM_ERR, "ApplicationInstanceCertificate", "asymmetricSignature operation failed, mbedtls_rsa_check_privkey failed %d\n", rc) ;
			goto error ;
		}

		if ((rc = mbedtls_pk_can_do(&privKey, MBEDTLS_PK_RSA)) == 0) {
			debug_i(COM_ERR, "ApplicationInstanceCertificate", "asymmetricSignature operation failed, private key can not can't used to sign %d\n",	rc) ;
			goto error ;
		}


		mbedtls_sha256(plain_text_buffer, (size_t) plain_text_buffer_length, md,
				0) ; // the last parameter can take 0 => use SHA256, or 1 => use SHA224


		//mbedtls_rsa_set_padding((mbedtls_rsa_context *) privKey, MBEDTLS_RSA_PKCS_V15, 0) ; // the last parameter is the  MBEDTLS_RSA_PKCS_V21 hash identifier

		(*signatureLength) = SignatureLength;
		(*signature) = new uint8_t[SignatureLength];

		/*https://tls.mbed.org/api/rsa_8h.html#a47d9e8989e233e986c59dc2aeb4a4caf*/

		//todo check if RSA_PKCS1_PADDING is available
		if ((rc = mbedtls_rsa_pkcs1_sign((mbedtls_rsa_context *) privKey.pk_ctx, NULL, NULL, MBEDTLS_RSA_PRIVATE, MBEDTLS_MD_SHA256, MessageDigestLength, md, (*signature)))) {
		// the second and 3rd parameters: RNG function (Needed for PKCS#1 v2.1), RNG parameter
			debug (COM_ERR, "ApplicationInstanceCertificate", "asymmetricSignature operation failed, mbedtls_rsa_pkcs1_sign failed \n");
			rc = (int32_t) 1 ;
			goto error ;
		}

		debug(SEC_DBG,"ApplicationInstanceCertificate","asymmetricSignature operation finished with success") ;

error:
		return rc ;
	}
#endif // (SECURITY != UANONE)

public:

#if (VERIF_APPURI == VERIF_APPURI_ENABLED && SECURITY != UANONE)
int32_t getSubjectAltName(String **info, int type)
{
    /* This list is taken from openssl
     # define SAN_OTHERNAME   0
     # define SAN_EMAIL       1
     # define SAN_DNS         2
     # define SAN_X400        3
     # define SAN_DIRNAME     4
     # define SAN_EDIPARTY    5
     # define SAN_URI         6
     # define SAN_IPADD       7
     # define SAN_RID         8
     */

	mbedtls_x509_sequence *val = &(certificate.subject_alt_names) ;


	while (val != NULL) {
//		char tmp[1000000] ;
//		memcpy(tmp,(char*) val->buf.p,(int)val->buf.len) ;
//		tmp[(int)val->buf.len] = '\0' ;
//    	fprintf(stderr,"   tag=%d len=%d str=%s\n",val->buf.tag,(int)val->buf.len,tmp) ;
        if (val->buf.tag == (type | MBEDTLS_ASN1_CONTEXT_SPECIFIC)) {
        	*info = new String(val->buf.len,(char*) val->buf.p) ;
             //fprintf(stderr,"       -> %s\n", (*info)->get()) ;
             return 0 ;
        }
        val = val->next ;
    }

    return 1 ;
}
#endif

} ;

} /* namespace opcua */
#endif /* OPCUA_APPLICATIONINSTANCECERTIFICATE_H_ */
