
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_QUERYDATADESCRIPTION_H_
#define OPCUA_QUERYDATADESCRIPTION_H_

/*
 * OPC-UA, Part 4, 5.9.3.2, p. 48 in Table 43
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_NumericRange.h"
#include "OpcUa_IPCS_RelativePath.h"

namespace opcua {

class MYDLL QueryDataDescription
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_QueryDataDescription ; }

private:

	RelativePath * relativePath ;
	IntegerId    * attributeId ;
	NumericRange * indexRange ;

public:

	static QueryDataDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_QueryDataDescription const& pQueryDataDescription)
	{
		RelativePath * relativePath = RelativePath ::fromCtoCpp( pStatus, pQueryDataDescription.RelativePath) ;
		IntegerId    * attributeId  = IntegerId    ::fromCtoCpp( pStatus, pQueryDataDescription.AttributeId) ;
		NumericRange * indexRange   = NumericRange ::fromCtoCpp( pStatus, pQueryDataDescription.IndexRange) ;

		if (*pStatus == STATUS_OK)
			return
					new QueryDataDescription(
							relativePath,
							attributeId,
							indexRange
							) ;

		if (relativePath != NULL)
			relativePath->checkRefCount() ;
		if (attributeId != NULL)
			attributeId->checkRefCount() ;
		if (indexRange != NULL)
			indexRange->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_QueryDataDescription& pQueryDataDescription) const
	{
		relativePath ->fromCpptoC (pStatus, pQueryDataDescription.RelativePath) ;
		attributeId  ->fromCpptoC (pStatus, pQueryDataDescription.AttributeId) ;
		indexRange   ->fromCpptoC (pStatus, pQueryDataDescription.IndexRange) ;
	}

public:

	QueryDataDescription(
			RelativePath * _relativePath,
			IntegerId    * _attributeId,
			NumericRange * _indexRange
			)
		: Structure()
	{
		(relativePath = _relativePath) ->take() ;
		(attributeId  = _attributeId)  ->take() ;
		(indexRange   = _indexRange)   ->take() ;
	}

	virtual ~QueryDataDescription()
	{
		relativePath ->release() ;
		attributeId  ->release() ;
		indexRange   ->release() ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_QUERYDATADESCRIPTION_H_ */
