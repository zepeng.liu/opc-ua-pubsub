
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSDESCRIPTION_H_
#define OPCUA_BROWSDESCRIPTION_H_

/*
 * OPC-UA, Part 4, 5.8.2.2, p. 38-39, inside Table 30
 */

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_BrowseDirection.h"

namespace opcua {

class MYDLL BrowseDescription
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowseDescription ; }

private:

	NodeId          * nodeId ;
	BrowseDirection * browseDirection ;
	NodeId          * referenceTypeId ;
	Boolean         * includeSubtypes ;
	UInt32          * nodeClassMask ;

	UInt32          * resultMask ;

public:

	static BrowseDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowseDescription const& pBrowseDescription)
	{
		NodeId          * nodeId          = NodeId          ::fromCtoCpp (pStatus, pBrowseDescription.NodeId) ;
		BrowseDirection * browseDirection = BrowseDirection ::fromCtoCpp (pStatus, pBrowseDescription.BrowseDirection) ;
		NodeId          * referenceTypeId = NodeId          ::fromCtoCpp (pStatus, pBrowseDescription.ReferenceTypeId) ;
		Boolean         * includeSubtypes = Boolean         ::fromCtoCpp (pStatus, pBrowseDescription.IncludeSubtypes) ;
		UInt32          * nodeClassMask   = UInt32          ::fromCtoCpp (pStatus, pBrowseDescription.NodeClassMask) ;

		UInt32          * resultMask      = UInt32          ::fromCtoCpp (pStatus, pBrowseDescription.ResultMask) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowseDescription(
							nodeId,
							browseDirection,
							referenceTypeId,
							includeSubtypes,
							nodeClassMask,

							resultMask
							) ;

		if (nodeId != NULL)
			nodeId->checkRefCount() ;
		if (browseDirection != NULL)
			browseDirection->checkRefCount() ;
		if (referenceTypeId != NULL)
			referenceTypeId->checkRefCount() ;
		if (includeSubtypes != NULL)
			includeSubtypes->checkRefCount() ;
		if (nodeClassMask != NULL)
			nodeClassMask->checkRefCount() ;

		if (resultMask != NULL)
			resultMask->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowseDescription& pBrowseDescription) const
	{
		nodeId          ->fromCpptoC (pStatus, pBrowseDescription.NodeId) ;
		browseDirection ->fromCpptoC (pStatus, pBrowseDescription.BrowseDirection) ;
		referenceTypeId ->fromCpptoC (pStatus, pBrowseDescription.ReferenceTypeId) ;
		includeSubtypes ->fromCpptoC (pStatus, pBrowseDescription.IncludeSubtypes) ;
		nodeClassMask   ->fromCpptoC (pStatus, pBrowseDescription.NodeClassMask) ;

		resultMask      ->fromCpptoC (pStatus, pBrowseDescription.ResultMask) ;
}

public:

	BrowseDescription(
			NodeId          * _nodeId,
			BrowseDirection * _browseDirection,
			NodeId          * _referenceTypeId,
			Boolean         * _includeSubtypes,
			UInt32          * _nodeClassMask,
			UInt32          * _resultMask
			)
		: Structure()
	{
		(nodeId          = _nodeId)          ->take() ;
		(browseDirection = _browseDirection) ->take() ;
		(referenceTypeId = _referenceTypeId) ->take() ;
		(includeSubtypes = _includeSubtypes) ->take() ;
		(nodeClassMask   = _nodeClassMask)   ->take() ;
		(resultMask      = _resultMask)      ->take() ;
	}

	virtual ~BrowseDescription()
	{
		nodeId          ->release() ;
		browseDirection ->release() ;
		referenceTypeId ->release() ;
		includeSubtypes ->release() ;
		nodeClassMask   ->release() ;
		resultMask      ->release() ;
	}

public:

	inline NodeId * getNodeId()
	{
		return nodeId ;
	}

	inline BrowseDirection * getBrowseDirection()
	{
		return browseDirection ;
	}

	inline NodeId * getReferenceTypeId()
	{
		return referenceTypeId ;
	}

	inline Boolean * getIncludeSubtypes()
	{
		return includeSubtypes ;
	}

	inline UInt32 * getNodeClassMask()
	{
		return nodeClassMask ;
	}

	inline UInt32 * getResultMask()
	{
		return resultMask ;
	}

};     /* class BrowseDescriotion */
}      /* namespace opcua */
#endif /* WITH_BROWSE */
#endif /* OPCUA_BROWSDESCRIPTION_H_ */
