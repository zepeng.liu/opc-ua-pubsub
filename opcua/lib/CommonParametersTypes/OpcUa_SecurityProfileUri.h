
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_SECURITYPROFILEURI_H_
#define OPCUA_SECURITYPROFILEURI_H_

/*
 * OPC-UA Part 7, p. 21
 * IdType =
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

//#include <openssl/x509v3.h>
//#include <openssl/bn.h>
//#include <openssl/asn1.h>
//#include <openssl/x509.h>
//#include <openssl/x509_vfy.h>
//#include <openssl/bio.h>
//#include <openssl/hmac.h>
//#ifdef __APPLE__
//#include <openssl/evp.h>
//#endif
//

namespace opcua
{

class MYDLL SecurityProfileUri
{
public:

	static String * SecurityPolicy_None ;
	static String * SecurityPolicy_Basic128Rsa15 ;
	static String * SecurityPolicy_256 ;

};
} /* namespace opcua */
#endif /* OPCUA_SECURITYPROFILEURI_H_ */
