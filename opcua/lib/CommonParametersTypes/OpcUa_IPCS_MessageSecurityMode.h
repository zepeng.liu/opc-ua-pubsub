
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MESSAGESECURITYMODE_H_
#define OPCUA_MESSAGESECURITYMODE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 7.14, p. 122
 */

enum _MessageSecurityMode {
	MessageSecurityMode_INVALID_0        = 0,
	MessageSecurityMode_NONE_1           = 1,
	MessageSecurityMode_SIGN_2           = 2,
	MessageSecurityMode_SIGNANDENCRYPT_3 = 3
} ;

class MYDLL MessageSecurityMode
		: public Enumeration
{
public:

	static MessageSecurityMode * invalid_0 ;
	static MessageSecurityMode * none_1 ;
	static MessageSecurityMode * sign_2 ;
	static MessageSecurityMode * signandencrypt_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_MessageSecurityMode ; }

public:

	static MessageSecurityMode * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MessageSecurityMode const& value)
	{
		if (OpcUa_MessageSecurityMode_Invalid <= value && value <= OpcUa_MessageSecurityMode_SignAndEncrypt)
			return new MessageSecurityMode((_MessageSecurityMode)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MessageSecurityMode& pValue) const
	{
		int32_t value = get() ;
		if (MessageSecurityMode_INVALID_0 <= value && value <= MessageSecurityMode_SIGNANDENCRYPT_3)
			pValue = (OpcUa_MessageSecurityMode)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	MessageSecurityMode(_MessageSecurityMode _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* OPCUA_MESSAGESECURITYMODE_H_ */
