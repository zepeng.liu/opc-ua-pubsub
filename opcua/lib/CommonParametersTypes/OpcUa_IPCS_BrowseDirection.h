
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSEDIRECTION_H_
#define OPCUA_BROWSEDIRECTION_H_

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 5.8.8.2, p. 39, inside Table 30
 */

enum _BrowseDirection {
	BrowseDirection_FORWARD_0        = 0,
	BrowseDirection_INVERSE_1        = 1,
	BrowseDirection_BOTH_2           = 2
} ;

class MYDLL BrowseDirection
		: public Enumeration
{
public:

	static BrowseDirection * forward_0 ;
	static BrowseDirection * inverse_1 ;
	static BrowseDirection * both_2 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowseDirection ; }

public:

	static BrowseDirection * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowseDirection const& value)
	{
		if (OpcUa_BrowseDirection_Forward <= value && value <= OpcUa_BrowseDirection_Both)
			return new BrowseDirection((_BrowseDirection)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowseDirection& pValue) const
	{
		int32_t value = get() ;
		if (BrowseDirection_FORWARD_0 <= value && value <= BrowseDirection_BOTH_2)
			pValue = (OpcUa_BrowseDirection)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	BrowseDirection(_BrowseDirection _value)
		: Enumeration(_value)
	{}

}; // Class
} /* namespace opcua */
#endif /* WITH_BROWSE */
#endif /* OPCUA_BROWSEDIRECTION_H_ */
