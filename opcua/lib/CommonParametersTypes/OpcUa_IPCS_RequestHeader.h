/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REQUESTHEADER_H_
#define OPCUA_REQUESTHEADER_H_

/*
 * OPC-UA Part 7, 7.26, p. 138
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_IntegerId.h"
#include "OpcUa_SessionAuthenticationToken.h"

namespace opcua {

class MYDLL RequestHeader
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RequestHeader ; }

private:

	NodeId          * authenticationToken ;
	UtcTime         * timeStamp ;
	IntegerId       * requestHandle ;
	UInt32          * returnDiagnostics ;
	String          * auditEntryId ;
	UInt32          * timeoutHint ;
	ExtensionObject * extensionObject ;

public:

	static RequestHeader * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader)
	{
		NodeId          * authenticationToken = NodeId          ::fromCtoCpp(pStatus, pRequestHeader.AuthenticationToken) ;
		UtcTime         * timeStamp           = UtcTime         ::fromCtoCpp(pStatus, pRequestHeader.Timestamp) ;
		IntegerId       * requestHandle       = IntegerId       ::fromCtoCpp(pStatus, pRequestHeader.RequestHandle) ;
		UInt32          * returnDiagnostics   = UInt32          ::fromCtoCpp(pStatus, pRequestHeader.ReturnDiagnostics) ;
		String          * auditEntryId        = String          ::fromCtoCpp(pStatus, pRequestHeader.AuditEntryId) ;

		UInt32          * timeoutHint         = UInt32			::fromCtoCpp(pStatus, pRequestHeader.TimeoutHint) ;
		ExtensionObject * extensionObject     = ExtensionObject ::fromCtoCpp(pStatus, pRequestHeader.AdditionalHeader) ;

		if (*pStatus == STATUS_OK)
			return
					new RequestHeader(
							authenticationToken,
							timeStamp,
							requestHandle,
							returnDiagnostics,
							auditEntryId,

							timeoutHint,
							extensionObject
							) ;

		if (authenticationToken != NULL)
			authenticationToken->checkRefCount() ;
		if (timeStamp != NULL)
			timeStamp->checkRefCount() ;
		if (requestHandle != NULL)
			requestHandle->checkRefCount() ;
		if (returnDiagnostics != NULL)
			returnDiagnostics->checkRefCount() ;
		if (auditEntryId != NULL)
			auditEntryId->checkRefCount() ;

		if (timeoutHint != NULL)
			timeoutHint->checkRefCount() ;
		if (extensionObject != NULL)
			extensionObject->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader& pRequestHeader) const
	{
		authenticationToken ->fromCpptoC(pStatus, pRequestHeader.AuthenticationToken) ;
		timeStamp           ->fromCpptoC(pStatus, pRequestHeader.Timestamp) ;
		requestHandle       ->fromCpptoC(pStatus, pRequestHeader.RequestHandle) ;
		returnDiagnostics   ->fromCpptoC(pStatus, pRequestHeader.ReturnDiagnostics) ;
		auditEntryId        ->fromCpptoC(pStatus, pRequestHeader.AuditEntryId) ;
		timeoutHint         ->fromCpptoC(pStatus, pRequestHeader.TimeoutHint) ;
		extensionObject     ->fromCpptoC(pStatus, pRequestHeader.AdditionalHeader) ;
	}

public:

	RequestHeader(
			NodeId          * _authenticationToken,
			UtcTime         * _timeStamp,
			IntegerId       * _requestHandle,
			UInt32          * _returnDiagnostics,
			String          * _auditEntryId,
			UInt32          * _timeoutHint,
			ExtensionObject * _extensionObject = ExtensionObject::null
			)
		: Structure()
	{
		(authenticationToken = _authenticationToken) ->take() ;
		(timeStamp           = _timeStamp)           ->take() ;
		(requestHandle       = _requestHandle)       ->take() ;
		(returnDiagnostics   = _returnDiagnostics)   ->take() ;
		(auditEntryId        = _auditEntryId)        ->take() ;
		(timeoutHint         = _timeoutHint)         ->take() ;
		(extensionObject     = _extensionObject)     ->take() ;
	}

	virtual ~RequestHeader()
	{
		authenticationToken ->release() ;
		timeStamp           ->release() ;
		requestHandle       ->release() ;
		returnDiagnostics   ->release() ;
		auditEntryId        ->release() ;
		timeoutHint         ->release() ;
		extensionObject     ->release() ;
	}

public:

	inline IntegerId * getRequestHandle()
	{
		return requestHandle ;
	}

	inline SessionAuthenticationToken * getAuthenticationToken()
	{
		return authenticationToken ;
	}

	inline void setTimeStamp(UtcTime * _timeStamp)
	{
		UtcTime * tmpTimeStamp = timeStamp ;
		(timeStamp = _timeStamp)->take() ;
		tmpTimeStamp->release() ;
	}

	inline void setRequestHandle(IntegerId * _requestHandle)
	{
		IntegerId * tmpRequestHandle = requestHandle ;
		(requestHandle = _requestHandle) ->take() ;
		tmpRequestHandle->release() ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_REQUESTHEADER_H_ */
