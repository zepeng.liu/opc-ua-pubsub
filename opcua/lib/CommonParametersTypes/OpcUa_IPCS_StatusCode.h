
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_STATUSCODE_H_
#define OPCUA_STATUSCODE_H_

/*
 * Part 4, 7.33
 * Manque plein d'informations
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua {

#define StatusCode_Overflow     ((int32_t)(1 << 7))

class MYDLL StatusCode
		: public UInt32
{
public:

	static StatusCode * const Good ;
	static StatusCode * const Bad_RequestTypeInvalid ;
	static StatusCode * const Bad_SecureChannelIdInvalid ;
	static StatusCode * const Bad_SessionIdInvalid ;
	static StatusCode * const Bad_CertificateUriInvalid;
	static StatusCode * const Bad_SessionNotActivated ;
	static StatusCode * const Bad_SessionClosed ;
	static StatusCode * const Bad_NotImplemented ;
	static StatusCode * const Bad_IndexRangeInvalid ;
	static StatusCode * const Bad_AttributeIdInvalid ;
	static StatusCode * const Bad_TypeMismatch ;
	static StatusCode * const Bad_NotReadable ;
	static StatusCode * const Bad_NotWritable ;
	static StatusCode * const Bad_UserAccessDenied ;
	static StatusCode * const Bad_NodeIdUnknown	;
	static StatusCode * const Bad_NotSupported	;
	static StatusCode * const Bad_TooManyPublishRequests ;
	static StatusCode * const Bad_SubscriptionIdInvalid ;
	static StatusCode * const Bad_NothingToDo ;
	static StatusCode * const Bad_MessageNotAvailable ;
	static StatusCode * const Bad_NoSubscription ;
	static StatusCode * const Good_SubscriptionTransferred ;
	static StatusCode * const Bad_Timeout ;
	static StatusCode * const Bad_DataEncodingUnsupported ;
	static StatusCode * const Bad_DataEncodingInvalid ;
	static StatusCode * const Bad_NodeClassInvalid ;
	static StatusCode * const Bad_MonitoredItemFilterUnsupported ;
	static StatusCode * const Bad_MonitoredItemIdInvalid ;
	static StatusCode * const Bad_TimestampsToReturnInvalid ;
	static StatusCode * const Bad_NodeIdInvalid	;
	static StatusCode * const Bad_ParentNodeIdInvalid ;
	static StatusCode * const Bad_ReferenceTypeIdInvalid ;
	static StatusCode * const Bad_NodeIdExists ;
	static StatusCode * const Bad_TypeDefinitionInvalid ;
	static StatusCode * const Bad_NodeAttributesInvalid ;
	static StatusCode * const Bad_SourceNodeIdInvalid ;
	static StatusCode * const Bad_TargetNodeIdInvalid ;
	static StatusCode * const Bad_MaxAgeInvalid ;
	static StatusCode * const Bad_OutOfRange ;
	static StatusCode * const Bad_IndexRangeNoData ;
	static StatusCode * const Bad_NoMatch ;
	static StatusCode * const Bad_SequenceNumberUnknown ;

	static TableStatusCode   * const tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_StatusCode ; }

public:

	static inline StatusCode * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_StatusCode const& pStatusCode)
	{
		debug_i(IPCS_DBG,"StatusCode","Aller fromCtoCpp: value = 0x%08x",(uint32_t)pStatusCode) ;
		NOT_USED(pStatus) ;
		return new StatusCode((uint32_t)pStatusCode) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_StatusCode& pStatusCode) const
	{
		debug_i(IPCS_DBG,"StatusCode","Retour fromCpptoC: value = 0x%08x",get()) ;
		NOT_USED(pStatus) ;
		pStatusCode = (SOPC_StatusCode)get() ;
	}

public:

	StatusCode(uint32_t _value)
		: UInt32(_value)
	{}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_StatusCode,this) ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_STATUSCODE_H_ */
