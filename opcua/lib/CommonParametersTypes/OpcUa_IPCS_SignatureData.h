
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SIGNATUREDATA_H_
#define OPCUA_SIGNATUREDATA_H_

/*
 * OPC-UA Part 4, 7.30, p. 141
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"


namespace opcua {

class MYDLL SignatureData
		: public Structure
{
public:

	static SignatureData * fakeSignatureData ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_SignatureData ; }

private:

	String     * algorithm ;
	ByteString * signature ;

public:

	static SignatureData * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_SignatureData const& pSignatureData)
	{
		String     * algorithm = String     ::fromCtoCpp(pStatus, pSignatureData.Algorithm) ;
		ByteString * signature = ByteString ::fromCtoCpp(pStatus, pSignatureData.Signature) ;

		if (*pStatus == STATUS_OK)
			return
					new SignatureData(
							algorithm,
							signature
							) ;

		if (algorithm != NULL)
			algorithm->checkRefCount() ;
		if (signature != NULL)
			signature->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_SignatureData& pSignatureData) const
	{
		algorithm ->fromCpptoC(pStatus, pSignatureData.Algorithm) ;
		signature ->fromCpptoC(pStatus, pSignatureData.Signature) ;
	}

public:

	SignatureData(
			String     * _algorithm,
			ByteString * _signature
			)
		: Structure()
	{
		(algorithm = _algorithm) ->take() ;
		(signature = _signature) ->take() ;
	}

	virtual ~SignatureData()
	{
		algorithm ->release() ;
		signature ->release() ;
	}

public:

	inline String * getAlgorithm()
	{
		return algorithm;
	}

	inline ByteString * getSignature()
	{
		return signature;
	}

};
} /* namespace opcua */
#endif /* OPCUA_RESPONSEHEADER_H_ */
