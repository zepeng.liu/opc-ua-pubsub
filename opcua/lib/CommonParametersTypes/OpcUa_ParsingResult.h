
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_PARSINGRESULT_H_
#define OPCUA_PARSINGRESULT_H_

/*
 * OPC-UA Part 4, 5.9.3.2, p. 49, inside Table 44
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_IPCS_DiagnosticInfo.h"

namespace opcua {

class MYDLL ParsingResult
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ParsingResult ; }

private:

	StatusCode            * statusCode ;
	TableStatusCode       * dataStatusCodes ;
	TableDiagnosticInfo   * dataDiagnosticInfos ;

public:

	ParsingResult(
			StatusCode            * _statusCode,
			TableStatusCode       * _dataStatusCodes,
			TableDiagnosticInfo   * _dataDiagnosticInfos
			)
		: Structure()
	{
		(statusCode          = _statusCode)          ->take() ;
		(dataStatusCodes     = _dataStatusCodes)     ->take() ;
		(dataDiagnosticInfos = _dataDiagnosticInfos) ->take() ;
	}

	virtual ~ParsingResult()
	{
		statusCode          ->release() ;
		dataStatusCodes     ->release() ;
		dataDiagnosticInfos ->release() ;
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline TableStatusCode   * getDataStatusCodes()
	{
		return dataStatusCodes ;
	}

	inline TableDiagnosticInfo   * getDataDiagnosticInfos()
	{
		return dataDiagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_PARSINGRESULT_H_ */
