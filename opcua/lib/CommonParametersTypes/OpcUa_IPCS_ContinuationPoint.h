
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CONTINUATIONPOINT_H_
#define OPCUA_CONTINUATIONPOINT_H_

/*
 * OPC-UA Part4, 7.6, p. 116
 */

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../StandardDataTypes/All.h"

#include <string.h>

namespace opcua {

class MYDLL ContinuationPoint
		: public ByteString
{
public:

	static ContinuationPoint * empty ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ContinuationPoint ; }

public:

	static inline ContinuationPoint * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ByteString const& pByteString)
	{
		NOT_USED(pStatus) ;
		return new ContinuationPoint(pByteString.Length,pByteString.Data) ;
	}

public:

	ContinuationPoint(int32_t const _length, const uint8_t * const _array)
		: ByteString(_length,_array)
	{}

public:

	inline bool isNullOrEmpty()
	{
		return ByteString::isNullOrEmpty() ;
	}

public:

	void * getPtr() const
	{
		void * ptr ;
		memcpy(&ptr,getArray(),getLen()) ;
		return ptr ;
	}


};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_CONTINUATIONPOINT_H_ */
