
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_READVALUEIDDEFINE_H_
#define OPCUA_READVALUEIDDEFINE_H_

namespace opcua {

/* Part6, A.1, p. 52 */

#define AttributeId_NodeId					((int32_t)  1)
#define AttributeId_NodeClass				((int32_t)  2)
#define AttributeId_BrowseName				((int32_t)  3)
#define AttributeId_DisplayName				((int32_t)  4)
#define AttributeId_Description				((int32_t)  5)
#define AttributeId_WriteMask				((int32_t)  6)
#define AttributeId_UserWriteMask			((int32_t)  7)
#define AttributeId_IsAbstract				((int32_t)  8)
#define AttributeId_Symmetric				((int32_t)  9)
#define AttributeId_InverseName				((int32_t) 10)
#define AttributeId_ContainsNoLoops			((int32_t) 11)
#define AttributeId_EventNotifier			((int32_t) 12)
#define AttributeId_Value					((int32_t) 13)
#define AttributeId_DataType				((int32_t) 14)
#define AttributeId_ValueRank				((int32_t) 15)
#define AttributeId_ArrayDimensions			((int32_t) 16)
#define AttributeId_AccessLevel				((int32_t) 17)
#define AttributeId_UserAccessLevel			((int32_t) 18)
#define AttributeId_MinimumSamplingInterval	((int32_t) 19)
#define AttributeId_Historizing				((int32_t) 20)
#define AttributeId_Executable				((int32_t) 21)
#define AttributeId_UserExecutable			((int32_t) 22)


} /* namespace opcua */
#endif /* OPCUA_READVALUEIDDEFINE_H_ */
