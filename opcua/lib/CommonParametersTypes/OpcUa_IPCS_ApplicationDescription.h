
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_APPLICATIONDESCRIPTION_H_
#define OPCUA_APPLICATIONDESCRIPTION_H_

/*
 * Part 4, 7.1, p. 106
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_ApplicationType.h"

namespace opcua {

class MYDLL ApplicationDescription
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ApplicationDescription ; }

private:

	String          * applicationUri ;
	String          * productUri ;
	LocalizedText   * applicationName ;
	ApplicationType * applicationType ;
	String          * gatewwayServerUri ;

	String          * discoveryProfileUri ;
	TableString     * discoveryUrls ;

public:

	static ApplicationDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ApplicationDescription const& pApplicationDescription)
	{
		String          * applicationUri      = String          ::fromCtoCpp(pStatus, pApplicationDescription.ApplicationUri) ;
		String          * productUri          = String          ::fromCtoCpp(pStatus, pApplicationDescription.ProductUri) ;
		LocalizedText   * applicationName     = LocalizedText   ::fromCtoCpp(pStatus, pApplicationDescription.ApplicationName) ;
		ApplicationType * applicationType     = ApplicationType ::fromCtoCpp(pStatus, pApplicationDescription.ApplicationType) ;
		String          * gatewwayServerUri   = String          ::fromCtoCpp(pStatus, pApplicationDescription.GatewayServerUri) ;

		String          * discoveryProfileUri = String	        ::fromCtoCpp(pStatus, pApplicationDescription.DiscoveryProfileUri) ;
		TableString     * discoveryUrls       = TableString::fromCtoCpp(pStatus, pApplicationDescription.NoOfDiscoveryUrls, pApplicationDescription.DiscoveryUrls) ;

		if (*pStatus == STATUS_OK)
			return
					new ApplicationDescription(
							applicationUri,
							productUri,
							applicationName,
							applicationType,
							gatewwayServerUri,

							discoveryProfileUri,
							discoveryUrls
					) ;

		if (applicationUri != NULL)
			applicationUri->checkRefCount() ;
		if (productUri != NULL)
			productUri->checkRefCount() ;
		if (applicationName != NULL)
			applicationName->checkRefCount() ;
		if (applicationType != NULL)
			applicationType->checkRefCount() ;
		if (gatewwayServerUri != NULL)
			gatewwayServerUri->checkRefCount() ;

		if (discoveryProfileUri != NULL)
			discoveryProfileUri->checkRefCount() ;
		if (discoveryUrls != NULL)
			discoveryUrls->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ApplicationDescription& pApplicationDescription) const
	{
		applicationUri      ->fromCpptoC(pStatus, pApplicationDescription.ApplicationUri) ;
		productUri          ->fromCpptoC(pStatus, pApplicationDescription.ProductUri) ;
		applicationName     ->fromCpptoC(pStatus, pApplicationDescription.ApplicationName) ;
		applicationType     ->fromCpptoC(pStatus, pApplicationDescription.ApplicationType) ;
		gatewwayServerUri   ->fromCpptoC(pStatus, pApplicationDescription.GatewayServerUri) ;

		discoveryProfileUri ->fromCpptoC(pStatus, pApplicationDescription.DiscoveryProfileUri) ;
		discoveryUrls       ->fromCpptoC(pStatus, pApplicationDescription.NoOfDiscoveryUrls,pApplicationDescription.DiscoveryUrls) ;
	}

public:

	ApplicationDescription(
			String          * _applicationUri,
			String          * _productUri,
			LocalizedText   * _applicationName,
			ApplicationType * _applicationType,
			String          * _gatewwayServerUri,
			String          * _discoveryProfileUri,
			TableString     * _discoveryUrls
			)
		: Structure()
	{
		(applicationUri      = _applicationUri)      ->take() ;
		(productUri          = _productUri)          ->take() ;
		(applicationName     = _applicationName)     ->take() ;
		(applicationType     = _applicationType)     ->take() ;
		(gatewwayServerUri   = _gatewwayServerUri)   ->take() ;
		(discoveryProfileUri = _discoveryProfileUri) ->take() ;
		(discoveryUrls       = _discoveryUrls)       ->take() ;
	}

	virtual ~ApplicationDescription()
	{
		applicationUri      ->release() ;
		productUri          ->release() ;
		applicationName     ->release() ;
		applicationType     ->release() ;
		gatewwayServerUri   ->release() ;
		discoveryProfileUri ->release() ;
		discoveryUrls       ->release() ;
	}

public:

#if (VERIF_APPURI == VERIF_APPURI_ENABLED)
	String * getApplicationUri ()
	{
		return applicationUri ;
	}
#endif

};
} /* namespace opcua */
#endif /* APPLICATIONDESCRIPTION_H_ */
