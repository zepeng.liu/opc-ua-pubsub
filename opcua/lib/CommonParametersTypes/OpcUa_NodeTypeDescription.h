
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_NODETYPEDESCRIPTION_H_
#define OPCUA_NODETYPEDESCRIPTION_H_

/*
 * OPC-UA, Part 4, 5.9.3.2, p. 48 in Table 43
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_QueryDataDescription.h"

namespace opcua {

class MYDLL NodeTypeDescription
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_NodeTypeDescription ; }

private:

	ExpandedNodeId              * typeDefinitionNode ;
	Boolean                     * includeSubtypes ;
	TableQueryDataDescription   * dataToReturn ;

public:

	NodeTypeDescription(
			ExpandedNodeId              * _typeDefinitionNode,
			Boolean                     * _includeSubtypes,
			TableQueryDataDescription   * _dataToReturn
			)
		: Structure()
	{
		(typeDefinitionNode = _typeDefinitionNode) ->take() ;
		(includeSubtypes    = _includeSubtypes)    ->take() ;
		(dataToReturn       = _dataToReturn)       ->take() ;
	}

	virtual ~NodeTypeDescription()
	{
		typeDefinitionNode ->release() ;
		includeSubtypes    ->release() ;
		dataToReturn       ->release() ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_NODETYPEDESCRIPTION_H_ */
