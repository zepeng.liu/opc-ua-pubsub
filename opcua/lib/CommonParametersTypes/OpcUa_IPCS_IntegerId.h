
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_INTEGERID_H_
#define OPCUA_INTEGERID_H_

/*
 * OPC-UA Part 4, 7.13
 */

#include "../OpcUa.h"

#include "../StandardDataTypes/OpcUa_IPCS_UInt32.h"
#include "../Utils/OpcUa_Alea.h"

namespace opcua {

class MYDLL IntegerId
		: public UInt32
{
public:

	static IntegerId * AttributeId_Value_as_IntegerId ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_IntegerId ; }

public:

	IntegerId(uint32_t _value)
		: UInt32(_value)
	{
	}

public:

	static inline IntegerId * fromCtoCpp(SOPC_StatusCode * pStatus, uint32_t const& value)
	{
		NOT_USED(pStatus) ;
		return new IntegerId(value) ;
	}

public:

	static IntegerId * alea()
	{
		while (true) {
			uint32_t result = Alea::alea->random_uint32() ;

			if (result != 0) {
				return new IntegerId(Alea::alea->random_uint32()) ;
			}
		}
	}

	static IntegerId * alea(int32_t i, int32_t n)
	{
		uint32_t result = Alea::alea->random_uint32() ;

		result = (result - (result % n)) + i ;

		if (result == 0)
			result = n ;

		return new IntegerId(result) ;
	}

} ;
}      /* namespace opcua */
#endif /* OPCUA_INTEGERID_H_ */
