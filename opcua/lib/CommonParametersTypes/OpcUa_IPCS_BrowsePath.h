
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSEPATH_H_
#define OPCUA_BROWSEPATH_H_

/*
 * Part 4, 5.8.4.2, p. 43
 */

#include "../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_RelativePath.h"

namespace opcua {

class MYDLL BrowsePath
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowsePath ; }

private:

	NodeId       * startingNode ;
	RelativePath * relativePath ;

public:

	static BrowsePath * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowsePath const& pBrowsePath)
	{
		NodeId       * startingNode = NodeId       ::fromCtoCpp( pStatus, pBrowsePath.StartingNode) ;
		RelativePath * relativePath = RelativePath ::fromCtoCpp( pStatus, pBrowsePath.RelativePath) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowsePath(
							startingNode,
							relativePath
							) ;

		if (startingNode != NULL)
			startingNode->checkRefCount() ;
		if (relativePath != NULL)
			relativePath->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowsePath& pBrowsePath) const
	{
		startingNode ->fromCpptoC (pStatus, pBrowsePath.StartingNode) ;
		relativePath ->fromCpptoC (pStatus, pBrowsePath.RelativePath) ;
	}

public:

	BrowsePath(
			NodeId        * _startingNode,
			RelativePath  * _relativePath
			)
		: Structure()
	{
		(startingNode = _startingNode) ->take() ;
		(relativePath = _relativePath) ->take() ;
	}

	virtual ~BrowsePath()
	{
		startingNode ->release() ;
		relativePath ->release() ;
	}

public:

	virtual inline NodeId * getStartingNode()
	{
		return startingNode ;
	}

	inline RelativePath * getRelativePath()
	{
		return relativePath;
	}

};     /* class BrowsePath */
}      /* namespace opcua */
#endif /* WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS */
#endif /* OPCUA_BROWSEPATH_H_ */
