
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

#include "OpcUa_IPCS_ElementOperand.h"
#include "OpcUa_IPCS_LiteralOperand.h"
#include "OpcUa_IPCS_AttributeOperand.h"
#include "OpcUa_IPCS_SimpleAttributeOperand.h"

#if (WITH_QUERY == 1)

namespace opcua {

	MYDLL FilterOperand * FilterOperand::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pFilterOperand)
	{
		ExpandedNodeId * expandedNodeId   = ExpandedNodeId::fromCtoCpp(pStatus,pFilterOperand.TypeId) ;

		if (*pStatus != STATUS_OK)
			return NULL ;

		NodeId * parameterTypeId = expandedNodeId->getNodeId() ;

		Byte * encoding = NULL ;

		if (parameterTypeId->getNamespaceIndex()->get() == 0 && parameterTypeId->getIdentifierType()->get() == IdType_NUMERIC_0) {

			encoding = Byte::fromCtoCpp(pStatus,pFilterOperand.Encoding) ;

			if (*pStatus != STATUS_OK) {
				*pStatus = _Bad_DecodingError ;
				expandedNodeId->checkRefCount() ;
				return NULL ;
			}

			if (encoding->equals((uint8_t)0x03)) {
				switch (pFilterOperand.Body.Object.ObjType->TypeId) {
				case OpcUaId_ElementOperand:
				{
					_OpcUa_ElementOperand * elementOperand = static_cast<_OpcUa_ElementOperand *>(pFilterOperand.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return ElementOperand::fromCtoCpp(pStatus,*elementOperand) ;
				}
				case OpcUaId_LiteralOperand:
				{
					_OpcUa_LiteralOperand * literalOperand = static_cast<_OpcUa_LiteralOperand *>(pFilterOperand.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return LiteralOperand::fromCtoCpp(pStatus,*literalOperand) ;
				}
				case OpcUaId_AttributeOperand:
				{
					_OpcUa_AttributeOperand * attributeOperand = static_cast<_OpcUa_AttributeOperand *>(pFilterOperand.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return AttributeOperand::fromCtoCpp(pStatus,*attributeOperand) ;
				}
				case OpcUaId_SimpleAttributeOperand:
				{
					_OpcUa_SimpleAttributeOperand * simpleAttributeOperand = static_cast<_OpcUa_SimpleAttributeOperand *>(pFilterOperand.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return SimpleAttributeOperand::fromCtoCpp(pStatus,*simpleAttributeOperand) ;
				}
				default:
					break ;
				}
			} else {
				debug_i(COM_ERR,"FilterOperand","fromCtoCpp: Encoding is not 0x01, it is 0x%02x",encoding->get()) ;
			}
		}

		*pStatus = _Bad_DecodingError ;

		if (expandedNodeId != NULL)
			expandedNodeId->checkRefCount() ;
		if (encoding != NULL)
			encoding->checkRefCount() ;

		return NULL ;
	}

} // namespace

#endif // WITH_QUERY

