
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_READVALUEID_H_
#define OPCUA_READVALUEID_H_

/*
 * OPC-UA Part 4, 7.23, p. 135
 */

#include "../OpcUa.h"

#if (WITH_READ == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_IntegerId.h"
#include "OpcUa_IPCS_NumericRange.h"

#include "OpcUa_ReadValueIdDefine.h"

namespace opcua {

class MYDLL ReadValueId
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ReadValueId ; }

private:

	NodeId        * nodeId ;
	IntegerId     * attributeId ;
	NumericRange  * indexRange ;
	QualifiedName * dataEncoding ;

public:

	static ReadValueId * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ReadValueId const& pReadValueId)
	{
		NodeId        * nodeId       = NodeId        ::fromCtoCpp(pStatus, pReadValueId.NodeId) ;
		IntegerId     * attributeId  = IntegerId     ::fromCtoCpp(pStatus, pReadValueId.AttributeId) ;
		NumericRange  * indexRange   = NumericRange  ::fromCtoCpp(pStatus, pReadValueId.IndexRange) ;
		QualifiedName * dataEncoding = QualifiedName ::fromCtoCpp(pStatus, pReadValueId.DataEncoding) ;

		if (*pStatus == STATUS_OK)
			return
					new ReadValueId(
							nodeId,
							attributeId,
							indexRange,
							dataEncoding
							) ;

		if (nodeId != NULL)
			nodeId->checkRefCount() ;
		if (attributeId != NULL)
			attributeId->checkRefCount() ;
		if (indexRange != NULL)
			indexRange->checkRefCount() ;
		if (dataEncoding != NULL)
			dataEncoding->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ReadValueId& pReadValueId) const
	{
		nodeId       ->fromCpptoC(pStatus, pReadValueId.NodeId) ;
		attributeId  ->fromCpptoC(pStatus, pReadValueId.AttributeId) ;
		indexRange   ->fromCpptoC(pStatus, pReadValueId.IndexRange) ;
		dataEncoding ->fromCpptoC(pStatus, pReadValueId.DataEncoding) ;
	}

public:

	ReadValueId(
			NodeId        * _nodeId,
			IntegerId     * _attributeId,
			NumericRange  * _indexRange,
			QualifiedName * _dataEncoding
			)
		: Structure()
	{
		(nodeId       = _nodeId)       ->take() ;
		(attributeId  = _attributeId)  ->take() ;
		(indexRange   = _indexRange)   ->take() ;
		(dataEncoding = _dataEncoding) ->take() ;
	}

	virtual ~ReadValueId()
	{
		nodeId       ->release() ;
		attributeId  ->release() ;
		indexRange   ->release() ;
		dataEncoding ->release() ;
	}

public:

	inline NodeId * getNodeId()
	{
		return nodeId ;
	}

	inline IntegerId * getAttributeId()
	{
		return attributeId ;
	}

	inline NumericRange * getIndexRange()
	{
		return indexRange ;
	}

	inline QualifiedName * getDataEncoding()
	{
		return dataEncoding ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_READVALUEID_H_ */
