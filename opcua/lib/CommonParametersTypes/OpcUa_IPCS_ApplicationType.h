
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_APPLICATIONTYPE_H_
#define OPCUA_APPLICATIONTYPE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/* OPC-UA Part 4, 7.1, p. 106 in Table 103 */

enum _ApplicationType {
	ApplicationType_SERVER_0 = 0,
	ApplicationType_CLIENT_1 = 1,
	ApplicationType_CLIENTANDSERVER_2 = 2,
	ApplicationType_DISCOVERYSERVER_3 = 3
} ;

class MYDLL ApplicationType
		: public Enumeration
{
public:

	static ApplicationType * server_0 ;
	static ApplicationType * client_1 ;
	static ApplicationType * clientandserver_2 ;
	static ApplicationType * discoveryserver_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ApplicationType ; }

public:

	static ApplicationType * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ApplicationType const& value)
	{
		if (OpcUa_ApplicationType_Server <= value && value <= OpcUa_ApplicationType_DiscoveryServer)
			return new ApplicationType((_ApplicationType)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ApplicationType& pValue) const
	{
		int32_t value = get() ;
		if (ApplicationType_SERVER_0 <= value && value <= ApplicationType_DISCOVERYSERVER_3)
			pValue = (OpcUa_ApplicationType)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	ApplicationType(_ApplicationType _value)
		: Enumeration(_value)
	{}

}; // Class
} /* namespace opcua */
#endif /* OPCUA_SECURITYTOKENREQUESTTYPE_H_ */
