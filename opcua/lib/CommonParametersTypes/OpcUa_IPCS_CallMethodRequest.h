/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CALLMETHODREQUEST_H_
#define OPCUA_CALLMETHODREQUEST_H_

/*
 * OPC-UA Part 4, 5.11.2.2, p. 60
 */

#include "../OpcUa.h"

#if (WITH_CALL == 1)

#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL CallMethodRequest
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CallMethodRequest ; }

private:

	NodeId         * objectId ;
	NodeId         * methodId ;
	TableVariant   * inputArguments ;

public:

	static CallMethodRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_CallMethodRequest const& pCallMethodRequest)
	{
		NodeId         * objectId       = NodeId ::fromCtoCpp(pStatus, pCallMethodRequest.ObjectId) ;
		NodeId         * methodId       = NodeId ::fromCtoCpp(pStatus, pCallMethodRequest.MethodId) ;
		TableVariant   * inputArguments = TableVariant::fromCtoCpp(pStatus, pCallMethodRequest.NoOfInputArguments,pCallMethodRequest.InputArguments) ;

		if (*pStatus == STATUS_OK)
			return
					new CallMethodRequest(
							objectId,
							methodId,
							inputArguments
							) ;

		if (objectId != NULL)
			objectId->checkRefCount() ;
		if (methodId != NULL)
			methodId->checkRefCount() ;
		if (inputArguments != NULL)
			inputArguments->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_CallMethodRequest& pCallMethodRequest) const
	{
		objectId       ->fromCpptoC(pStatus, pCallMethodRequest.ObjectId) ;
		methodId       ->fromCpptoC(pStatus, pCallMethodRequest.MethodId) ;
		inputArguments ->fromCpptoC(pStatus, pCallMethodRequest.NoOfInputArguments,pCallMethodRequest.InputArguments) ;
	}

public:

	CallMethodRequest(
			NodeId         * _objectId,
			NodeId         * _methodId,
			TableVariant   * _inputArguments
			)
		: Structure()
	{
		(objectId       = _objectId)       ->take() ;
		(methodId       = _methodId)       ->take() ;
		(inputArguments = _inputArguments) ->take() ;
	}

	virtual ~CallMethodRequest()
	{
		objectId       ->release() ;
		methodId       ->release() ;
		inputArguments ->release() ;
	}

public:

	inline NodeId * getObjectId()
	{
		return objectId ;
	}

	inline NodeId * getMethodId()
	{
		return methodId ;
	}

	inline TableVariant   * getInputArguments()
	{
		return inputArguments ;
	}

};     /* class CallMethodRequest */
}      /* namespace opcua */
#endif /* #if (WITH_CALL == 1) */
#endif /* OPCUA_CALLMETHODREQUEST_H_ */
