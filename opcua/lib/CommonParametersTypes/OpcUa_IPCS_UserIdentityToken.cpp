
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

#include "OpcUa_IPCS_UserIdentityToken.h"
#include "OpcUa_IPCS_AnonymousIdentityToken.h"
#include "OpcUa_IPCS_UserNameIdentityToken.h"
#include "OpcUa_IPCS_X509IdentityToken.h"
#include "OpcUa_IPCS_IssuedIdentityToken.h"

namespace opcua {

	MYDLL UserIdentityToken * UserIdentityToken::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pUserIdentityToken)
	{
		ExpandedNodeId * expandedNodeId   = ExpandedNodeId::fromCtoCpp(pStatus,pUserIdentityToken.TypeId) ;

		if (*pStatus != STATUS_OK)
			return NULL ;

		NodeId * parameterTypeId = expandedNodeId->getNodeId() ;

		Byte * encoding = NULL ;

		if (parameterTypeId->getNamespaceIndex()->get() == 0 && parameterTypeId->getIdentifierType()->get() == IdType_NUMERIC_0) {

			encoding = Byte::fromCtoCpp(pStatus,pUserIdentityToken.Encoding) ;

			if (*pStatus != STATUS_OK) {
				*pStatus = _Bad_DecodingError ;
				expandedNodeId->checkRefCount() ;
				return NULL ;
			}

			if (encoding->equals((uint8_t)0x03)) {
				switch (pUserIdentityToken.Body.Object.ObjType->TypeId) {
				case OpcUaId_AnonymousIdentityToken:
				{
					_OpcUa_AnonymousIdentityToken * tokenAnonymousIdentityToken = static_cast<_OpcUa_AnonymousIdentityToken *>(pUserIdentityToken.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return AnonymousIdentityToken::fromCtoCpp(pStatus,*tokenAnonymousIdentityToken) ;
				}
				case OpcUaId_UserNameIdentityToken:
				{
					_OpcUa_UserNameIdentityToken * tokenUserNameIdentityToken = static_cast<_OpcUa_UserNameIdentityToken *>(pUserIdentityToken.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return UserNameIdentityToken::fromCtoCpp(pStatus,*tokenUserNameIdentityToken) ;
				}
				case OpcUaId_X509IdentityToken:
				{
					_OpcUa_X509IdentityToken * tokenX509IdentityToken = static_cast<_OpcUa_X509IdentityToken *>(pUserIdentityToken.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return X509IdentityToken::fromCtoCpp(pStatus,*tokenX509IdentityToken) ;
				}
				case OpcUaId_IssuedIdentityToken:
				{
					_OpcUa_IssuedIdentityToken * tokenIssuedIdentityToken = static_cast<_OpcUa_IssuedIdentityToken *>(pUserIdentityToken.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return IssuedIdentityToken::fromCtoCpp(pStatus,*tokenIssuedIdentityToken) ;
				}
				default:
					break ;
				}
			} else {
				debug_i(COM_ERR,"UserIdentityToken","fromCtoCpp: Encoding is not 0x01, it is 0x%02x",encoding->get()) ;
			}
		}

		*pStatus = _Bad_DecodingError ;

		if (expandedNodeId != NULL)
			expandedNodeId->checkRefCount() ;
		if (encoding != NULL)
			encoding->checkRefCount() ;

		return NULL ;
	}

} // namespace



