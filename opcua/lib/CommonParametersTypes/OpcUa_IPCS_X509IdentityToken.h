
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_X509IDENTITYTOKEN_H_
#define OPCUA_X509IDENTITYTOKEN_H_

/*
 * OPC-UA Part 4, 7.35.4, p. 149
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_UserIdentityToken.h"

namespace opcua {

class MYDLL X509IdentityToken
	: public UserIdentityToken
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_X509IdentityToken ; }

private:

	String     * policyId ;
	ByteString * certificateData ;

public:

	static X509IdentityToken * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_X509IdentityToken const& pX509IdentityToken)
	{
		String     * policyId        = String     ::fromCtoCpp(pStatus,pX509IdentityToken.PolicyId) ;
		ByteString * certificateData = ByteString ::fromCtoCpp(pStatus,pX509IdentityToken.CertificateData) ;

		if (*pStatus == STATUS_OK)
			return
					new X509IdentityToken(
							policyId,
							certificateData
							) ;

		if (policyId != NULL)
			policyId->checkRefCount() ;
		if (certificateData != NULL)
			certificateData->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pX509IdentityToken) const
	{
		SOPC_ExtensionObject_Initialize(&pX509IdentityToken) ;

		expandedNodeId->fromCpptoC(pStatus,pX509IdentityToken.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pX509IdentityToken.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pX509IdentityToken.Body.Object.ObjType = &OpcUa_X509IdentityToken_EncodeableType ;
			pX509IdentityToken.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_X509IdentityToken)) ;
			OpcUa_X509IdentityToken_Initialize(pX509IdentityToken.Body.Object.Value) ;
			X509IdentityToken::fromCpptoC(pStatus,*static_cast<_OpcUa_X509IdentityToken *>(pX509IdentityToken.Body.Object.Value)) ;
			pX509IdentityToken.Length = sizeof(_OpcUa_X509IdentityToken) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_X509IdentityToken& pX509IdentityToken) const
	{
		policyId        ->fromCpptoC(pStatus,pX509IdentityToken.PolicyId) ;
		certificateData ->fromCpptoC(pStatus,pX509IdentityToken.CertificateData) ;
	}

public:

	X509IdentityToken(
			String     * _policyId,
			ByteString * _certificateData
			)
		: UserIdentityToken()
	{
		(policyId        = _policyId)        ->take() ;
		(certificateData = _certificateData) ->take() ;
	}

	virtual ~X509IdentityToken()
	{
		policyId        ->release() ;
		certificateData ->release() ;
	}

public:

	inline String * getPolicyId()
	{
		return policyId ;
	}

	inline ByteString * getCertificateData()
	{
		return certificateData ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_X509IDENTITYTOKEN_H_ */
