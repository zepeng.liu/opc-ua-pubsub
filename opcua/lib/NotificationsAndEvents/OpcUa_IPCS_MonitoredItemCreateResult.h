/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITOREDITEMCREATERESULT_H_
#define OPCUA_MONITOREDITEMCREATERESULT_H_

/*
 * OPC-UA Part 4, 5.12.2.2, p. 67, Table 64 (inside the table)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringFilterResult.h"
#include "OpcUa_IPCS_MonitoringNullFilterResult.h"

namespace opcua {

class MYDLL MonitoredItemCreateResult
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemCreateResult; }

private:

	StatusCode             * statusCode ;
	IntegerId              * monitoredItemId ;
	Duration               * revisedSamplingInterval ;
	Counter                * revisedQueueSize ;
	MonitoringFilterResult * filterResult ;

public:

	static MonitoredItemCreateResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemCreateResult const& pMonitoredItemCreateResult)
	{
		StatusCode             * statusCode              = StatusCode             ::fromCtoCpp (pStatus, pMonitoredItemCreateResult.StatusCode) ;
		IntegerId              * monitoredItemId         = IntegerId              ::fromCtoCpp (pStatus, pMonitoredItemCreateResult.MonitoredItemId) ;
		Duration               * revisedSamplingInterval = Duration               ::fromCtoCpp (pStatus, pMonitoredItemCreateResult.RevisedSamplingInterval) ;
		Counter                * revisedQueueSize        = Counter                ::fromCtoCpp (pStatus, pMonitoredItemCreateResult.RevisedQueueSize) ;
		MonitoringFilterResult * filterResult            = MonitoringFilterResult ::fromCtoCpp (pStatus, pMonitoredItemCreateResult.FilterResult) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoredItemCreateResult(
							statusCode,
							monitoredItemId,
							revisedSamplingInterval,
							revisedQueueSize,
							filterResult
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (monitoredItemId != NULL)
			monitoredItemId->checkRefCount() ;
		if (revisedSamplingInterval != NULL)
			revisedSamplingInterval->checkRefCount() ;
		if (revisedQueueSize != NULL)
			revisedQueueSize->checkRefCount() ;
		if (filterResult != NULL)
			filterResult->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemCreateResult& pMonitoredItemCreateResult) const
	{
		statusCode              ->fromCpptoC(pStatus, pMonitoredItemCreateResult.StatusCode) ;
		monitoredItemId         ->fromCpptoC(pStatus, pMonitoredItemCreateResult.MonitoredItemId) ;
		revisedSamplingInterval ->fromCpptoC(pStatus, pMonitoredItemCreateResult.RevisedSamplingInterval) ;
		revisedQueueSize        ->fromCpptoC(pStatus, pMonitoredItemCreateResult.RevisedQueueSize) ;
		filterResult            ->fromCpptoC(pStatus, pMonitoredItemCreateResult.FilterResult) ;
	}

public:

	MonitoredItemCreateResult(
			StatusCode             * _statusCode,
			IntegerId              * _monitoredItemId,
			Duration               * _revisedSamplingInterval,
			Counter                * _revisedQueueSize,
			MonitoringFilterResult * _filterResult
			)
		: Structure()
	{
		(statusCode              = _statusCode)              ->take();
		(monitoredItemId         = _monitoredItemId)         ->take();
		(revisedSamplingInterval = _revisedSamplingInterval) ->take();
		(revisedQueueSize        = _revisedQueueSize)        ->take();
		(filterResult            = _filterResult)            ->take();
	}

	MonitoredItemCreateResult(StatusCode * _statusCode)
		: Structure()
	{
		(statusCode              = _statusCode)                                  ->take();
		(monitoredItemId         = new IntegerId(-1))                            ->take();
		(revisedSamplingInterval = Duration::zero)                               ->take();
		(revisedQueueSize        = Counter::zero)                                ->take();
		(filterResult            = MonitoringNullFilterResult::nullFilterResult) ->take() ;
	}


	virtual ~MonitoredItemCreateResult()
	{
		statusCode              ->release();
		monitoredItemId         ->release();
		revisedSamplingInterval ->release();
		revisedQueueSize        ->release();
		filterResult            ->release();
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode;
	}

	inline IntegerId * getMonitoredItemId()
	{
		return monitoredItemId;
	}

	inline Duration * getRevisedSamplingInterval()
	{
		return revisedSamplingInterval;
	}

	inline Counter * getRevisedQueueSize()
	{
		return revisedQueueSize;
	}

	inline MonitoringFilterResult * getFilterResult()
	{
		return filterResult;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITOREDITEMCREATERESULT_H_ */
