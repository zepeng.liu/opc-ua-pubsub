/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALLNOTIFICATIONSANDEVENTS_H_
#define ALLNOTIFICATIONSANDEVENTS_H_

#include "OpcUa_IPCS_DataChangeFilter.h"
#include "OpcUa_IPCS_DataChangeTrigger.h"
#include "OpcUa_IPCS_DataChangeNotification.h"
#include "OpcUa_IPCS_EventFieldList.h"
#include "OpcUa_IPCS_EventNotificationList.h"
#include "OpcUa_IPCS_MonitoredItemCreateRequest.h"
#include "OpcUa_IPCS_MonitoredItemCreateResult.h"
#include "OpcUa_IPCS_MonitoredItemNotification.h"
#include "OpcUa_MonitoredItem.h"
#include "OpcUa_MonitoredItemsList.h"
#include "OpcUa_IPCS_MonitoringFilter.h"
#include "OpcUa_IPCS_MonitoringFilterResult.h"
#include "OpcUa_IPCS_MonitoringMode.h"
#include "OpcUa_IPCS_MonitoringNullFilterResult.h"
#include "OpcUa_IPCS_MonitoringParameters.h"
#include "OpcUa_IPCS_NotificationData.h"
#include "OpcUa_IPCS_NotificationMessage.h"
#include "OpcUa_IPCS_StatusChangeNotification.h"
#include "OpcUa_IPCS_SubscriptionAcknowledgement.h"
#include "OpcUa_NotificationDataList.h"
#include "OpcUa_NotificationMessagesList.h"
#include "OpcUa_IPCS_PublishingReq.h"
#include "OpcUa_IPCS_Subscription.h"
#include "OpcUa_PublishingReqQueue.h"
#include "OpcUa_ReferenceDescriptionList.h"
#include "OpcUa_SamplingTimer.h"
#include "OpcUa_SubscriptionsList.h"
#include "OpcUa_SubscriptionsTable.h"
#include "OpcUa_IPCS_TransferResult.h"
#include "OpcUa_VariableValueMonitoredItem.h"

#endif /* ALLNOTIFICATIONSANDEVENTS_H_ */
