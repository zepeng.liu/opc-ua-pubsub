
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_NOTIFICATIONDATA_H_
#define OPCUA_NOTIFICATIONDATA_H_

/*
 * OPC-UA Part 4, 7.19, p. 131
 * An extensible parameters (Part 4, 7.11, 122)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"

namespace opcua {

class MYDLL NotificationData
	: public Structure
{
public:

	virtual ExpandedNodeId * getNodeId() const = 0 ;

public:

	static NotificationData * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pMonitoringFilter) ;

	virtual void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pMonitoringFilter) const = 0 ;

public:

	NotificationData()
		: Structure()
	{}

	virtual ~NotificationData()
	{}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_NOTIFICATIONDATA_H_ */
