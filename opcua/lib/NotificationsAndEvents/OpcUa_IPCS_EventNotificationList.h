
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_EVENTNOTIFICATIONLIST_H_
#define OPCUA_EVENTNOTIFICATIONLIST_H_

/*
 * OPC-UA Part 4, 7.19.3, p. 132
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_NotificationData.h"
#include "OpcUa_IPCS_EventFieldList.h"

namespace opcua {

class MYDLL EventNotificationList
	: public NotificationData
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_EventNotificationList ; }

private:

	TableEventFieldList   * events ;

public:

	static EventNotificationList * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_EventNotificationList const& pEventNotificationList)
	{
		TableEventFieldList   * events  = TableEventFieldList::fromCtoCpp (pStatus, pEventNotificationList.NoOfEvents, pEventNotificationList.Events) ;

		if (*pStatus == STATUS_OK)
			return new EventNotificationList(events) ;

		if (events != NULL)
			events->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pEventNotificationList) const
	{
		expandedNodeId->fromCpptoC(pStatus,pEventNotificationList.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pEventNotificationList.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pEventNotificationList.Body.Object.ObjType = &OpcUa_EventNotificationList_EncodeableType ;
			pEventNotificationList.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_EventNotificationList)) ;
			OpcUa_EventNotificationList_Initialize(pEventNotificationList.Body.Object.Value) ;
			EventNotificationList::fromCpptoC(pStatus,*static_cast<_OpcUa_EventNotificationList *>(pEventNotificationList.Body.Object.Value)) ;
			pEventNotificationList.Length = sizeof(_OpcUa_EventNotificationList) ;
		}
	}

private:

	inline void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_EventNotificationList& pEventNotificationList) const
	{
		events ->fromCpptoC (pStatus, pEventNotificationList.NoOfEvents, pEventNotificationList.Events) ;
	}

public:

	EventNotificationList(
			TableEventFieldList   * _events
			)
		: NotificationData()
	{
		(events = _events)  ->take() ;
	}

	virtual ~EventNotificationList()
	{
		events ->release() ;
	}

public:

	inline TableEventFieldList   * getEvents()
	{
		return events ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_EVENTNOTIFICATIONLIST_H_ */
