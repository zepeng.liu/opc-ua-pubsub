
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATACHANGENOTIFICATION_H_
#define OPCUA_DATACHANGENOTIFICATION_H_

/*
 * OPC-UA Part 4, 7.19.2, p. 132
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_NotificationData.h"
#include "OpcUa_IPCS_MonitoredItemNotification.h"

namespace opcua {

class MYDLL DataChangeNotification
	: public NotificationData
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_DataChangeNotification ; }

private:

	TableMonitoredItemNotification   * monitoredItems ;
	TableDiagnosticInfo              * diagnosticInfos ;

public:

	static DataChangeNotification * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_DataChangeNotification const& pDataChangeNotification)
	{
		TableMonitoredItemNotification   * monitoredItems  = TableMonitoredItemNotification::fromCtoCpp (pStatus, pDataChangeNotification.NoOfMonitoredItems,  pDataChangeNotification.MonitoredItems) ;
		TableDiagnosticInfo              * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp            (pStatus, pDataChangeNotification.NoOfDiagnosticInfos, pDataChangeNotification.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new DataChangeNotification(
							monitoredItems,
							diagnosticInfos
							) ;

		if (monitoredItems != NULL)
			monitoredItems->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pDataChangeNotification) const
	{
		expandedNodeId->fromCpptoC(pStatus,pDataChangeNotification.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pDataChangeNotification.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pDataChangeNotification.Body.Object.ObjType = &OpcUa_DataChangeNotification_EncodeableType ;
			pDataChangeNotification.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_DataChangeNotification)) ;
			OpcUa_DataChangeNotification_Initialize(pDataChangeNotification.Body.Object.Value) ;
			DataChangeNotification::fromCpptoC(pStatus,*static_cast<_OpcUa_DataChangeNotification *>(pDataChangeNotification.Body.Object.Value)) ;
			pDataChangeNotification.Length = sizeof(_OpcUa_DataChangeNotification) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_DataChangeNotification& pDataChangeNotification) const
	{
		monitoredItems  ->fromCpptoC (pStatus, pDataChangeNotification.NoOfMonitoredItems,  pDataChangeNotification.MonitoredItems) ;
		diagnosticInfos ->fromCpptoC (pStatus, pDataChangeNotification.NoOfDiagnosticInfos, pDataChangeNotification.DiagnosticInfos) ;
	}

public:

	DataChangeNotification(
			TableMonitoredItemNotification   * _monitoredItems,
			TableDiagnosticInfo              * _diagnosticInfos
			)
		: NotificationData()
	{
		(monitoredItems  = _monitoredItems)  ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~DataChangeNotification()
	{
		monitoredItems  ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	inline TableMonitoredItemNotification   * getMonitoredItems()
	{
		return monitoredItems ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_DATACHANGENOTIFICATION_H_ */
