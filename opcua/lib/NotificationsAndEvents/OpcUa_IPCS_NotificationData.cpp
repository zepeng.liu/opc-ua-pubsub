
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"

#include "OpcUa_IPCS_NotificationData.h"

#include "OpcUa_IPCS_DataChangeNotification.h"
#include "OpcUa_IPCS_EventNotificationList.h"
#include "OpcUa_IPCS_StatusChangeNotification.h"

namespace opcua {

	MYDLL NotificationData * NotificationData::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pNotificationData)
	{
		ExpandedNodeId * expandedNodeId = ExpandedNodeId::fromCtoCpp(pStatus,pNotificationData.TypeId) ;

		if (*pStatus != STATUS_OK)
			return NULL ;

		NodeId * parameterTypeId = expandedNodeId->getNodeId() ;

		Byte * encoding = NULL ;

		if (parameterTypeId->getNamespaceIndex()->get() == 0 && parameterTypeId->getIdentifierType()->get() == IdType_NUMERIC_0) {

			encoding = Byte::fromCtoCpp(pStatus,pNotificationData.Encoding) ;

			if (*pStatus != STATUS_OK) {
				*pStatus = _Bad_DecodingError ;
				expandedNodeId->checkRefCount() ;
				return NULL ;
			}

			if (encoding->equals((uint8_t)0x03)) {
				switch (pNotificationData.Body.Object.ObjType->TypeId) {
				case OpcUaId_DataChangeNotification:
				{
					_OpcUa_DataChangeNotification * dataChangeNotification = static_cast<_OpcUa_DataChangeNotification *>(pNotificationData.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return DataChangeNotification::fromCtoCpp(pStatus,*dataChangeNotification) ;
				}
				case OpcUaId_EventNotificationList:
				{
					_OpcUa_EventNotificationList * eventNotificationList = static_cast<_OpcUa_EventNotificationList *>(pNotificationData.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return EventNotificationList::fromCtoCpp(pStatus,*eventNotificationList) ;
				}
				case OpcUaId_StatusChangeNotification:
				{
					_OpcUa_StatusChangeNotification * statusChangeNotification = static_cast<_OpcUa_StatusChangeNotification *>(pNotificationData.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return StatusChangeNotification::fromCtoCpp(pStatus,*statusChangeNotification) ;
				}
				default:
					break ;
				}
			} else {
				debug_i(COM_ERR,"NotificationData","fromCtoCpp: Encoding is not 0x01, it is 0x%02x",encoding->get()) ;
			}
		}

		*pStatus = _Bad_DecodingError ;

		if (expandedNodeId != NULL)
			expandedNodeId->checkRefCount() ;
		if (encoding != NULL)
			encoding->checkRefCount() ;

		return NULL ;
	}

} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */


