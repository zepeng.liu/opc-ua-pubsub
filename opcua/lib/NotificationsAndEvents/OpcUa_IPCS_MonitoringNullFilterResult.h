
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITORINGNULLFILTERRESULT_H_
#define OPCUA_MONITORINGNULLFILTERRESULT_H_

/*
 * OPC-UA Part 4, 7.16, the null filter result
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringFilterResult.h"

namespace opcua {

class MYDLL MonitoringNullFilterResult
	: public MonitoringFilterResult
{
public:

	static MonitoringNullFilterResult * nullFilterResult ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoringFilterResult ; }

public:

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pMonitoringNullFilterResult) const
	{
		SOPC_ExtensionObject_Initialize(&pMonitoringNullFilterResult) ;

		expandedNodeId->fromCpptoC(pStatus,pMonitoringNullFilterResult.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pMonitoringNullFilterResult.Encoding = SOPC_ExtObjBodyEncoding_None ;
			pMonitoringNullFilterResult.Body.Object.ObjType = 0x00 ;
			pMonitoringNullFilterResult.Body.Object.Value = NULL ;
			pMonitoringNullFilterResult.Length = 0 ;
		}
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getExpandedNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITORINGNULLFILTERRESULT_H_ */
