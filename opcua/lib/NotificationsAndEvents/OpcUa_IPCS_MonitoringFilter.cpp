
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"

#include "OpcUa_IPCS_MonitoringFilter.h"
#include "OpcUa_IPCS_DataChangeFilter.h"

namespace opcua {

MYDLL MonitoringFilter * MonitoringFilter::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pMonitoringFilter)
{
	ExpandedNodeId * expandedNodeId   = ExpandedNodeId::fromCtoCpp(pStatus,pMonitoringFilter.TypeId) ;

	if (*pStatus != STATUS_OK)
		return NULL ;

	NodeId * parameterTypeId = expandedNodeId->getNodeId() ;

	Byte * encoding = NULL ;

   	if (parameterTypeId->getNamespaceIndex()->get() == 0 && parameterTypeId->getIdentifierType()->get() == IdType_NUMERIC_0) {

		encoding = Byte::fromCtoCpp(pStatus,pMonitoringFilter.Encoding) ;

		if (*pStatus != STATUS_OK) {
			*pStatus = _Bad_DecodingError ;
			expandedNodeId->checkRefCount() ;
			return NULL ;
		}

		if (encoding->equals((uint8_t)0x03)) {
			switch (pMonitoringFilter.Body.Object.ObjType->TypeId) {
			case OpcUaId_DataChangeFilter:
			{
				_OpcUa_DataChangeFilter * dataChangeFilter = static_cast<_OpcUa_DataChangeFilter *>(pMonitoringFilter.Body.Object.Value) ;
				encoding->checkRefCount() ;
				expandedNodeId->checkRefCount() ;
				return DataChangeFilter::fromCtoCpp(pStatus,*dataChangeFilter) ;
			}
			case OpcUaId_EventFilter:
				debug(COM_ERR,"MonitoringFilter","fromCtoCpp: not implemented: OpcUaId_EventFilter") ;
				break ;
			case OpcUaId_AggregateFilter:
				debug(COM_ERR,"MonitoringFilter","fromCtoCpp: not implemented: OpcUaId_AggregateFilter") ;
				break ;
			default:
				break ;
			}
		} else {
			debug_i(COM_ERR,"MonitoringFilter","fromCtoCpp: Encoding is not 0x01, it is 0x%02x",encoding->get()) ;
		}
   	}

	*pStatus = _Bad_DecodingError ;

	if (expandedNodeId != NULL)
		expandedNodeId->checkRefCount() ;
	if (encoding != NULL)
		encoding->checkRefCount() ;

	return NULL ;
}

}
#endif /* WITH_SUBSCRIPTION */


