
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_PUBLISHINGREQ_H_
#define OPCUA_PUBLISHINGREQ_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"

namespace opcua {

class Channel;

class MYDLL PublishingReq
{
private:

	uint32_t                     scConnectionId ;
    uint32_t                     requestId ;
    RequestHeader              * requestHeader ;
	TableStatusCode            * results ;

public:

	PublishingReq(
			uint32_t					 _scConnectionId,
			uint32_t                     _requestId,
		    RequestHeader              * _requestHeader,
			TableStatusCode            * _results
			)
	{
		scConnectionId  = _scConnectionId ;
		requestId       = _requestId ;
		(requestHeader  = _requestHeader)->take() ;
		(results        = _results)->take() ;
	}

	virtual ~PublishingReq()
	{
		requestHeader ->release() ;
		results       ->release() ;
	}

public:

	inline uint32_t getScConnectionId()
	{
		return scConnectionId ;
	}

	inline uint32_t getRequestId()
	{
		return requestId ;
	}

	inline RequestHeader * getRequestHeader()
	{
		return requestHeader ;
	}

	inline TableStatusCode * getResults()
	{
		return results ;
	}

};

}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_PUBLISHINGREQ_H_ */
