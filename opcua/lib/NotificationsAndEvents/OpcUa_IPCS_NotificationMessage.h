
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_NOTIFICATIONMESSAGE_H_
#define OPCUA_NOTIFICATIONMESSAGE_H_

/*
 * OPC-UA Part 4, 7.20, p. 133, Table 151
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_NotificationData.h"

namespace opcua {

class MYDLL NotificationMessage
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_NotificationMessage ; }

private:

	Counter                 * sequenceNumber ;
	UtcTime                 * publishTime ;
	TableNotificationData   * notificationData ;

public:

	static NotificationMessage * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_NotificationMessage const& pNotificationMessage)
	{
		Counter                 * sequenceNumber   = Counter ::fromCtoCpp            (pStatus, pNotificationMessage.SequenceNumber) ;
		UtcTime                 * publishTime      = UtcTime            ::fromCtoCpp (pStatus, pNotificationMessage.PublishTime) ;
		TableNotificationData   * notificationData = TableNotificationData::fromCtoCpp (pStatus, pNotificationMessage.NoOfNotificationData, pNotificationMessage.NotificationData) ;

		if (*pStatus == STATUS_OK)
			return
					new NotificationMessage(
							sequenceNumber,
							publishTime,
							notificationData
							) ;

		if (sequenceNumber != NULL)
			sequenceNumber->checkRefCount() ;
		if (publishTime != NULL)
			publishTime->checkRefCount() ;
		if (notificationData != NULL)
			notificationData->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_NotificationMessage& pNotificationMessage) const
	{
		sequenceNumber   ->fromCpptoC (pStatus, pNotificationMessage.SequenceNumber) ;
		publishTime      ->fromCpptoC (pStatus, pNotificationMessage.PublishTime) ;
		notificationData ->fromCpptoC (pStatus, pNotificationMessage.NoOfNotificationData, pNotificationMessage.NotificationData) ;
	}

public:

	NotificationMessage(
			Counter                 * _sequenceNumber,
			UtcTime                 * _publishTime,
			TableNotificationData   * _notificationData
			)
		: Structure()
	{
		(sequenceNumber   = _sequenceNumber)   ->take() ;
		(publishTime      = _publishTime)      ->take() ;
		(notificationData = _notificationData) ->take() ;
	}

	virtual ~NotificationMessage()
	{
		sequenceNumber   ->release() ;
		publishTime      ->release() ;
		notificationData ->release() ;
	}

public:

	inline Counter * getSequenceNumber()
	{
		return sequenceNumber ;
	}

	inline UtcTime * getPublishTime()
	{
		return publishTime ;
	}

	inline TableNotificationData   * getNotificationData()
	{
		return notificationData ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_NOTIFICATIONMESSAGE_H_ */
