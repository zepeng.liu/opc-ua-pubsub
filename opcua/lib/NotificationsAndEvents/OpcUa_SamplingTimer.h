/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SAMPLINGTIMER_H_
#define OPCUA_SAMPLINGTIMER_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Threads/All.h"
#include "../Utils/OpcUa_Clock.h"
#include "OpcUa_MonitoredItemsList.h"
#include "OpcUa_SubscriptionsTable.h"

#include <stdio.h>

namespace opcua {

class MYDLL SamplingTimer
	: public Thread
{
private:

	uint64_t milliseconds ;
	bool     ended ;

public:

	SamplingTimer()
		: Thread()
	{
		debug(SUB_DBG,"SamplingTimer","SamplingTimer creation begins") ;
		_NotificationsAvailable = false ;
		monitoredItemsList = NULL ;;
		milliseconds = (uint64_t)1000 ;
		ended = false ;
		Thread::start() ;
		debug(SUB_DBG,"SamplingTimer","SamplingTimer creation ends") ;
	}

	virtual ~SamplingTimer()
	{
		debug(SUB_DBG,"SamplingTimer","SamplingTimer destruction begins") ;
		stop() ;
		debug(SUB_DBG,"SamplingTimer","SamplingTimer destruction ends") ;
	}

public:

	void stop()
	{
		debug(SUB_DBG,"SamplingTimer","SamplingTimer stop() begins") ;
		SubscriptionsTable::subscriptionsLock() ;
		ended = true ;
		SubscriptionsTable::subscriptionsUnlock() ;
		Thread::join() ;
	}

public:

	void run()
	{
		debug(SUB_DBG,"SamplingTimer","SamplingTimer thread starts") ;

		while (true) {

			debug_i(SUB_DBG,"SamplingTimer","Sleeping %d ms",milliseconds) ;

			Clock::sleep(static_cast<int>(milliseconds)) ;

			uint64_t now = Clock::get_millisecondssince1601() ;

			SubscriptionsTable::subscriptionsLock() ;

			if (ended) {
				SubscriptionsTable::subscriptionsUnlock() ;
				return ;
			}

			debug(SUB_DBG,"SamplingTimer","SamplingTimer lock acquired") ;

			MonitoredItemsList * _monitoredItemsList = monitoredItemsList ;

			while (_monitoredItemsList != NULL) {
				debug_p(SUB_DBG,"SamplingTimer","SamplingTimer monitoredItem %p",_monitoredItemsList->getCar()) ;
				_monitoredItemsList->getCar()->go(now,this) ;
				_monitoredItemsList = _monitoredItemsList->getCdr() ;
			}

			SubscriptionsTable::subscriptionsUnlock() ;

			debug(SUB_DBG,"SamplingTimer","loop again") ;
		}

		debug(SUB_DBG,"SamplingTimer","SamplingTimer thread ends") ;
	}

private:

	MonitoredItemsList * monitoredItemsList ;

private:

	bool _NotificationsAvailable ;

public:

	void setNotificationsAvailable(bool __NotificationsAvailable)
	{
//		if (! _NotificationsAvailable && __NotificationsAvailable) {
//				// TBD Part 4, p. 78, creates a "New Notification" queued event
//		}
		_NotificationsAvailable = __NotificationsAvailable ;
	}

public:

	inline bool NotificationsAvailable()
	{
		return _NotificationsAvailable ;
	}

public:

	void setNotifications()
	{
		debug(SUB_DBG,"SamplingTimer","setNotifications starts") ;

		MonitoredItemsList * _monitoredItemsList = monitoredItemsList ;

		while (_monitoredItemsList != NULL) {
			MonitoredItem * monitoredItem = _monitoredItemsList->getCar() ;
			if (monitoredItem->HasNotificationsAvailable()) {
				setNotificationsAvailable(true) ;
				debug(SUB_DBG,"SamplingTimer","setNotifications ends with TRUE") ;
				return ;
			}
			_monitoredItemsList = _monitoredItemsList->getCdr() ;
		}

		setNotificationsAvailable(false) ;
		debug(SUB_DBG,"SamplingTimer","setNotifications ends with FALSE") ;
	}

public:

	void setMonitoredItemsList(MonitoredItemsList * _monitoredItemsList)
	{
		debug(SUB_DBG,"SamplingTimer","setMonitoredItemsList starts") ;

		monitoredItemsList = _monitoredItemsList ;

		milliseconds = (uint64_t)2000 ;
		setNotificationsAvailable(false) ;

		while (_monitoredItemsList != NULL) {
			MonitoredItem * monitoredItem = _monitoredItemsList->getCar() ;
			if (monitoredItem->HasNotificationsAvailable())
				setNotificationsAvailable(true) ;
			uint64_t samplingInterval = static_cast<uint64_t>(monitoredItem->getSamplingInterval()) ;
			if (samplingInterval < milliseconds)
				milliseconds = samplingInterval ;
			_monitoredItemsList = _monitoredItemsList->getCdr() ;
		}

		debug_i(SUB_DBG,"SamplingTimer","setMonitoredItemsList ends result is : %d ms",milliseconds) ;
	}

};

}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SAMPLINGTIMER_H_ */
