
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_NOTIFICATIONMESSAGESLIST_H_
#define OPCUA_NOTIFICATIONMESSAGESLIST_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "OpcUa_IPCS_NotificationMessage.h"

// A field of Subscription

namespace opcua {

class MYDLL NotificationMessagesList
	: public LinkedList<NotificationMessage>
{
public:

	NotificationMessagesList(NotificationMessage * _car, NotificationMessagesList * _cdr)
		: LinkedList<NotificationMessage>(_car,_cdr)
    {
		getCar()->take() ;
    }

	virtual ~NotificationMessagesList()
	{
		getCar()->release() ;
	}

public:

	inline NotificationMessagesList * getCdr() { return (NotificationMessagesList *)LinkedList<NotificationMessage>::getCdr() ; }

public:

	TableCounter   * getAvailableSequenceNumbers()
	{
		int32_t                           length = NotificationMessagesList::getLength(this) ;
		TableCounter                    * result = new TableCounter  (length) ;
		LinkedList<NotificationMessage> * tmp    = this ;
		for (int32_t i = 0 ; i < length ; i++) {
			result->set(i,tmp->getCar()->getSequenceNumber()) ;
			tmp = tmp->getCdr() ;
		}
		return result ;
	}

public:

	NotificationMessage * searchFor(uint32_t sequenceNumber)
	{
		NotificationMessagesList * tmp = this ;

		while (tmp != NULL) {
			NotificationMessage * car = tmp->getCar() ;
			if (car->getSequenceNumber()->equals(sequenceNumber))
					return car ;
			tmp = tmp->getCdr() ;
		}

		return NULL ;
	}
};


} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_NOTIFICATIONMESSAGESLIST_H_ */

