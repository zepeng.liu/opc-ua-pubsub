
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_SUBSCRIPTIONSTABLE_H_
#define OPCUA_SUBSCRIPTIONSTABLE_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_SyncHashTableInt.h"
#include "../Threads/OpcUa_Mutex.h"

namespace opcua {

class Subscription ;

class MYDLL SubscriptionsTable
	: public SyncHashTableInt<Subscription>
{
public:

	static SubscriptionsTable * subscriptionsTable ;

private:

	static Mutex * iSubscription ;

public:

	static void alloc()
	{
		subscriptionsTable = new SubscriptionsTable() ;
		iSubscription = new Mutex() ;
	}

	static void free()
	{
		delete subscriptionsTable ;
		delete iSubscription ;
	}

public:

	static inline void subscriptionsLock()
	{
		iSubscription->lock() ;
	}

	static inline void subscriptionsUnlock()
	{
		iSubscription->unlock() ;
	}

private:

	SubscriptionsTable() ;
	~SubscriptionsTable() ;

public:

	int32_t newSubscriptionId()
	{
		int result ;
		while (exists(result = Alea::alea->random_uint32()) || (result < 1) || (result > INT32_MAX)) ; // Must be an int32_t
		return (int32_t)result ;
	}

public:

	void putSubscription(Subscription * subscription) ;

	void removeSubscription(Subscription * subscription) ;

	inline Subscription * getSubscription(IntegerId * subscriptionId)
	{
		return getSubscription(subscriptionId->get()) ;
	}

	inline Subscription * getSubscription(int32_t subscriptionId)
	{
		return SyncHashTableInt<Subscription>::get((uint32_t)subscriptionId) ;
	}

};


} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_SUBSCRIPTIONTABLE_H_ */
