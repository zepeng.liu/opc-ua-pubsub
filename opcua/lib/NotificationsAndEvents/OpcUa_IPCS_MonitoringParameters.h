/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITORINGPARAMETERS_H_
#define OPCUA_MONITORINGPARAMETERS_H_

/*
 * OPC-UA Part 4, 7.15, p. 123, Table 128
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringFilter.h"

namespace opcua {

class MYDLL MonitoringParameters: public Structure {
public:

	virtual uint32_t getTypeId() const {
		return OpcUaId_MonitoringParameters;
	}

private:

	IntegerId        * clientHandle ;
	Duration         * samplingInterval ;
	MonitoringFilter * filter ;
	Counter          * queueSize ;
	Boolean          * discardOldest ;

public:

	static MonitoringParameters * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoringParameters const& pMonitoringParameters)
	{
		IntegerId        * clientHandle     = IntegerId        ::fromCtoCpp (pStatus, pMonitoringParameters.ClientHandle) ;
		Duration         * samplingInterval = Duration         ::fromCtoCpp (pStatus, pMonitoringParameters.SamplingInterval) ;
		MonitoringFilter * filter           = MonitoringFilter ::fromCtoCpp (pStatus, pMonitoringParameters.Filter) ;
		Counter          * queueSize        = Counter          ::fromCtoCpp (pStatus, pMonitoringParameters.QueueSize) ;
		Boolean          * discardOldest    = Boolean          ::fromCtoCpp (pStatus, pMonitoringParameters.DiscardOldest) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoringParameters(
							clientHandle,
							samplingInterval,
							filter,
							queueSize,
							discardOldest
							) ;

		if (clientHandle != NULL)
			clientHandle->checkRefCount() ;
		if (samplingInterval != NULL)
			samplingInterval->checkRefCount() ;
		if (filter != NULL)
			filter->checkRefCount() ;
		if (queueSize != NULL)
			queueSize->checkRefCount() ;
		if (discardOldest != NULL)
			discardOldest->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoringParameters& pMonitoringParameters) const
	{
		clientHandle     ->fromCpptoC(pStatus, pMonitoringParameters.ClientHandle) ;
		samplingInterval ->fromCpptoC(pStatus, pMonitoringParameters.SamplingInterval) ;
		filter           ->fromCpptoC(pStatus, pMonitoringParameters.Filter) ;
		queueSize        ->fromCpptoC(pStatus, pMonitoringParameters.QueueSize) ;
		discardOldest    ->fromCpptoC(pStatus, pMonitoringParameters.DiscardOldest) ;
	}

public:

	MonitoringParameters(
			IntegerId        * _clientHandle,
			Duration         * _samplingInterval,
			MonitoringFilter * _filter,
			Counter          * _queueSize,
			Boolean          * _discardOldest
			) : Structure()
	{
		(clientHandle     = _clientHandle)     ->take() ;
		(samplingInterval = _samplingInterval) ->take() ;
		(filter           = _filter)           ->take() ;
		(queueSize        = _queueSize)        ->take() ;
		(discardOldest    = _discardOldest)    ->take() ;
	}

	virtual ~MonitoringParameters()
	{
		clientHandle     ->release() ;
		samplingInterval ->release() ;
		filter           ->release() ;
		queueSize        ->release() ;
		discardOldest    ->release() ;
	}

public:

	inline IntegerId * getClientHandle()
	{
		return clientHandle;
	}

	inline Duration * getSamplingInterval()
	{
		return samplingInterval;
	}

	inline MonitoringFilter * getFilter()
	{
		return filter;
	}

	inline Counter * getQueueSize()
	{
		return queueSize;
	}

	inline Boolean * getDiscardOldest()
	{
		return discardOldest;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITORINGPARAMETERS_H_ */
