
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_EVENTFIELDLIST_H_
#define OPCUA_EVENTFIELDLIST_H_

/*
 * OPC-UA Part 4, 7.19.3, p. 132, inside Table 149
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL EventFieldList
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_EventFieldList ; }

private:

	IntegerId      * clientHandle ;
	TableVariant   * eventFields ;

public:

	static EventFieldList * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_EventFieldList const& pEventFieldList)
	{
		IntegerId      * clientHandle = IntegerId ::fromCtoCpp (pStatus, pEventFieldList.ClientHandle) ;
		TableVariant   * eventFields  = TableVariant::fromCtoCpp (pStatus, pEventFieldList.NoOfEventFields, pEventFieldList.EventFields) ;

		if (*pStatus == STATUS_OK)
			return
					new EventFieldList(
							clientHandle,
							eventFields
							) ;

		if (clientHandle != NULL)
			clientHandle->checkRefCount() ;
		if (eventFields != NULL)
			eventFields->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_EventFieldList& pEventFieldList) const
	{
		clientHandle ->fromCpptoC (pStatus, pEventFieldList.ClientHandle) ;
		eventFields  ->fromCpptoC (pStatus, pEventFieldList.NoOfEventFields, pEventFieldList.EventFields) ;
	}

public:

	EventFieldList(
			IntegerId      * _clientHandle,
			TableVariant   * _eventFields
			)
		: Structure()
	{
		(clientHandle = _clientHandle) ->take() ;
		(eventFields  = _eventFields)  ->take() ;
	}

	virtual ~EventFieldList()
	{
		clientHandle ->release() ;
		eventFields  ->release() ;
	}

public:

	inline IntegerId * getClientHandle()
	{
		return clientHandle ;
	}

	inline TableVariant   * getEventFields()
	{
		return eventFields ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_EVENTFIELDLIST_H_ */
