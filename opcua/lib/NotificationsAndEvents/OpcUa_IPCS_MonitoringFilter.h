
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_MONITORINGFILTER_H_
#define OPCUA_MONITORINGFILTER_H_

/*
 * OPC-UA Part 4, 7.16, p. 123
 * An extensible parameters (Part 4, 7.11, 122)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../AddressSpace/All.h"
#include "OpcUa_IPCS_MonitoringFilterResult.h"

namespace opcua {

class MYDLL MonitoringFilter
	: public Structure
{
public:

	virtual ExpandedNodeId * getNodeId() const = 0 ;

public:

	static MonitoringFilter * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pMonitoringFilter) ;

	virtual void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pMonitoringFilter) const = 0 ;

public:

	virtual MonitoringFilterResult * getMonitoringFilterResult() = 0 ;

public:

	virtual bool isOk(DataValue * dataValue, Base * object) = 0 ;

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITORINGFILTER_H_ */
