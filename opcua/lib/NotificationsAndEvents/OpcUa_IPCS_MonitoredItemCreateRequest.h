/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITOREDITEMCREATEREQUEST_H_
#define OPCUA_MONITOREDITEMCREATEREQUEST_H_

/*
 * OPC-UA Part 4, 5.12.2.2, p. 67, Table 64 (inside the table)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringMode.h"
#include "OpcUa_IPCS_MonitoringParameters.h"

namespace opcua {

class MYDLL MonitoredItemCreateRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemCreateRequest ; }

private:

	ReadValueId          * itemToMonitor ;
	MonitoringMode       * monitoringMode ;
	MonitoringParameters * requestedParameters ;

public:

	static MonitoredItemCreateRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemCreateRequest const& pMonitoredItemCreateRequest)
	{
		ReadValueId          * itemToMonitor       = ReadValueId          ::fromCtoCpp (pStatus, pMonitoredItemCreateRequest.ItemToMonitor) ;
		MonitoringMode       * monitoringMode      = MonitoringMode       ::fromCtoCpp (pStatus, pMonitoredItemCreateRequest.MonitoringMode) ;
		MonitoringParameters * requestedParameters = MonitoringParameters ::fromCtoCpp (pStatus, pMonitoredItemCreateRequest.RequestedParameters) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoredItemCreateRequest(
							itemToMonitor,
							monitoringMode,
							requestedParameters
							) ;

		if (itemToMonitor != NULL)
			itemToMonitor->checkRefCount() ;
		if (monitoringMode != NULL)
			monitoringMode->checkRefCount() ;
		if (requestedParameters != NULL)
			requestedParameters->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemCreateRequest& pMonitoredItemCreateRequest) const
	{
		itemToMonitor       ->fromCpptoC(pStatus, pMonitoredItemCreateRequest.ItemToMonitor) ;
		monitoringMode      ->fromCpptoC(pStatus, pMonitoredItemCreateRequest.MonitoringMode) ;
		requestedParameters ->fromCpptoC(pStatus, pMonitoredItemCreateRequest.RequestedParameters) ;
	}

public:

	MonitoredItemCreateRequest(
			ReadValueId          * _itemToMonitor,
			MonitoringMode       * _monitoringMode,
			MonitoringParameters * _requestedParameters)
	{
		(itemToMonitor       = _itemToMonitor)       ->take() ;
		(monitoringMode      = _monitoringMode)      ->take() ;
		(requestedParameters = _requestedParameters) ->take() ;
	}

	virtual ~MonitoredItemCreateRequest()
	{
		itemToMonitor       ->release() ;
		monitoringMode      ->release() ;
		requestedParameters ->release() ;
	}

public:

	inline ReadValueId * getItemToMonitor()
	{
		return itemToMonitor ;
	}

	inline MonitoringMode * getMonitoringMode()
	{
		return monitoringMode ;
	}

	inline MonitoringParameters * getRequestedParameters()
	{
		return requestedParameters ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITOREDITEMCREATEREQUEST_H_ */
