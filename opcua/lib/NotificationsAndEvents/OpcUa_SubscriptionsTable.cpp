
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "OpcUa_IPCS_Subscription.h"
#include "OpcUa_SubscriptionsTable.h"

namespace opcua {

SubscriptionsTable * SubscriptionsTable::subscriptionsTable = NULL ;
Mutex              * SubscriptionsTable::iSubscription = NULL ;

SubscriptionsTable::SubscriptionsTable()
	: SyncHashTableInt<Subscription>(SIZE_OF_SUBSCRIPTIONS_TABLE)
{}

SubscriptionsTable::~SubscriptionsTable()
{
//	Subscription * subscription ;
//	while ((subscription = removeOne()) != NULL)
//		delete subscription ;
}

void SubscriptionsTable::putSubscription(Subscription * subscription)
{
	put((uint32_t)subscription->getSubscriptionId(),subscription) ;
}

void SubscriptionsTable::removeSubscription(Subscription * subscription)
{
	debug(SUB_DBG,"SubscriptionsTable","removeSubscription begins") ;

	if (! remove((uint32_t)subscription->getSubscriptionId(),false))
		debug(COM_ERR,"SubscriptionsTable","removeSubscription failed") ;

	debug(SUB_DBG,"SubscriptionsTable","removeSubscription ends") ;
}

} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
