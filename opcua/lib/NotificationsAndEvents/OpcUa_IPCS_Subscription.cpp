/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPC-UA Part 5.13, p. 73
 */

#include "../Server/OpcUa_AddressSpace.h"
#include "OpcUa_IPCS_Subscription.h"

#if (WITH_SUBSCRIPTION == 1)

namespace opcua {

MYDLL MonitoredItemCreateResult * Subscription::createMonitoredItem(
		AddressSpace                * addressSpace,
		TimestampsToReturn          * timestampsToReturn,
		MonitoredItemCreateRequest  * createRequest)
{
	debug(SUB_DBG,"Subscription","createMonitoredItem begins") ;

	ReadValueId          * itemToMonitor       = createRequest->getItemToMonitor() ;
		NodeId           * nodeId              = itemToMonitor->getNodeId() ;
		IntegerId	     * attributeId         = itemToMonitor->getAttributeId() ;
		NumericRange     * numericRange        = itemToMonitor->getIndexRange() ;
		QualifiedName    * dataEncoding        = itemToMonitor->getDataEncoding() ;
	MonitoringMode       * monitoringMode      = createRequest->getMonitoringMode() ;
	MonitoringParameters * requestedParameters = createRequest->getRequestedParameters() ;

	debug(SUB_DBG,"Subscription","createMonitoredItem testing itemMonitor->nodeId") ;

	Base * object = addressSpace->get(nodeId) ;

	if (object == NULL) {
		debug(COM_ERR,"Subscription","createMonitoredItem : returning nodeId unknown") ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NodeIdUnknown) ;
	}


	if (dataEncoding->isNull() || dataEncoding->equals(QualifiedName::defaultBinary)) {
		debug(SUB_DBG,"Subscription","createMonitoredItem dataEncoding=\"DefaultBinary\"") ;
	} else if (dataEncoding->equals(QualifiedName::defaultXML)) {
		debug(COM_ERR,"Subscription","createMonitoredItem : returning unsupported dataEncoding=\"DefaultXML\"") ;
		// TBD Supporting DefaultXML
		return new MonitoredItemCreateResult(StatusCode::Bad_DataEncodingUnsupported) ;
	} else {
		debug(COM_ERR,"Subscription","createMonitoredItem : returning unknown dataEncoding") ;
		return new MonitoredItemCreateResult(StatusCode::Bad_DataEncodingInvalid) ;
	}

	_NodeClass _nodeClass = (_NodeClass)(object->getNodeClass()->get()) ;

	switch (_nodeClass) {
	case NodeClass_OBJECT_1:
		// TBD Monitor events, with filter
		debug(COM_ERR,"Subscription","createMonitoredItem with OBJECT_1 (reaching end of function, must not occur)") ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NotImplemented) ;
	case NodeClass_VARIABLE_2: {
			debug(SUB_DBG,"Subscription","createMonitoredItem processing VARIABLE_2") ;
			int32_t _attributeId = attributeId->get() ;
			if (_attributeId == AttributeId_Value) {
				debug(SUB_DBG,"Subscription","createMonitoredItem : Change in value, with filter, change in status (returning VariableValueMonitoredItem)") ;
				VariableValueMonitoredItem * variableValueMonitoredItem
					= new VariableValueMonitoredItem(
							object,
							numericRange,
							timestampsToReturn,
							monitoringMode,
							requestedParameters) ;
				registerMonitoredItem(variableValueMonitoredItem) ;
				variableValueMonitoredItem->go(Clock::get_millisecondssince1601(),samplingTimer) ;
				return variableValueMonitoredItem->getMonitoredItemCreateResult() ;
			} else {
				MonitoringFilter * filter = requestedParameters->getFilter() ;
				if (filter->getTypeId() != (uint32_t)0) {
					debug_i(COM_ERR,"Subscription","createMonitoredItem : VARIABLE_2 not Value, filter not NULL (filter = %d)",filter->getTypeId()) ;
					return new MonitoredItemCreateResult(StatusCode::Bad_MonitoredItemFilterUnsupported) ;
				}
				// TBD Change in value, no filter
			}
		}
		debug(COM_ERR,"Subscription","createMonitoredItem with VARIABLE_2 (reaching end of function, must not occur)") ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NotImplemented) ;
	case NodeClass_METHOD_4:
	case NodeClass_OBJECT_TYPE_8:
	case NodeClass_VARIABLE_TYPE_16:
	case NodeClass_REFERENCE_TYPE_32:
	case NodeClass_DATA_TYPE_64:
		debug_i(COM_ERR,"Subscription","createMonitoredItem : returning Bad_NotImplemented (NodeClass = %d)",_nodeClass) ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NotImplemented) ;
	case NodeClass_VIEW_128:
		// TBD Monitor events, with filter
		debug(COM_ERR,"Subscription","createMonitoredItem with VIEW_128 (reaching end of function, must not occur)") ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NotImplemented) ;
	default:
		debug_i(COM_ERR,"Subscription","createMonitoredItem returning Bad_NodeClassInvalid (NodeClass = %d)",_nodeClass) ;
		return new MonitoredItemCreateResult(StatusCode::Bad_NodeClassInvalid) ;
	}

	debug(COM_ERR,"Subscription","createMonitoredItem returning Bad_NotImplemented (reaching end of function, must not occur)") ;
	return new MonitoredItemCreateResult(StatusCode::Bad_NotImplemented) ;
}

}

#endif // WITH_SUBSCRIPTION

