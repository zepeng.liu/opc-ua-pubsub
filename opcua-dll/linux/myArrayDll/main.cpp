/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib/OpcUa.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/AddressSpace/All.h"
#include "lib/CommonParametersTypes/OpcUa_IPCS_NumericRange.h"
#include "lib/Utils/OpcUa_Clock.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

#include <stdlib.h>
#include <inttypes.h>

extern "C" {



static opcua::UInt32 * array[10][20] ;

int32_t start()
{
	for (int i = 0 ; i < 10 ; i++) {
		for (int j = 0 ; j < 20 ; j++) {
			(array[i][j] = new opcua::UInt32(0))->take() ;
		}
	}

	return (int32_t)0 ;
}

int32_t stop()
{
	for (int i = 0 ; i < 10 ; i++) {
		for (int j = 0 ; j < 20 ; j++) {
			array[i][j]->release() ;
		}
	}

	return (int32_t)0 ;
}

static int32_t getRange(opcua::NumericRange * indexRange, int32_t * rc, long * i_idx1, long * i_idx2, long * j_idx1, long * j_idx2)
{
	if (indexRange->isNullOrEmpty()) {

		* i_idx1 = 0 ;
		* i_idx2 = 9 ;

		* j_idx1 = 0 ;
		* j_idx2 = 19 ;

	} else {

		const char * indexRangeStr = indexRange->get() ;
		const char * indexRangeEnd = indexRangeStr + strlen(indexRangeStr) ;

		char * tmp;

		const char * comma = strchr(indexRangeStr,',') ;

		if (comma == NULL || comma <= indexRangeStr) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-1 ;
		}

		* i_idx1 = strtol(indexRangeStr,&tmp,0) ;

		if (tmp <= indexRangeStr) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-2 ;
		}

		const char * colon = strchr(indexRangeStr,':') ;

		if (colon == NULL || colon > comma) {
			* i_idx2 = * i_idx1 ;
		} else {
			colon++ ;
			* i_idx2 = strtol(colon,&tmp,0) ;
			if (tmp <= colon) {
				* rc = _Bad_IndexRangeInvalid ;
				return (int32_t)-2 ;
			}
		}

		if (* i_idx1 < 0 || * i_idx1 > * i_idx2 || * i_idx2 > 10) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-3 ;
		}

		comma++ ;

		if (comma >= indexRangeEnd) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-4 ;
		}

		* j_idx1 = strtol(comma,&tmp,0) ;

		if (tmp <= comma) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-5 ;
		}

		colon = strchr(comma+1,':') ;

		if (colon == NULL) {
			* j_idx2 = * j_idx1 ;
		} else {
			colon++ ;
			* j_idx2 = strtol(colon,&tmp,0) ;
			if (tmp <= colon) {
				* rc = _Bad_IndexRangeInvalid ;
				return (int32_t)-6 ;
			}
		}

		if (* j_idx1 < 0 || * j_idx1 > * j_idx2 || * j_idx2 > 20) {
			* rc = _Bad_IndexRangeInvalid ;
			return (int32_t)-7 ;
		}
	}

	return (int32_t)0 ;
}

int32_t get(opcua::ExternalVariable  * node,
			opcua::NumericRange      * indexRange,
			opcua::BaseDataType     ** value,
			int64_t                  * microseconds,
			int32_t                  * rc)
{
	node = NULL ;

	long i_idx1, i_idx2 ;
	long j_idx1, j_idx2 ;

	int32_t tmp_rc = getRange(indexRange,rc,&i_idx1,&i_idx2,&j_idx1, &j_idx2) ;

	if (tmp_rc != 0)
		return tmp_rc ;

	opcua::TableUInt32 * result = new opcua::TableUInt32((i_idx2-i_idx1+1)*(j_idx2-j_idx1+1)) ;

	long jj = (j_idx2-j_idx1+1) ;
	for (long i = i_idx1 ; i <= i_idx2 ; i++) {
		long ii = (i-i_idx1) ;
		for (long j = j_idx1 ; j <= j_idx2 ; j++) {
			result->set(ii*jj + (j - j_idx1),array[i][j]) ;
		}
	}


	* value        = result ;
	* microseconds = opcua::Clock::get_microsecondssince1601() ;
	* rc           = _Good_LocalOverride ;

	return (int32_t)0 ;
}

int32_t put(opcua::ExternalVariable * node,
			opcua::NumericRange     * indexRange,
			opcua::BaseDataType     * value,
			int32_t                 * rc)
{
	node = NULL ;

	if (value == NULL) {
		* rc = _Bad_NotWritable ;
		return (int32_t)1 ;
	}

	long i_idx1, i_idx2 ;
	long j_idx1, j_idx2 ;

	int32_t tmp_rc = getRange(indexRange,rc,&i_idx1,&i_idx2,&j_idx1, &j_idx2) ;

	if (tmp_rc != 0)
		return tmp_rc ;

	if (value->getLength() != (i_idx2-i_idx1+1)*(j_idx2-j_idx1+1)) { // It's an array !
		* rc = _Bad_NotImplemented ;
		return (int32_t)1 ;
	}

	long jj = (j_idx2-j_idx1+1) ;
	for (int i = i_idx1 ; i <= i_idx2 ; i++) {
		long ii = (i-i_idx1) ;
		for (int j = j_idx1 ; j <= j_idx2 ; j++) {
			opcua::BaseDataType * tmp = array[i][j] ;
			opcua::BaseDataType * rep = value->getArrayElt(ii*jj + (j - j_idx1)) ;
			if (rep->getTypeId() != OpcUaId_UInt32) {
				* rc = _Bad_TypeMismatch ;
				return (int32_t) 2 ;
			}
			(array[i][j] = dynamic_cast<opcua::UInt32 *>(rep))->take() ;
			tmp->release() ;
		}
	}

	* rc = _Good ;
	return (int32_t)0 ;
}

}
