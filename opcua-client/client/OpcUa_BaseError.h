
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASEERROR_H_
#define CLIENT_OPCUA_BASEERROR_H_

#include "lib/OpcUa.h"
#include "lib/Threads/OpcUa_Thread.h"
#include "lib/Services/All.h"


namespace opcua {

//typedef struct {
//	void    * baseClient ;
//	void    * pData ;
//	uint8_t   event ;
//} CallbackData_Invoke;


//typedef struct {
//	void * stub;
//} StubClient_CallbackData ;

class BaseError
	: public Thread
{
protected:

	int                         num ;
	SOPC_SecureChannel_Config * pScConfig ;
	uint32_t                    channel_config_idx ;

    String                    * endpointUrl ;

    const uint32_t              sleepTimeout ;
    const uint32_t              loopTimeout ;

    bool                        sessionActivated ;

    SyncQueue<BaseDataType>   * responsesQueue ;






	int noEvent ;
	int noResp ;
	int disconnect ;
	int connected ;

	// For CREATE_SESSION
	Nonce                     * clientNonce ;

	// After CREATE_SESSION
	NodeId                    * sessionId              ;
	uint32_t                    authenticationTokenNum ;
    Nonce                     * serverNonce            ;

    Sequence                  * requestSequence ;
    Channel 			      * clientChannel;

public:

    BaseError(int _num, SOPC_SecureChannel_Config * _pScConfig, uint32_t _channel_config_idx, String * _endpointUrl)
	: Thread(),
	  num(_num),
	  pScConfig(_pScConfig),
	  channel_config_idx(_channel_config_idx),
	  endpointUrl(_endpointUrl),
	  sleepTimeout(250),  // ms
	  loopTimeout(60000), // ms
	  sessionActivated(false),
	  responsesQueue(new SyncQueue<BaseDataType>(MAX_RESPONSES_PER_CLIENT)),

	  noEvent(1),
	  noResp(1) ,
	  disconnect(0),
	  connected(0),
	  clientNonce(NULL),
	  sessionId(NULL),
	  authenticationTokenNum(0),
	  serverNonce(NULL)
	{
    	requestSequence = new Sequence() ;
    	requestSequence->take() ;
    	clientChannel = new Channel(0) ;
	}

	virtual ~BaseError()
	{
		delete responsesQueue ;
		if (clientNonce  != NULL)
			clientNonce->release() ;
		requestSequence->release() ;
		if (clientChannel != NULL)
			delete clientChannel;
	}

public:

	inline void setSessionActivated(bool _sessionActivated)
	{
		sessionActivated = _sessionActivated ;
	}

	inline void pushResponse(BaseDataType *response)
	{
		responsesQueue->push(response) ;
	}

//protected: // ERROR CASE
//
//	SOPC_StatusCode Invoke_Error(
//			const char            * type,
//			void                  * response,
//			SOPC_EncodeableType   * responseType)
//	{
//		if (responseType->TypeId == OpcUaId_ServiceFault) { /* check for fault */
//			SOPC_StatusCode status = ((OpcUa_ServiceFault *)response)->ResponseHeader.ServiceResult ;
//			debug_si((uint32_t)IPCS_ERR,
//				     "BaseError",
//			     	 "%s: Service fault response: status = 0x%08x",
//					 type,
//				     status) ;
// 			OpcUa_ServiceFault_Clear(response);
//			return status ;
//		}
//
//		debug_si((uint32_t)IPCS_ERR,
//				 "BaseError",
//				 type,
//				 "%s: Bad response type = %d",
//				 responseType->TypeId) ;
//		return _Bad_InternalError ;
//	}

} ;    /* class BaseError */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASEERROR_H_ */
