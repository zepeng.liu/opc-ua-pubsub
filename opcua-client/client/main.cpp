
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib/OpcUa.h"

#define SAMPLE 3

#if (SAMPLE==1)
#   include "Client_01_Subscription/StartClients_01.h"
#endif
#if (SAMPLE==2)
#   include "Client_02_Call/StartClients_02.h"
#endif
#if (SAMPLE==3)
#   include "Client_03_Read_Write/StartClients_03.h"
#endif
#if (SAMPLE==4)
#   include "Client_04_Register_Unregister/StartClients_04.h"
#endif
#if (SAMPLE==5)
#   include "Client_05_Browse/StartClients_05.h"
#endif

extern "C" {

	void ComEvent_FctClient(SOPC_App_Com_Event event, uint32_t idOrStatus, void* param, uintptr_t appContext) ;

	extern int           endpointOpened ; ; // 0=false, Managed in s2opc_server/S2OPC_StubServices.cpp
	extern bool 	     isServer ;

} // extern "C"

namespace opcua {


	void _ComEvent_FctClient(SOPC_App_Com_Event event, uint32_t idOrStatus, void* param, uintptr_t appContext)
	{
		switch (event) {
		case SE_RCV_SESSION_RESPONSE:
			debug(IPCS_DBG,"ComEvent_FctClient","received: SE_RCV_SESSION_RESPONSE") ;
			break ;
		case SE_RCV_DISCOVERY_RESPONSE:
			debug(IPCS_DBG,"ComEvent_FctClient","received: SE_RCV_DISCOVERY_RESPONSE (unimplemented)") ;
			break ;
		case SE_ACTIVATED_SESSION:
			debug(IPCS_DBG,"ComEvent_FctClient","received: SE_ACTIVATED_SESSION") ;
			break ;
		case SE_SESSION_ACTIVATION_FAILURE:
			debug(IPCS_ERR,"ComEvent_FctClient","received: SE_SESSION_ACTIVATION_FAILURE") ;
			break ;
		case SE_CLOSED_SESSION:
			debug(IPCS_DBG,"ComEvent_FctClient","received: SE_CLOSED_SESSION") ;
			break ;
		case SE_SND_REQUEST_FAILED:
			debug(IPCS_ERR,"ComEvent_FctClient","received: SE_SND_REQUEST_FAILED") ;
			break ;
		default:
			debug_ii(IPCS_ERR,"ComEvent_FctClient","received unknown event = %d, with idOrStatus=%d",event,idOrStatus) ;
		}
		debug_i(IPCS_DBG,"ComEvent_FctClient","idOrStatus = %d",idOrStatus) ;
		debug_i(IPCS_DBG,"ComEvent_FctClient","appContext = %d",appContext) ;
	}


	int main(int argc, char *argv[], char *envp[])
	{
		isServer = false ;

		NOT_USED(envp) ;

	    SOPC_StatusCode status = STATUS_OK ;

		if (argc != 6) {
			fprintf(stderr,"Usage: %s <self-ip> <self-port> <certificates-dir> <server-ip> <server-port>\n",argv[0]) ;
			exit(-1) ;
		}

	#if (SIEM_LOG == 1)
		Debug::init_siem_file_name(argv[1],argv[2]) ;
	#endif

	#ifdef _WIN32
		debug(MAIN_LIFE_DBG,"Sample Client","Initializing Windows WSA") ;
		WSADATA wsaData ;
		CHECK_LIB(WSAStartup(MAKEWORD(2,2),&wsaData) == 0) ;
	#endif

		debug_s(MAIN_LIFE_DBG,"main","Self   IP: %s",argv[1]) ;
		debug_s(MAIN_LIFE_DBG,"main","Self Port: %s",argv[2]) ;
		debug_s(MAIN_LIFE_DBG,"main","Certs dir: %s",argv[3]) ;

		debug(MAIN_LIFE_DBG,"Sample Client","Server Settings") ;

		debug(MAIN_LIFE_DBG,"Sample Client","Loading Connexion certificates") ;
		Certificates::init(argv[3]) ;

	    // Get Toolkit Configuration
	    SOPC_Build_Info build_info = SOPC_ToolkitConfig_GetBuildInfo();
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitVersion: %s", build_info.toolkitVersion);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitSrcCommit: %s", build_info.toolkitSrcCommit);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitDockerId: %s", build_info.toolkitDockerId);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitBuildDate: %s", build_info.toolkitBuildDate);

		ChannelsTable::self = new ChannelsTable(100) ;

	    // Endpoint
	    char sEndpointUrl[1024] ;
	    sprintf(sEndpointUrl,"opc.tcp://%s:%s/endpoint",argv[4],argv[5]) ;
	    debug_s(MAIN_LIFE_DBG,"Sample Client","Server: \"%s\"",sEndpointUrl) ;

	    String * endpointUrl = new String(sEndpointUrl) ;
	    endpointUrl->take() ;



	    // Paths to client certificate/key and server certificate

	    // Client private certificate
	    char certificateLocation[1024] ;
	    sprintf(certificateLocation,"%s/certs/self.der",argv[3]) ;

	    // Client private key
	    char keyLocation[1024] ;
	    sprintf(keyLocation,"%s/keys/self.key",argv[3]) ;

	    // Server certificate name
	    char certificateSrvLocation[1024] ;
	    sprintf(certificateSrvLocation,"%s/certs/%s.der",argv[3],argv[4]) ;

	    // Server CA
	    char certificateAuthority[1024] ;
	    sprintf(certificateAuthority,"%s/certs/chainOfTrust.crt",argv[3]) ;


	    // The certificates and keys: loading

		debug_s(MAIN_LIFE_DBG,"Sample Client","Loading client certificate \"%s\"",certificateLocation) ;
	    SOPC_Certificate *crt_cli = NULL ;
	    status = SOPC_KeyManager_Certificate_CreateFromFile(certificateLocation, &crt_cli);

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Client","Failed to load client certificate: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

		debug_s(MAIN_LIFE_DBG,"Sample Client","Loading client private key \"%s\"",keyLocation) ;
	    SOPC_AsymmetricKey *priv_cli = NULL;
        status = SOPC_KeyManager_AsymmetricKey_CreateFromFile(keyLocation, &priv_cli, NULL, 0);

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Client","Failed to load client private key: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

		debug_s(MAIN_LIFE_DBG,"Sample Client","Loading server certificate \"%s\"",certificateSrvLocation) ;
	    SOPC_Certificate *crt_srv = NULL;
	    status = SOPC_KeyManager_Certificate_CreateFromFile(certificateSrvLocation, &crt_srv);

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Client","Failed to load server certificate: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

		debug_s(MAIN_LIFE_DBG,"Sample Client","Loading server certificate authority \"%s\"",certificateAuthority) ;
	    SOPC_Certificate *crt_ca = NULL;
	    status = SOPC_KeyManager_Certificate_CreateFromFile(certificateAuthority, &crt_ca) ;

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Client","Failed to load certificate authority: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }



	    // Init PKI provider with certificate authority
	    debug(MAIN_LIFE_DBG,"Sample Client","Init PKI provider with certificate authority") ;
	    SOPC_PKIProvider* pki = NULL;
	    status = SOPC_PKIProviderStack_Create(crt_ca, NULL, &pki) ;

	    if(STATUS_OK != status) {
	    	debug_i(COM_ERR,"Sample Client","Failed to init PKI provider with certificate authority: status=0x%08x",status) ;
	    	EXIT(-1) ;
	    }



	    // Init stack configuration
	    debug(MAIN_LIFE_DBG,"Sample Client","Init stack configuration") ;
	    status = SOPC_Toolkit_Initialize(ComEvent_FctClient);

	    if(STATUS_OK != status) {
	    	debug_i(COM_ERR,"Sample Client","Failed to init stack configuration: status=0x%08x",status) ;
	    	EXIT(-1) ;
	    }


	    // Namespace table

	    SOPC_NamespaceTable nsTable ;
	    SOPC_Namespace_Initialize(&nsTable) ;
	    SOPC_ToolkitConfig_SetNamespaceUris(&nsTable) ;




		debug(MAIN_LIFE_DBG,"Sample Client","Create parameters") ;
#if (SECURITY == UANONE)
	    const char * pRequestedSecurityPolicyUri = SOPC_SecurityPolicy_None_URI;
#else
	    const char * pRequestedSecurityPolicyUri = SOPC_SecurityPolicy_Basic256Sha256_URI;
#endif

	    // Policy security:
#if (SECURITY == UANONE)
	    OpcUa_MessageSecurityMode messageSecurityMode         = OpcUa_MessageSecurityMode_None;
#endif
#if (SECURITY == UASIGN_Basic256Sha256)
	    OpcUa_MessageSecurityMode messageSecurityMode         = OpcUa_MessageSecurityMode_Sign;
#endif
#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
	    OpcUa_MessageSecurityMode messageSecurityMode         = OpcUa_MessageSecurityMode_SignAndEncrypt;
#endif


#ifdef _WIN32
	    // A Secure channel connection configuration
	    SOPC_SecureChannel_Config scConfig ;
	    scConfig.isClientSc        = true ;
	    scConfig.url               = sEndpointUrl ;
	    scConfig.crt_cli           = crt_cli ;
	    scConfig.key_priv_cli      = priv_cli ;
	    scConfig.crt_srv           = crt_srv ;
	    scConfig.pki               = pki ;
	    scConfig.reqSecuPolicyUri  = pRequestedSecurityPolicyUri ;
	    scConfig.requestedLifetime = 20000 ;
	    scConfig.msgSecurityMode   = messageSecurityMode ;
#else
	    // A Secure channel connection configuration
	    SOPC_SecureChannel_Config scConfig = {.isClientSc        = true,
	                                          .url               = sEndpointUrl,
	                                          .crt_cli           = crt_cli,
	                                          .key_priv_cli      = priv_cli,
	                                          .crt_srv           = crt_srv,
	                                          .pki               = pki,
	                                          .reqSecuPolicyUri  = pRequestedSecurityPolicyUri,
	                                          .requestedLifetime = 20000,
	                                          .msgSecurityMode   = messageSecurityMode};
#endif



#if (SECURITY == UA_NONE)
		debug(MAIN_LIFE_DBG,"Sample Client","Running client in UA_NONE mode") ;
#endif
#if (SECURITY == UASIGN_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Client","Running client in UA_SIGN mode") ;
#endif
#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Client","Running client in UA_SIGNANDENCRYPT mode") ;
#endif

		int result
#if (SAMPLE==1)
			=  opcua::StartClients_01::startClients(
#endif
#if (SAMPLE==2)
			=  opcua::StartClients_02::startClients(
#endif
#if (SAMPLE==3)
			=  opcua::StartClients_03::startClients(
#endif
#if (SAMPLE==4)
			=  opcua::StartClients_04::startClients(
#endif
#if (SAMPLE==5)
			=  opcua::StartClients_05::startClients(
#endif
					endpointUrl,
					&scConfig
					) ;

	    debug(MAIN_LIFE_DBG,"Sample Client","That's all folks !") ;

	    SOPC_KeyManager_Certificate_Free(crt_cli) ;
	    SOPC_KeyManager_Certificate_Free(crt_srv) ;
	    SOPC_KeyManager_Certificate_Free(crt_ca) ;
	    SOPC_KeyManager_AsymmetricKey_Free(priv_cli) ;
	    SOPC_PKIProviderStack_Free(pki) ;

	    return result ;

	} /* opcua::main */

} /* namespace opcua */



extern "C" {

	void ComEvent_FctClient(SOPC_App_Com_Event event, uint32_t idOrStatus, void* param, uintptr_t appContext)
	{
		opcua::_ComEvent_FctClient(event,idOrStatus,param,appContext) ;
	}

} // extern "C"


int main(int argc, char *argv[], char *envp[])
{
	return opcua::main(argc,argv,envp) ;
}



