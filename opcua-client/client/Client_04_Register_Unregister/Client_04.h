
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_04_H_
#define CLIENT_04_H_

#include "lib/OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "lib/Stacks/All.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

#include "../OpcUa_BaseClient.h"

namespace opcua {

class Client_04
	: public BaseClient
{
public:

	void run()
	{
		int nbExt =  1 ; // Number of total loops from CreateSession to CloseSession
		int nbInt =  1 ; // Number of internal loops (after ActivateSession and before CloseSession)

		SOPC_StatusCode status = STATUS_OK ;

		// EXTERNAL LOOP
		while (--nbExt >= 0) {

			// CREATE SESSION
			debug_i(MAIN_LIFE_DBG,"Client_04","Create Session begin for client=%d",num) ;
			status = createSession() ;
			if(status != STATUS_OK) {
				debug_ii(COM_ERR,"Client_04","Cannot Create Session: client=%d & status=0x%08x",num,status) ;
				return ;
			}
			debug_i(MAIN_LIFE_DBG,"Client_04","Create Session succeeds for cleint=%d",num) ;

			// ACTIVATE SESSION
			debug_i(MAIN_LIFE_DBG,"Client_04","Activate Session begin for client=%d",num) ;
			status = activateSession() ;
			if(status != STATUS_OK) {
				debug_ii(COM_ERR,"Client_04","Cannot Activate Session: client=%d & status=0x%08x",num,status) ;
				return ;
			}
			debug_i(MAIN_LIFE_DBG,"Client_04","Activate Session succeeds for client=%d",num) ;

			// INTERNAL LOOP
			int _nbInt = nbInt ;
			while (--_nbInt >= 0) {

				// REGISTER WITH GOOD NODES
				debug_i(MAIN_LIFE_DBG,"Client_04","Register Good begin for client=%d",num) ;
				status = doRegisterNodesGood() ;
				if(status != STATUS_OK) {
					debug_ii(COM_ERR,"Client_04","Cannot Register Good: client=%d & status=0x%08x",num,status) ;
					break  ;
				}
				debug_i(MAIN_LIFE_DBG,"Client_04","Register Good succeeds for client=%d",num) ;

				// REGISTER WITH NOT GOOD NODES
				debug_i(MAIN_LIFE_DBG,"Client_04","Register Not Good begin for client=%d",num) ;
				status = doRegisterNodesNotGood() ;
				if(status != STATUS_OK) {
					debug_ii(COM_ERR,"Client_04","Cannot Register Not Good: client=%d & status=0x%08x (this is the good result)",num,status) ;
				} else {
					debug_i(MAIN_LIFE_DBG,"Client_04","Register Not Good succeeds for client=%d",num) ;
				}

				// UNREGISTER WITH GOOD NODES
				debug_i(MAIN_LIFE_DBG,"Client_04","Unregister Good begin for client=%d",num) ;
				status = doUnregisterNodesGood() ;
				if(status != STATUS_OK) {
					debug_ii(COM_ERR,"Client_04","Cannot Unregister Good: client=%d & status=0x%08x",num,status) ;
					break  ;
				}
				debug_i(MAIN_LIFE_DBG,"Client_04","Unregister Good succeeds for client=%d",num) ;

			} // nbInt

			// CLOSE SESSION
			debug_i(MAIN_LIFE_DBG,"Client_04","Close Session begin for client=%d",num) ;
			status = closeSession() ;
			if(status != STATUS_OK) {
				debug_ii(COM_ERR,"Client_04","Cannot Close Session: client=%d & status=0x%08x",num,status) ;
				return ;
			}
			debug_i(MAIN_LIFE_DBG,"Client_04","Close Session succeeds for client=%d",num) ;
		} // nbExt

		debug_i(MAIN_LIFE_DBG,"Client_04","Ending thread for client=%d",num) ;
	} // run()

public:

	Client_04(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx,String * endpointUrl)
		: BaseClient(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~Client_04()
	{}

public:

	SOPC_StatusCode createSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (Certificates::computeRandNonce(&clientNonce)) {
			debug(COM_ERR, "Client_04", "computeRandNonce(clientNonce) operation failed");
			if (clientNonce != NULL)
				clientNonce->checkRefCount() ;
			return 0x00000001 ;
		}

		clientNonce->take() ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSessionRequest * createSessionRequest =
		// OPC UA, Part 4, 5.5.2.2, table 11, p. 26		=
				new CreateSessionRequest(
						// OPC_UA, Part 4, 7.26, table 157, p. 138
						new RequestHeader(
								SessionAuthenticationToken::nullNodeId,
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
								),
						// OPC_UA, Part 4, 7.1, p. 106
						new ApplicationDescription(
								new String("opc-tcp://192.168.1.150:10000"),
								new String("opc-rosa"),
								new LocalizedText(new String("Client_04"),LocaleId::en),
								ApplicationType::client_1,
								String::empty,
								String::empty,
								new TableString  (0)),
						String::empty,
						endpointUrl,
						new String("Session Client_04"),
						clientNonce,
						Certificates::selfCertificate,
						Duration::maximum,
						UInt32::maximum);

		CreateSessionResponse * createSessionResponse = NULL ;

		status = Connexion_CreateSession(createSessionRequest,&createSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Create Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_04","Create Session: processing parameters from response");

		sessionId              = createSessionResponse->getSessionId();
		authenticationTokenNum = createSessionResponse->getAuthenticationToken()->get() ;
	    serverNonce            = createSessionResponse->getServerNonce() ;

		#if (SECURITY != UANONE)
		if (createSessionResponse->getServerCertificate() == NULL ||
			createSessionResponse->getServerSignature  () == NULL ||
			Certificates::selfCertificate                 == NULL ||
			clientNonce 								  == NULL) {

			debug (COM_ERR,"Client_04","Create Session: channel parameter has not been found") ;
			status = 0x00000002 ;
			goto error ;
		}
#endif

		session = new Session(authenticationTokenNum, sessionId);
		session->setLastCreateOrActivateSessionNonce(serverNonce) ;

#if (SECURITY != UANONE)
		session->setDistantNonce(serverNonce) ;
		session->setLocalNonce(clientNonce) ;
		session->setDistantCertificate(createSessionResponse->getServerCertificate()) ;
#endif

		clientChannel->pushNewSession(authenticationTokenNum, session);

	    createSessionRequest->checkRefCount() ;
	    createSessionResponse->checkRefCount() ;

		clientNonce->release() ;
		clientNonce = NULL ;

		return STATUS_OK ;

error:
		clientNonce->release() ;
		clientNonce = NULL ;

		createSessionRequest->checkRefCount() ;

		if (createSessionResponse != NULL)
			createSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

public:

	SOPC_StatusCode activateSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Nonce  * serverNonce = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_04", "activateSession(): clientChannel null");
			return 0x00000001 ;
		}

		Session * session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_04", "activateSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

#if (SECURITY == UANONE)
		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
				// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						new UInt32(5000) // TimeoutHint
				),
				SignatureData::fakeSignatureData,
				new TableSignedSoftwareCertificate  (0),
				tableLocaleId,
				new AnonymousIdentityToken(new String("Anonymous")),
				SignatureData::fakeSignatureData);
#endif

#if (SECURITY != UANONE)
		Nonce * distantNonce = session->getDistantNonce() ;
		if (distantNonce == NULL) {
			debug(COM_ERR, "Client_04", "activateSession(): distantNonce == NULL");
			return 0x00000003 ;
		}

		ApplicationInstanceCertificate * distantCertificate = session->getDistantCertificate() ;
		if (distantCertificate == NULL) {
			debug(COM_ERR, "Client_04", "activateSession(): distantCertificate == NULL");
			return 0x00000003 ;
		}

		SignatureData * clientSignatureData = NULL ;
		if ((status = Certificates::computeSignatureData(&clientSignatureData, session->getDistantCertificate(), session->getDistantNonce()))) {
			debug_i(COM_ERR, "Client_04", "activateSession(): Certificates::computeSignatureData(..) returns %d",status);
			return status ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							new UInt32(5000) // TimeoutHint
					),
					clientSignatureData, // clientSignatureData
					new TableSignedSoftwareCertificate  (0),
					tableLocaleId,
					new AnonymousIdentityToken(new String("Anonymous")),
					SignatureData::fakeSignatureData); // fakeSignature
#endif

		ActivateSessionResponse * activateSessionResponse = NULL ;

		status = Connexion_ActivateSession(activateSessionRequest,&activateSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Activate Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_04","Activate Session: processing parameters from response");

		serverNonce = activateSessionResponse->getServerNonce();

		session->setLastCreateOrActivateSessionNonce(serverNonce);

	    activateSessionRequest  ->checkRefCount() ;
	    activateSessionResponse ->checkRefCount() ;

		return STATUS_OK ;

error:

		activateSessionRequest->checkRefCount() ;

		if (activateSessionResponse != NULL)
			activateSessionResponse->checkRefCount() ;

		return status ;
	}

public:

	SOPC_StatusCode closeSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_04", "closeSession(): clientChannel null");
			return 0x00000001 ;
		}

		session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_04", "closeSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		// OPC-UA, Part3, 5.6.2, p. 26
		CloseSessionRequest * closeSessionRequest = new CloseSessionRequest(
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
				),
				Boolean::booleanFalse);

		CloseSessionResponse * closeSessionResponse = NULL ;

		status = Connexion_CloseSession(closeSessionRequest,&closeSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Close Session: status=0x%08x",status) ;
			goto error ;
		}

	    clientChannel->deleteSession(authenticationTokenNum) ;

error:

		closeSessionRequest->checkRefCount() ;

		if (closeSessionResponse != NULL)
			closeSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

		return status ;
	}

public:

	SOPC_StatusCode doRegisterNodesGood()
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_04","Testing Register Good") ;

		NodeId * nodeToRegister0 = new NodeId(UInt16::zero,new String("XMouse")) ;
		NodeId * nodeToRegister1 = new NodeId(UInt16::zero,new String("YMouse")) ;

		TableNodeId   * nodesToRegister = new TableNodeId  (2) ;
		nodesToRegister->set(0,nodeToRegister0) ;
		nodesToRegister->set(1,nodeToRegister1) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		RegisterNodesRequest * registerNodesRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new RegisterNodesRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero // TimeoutHint
						),
					nodesToRegister);

		RegisterNodesResponse * registerNodesResponse = NULL ;

		status = Connexion_RegisterNodes(registerNodesRequest,&registerNodesResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Register Good: status=0x%08x",status) ;
			goto error ;
		}

	    registerNodesRequest  ->checkRefCount() ;
	    registerNodesResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_04","Register Good OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_04","Register Good NOT OK") ;

		registerNodesRequest->checkRefCount() ;

		if (registerNodesResponse != NULL)
			registerNodesResponse->checkRefCount() ;

		return status ;
	}

public:

	SOPC_StatusCode doRegisterNodesNotGood()
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_04","Testing Register Not Good") ;

		NodeId * nodeToRegister0 = new NodeId(UInt16::zero,new String("XMouse")) ;
		NodeId * nodeToRegister1 = new NodeId(UInt16::zero,new String("ZZZ YMouse")) ;

		TableNodeId   * nodesToRegister = new TableNodeId(2) ;
		nodesToRegister->set(0,nodeToRegister0) ;
		nodesToRegister->set(1,nodeToRegister1) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		RegisterNodesRequest * registerNodesRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new RegisterNodesRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero // TimeoutHint
						),
					nodesToRegister);

		RegisterNodesResponse * registerNodesResponse = NULL ;

		status = Connexion_RegisterNodes(registerNodesRequest,&registerNodesResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Register Not Good: status=0x%08x (this is the good result)",status) ;
			goto error ;
		}

	    registerNodesRequest  ->checkRefCount() ;
	    registerNodesResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_04","Register Not Good OK (this is not a good result)") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_04","Register Not Good NOT OK (this is the good result)") ;

		registerNodesRequest->checkRefCount() ;

		if (registerNodesResponse != NULL)
			registerNodesResponse->checkRefCount() ;

		return status ;
	}

public:

	SOPC_StatusCode doUnregisterNodesGood()
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_04","Testing Unregister Good") ;

		NodeId * nodeToUnregister0 = new NodeId(UInt16::zero,new String("XMouse")) ;
		NodeId * nodeToUnregister1 = new NodeId(UInt16::zero,new String("YMouse")) ;

		TableNodeId   * nodesToUnregister = new TableNodeId  (2) ;
		nodesToUnregister->set(0,nodeToUnregister0) ;
		nodesToUnregister->set(1,nodeToUnregister1) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		UnregisterNodesRequest * unRegisterNodesRequest
		// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
		= new UnregisterNodesRequest(
		// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero // TimeoutHint
						),
				nodesToUnregister);

		UnregisterNodesResponse * unRegisterNodesResponse = NULL ;

		status = Connexion_UnregisterNodes(unRegisterNodesRequest,&unRegisterNodesResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_04","Cannot Unregister Good: status=0x%08x",status) ;
			goto error ;
		}

	    unRegisterNodesRequest  ->checkRefCount() ;
	    unRegisterNodesResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_04","Unregister Good OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_04","Unregister Good NOT OK") ;

		unRegisterNodesRequest->checkRefCount() ;

		if (unRegisterNodesResponse != NULL)
			unRegisterNodesResponse->checkRefCount() ;

		return status ;
	}


};     /* class */
}      /* namespace opcua */
#endif /* WITH_REGISTER_UNREGISTER_NODES == 1 */
#endif /* CLIENT04_H_ */
