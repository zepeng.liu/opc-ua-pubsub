
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_05_H_
#define CLIENT_05_H_

#include "lib/OpcUa.h"

#if (WITH_BROWSE == 1)

#include "lib/Stacks/All.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

#include "../OpcUa_BaseClient.h"

namespace opcua {

class Client_05
	: public BaseClient
{
public:

	void run()
	{
		int nbExt = 5 ; // Number of total loops from CreateSession to CloseSession
		int nbInt = 8 ; // Number of internal loops (after ActivateSession and before CloseSession)

		SOPC_StatusCode status = STATUS_OK ;

		// EXTERNAL LOOP
		while (--nbExt >= 0) {

			// CREATE SESSION
			debug(MAIN_LIFE_DBG,"Client_05","Create Session begin") ;
			status = createSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_05","Cannot Create Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_05","Create Session succeeds") ;

			// ACTIVATE SESSION
			debug(MAIN_LIFE_DBG,"Client_05","Activate Session begin") ;
			status = activateSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_05","Cannot Activate Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_05","Activate Session succeeds") ;

			// INTERNAL LOOP
			int _nbInt = nbInt ;
			while (--_nbInt >= 0) {

				// BROWSE
				debug(MAIN_LIFE_DBG,"Client_05","Browse begin") ;
				status = doBrowse() ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_05","Cannot Browse: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_05","Browse succeeds") ;

			} // nbInt

			// CLOSE SESSION
			debug(MAIN_LIFE_DBG,"Client_05","Close Session begin") ;
			status = closeSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_05","Cannot Close Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_05","Close Session succeeds") ;
		} // nbExt

		debug(MAIN_LIFE_DBG,"Client_05","Ending thread") ;
	} // run()

public:

	Client_05(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx,String * endpointUrl)
		: BaseClient(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~Client_05()
	{}

public:

	SOPC_StatusCode createSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (Certificates::computeRandNonce(&clientNonce)) {
			debug(COM_ERR, "Client_05", "computeRandNonce(clientNonce) operation failed");
			if (clientNonce != NULL)
				clientNonce->checkRefCount() ;
			return 0x00000001 ;
		}

		clientNonce->take() ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSessionRequest * createSessionRequest =
		// OPC UA, Part 4, 5.5.2.2, table 11, p. 26		=
				new CreateSessionRequest(
						// OPC_UA, Part 4, 7.26, table 157, p. 138
						new RequestHeader(
								SessionAuthenticationToken::nullNodeId,
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
								),
						// OPC_UA, Part 4, 7.1, p. 106
						new ApplicationDescription(
								new String("opc-tcp://192.168.1.150:10000"),
								new String("opc-rosa"),
								new LocalizedText(new String("Client_05"),LocaleId::en),
								ApplicationType::client_1,
								String::empty,
								String::empty,
								new TableString  (0)),
						String::empty,
						endpointUrl,
						new String("Session Client_05"),
						clientNonce,
						Certificates::selfCertificate,
						Duration::maximum,
						UInt32::maximum);

		CreateSessionResponse * createSessionResponse = NULL ;
		fprintf(stderr,"Begin\n") ;
		status = Connexion_CreateSession(createSessionRequest,&createSessionResponse) ;
		fprintf(stderr,"End\n") ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_05","Cannot Create Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_05","Create Session: processing parameters from response");

		sessionId              = createSessionResponse->getSessionId();
		authenticationTokenNum = createSessionResponse->getAuthenticationToken()->get() ;
	    serverNonce            = createSessionResponse->getServerNonce() ;

		#if (SECURITY != UANONE)
		if (createSessionResponse->getServerCertificate() == NULL ||
			createSessionResponse->getServerSignature  () == NULL ||
			Certificates::selfCertificate                 == NULL ||
			clientNonce 								  == NULL) {

			debug (COM_ERR,"Client_05","Create Session: channel parameter has not been found") ;
			status = 0x00000002 ;
			goto error ;
		}
#endif

		session = new Session(authenticationTokenNum, sessionId);
		session->setLastCreateOrActivateSessionNonce(serverNonce) ;

#if (SECURITY != UANONE)
		session->setDistantNonce(serverNonce) ;
		session->setLocalNonce(clientNonce) ;
		session->setDistantCertificate(createSessionResponse->getServerCertificate()) ;
#endif

		clientChannel->pushNewSession(authenticationTokenNum, session);

	    createSessionRequest->checkRefCount() ;
	    createSessionResponse->checkRefCount() ;

		clientNonce->release() ;
		clientNonce = NULL ;

		return STATUS_OK ;

error:
		clientNonce->release() ;
		clientNonce = NULL ;

		createSessionRequest->checkRefCount() ;

		if (createSessionResponse != NULL)
			createSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

public:

	SOPC_StatusCode activateSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Nonce  * serverNonce = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_05", "activateSession(): clientChannel null");
			return 0x00000001 ;
		}

		Session * session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_05", "activateSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

#if (SECURITY == UANONE)
		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
				// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						new UInt32(5000) // TimeoutHint
				),
				SignatureData::fakeSignatureData,
				new TableSignedSoftwareCertificate  (0),
				tableLocaleId,
				new AnonymousIdentityToken(new String("Anonymous")),
				SignatureData::fakeSignatureData);
#endif

#if (SECURITY != UANONE)
		Nonce * distantNonce = session->getDistantNonce() ;
		if (distantNonce == NULL) {
			debug(COM_ERR, "Client_05", "activateSession(): distantNonce == NULL");
			return 0x00000003 ;
		}

		ApplicationInstanceCertificate * distantCertificate = session->getDistantCertificate() ;
		if (distantCertificate == NULL) {
			debug(COM_ERR, "Client_05", "activateSession(): distantCertificate == NULL");
			return 0x00000003 ;
		}

		SignatureData * clientSignatureData = NULL ;
		if ((status = Certificates::computeSignatureData(&clientSignatureData, session->getDistantCertificate(), session->getDistantNonce()))) {
			debug_i(COM_ERR, "Client_05", "activateSession(): Certificates::computeSignatureData(..) returns %d",status);
			return status ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							new UInt32(5000) // TimeoutHint
					),
					clientSignatureData, // clientSignatureData
					new TableSignedSoftwareCertificate  (0),
					tableLocaleId,
					new AnonymousIdentityToken(new String("Anonymous")),
					SignatureData::fakeSignatureData); // fakeSignature
#endif

		ActivateSessionResponse * activateSessionResponse = NULL ;

		status = Connexion_ActivateSession(activateSessionRequest,&activateSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_05","Cannot Activate Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_05","Activate Session: processing parameters from response");

		serverNonce = activateSessionResponse->getServerNonce();

		session->setLastCreateOrActivateSessionNonce(serverNonce);

	    activateSessionRequest  ->checkRefCount() ;
	    activateSessionResponse ->checkRefCount() ;

		return STATUS_OK ;

error:

		activateSessionRequest->checkRefCount() ;

		if (activateSessionResponse != NULL)
			activateSessionResponse->checkRefCount() ;

		return status ;
	}

public:

	SOPC_StatusCode closeSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_05", "closeSession(): clientChannel null");
			return 0x00000001 ;
		}

		session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_05", "closeSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		// OPC-UA, Part3, 5.6.2, p. 26
		CloseSessionRequest * closeSessionRequest = new CloseSessionRequest(
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
				),
				Boolean::booleanFalse);

		CloseSessionResponse * closeSessionResponse = NULL ;

		status = Connexion_CloseSession(closeSessionRequest,&closeSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_05","Cannot Close Session: status=0x%08x",status) ;
			goto error ;
		}

	    clientChannel->deleteSession(authenticationTokenNum) ;

error:

		closeSessionRequest->checkRefCount() ;

		if (closeSessionResponse != NULL)
			closeSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

		return status ;
	}

public:

	SOPC_StatusCode doBrowse()
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_05","Testing Browse") ;

		NodeId * nodeToBrowse0 = new NodeId(UInt16::zero,new UInt32(84))       ; // Root
		NodeId * nodeToBrowse1 = new NodeId(UInt16::zero,new UInt32(87))       ; // Views
		NodeId * nodeToBrowse2 = new NodeId(UInt16::zero,new String("eolane")) ;

		BrowseDescription * browseDescription0
			= new BrowseDescription(
					nodeToBrowse0,
					BrowseDirection::forward_0,
					new NodeId(UInt16::zero,new UInt32(35)), // 35=Organizes ou bien NodeId::nullNodeId,
					Boolean::booleanFalse,
					UInt32::zero,
					UInt32::zero) ;

		BrowseDescription * browseDescription1
			= new BrowseDescription(
					nodeToBrowse1,
					BrowseDirection::forward_0,
					NodeId::nullNodeId,
					Boolean::booleanFalse,
					UInt32::zero,
					UInt32::zero) ;

		BrowseDescription * browseDescription2
			= new BrowseDescription(
					nodeToBrowse2,
					BrowseDirection::forward_0,
					NodeId::nullNodeId,
					Boolean::booleanFalse,
					UInt32::zero,
					UInt32::zero) ;

		TableBrowseDescription   * nodesToBrowse = new TableBrowseDescription  (3) ;
		nodesToBrowse->set(0,browseDescription0) ;
		nodesToBrowse->set(1,browseDescription1) ;
		nodesToBrowse->set(2,browseDescription2) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		BrowseRequest * browseRequest
			= new BrowseRequest(
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
					ViewDescription::nullView,
					Counter::zero,
					nodesToBrowse) ;

		BrowseResponse * browseResponse = NULL ;

		status = Connexion_Browse(browseRequest,&browseResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_05","Cannot Browse: status=0x%08x",status) ;
			goto error ;
		}

	    debug(MAIN_LIFE_DBG,"Client_05","Begin exploiting Browse result") ;
	    {
			TableBrowseResult   * results = browseResponse->getResults() ;
			int32_t               length  = results->getLength() ;

		    debug_i(MAIN_LIFE_DBG,"Client_05","Browse :    number of results = %d",length) ;

			for (int32_t i = 0 ; i < length ; i++) {

			    debug_i(MAIN_LIFE_DBG,"Client_05","Browse :        examining result[%d]",i) ;
				BrowseResult * browseResponse = results->get(i) ;

				StatusCode *  statusCode = browseResponse->getStatusCode() ;
				int32_t      _statusCode = statusCode->get() ;

			    debug_i(MAIN_LIFE_DBG,"Client_05","Browse :            status code = 0x%08x",_statusCode) ;

				ContinuationPoint * continuationPoint = browseResponse->getContinuationPoint() ;

				if (continuationPoint->isNullOrEmpty()) {
				    debug(MAIN_LIFE_DBG,"Client_05","Browse :            continuation point == NULL (that's normal)") ;
				} else {
				    debug(MAIN_LIFE_DBG,"Client_05","Browse :            continuation point != NULL (that's an error)") ;
				}

				TableReferenceDescription   * references = browseResponse->getReferences() ;
				int32_t                       rLength    = references->getLength() ;

			    debug_i(MAIN_LIFE_DBG,"Client_05","Browse :            number of browsed = %d",rLength) ;

			    char msg[1024] ;

				for (int32_t j = 0 ; j < rLength ; j++) {

					ReferenceDescription * referenceDescription = references->get(j) ;

					ExpandedNodeId * expandedNodeId  = referenceDescription->getNodeId() ;
					NodeId         * referenceTypeId = referenceDescription->getReferenceTypeId() ;

					String * strExpandedNodeId  = expandedNodeId->toString() ;
					String * strReferenceTypeId = referenceTypeId->toString() ;

					const char * _strExpandedNodeId  = strExpandedNodeId->get() ;
					const char * _strReferenceTypeId = strReferenceTypeId->get() ;

					sprintf(msg,"browsed %d -> %s with reference %s",j,_strExpandedNodeId,_strReferenceTypeId) ;

					strExpandedNodeId  ->checkRefCount() ;
					strReferenceTypeId ->checkRefCount() ;

				    debug_s(MAIN_LIFE_DBG,"Client_05","Browse :                %s",msg) ;
				}

			}
	    }
	    debug(MAIN_LIFE_DBG,"Client_05","End exploiting Browse result") ;

	    browseRequest  ->checkRefCount() ;
	    browseResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_05","Testing Browse OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_05","Testing Browse NOT OK") ;

		browseRequest->checkRefCount() ;

		if (browseResponse != NULL)
			browseResponse->checkRefCount() ;

		return status ;
	}

};     /* class */
}      /* namespace opcua */
#endif /* WITH_BROWSE == 1 */
#endif /* CLIENT05_H_ */
