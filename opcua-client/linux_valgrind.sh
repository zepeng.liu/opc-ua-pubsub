#!/bin/bash

echo $PWD

export LD_LIBRARY_PATH=../opcua/Debug:../s2opc_stack/Debug:/usr/local/lib:$LD_LIBRARY_PATH

valgrind -v --leak-check=full ./Debug/opcua-client 127.0.0.1 $1 certificates.127.0.0.1 127.0.0.1 10000

#valgrind -v --track-origins=yes --show-reachable=yes --tool=memcheck --leak-check=full --show-leak-kinds=all --gen-suppressions=all --suppressions=/home/bellot/Desktop/opc-rosa/opcua-client/valgrind.supp ./Debug/opcua-client 127.0.0.1 $1 certificates.127.0.0.1 127.0.0.1 10000


