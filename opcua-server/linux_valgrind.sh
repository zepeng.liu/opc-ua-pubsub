#!/bin/bash

echo $PWD

export LD_LIBRARY_PATH=../opcua/Debug:$LD_LIBRARY_PATH

valgrind -v --tool=memcheck --leak-check=full --suppressions=valgrind.supp ./Debug/opcua-server server/SampleServer/Config/Config.xml 127.0.0.1 10000 certificates.127.0.0.1
