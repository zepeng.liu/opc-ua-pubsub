
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_CPP_
#define MAIN_CPP_

#include "lib/OpcUa.h"
#include "lib/Error/All.h"

#include "lib/OpcUa.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Threads/All.h"
#include "lib/Server/All.h"
#include "lib/Stacks/All.h"
#include "lib/Services/All.h"
#include "lib/AddressSpace/All.h"
#include "lib/Utils/OpcUa_Alea.h"
#include "lib/NotificationsAndEvents/OpcUa_SubscriptionsTable.h"
#include <lib/Error/OpcUa_Debug.h>
#include <lib/CommonParametersTypes/All.h>

#define APPLICATION_NAME "CONNEXION Server"

extern  "C" {
	extern int           endpointOpened ; ; // 0=false, Managed in s2opc_server/S2OPC_StubServices.cpp
	extern bool 	     isServer ;
}

namespace opcua {

extern "C" {

	extern int endpointOpened ; // Managed in s2opc_server/S2OPC_StubServices.cpp

	void ComEvent_FctServer(SOPC_App_Com_Event event, uint32_t idOrStatus, void* param, uintptr_t appContext)
	{
		NOT_USED(idOrStatus) ;
		NOT_USED(param) ;
		NOT_USED(appContext) ;
		if (event == SE_CLOSED_ENDPOINT) {
			debug(S2OPC_DBG,"Test_ComEvent_FctServer","Connexion_Server_Toolkit: closed endpoint event: OK");
		} else {
			debug_i(S2OPC_ERR,"Test_ComEvent_FctServer","Connexion_Server_Toolkit: unexpected endpoint event %d : NOK",event);
		}
	}
}

	int main_sample_server(int argc, char *argv[], char *envp[])
	{
		isServer = true ;

		if (argc != 5) {
			fprintf(stderr,"Usage: %s <config file> <self-ip> <self-port> <certificates dir>\n",argv[0]) ;
			EXIT(-1) ;
		}

#ifdef _WIN32
		debug(MAIN_LIFE_DBG,"Sample Client","Initializing Windows WSA") ;
		WSADATA wsaData ;
		CHECK_LIB(WSAStartup(MAKEWORD(2,2),&wsaData) == 0) ;
#endif


#if (SIEM_LOG == 1)
		Debug::init_siem_file_name(argv[2],argv[3]) ;
#endif

		debug_s(MAIN_LIFE_DBG, "main", "Config   : %s", argv[1]) ;
		debug_s(MAIN_LIFE_DBG, "main", "Self   IP: %s", argv[2]) ;
		debug_s(MAIN_LIFE_DBG, "main", "Self Port: %s", argv[3]) ;
		debug_s(MAIN_LIFE_DBG, "main", "Certs dir: %s", argv[4]) ;

	    // Get Toolkit Configuration
	    SOPC_Build_Info build_info = SOPC_ToolkitConfig_GetBuildInfo();
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitVersion: %s", build_info.toolkitVersion);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitSrcCommit: %s", build_info.toolkitSrcCommit);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitDockerId: %s", build_info.toolkitDockerId);
	    debug_s(MAIN_LIFE_DBG,"main","ToolkitBuildDate: %s", build_info.toolkitBuildDate);

		ChannelsTable::self = new ChannelsTable(100) ;

		debug(MAIN_LIFE_DBG,"Sample Server","Reading certificates old manner") ;
		Certificates::init(argv[4]) ;

		debug(MAIN_LIFE_DBG,"Sample Server","Starting") ;

#if (WITH_SUBSCRIPTION == 1)
		debug(MAIN_LIFE_DBG,"Sample Server","Allocating subscriptions table") ;
		SubscriptionsTable::alloc() ;
#endif



	    debug(MAIN_LIFE_DBG,"Sample Server","Building address space") ;
		AddressSpace * addressSpace = new AddressSpace() ;
		addressSpace->load(argv[1]) ;

		Base * nodeId2259 = addressSpace->get((uint32_t)2259) ; // ServerState, thanks to Systerel

		if (nodeId2259 != NULL) {
			nodeId2259->putAttribute(
					new IntegerId(AttributeId_Value),
					NumericRange::null,
					OpcUaId_ServerState,
					new ServerState(ServerState_RUNNING_0)
			) ;
		} else {
			debug(COM_ERR,"Sample Server","Cannot find NodeId(0,2259)") ;
		}


		debug(MAIN_LIFE_DBG,"Sample Server","Creating channels table") ;
	    ChannelsTable::self = new ChannelsTable(200) ;


		// User Token Policy

		TableUserTokenPolicy * tableUserTokenPolicy = new TableUserTokenPolicy(1) ;
		tableUserTokenPolicy-> set (
				0,
				new UserTokenPolicy( /* 4.7.36 */
						new String("Anonymous"),
						UserIdentityTokenType::anonymous_0,
						String::empty, /* issuedTokenType */
						String::empty, /* issuerEndpointUrl */
						SecurityProfileUri::SecurityPolicy_None  /* securityPolicyUrl */
				)
		) ;


	    char endpointUrl[1024] ;
	    sprintf(endpointUrl,"opc.tcp://%s:%s/endpoint",argv[2],argv[3]) ;
	    debug_s(MAIN_LIFE_DBG,"Sample Server","Server endpoint: %s",endpointUrl) ;

		// EndpointDescription

		debug_s(MAIN_LIFE_DBG,"Sample Server","Building array of endpoints descriptions for %s",endpointUrl) ;

        char application_urn[1024] ;
        sprintf(application_urn,"urn:CONNEXION:%s",argv[2]) ;

		EndpointDescription::selfEndpoints = new TableEndpointDescription(1) ;
		EndpointDescription::selfEndpoints->take() ;

		EndpointDescription::selfEndpoints -> set(
				0,
				new EndpointDescription(
						new String(endpointUrl),
						new ApplicationDescription(
								new String(endpointUrl),
								new String(application_urn),
								new LocalizedText(new String(APPLICATION_NAME),LocaleId::empty),
								ApplicationType::server_0,
								String::empty,    /* gatewayServerUrl */
								String::empty,    /* discoveryProfileUri */
								String::tableZero /* discoveryUrls[] */
						),
						Certificates::selfCertificate,

#if   (SECURITY == UANONE)
						MessageSecurityMode::none_1,
#elif (SECURITY == UASIGN_Basic256Sha256)
						MessageSecurityMode::sign_2,
#elif (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
						MessageSecurityMode::signandencrypt_3,
#endif

#if   (SECURITY == UANONE)
						SecurityProfileUri::SecurityPolicy_None,
#else
						SecurityProfileUri::SecurityPolicy_256,
#endif

						tableUserTokenPolicy,
						new String("http://opcfoundation.org/UA-Profile/Transport/uatcp-uasc-uabinary"),
						new Byte(80) /* securityLevel */
				)
			);

		// Server certificate name
	    char certificateSrvLocation[1024] ;
	    sprintf(certificateSrvLocation,"%s/certs/self.der",argv[4]) ;

	    // Server private key
	    char keyLocation[1024] ;
	    sprintf(keyLocation,"%s/keys/self.key",argv[4]) ;

	    // Server CA
	    char certificateAuthority[1024] ;
	    sprintf(certificateAuthority,"%s/certs/chainOfTrust.crt",argv[4]) ;


	    SOPC_ReturnStatus status = SOPC_STATUS_OK;


		SOPC_Certificate   * serverCertificate = NULL;
	    SOPC_AsymmetricKey * asymmetricKey     = NULL;
	    SOPC_PKIProvider   * pkiProvider       = NULL;


#if (SECURITY != UANONE)
	    SOPC_Certificate   * authCertificate   = NULL;

	    debug_s(MAIN_LIFE_DBG,"Sample Server","Loading server certificate \"%s\"",certificateSrvLocation) ;
	    status = SOPC_KeyManager_Certificate_CreateFromFile(certificateSrvLocation,&serverCertificate) ;

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Failed to load server certificate: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

	    debug_s(MAIN_LIFE_DBG,"Sample Server","Loading server private key \"%s\"",keyLocation) ;
	    status = SOPC_KeyManager_AsymmetricKey_CreateFromFile(keyLocation, &asymmetricKey, NULL, 0);

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Failed to load server private key: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

	    debug_s(MAIN_LIFE_DBG,"Sample Server","Loading server CA \"%s\"",certificateAuthority) ;
	    status = SOPC_KeyManager_Certificate_CreateFromFile(certificateAuthority, &authCertificate) ;

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Failed to load server CA: status=0x%08x",status) ;
	        EXIT(-1) ;
	    }

	    // Creating PKI

	    debug(MAIN_LIFE_DBG,"Sample Server","Creating PKI") ;

	    status = SOPC_PKIProviderStack_Create(authCertificate, NULL, &pkiProvider) ;
	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Cannot create PKI: status=%d",status) ;
	        EXIT(-1) ;
	    }

#endif



	    // Security Configuration

#if (SECURITY == UANONE)
 	    SOPC_SecurityPolicy secuConfig[1];
#else
	    SOPC_SecurityPolicy secuConfig[2];
#endif

	    // Security NONE for discovery
		debug(MAIN_LIFE_DBG,"Sample Server","Creating unsecure policy configutation for discovery") ;

	    SOPC_String_Initialize(&secuConfig[0].securityPolicy);

	    status = SOPC_String_AttachFromCstring(&secuConfig[0].securityPolicy, strdup(SOPC_SecurityPolicy_None_URI));
	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Failed to initialize string in build security policy configuration[0] for discovery: status=%d",status) ;
	        EXIT(-1) ;
	    }

	    debug(MAIN_LIFE_DBG,"Sample Server","Creating security policy configutation UANONE") ;
	    secuConfig[0].securityModes = SOPC_SECURITY_MODE_NONE_MASK ;

#if (SECURITY != UA_NONE)

	    // Main security

	    SOPC_String_Initialize(&secuConfig[1].securityPolicy);
	    status = SOPC_String_AttachFromCstring(&secuConfig[1].securityPolicy, strdup(SOPC_SecurityPolicy_Basic256Sha256_URI));

	    if(STATUS_OK != status) {
			debug_i(COM_ERR,"Sample Server","Failed to initialize string in build security policy configuration for server: status=%d",status) ;
	        EXIT(-1) ;
	    }

#   if (SECURITY == UASIGN_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Server","Creating security policy configutation UASIGN_Basic256Sha256") ;
	    secuConfig[1].securityModes = SOPC_SECURITY_MODE_SIGN_MASK ;
#   elif (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Server","Creating security policy configutation UASIGNANDENCRYPT_Basic256Sha256") ;
	    secuConfig[1].securityModes = SOPC_SECURITY_MODE_SIGNANDENCRYPT_MASK ;
#   endif

#endif

	    // Init stack configuration
        status = SOPC_Toolkit_Initialize(ComEvent_FctServer);
	    if(STATUS_OK != status) {
			debug(COM_ERR,"Sample Server","Failed to initialize stack configuration") ;
	        EXIT(-1) ;
	    }
	    debug(MAIN_LIFE_DBG,"SampleServer","Stack configuration initialized")


	    // Namespace table

	    SOPC_NamespaceTable nsTable ;
	    SOPC_Namespace_Initialize(&nsTable) ;
	    SOPC_ToolkitConfig_SetNamespaceUris(&nsTable) ;

	    // Endpoint URL

	    SOPC_Endpoint_Config epConfig;

	    epConfig.endpointURL = endpointUrl ;

        epConfig.serverCertificate = serverCertificate;
        epConfig.serverKey         = asymmetricKey;
        epConfig.pki               = pkiProvider;

#if (SECURITY == UANONE)
        epConfig.nbSecuConfigs = 1 ;
#else
        epConfig.nbSecuConfigs = 2 ;
#endif
        epConfig.secuConfigurations = secuConfig ;

	    OpcUa_ApplicationDescription_Initialize(&epConfig.serverDescription);
        SOPC_String_AttachFromCstring(&epConfig.serverDescription.ApplicationUri, application_urn);
        SOPC_String_AttachFromCstring(&epConfig.serverDescription.ProductUri, application_urn);
        epConfig.serverDescription.ApplicationType = OpcUa_ApplicationType_Server;
        SOPC_String_AttachFromCstring(&epConfig.serverDescription.ApplicationName.Text,(char *)APPLICATION_NAME);


	    // Adding endpoint
	    uint32_t epConfigIdx = SOPC_ToolkitServer_AddEndpointConfig(&epConfig) ;
	    if (epConfigIdx == 0) {
	    	debug(COM_ERR,"Sample Server","Failed to add endpoint") ;
	    	EXIT(-1)
	    }
	    debug(MAIN_LIFE_DBG,"SampleServer","Endpoint added")


	    // Toolkit configured
	    status = SOPC_Toolkit_Configured();
	    if(STATUS_OK != status) {
			debug(COM_ERR,"Sample Server","Cannot set Toolkit as configured") ;
	        EXIT(-1) ;
	    }
	    debug(MAIN_LIFE_DBG,"SampleServer","Toolkit configured")


	    // Opening endpoint
	    debug(MAIN_LIFE_DBG,"SampleServer","Opening endpoint")
        SOPC_ToolkitServer_AsyncOpenEndpoint(epConfigIdx);


#if (SECURITY == UA_NONE)
		debug(MAIN_LIFE_DBG,"Sample Server","Running server in UA_NONE mode") ;
#endif
#if (SECURITY == UASIGN_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Server","Running server in UA_SIGN mode") ;
#endif
#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		debug(MAIN_LIFE_DBG,"Sample Server","Running server in UA_SIGNANDENCRYPT mode") ;
#endif

	    const uint32_t sleepTimeout = 50; // Sleep timeout in milliseconds
		debug_i(MAIN_LIFE_DBG,"Sample Server","sleepTimeout = %d ms",sleepTimeout) ;

		const uint32_t loopTimeout = 10000; // Loop timeout in millisecond
		debug_i(MAIN_LIFE_DBG,"Sample Server","loopTimeout = %d ms",loopTimeout) ;

	    uint32_t loopCpt = 0; // Counter to stop waiting responses after 5 seconds

		while (STATUS_OK == status) {
			loopCpt = 0 ;
			while (STATUS_OK == status && endpointOpened > 0 && loopCpt * sleepTimeout <= loopTimeout) {
				loopCpt++;
				SOPC_Sleep(sleepTimeout) ;
				if(STATUS_OK != status) {
					debug_i(COM_ERR,"Sample Server","While looping, error: status=0x%08x",status) ;
				}
			}
		}

	    debug_i(MAIN_LIFE_DBG,"Sample Server","After looping, error: status=0x%08x",status) ;

	    // Asynchronous request to close the endpoint
	    SOPC_ToolkitServer_AsyncCloseEndpoint(epConfigIdx);

	    // Wait until endpoint is closed
	    loopCpt = 0;
	    status = STATUS_OK;
	    while (SOPC_STATUS_OK == status && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        // Retrieve received messages on socket
	        SOPC_Sleep(sleepTimeout);
	    }

	    if (loopCpt * sleepTimeout > loopTimeout) {
	        status = SOPC_STATUS_TIMEOUT;
	    }

	    debug_i(MAIN_LIFE_DBG,"Sample Server","After closing endpoint: status=0x%08x",status) ;

	    // Clear the toolkit configuration and stop toolkit threads
	    status = STATUS_OK;
	    SOPC_Toolkit_Clear();
	    debug_i(MAIN_LIFE_DBG,"Sample Server","After closing endpoint: status=0x%08x",status) ;


	    // Deallocate locally allocated data

		debug(MAIN_LIFE_DBG,"Sample Server","Deallocate locally allocated data") ;

		SOPC_String_Clear(&secuConfig[0].securityPolicy);
	    SOPC_String_Clear(&secuConfig[1].securityPolicy);
	    SOPC_String_Clear(&secuConfig[2].securityPolicy);

#if (SECURITY != UA_NONE)
	        SOPC_KeyManager_Certificate_Free(serverCertificate);
	        SOPC_KeyManager_AsymmetricKey_Free(asymmetricKey);
	        SOPC_KeyManager_Certificate_Free(authCertificate);
	        SOPC_PKIProviderStack_Free(pkiProvider);
#endif

		debug(MAIN_LIFE_DBG,"Sample Server","Cleaning addressSpace") ;
		delete addressSpace ;

#if (WITH_SUBSCRIPTION == 1)
		debug(MAIN_LIFE_DBG,"Sample Server","Freeing subscriptions table") ;
		SubscriptionsTable::free() ;
#endif

		debug(MAIN_LIFE_DBG,"Sample Server","Stopping") ;

	    return status;
	}

} /* namespace opcua */


int main(int argc, char *argv[], char *envp[])
{
 	return opcua::main_sample_server(argc,argv,envp) ;
}

#endif /* MAIN_CPP_ */
