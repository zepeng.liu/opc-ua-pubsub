#include <stdio.h>

#include "opcua/lib/OpcUa.h"

int main(int argc, char *argv[], char *envp[])
{
	union {
		int i ;
		char c[sizeof(int)] ;
	} foo ;

	foo.i = 1 ;
	if (foo.c[0] == 1)
		printf("As checked : Little endian\n") ;
	else
		printf("As checked : Big endian\n") ;

        #if (IS_BIG_ENDIAN == 0)
                printf("According to #define : Little endian\n") ;
        #else
                printf("According to #define : Big endian\n") ;
	#endif

	#ifdef __arm__
		printf("__arm__\n") ;
        #endif

        #ifdef _WIN32
                printf("_WIN32\n") ;
        #endif

        #ifdef __APPLE__
                printf("__APPLE__\n") ;
        #endif

	return 0 ;
}

